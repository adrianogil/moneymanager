#ifndef CATEGORY_H
#define CATEGORY_H

#include <QtGui/QColor>
#include <QtCore/QObject>
#include <QtCore/QString>
#include "entity.h"

class MoneyCategory : public Entity
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(bool editMode READ editMode WRITE setEditMode NOTIFY editModeChanged)
    Q_PROPERTY(qreal total READ total WRITE setTotal NOTIFY totalChanged)
    Q_PROPERTY(qreal totalExpenses READ totalExpenses WRITE setTotalExpenses NOTIFY totalExpensesChanged)
    Q_PROPERTY(QString color READ color WRITE setColor NOTIFY colorChanged)
    Q_PROPERTY(QColor colorRgb READ colorRgb WRITE setColorRgb NOTIFY colorRgbChanged)
    Q_PROPERTY(float percentage READ percentage WRITE setPercentage NOTIFY percentageChanged)

public:
    MoneyCategory(Entity *parent = 0) : Entity(parent) { this->m_total = 0; }
    MoneyCategory(int id, QString name, QString description, bool editMode, QObject *parent = 0)
        : Entity(parent)
    {
        setId(id);
        m_name = name;
        m_description = description;
        m_editMode = editMode;
        m_total = 0;
    }

    void setName(QString name)
    {
        if (name != m_name) {
            m_name = name;
            emit nameChanged();
        } else m_name = name;
    }
    void setDescription(QString description)
    {
        if (description != m_description) {
            m_description = description;
            emit descriptionChanged();
        } else m_description = description;
    }
    void setEditMode(bool editMode)
    {
        if (editMode != m_editMode) {
            m_editMode = editMode;
            emit editModeChanged();
        } else m_editMode = editMode;
    }
    void setTotal(float total)
    {
        if (total != m_total) {
            m_total = total;
            emit totalChanged();
        } else m_total = total;
    }
    void setTotalExpenses(qreal totalExpenses) { m_totalExpenses = totalExpenses; }

    void setColor(QString color) {
        m_color = color;
    }
    void setColorRgb(QColor colorRgb) { m_colorRgb = colorRgb; }
    void setPercentage(float percentage) { m_percentage = percentage; }

    QString name() { return m_name; }
    QString description() { return m_description; }
    bool editMode() { return m_editMode; }
    qreal total() { return m_total; }
    qreal totalExpenses() { return m_totalExpenses; }
    QString color() { return m_color; }
    QColor colorRgb() { return m_colorRgb; }
    float percentage() { return m_percentage; }
signals:
    void nameChanged();
    void descriptionChanged();
    void editModeChanged();
    void totalChanged();
    void totalExpensesChanged();
    void colorChanged();
    void colorRgbChanged();
    void percentageChanged();
private:
    bool m_editMode;
    QString m_name;
    QString m_description;
    qreal m_total;
    qreal m_totalExpenses;
    QString m_color;
    QColor m_colorRgb;
    float m_percentage;
};

#endif // CATEGORY_H
