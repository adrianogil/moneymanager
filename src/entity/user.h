#ifndef USER_H
#define USER_H

#include <QString>
#include "entity.h"

class MoneyUser : public Entity
{
    Q_OBJECT

    Q_PROPERTY(QString login READ login WRITE setLogin NOTIFY loginChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(QString question READ question WRITE setQuestion NOTIFY questionChanged)
    Q_PROPERTY(QString answer READ answer WRITE setAnswer NOTIFY answerChanged)
public:
    MoneyUser(QObject *parent = 0) : Entity(parent) { }
    MoneyUser(QString login, QString password, QObject *parent = 0)
        : Entity(parent)
    {
        m_login = login;
        m_password = password;
    }
    MoneyUser(QString login,
              QString password,
              QString question,
              QString answer,
              QObject *parent = 0)
        : Entity(parent)
    {
        m_login = login;
        m_password = password;
        m_question = question;
        m_answer = answer;
    }

    void setAnswer(QString answer) { m_answer = answer;}
    void setQuestion(QString question) { m_question = question;}
    void setLogin(QString login) { m_login = login;}
    void setPassword(QString password) { m_password = password;}

    QString login() {return m_login;}
    QString password() {return m_password;}
    QString answer() { return m_answer; }
    QString question() { return m_question; }
signals:
    void loginChanged();
    void passwordChanged();
    void answerChanged();
    void questionChanged();
private:
    QString m_login;
    QString m_password;
    QString m_question;
    QString m_answer;

};

#endif // USER_H
