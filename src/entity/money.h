#ifndef MONEY_H
#define MONEY_H

#include <QDate>
#include <QString>
#include "entity.h"
#include "category.h"
#include <QtCore/QVariant>
#include "appdebug.h"

#define MONEY_DATE_FORMAT "dd/MM/yy"

class Money : public Entity
{
    Q_OBJECT

    Q_PROPERTY(int moneyType READ moneyType WRITE setMoneyType NOTIFY moneyTypeChanged)
    Q_PROPERTY(QString moneyTypeStr READ moneyTypeStr WRITE setMoneyTypeStr NOTIFY moneyTypeChanged)
    Q_PROPERTY(qreal amount READ amount WRITE setAmount NOTIFY amountChanged)
    Q_PROPERTY(QString strAmount READ strAmount NOTIFY amountChanged)
    Q_PROPERTY(QString moneyDate READ moneyDate WRITE setMoneyDate NOTIFY moneyDateChanged)
    Q_PROPERTY(MoneyCategory* category READ category WRITE setCategory NOTIFY categoryChanged)
    // StrCategory -> used for Csv purposes
    Q_PROPERTY(QString strCategory READ strCategory NOTIFY categoryChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    // CategoryByName -> used for DAO purposes
    Q_PROPERTY(QString categoryByName READ categoryByName WRITE setCategoryByName
               NOTIFY categoryByNameChanged)
    Q_PROPERTY(QString details READ details WRITE setDetails NOTIFY detailsChanged)
    Q_PROPERTY(int frequency READ frequency WRITE setFrequency NOTIFY frequencyChanged)
    Q_PROPERTY(QString frequencyName READ frequencyName WRITE setFrequencyName
               NOTIFY frequencyNameChanged)

public:
    enum money_types {type_in, type_out, type_none};

    Money(QObject *parent = 0) :
        Entity(parent),
        m_amount(0),
        m_frequency(0) { }
    Money(const Entity &entity, QObject *parent = 0)
        : Entity(entity, parent)
    {
        setMoneyType(entity.property("moneyType").toString() == "Saida");
        //debugMsg << "Got moneyType " << moneyType();
        m_amount = entity.property("amount").toReal();
        //debugMsg << "Got amount " << m_amount;
        setMoneyDate(entity.property("moneyDate").toString());
        //debugMsg << "Got moneyDate " << moneyDate();
        setCategoryByName(entity.property("categoryByName").toString());
        //debugMsg << "Got category " << entity.property("categoryByName").toString();
        m_description = entity.property("description").toString();
        //debugMsg << "Got description " << m_description;
        m_details = entity.property("details").toString();
        m_frequency = entity.property("frequency").toInt();
        setFrequencyNameBy(m_frequency);
        //m_categoryName = "Sem categoria";
    }

    static QList<Money*> convertFromEntities(QList<Entity*> listEntities)
    {
        QList<Money*> listMoney;
        foreach (Entity *entity, listEntities) {
            Money* money = new Money(*entity);
            listMoney.append(money);
        }
        return listMoney;
    }

    static QList<Entity*> convert2Entities(QList<Money*> listMoney)
    {
        QList<Entity*> listEntity;
        foreach (Money *money, listMoney)
            listEntity.append(money);
        return listEntity;
    }

    int moneyType() { return m_type_money; }
    qreal amount() { return m_amount; }
    QString strAmount() { return QString::number(m_amount);}
    QString moneyDate() { return m_dtTransaction.toString(MONEY_DATE_FORMAT);}
    QString details() const { return m_details; }
    int frequency(){return m_frequency;}
    QString frequencyName(){return m_frequencyName;}
    MoneyCategory* category()
    {
        if (m_listMoneyCategory.length() > 0)
            return m_listMoneyCategory.at(0);
        return 0;
    }
    QString strCategory() {
        QString categoryName = "Sem Categoria";
        MoneyCategory *cat = category();
        if (cat) {
            categoryName = cat->name();
        }
        return categoryName;
    }
    QString description() { return m_description; }
    QString moneyTypeStr() const {return QString(m_type_money == 0? tr("Entrada"): tr("Saida")); }
    void setMoneyTypeStr(const QString &type)
    {
        if (type == tr("Entrada"))
            setMoneyType(0);
        else if (type == tr("Saida"))
            setMoneyType(1);
    }

    void setMoneyType(int type) { m_type_money = type == 0 ? type_in : type_out; }
    void setMoneyDate(QString strDate)
    {
        qDebug() << __PRETTY_FUNCTION__ << strDate;
        m_dtTransaction = QDate::fromString(strDate, MONEY_DATE_FORMAT);
        m_dtTransaction = m_dtTransaction.addYears(100);
        qDebug() << __PRETTY_FUNCTION__ << m_dtTransaction;
    }
    void setCategory(MoneyCategory* category)
    {
        // Despite the real implementation, this method consider only one category by Money Register
        m_listMoneyCategory.clear();
        m_listMoneyCategory.append(category);
    }
    void setCategoryByName(const QString &categoryName) {
        m_listMoneyCategory.clear();
        m_categoryName = categoryName;
    }

    void setFrequency(int freq){
        debugMsg << freq;
        m_frequency = freq;
        setFrequencyNameBy(freq);
    }


    void setFrequencyBy(const QString &freq){

        if (freq == "Nenhum") m_frequency = 0;
        else if (freq == "Diaria") m_frequency = 1;
        else if (freq == "Semanal") m_frequency = 2;
        else if (freq == "Mensal") m_frequency = 3;
        else if (freq == "Bimestral") m_frequency = 4;
        else if (freq == "Semestral") m_frequency = 5;
        else if (freq == "Anual") m_frequency = 6;

        m_frequencyName = freq;
    }

    void setDetails (const QString &details) {
        debugMsg << details;
        m_details = details;
    }

    void setFrequencyName(const QString &frequency)
    {
        m_frequencyName = frequency;
        setFrequencyBy(frequency);
    }

    void setFrequencyNameBy(int frequency){

        switch (frequency) {
        case 0:
            m_frequencyName = tr("Nenhum");
            break;
        case 1:
            m_frequencyName = tr("Diaria");
            break;
        case 2:
            m_frequencyName = tr("Semanal");
            break;
        case 3:
            m_frequencyName = tr("Mensal");
            break;
        case 4:
            m_frequencyName = tr("Bimestral");
            break;
        case 5:
            m_frequencyName = tr("Semestral");
            break;
        case 6:
            m_frequencyName = tr("Anual");
            break;
        }
        debugMsg << m_frequencyName;

    }

    void setTypeMoney(money_types type_money) { m_type_money = type_money; }
    void setAmount(qreal amount) { m_amount = amount;}
    void setDtTransaction(QDate dtTransaction) { m_dtTransaction = dtTransaction;}
    void setDescription(QString description) { m_description = description;}
    void setListMoneyCategory(QList<MoneyCategory*> listMoneyCategory) {
        m_listMoneyCategory = listMoneyCategory;
        if (m_listMoneyCategory.length() > 0 && m_listMoneyCategory.at(0)) {
            MoneyCategory *cat = m_listMoneyCategory.at(0);
            if (cat)
                m_categoryName = cat->name();
        }
    }
    void setTimestamp(QString timestamp) { m_timestamp = timestamp; }

    money_types typeMoney() { return m_type_money; }
    QDate dtTransaction() { return m_dtTransaction; }
    QList<MoneyCategory*> listMoneyCategory() { return m_listMoneyCategory; }
    QString timestamp() { return m_timestamp; }
    QString categoryByName()
    {
        return m_categoryName;
    }

    Q_INVOKABLE QStringList entityFields()
    {
        QStringList fields;
        fields << tr("moneyType")  << tr("categoryByName") << tr("description")
               << tr("amount") << tr("moneyDate");
        return fields;
    }

signals:
    void moneyTypeChanged();
    void amountChanged();
    void moneyDateChanged();
    void categoryChanged();
    void descriptionChanged();
    void categoryByNameChanged();
    void detailsChanged();
    void frequencyChanged();
    void frequencyNameChanged();

private:
    money_types m_type_money;
    qreal m_amount;
    QDate m_dtTransaction;
    QString m_description;
    QString m_timestamp;
    QList<MoneyCategory*> m_listMoneyCategory;
    QString m_categoryName;
    QString m_details;
    int m_frequency;
    QString m_frequencyName;

};

#endif // MONEY_H
