#ifndef GOAL_H
#define GOAL_H

#include <QDate>
#include <QString>
#include "entity.h"

#define FORMAT_DATE "dd/MM/yy"

class MoneyGoal : public Entity
{

    Q_OBJECT

    Q_PROPERTY(int goalType READ goalType WRITE setGoalType NOTIFY goalTypeChanged)
    Q_PROPERTY(qreal target READ target WRITE setTarget NOTIFY targetChanged)
    Q_PROPERTY(QDate dtInitial READ getDtInitial WRITE setDtInitial NOTIFY initialDateChanged)
    Q_PROPERTY(QDate dtFinal READ getDtFinal WRITE setDtFinal NOTIFY finalDateChanged)
    Q_PROPERTY(QString initialDate READ initialDate WRITE setInitialDate NOTIFY initialDateChanged)
    Q_PROPERTY(QString finalDate READ finalDate WRITE setFinalDate NOTIFY finalDateChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(int percentage READ percentage WRITE setPercentage NOTIFY percentageChanged)

public:
    enum goal_types {IN_greater_than = 0, OUT_less_than = 1, Total_greater_than = 2};

    MoneyGoal(QObject* parent = 0) : Entity(parent)
    {
        this->m_percentage = 0;
    }
    MoneyGoal(int idGoal,
              goal_types goal_money,
              qreal target,
              QDate dtInitial,
              QDate dtFinal,
              QString description,
              QObject* parent = 0)
        : Entity(parent)
    {
        setId(idGoal);
        m_goal_money = goal_money;
        m_target = target;
        m_dtInitial = dtInitial;
        m_dtFinal = dtFinal;
        m_description = description;
        m_percentage = 0;
    }
    MoneyGoal(const Entity &entity, QObject *parent = 0)
        : Entity(entity, parent)
    {
        debugMsg << entity.id();
        setGoalType(entity.property("goalType").toInt());
        m_target = entity.property("target").toFloat();
        m_dtInitial = entity.property("dtInitial").toDate();
        m_dtFinal = entity.property("dtFinal").toDate();
        m_description = entity.property("description").toString();
        m_percentage = 0;
    }

    void setTypeGoal(goal_types type_goal) { m_goal_money = type_goal; }
    void setTarget(qreal target) { m_target = target;}
    void setDtInitial(QDate dtInitial) { m_dtInitial = dtInitial; }
    void setDtFinal(QDate dtFinal) { m_dtFinal = dtFinal; }
    void setInitialDate(QString initialDate)
    {
        m_dtInitial = QDate::fromString(initialDate, FORMAT_DATE);
        m_dtInitial = m_dtInitial.addYears(100);
    }
    void setFinalDate(QString finalDate)
    {
        m_dtFinal = QDate::fromString(finalDate, FORMAT_DATE);
        m_dtFinal = m_dtFinal.addYears(100);
    }
    void setDescription(QString description) { m_description = description; }
    void setGoalType(int type)
    {
        if (type == 0)
            m_goal_money = IN_greater_than;
        else if (type == 1)
            m_goal_money = OUT_less_than;
        else m_goal_money = Total_greater_than;
    }
    void setPercentage(int percentage) {
        if (percentage != m_percentage) {
            m_percentage = percentage;
            emit percentageChanged();
        }
    }

    goal_types typeGoal() { return m_goal_money; }
    qreal target() { return m_target; }
    QDate getDtInitial() { return m_dtInitial; }
    QDate getDtFinal() { return m_dtFinal; }

    int goalType()
    {
        if (m_goal_money == IN_greater_than)
            return 0;
        else if (m_goal_money == OUT_less_than)
            return 1;
        return 2;
    }
    QString initialDate() { return m_dtInitial.toString(FORMAT_DATE); }
    QString finalDate() { return m_dtFinal.toString(FORMAT_DATE); }
    QString description() { return m_description; }
    int percentage() { return m_percentage; }

    static QList<MoneyGoal*> toGoalList(QList<Entity*> entityList) {
        QList<MoneyGoal*> goalList;
        foreach(Entity* entity, entityList) {
            debugMsg << entity->id();
            goalList.append(new MoneyGoal(*entity));
        }
        return goalList;
    }

signals:
    void goalTypeChanged();
    void targetChanged();
    void initialDateChanged();
    void finalDateChanged();
    void descriptionChanged();
    void percentageChanged();

private:
    goal_types m_goal_money;
    qreal m_target;
    QDate m_dtInitial;
    QDate m_dtFinal;
    QString m_description;
    int m_percentage;

};

#endif // GOAL_H
