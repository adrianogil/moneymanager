#ifndef ENTITY_H
#define ENTITY_H

#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtCore/QVariant>
#include <QtCore/QStringList>
#include "appdebug.h"

#define ENTITY_DATE_FORMAT "dd.MM.yyyy hh:mm:ss.zzz"

class Entity : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id READ id WRITE setId NOTIFY idChanged)
    Q_PROPERTY(QString strId READ strId NOTIFY idChanged)
    Q_PROPERTY(QDateTime dtCreation READ dtCreation WRITE setDtCreation NOTIFY dtCreationChanged)
    Q_PROPERTY(QDateTime dtLastModification READ dtLastModification WRITE setDtLastModification NOTIFY dtLastModificationChanged)
    Q_PROPERTY(QString strDtCreation READ strDtCreation WRITE setStrDtCreation)
    Q_PROPERTY(QString strDtLastModification READ strDtLastModification WRITE setStrDtLastModification)

public:
    Entity(QObject *parent = 0) : QObject(parent) {}
    Entity(const Entity &entity, QObject *parent = 0) : QObject(parent)
    {
        m_id = entity.id();
        m_dtCreation = entity.dtCreation();
        m_dtLastModification = entity.dtLastModification();
    }
    void setId(const int &id)
    {
        if (id != m_id) {
            m_id = id;
            emit idChanged();
        } else m_id = id;
    }
    void setDtCreation(const QDateTime &dtCreation) { m_dtCreation = dtCreation; }
    void setDtLastModification(const QDateTime &dtModification) { m_dtLastModification = dtModification; }
    void setStrDtCreation(const QString &strDtCreation) { m_dtCreation = QDateTime::fromString(strDtCreation, ENTITY_DATE_FORMAT);}
    void setStrDtLastModification(const QString &strDtModification) { m_dtLastModification = QDateTime::fromString(strDtModification, ENTITY_DATE_FORMAT); }

    int id() const { return m_id; }
    QString strId() const { return QString::number(m_id); }
    QDateTime dtCreation() const { return m_dtCreation; }
    QDateTime dtLastModification() const { return m_dtLastModification; }
    QString strDtCreation() const { return m_dtCreation.toString(ENTITY_DATE_FORMAT); }
    QString strDtLastModification() const { return m_dtLastModification.toString(ENTITY_DATE_FORMAT);}

    Q_INVOKABLE QVariant getPropertyValue(QString propertyType) const { return property(propertyType.toAscii()); }
    Q_INVOKABLE QStringList entityFields()
    {
        QStringList fields;
        fields << tr("id")  << tr("strDtCreation") << tr("strDtLastModification");
        return fields;
    }

signals:
    void idChanged();
    void dtCreationChanged();
    void dtLastModificationChanged();

private:
    int m_id;
    QDateTime m_dtCreation;
    QDateTime m_dtLastModification;

};


#endif // ENTITY_H
