#ifndef APPDEBUG_H
#define APPDEBUG_H

#include <QtCore/QDebug>

#define DEBUG_FILTER_STRING  ""

//#define debugMsg qDebug() << __PRETTY_FUNCTION__

#define debugMsg ( (QString(__PRETTY_FUNCTION__)).contains(DEBUG_FILTER_STRING)? \
    (qDebug() << __PRETTY_FUNCTION__) : \
    (qDebug()))

//class DebugFilter : public QDebug
//{
//public:
//    inline DebugFilter(QtMsgType t) : QDebug(t) {}
//    inline DebugFilter(const QDebug &d) : QDebug(d) {}
//    inline DebugFilter(QString *s) : QDebug(s) {}
//    inline DebugFilter &operator<<(const int &t) {
//        return ((*this) << t);
//    }
//    inline DebugFilter &operator<<(const QString &t) {
//        return ((*this) << t);
//    }
//};

//inline DebugFilter &operator <<(DebugFilter debug, const int &t)
//{
//    return DebugFilter(QtDebugMsg);
//}

//QDebug& debugFilterStream(){ return qtDebug; }

//inline DebugFilter debugMsg() { return DebugFilter(QtDebugMsg); }

#endif // APPDEBUG_H
