#ifndef WIDGETCHART_H
#define WIDGETCHART_H

 #include <QDeclarativeItem>

#include "money.h"
#include "linegraph.h"
#include "nightcharts.h"

class WidgetChart : public QDeclarativeItem
{
    Q_OBJECT

    Q_PROPERTY(QList<QObject*> moneyList READ moneyList WRITE setMoneyList NOTIFY moneyListChanged)
    Q_PROPERTY(QList<QObject*> categoryList READ categoryList WRITE setCategoryList NOTIFY categoryListChanged)
    Q_PROPERTY(int typeChart READ typeChart WRITE setTypeChart NOTIFY typeChartChanged)

public:
    WidgetChart(QDeclarativeItem *parent = 0);
    ~WidgetChart();

    int typeChart();
    void setTypeChart(int typeChart);
    QList<QObject*> moneyList();
    void setMoneyList(QList<QObject*> moneyList);
    QList<QObject*> categoryList();
    void setCategoryList(QList<QObject*> categoryList);
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = 0);

signals:
    void moneyListChanged();
    void typeChartChanged();
    void categoryListChanged();
    void pieces(int numberOfpieces);

private:
    QList<Money*> m_moneyList;
    QList<MoneyCategory*> m_categoryList;
    QList<QColor> m_colorList;
    THLineGraph* m_chart;
    Nightcharts* m_pieChart;
    int m_typeChart;
    int addedPieces;
};

#endif // WIDGETCHART_H
