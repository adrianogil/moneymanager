#include "widgetchart.h"
#include "appdebug.h"

#include <math.h>


WidgetChart::WidgetChart(QDeclarativeItem *parent) :
    QDeclarativeItem(parent)
{
    setFlag(QGraphicsItem::ItemHasNoContents, false);
    m_typeChart = 0;

}

WidgetChart::~WidgetChart()
{

}

void WidgetChart::setMoneyList(QList<QObject *> moneyList)
{
    debugMsg;
    m_moneyList.clear();
    foreach (QObject* obj, moneyList) {
        m_moneyList.append((Money*)obj);
    }

    if (m_moneyList.length() > 0) {
        m_chart = new THLineGraph;

        Money *firstMoney = m_moneyList.at(0);
        QDate lastdate = firstMoney->dtTransaction();
        double total = 0;
        foreach(Money* money, m_moneyList) {
            if (money->dtTransaction().day() == lastdate.day() &&
                money->dtTransaction().month() == lastdate.month() &&
                money->dtTransaction().year() == lastdate.year()) {
                total = total + (money->typeMoney() == Money::type_in? 1: -1) * money->amount();
            }
            else {
                qDebug() << __PRETTY_FUNCTION__ << lastdate.toString("dd MMM") << QString::number(total);
                m_chart->append(THLineGraphData(lastdate.toString("dd MMM"),
                                               (total < 0? "-": "") + QString("R$") + QString::number(abs(total))));
                lastdate = money->dtTransaction();
                total = total + (money->typeMoney() == Money::type_in? 1: -1) * money->amount();
            }
        }

        m_chart->append(THLineGraphData(lastdate.toString("dd MMM"),
                                        (total < 0? "-": "") + QString("R$") + QString::number(abs(total))));

        m_chart->setBackground(QColor(255,255,255));
        m_chart->setTextColor(QColor(0,0,0));

        m_chart->setSortY(true);
//        m_chart->setSortX(true);

        m_chart->resize(600,404);
        //m_chart->setMinimumWidth(437);
    }
    debugMsg << "END";
}

QList<QObject*> WidgetChart::moneyList()
{
    QList<QObject*> objList;
    foreach(Money* money, m_moneyList)
        objList.append(money);

    return objList;
}

void WidgetChart::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);
    //qDebug() << __PRETTY_FUNCTION__;
    if (m_typeChart == 0) {
        if (m_chart && !m_moneyList.isEmpty())
            // Line Chart about cash flow by time
            painter = m_chart->paintMe(painter, QSize(600, 370));
            emit pieces(1);
    } else {
        // Pie Chart showing total expenses by category
        if(addedPieces > 0)
        painter = m_pieChart->paintMe(painter);
        emit pieces(addedPieces);
    }

}


void WidgetChart::setTypeChart(int typeChart)
{
    m_typeChart = typeChart;
}


int WidgetChart::typeChart()
{
    return m_typeChart;
}

void WidgetChart::setCategoryList(QList<QObject *> categoryList)
{

    debugMsg << "START";

    m_categoryList.clear();
    foreach (QObject *obj, categoryList) {
        m_categoryList.append((MoneyCategory*)obj);
    }

    float total = 0;
    foreach (MoneyCategory* cat, m_categoryList) {
        total = total + cat->totalExpenses();
    }

    debugMsg << "1";
    m_pieChart = new Nightcharts();
    debugMsg << "2";

    addedPieces = 0;




    if (total > 0) {
        m_pieChart->setType(Nightcharts::Dpie);
        m_pieChart->setLegendType(Nightcharts::Round);//{Round,Vertical}
        m_pieChart->setCords(20, 20, (340)/1.5, (124)/1.5);
        int numberOfCategoryNotEmpty = 0;
        foreach (MoneyCategory* cat, m_categoryList) {
            if (cat->percentage() >= 1) {
                debugMsg << "Adding piece" << cat->name() << cat->colorRgb()
                         << cat->percentage();
                m_pieChart->addPiece(cat->name(), cat->colorRgb(),
                                 cat->percentage());
                addedPieces++;
            }
            if(cat->percentage() > 0)
                numberOfCategoryNotEmpty++;
        }
        m_pieChart->setNumberOfCategories(numberOfCategoryNotEmpty);
    }
    debugMsg << "3";
    // m_pieChart->setFont(QFont(painter->font().family(), 6));
    if (addedPieces > 0)
        m_pieChart->draw(QSize(250, 202));
    // m_pieChart->drawLegend();

    debugMsg << "END";
}

QList<QObject*> WidgetChart::categoryList()
{
    QList<QObject*> objList;
    foreach(MoneyCategory *cat, m_categoryList)
        objList.append(cat);

    return objList;
}
