﻿#include "dropboxmanager.h"

#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtCore/QUrl>
#include <QtCore/QDate>

#include "moneycsvresume.h"
#include "consumerdata.h"
#include "oauth.h"
#include "userdata.h"
#include "dropbox.h"
#include "json.h"

#include "csvmanager.h"

DropboxManager::DropboxManager(QObject *parent)
    :  QObject(parent)
    , m_appFolderExists(false)
{
    m_networkAccessManager = new QNetworkAccessManager;
    m_consumerData = new ConsumerData;
    m_oAuth = new OAuth(m_consumerData);
    m_userData = new UserData("","","");
    m_dropbox = new Dropbox;

    m_backupFilename = "MoneyManager_" + QDate::currentDate().toString("dd_MM_yy") + ".csv";

    m_csvManager = new CsvManager(this);

    connect(m_networkAccessManager, SIGNAL(finished(QNetworkReply*)),
            this, SLOT(handleNetworkReply(QNetworkReply*)));
}

void DropboxManager::login(QString email, QString password)
{
    debugMsg << email << password;

    QUrl url = m_dropbox->apiToUrl(Dropbox::TOKEN);
    url.addQueryItem("email", email);
    url.addQueryItem("password", password);

    m_userData->email = email.toLower();

    QNetworkRequest networkRequest(url);

    m_oAuth->addConsumerKeyQueryItem(&networkRequest);

    m_networkAccessManager->get(networkRequest);
}

void DropboxManager::requestCsvFilesList()
{
    // Return if user isn't logged
    if (m_userData->token.isNull())
        return;

    debugMsg;

    QString path = QString(DM_DROPBOX_APP_FOLDER) + "/";

    QUrl url = m_dropbox->apiToUrl(Dropbox::METADATA).toString() + path;
    url.addQueryItem("list", "true");

    QNetworkRequest networkRequest(url);

    m_oAuth->signRequest(m_userData, "GET", &networkRequest);

    m_networkAccessManager->get(networkRequest);
}

void DropboxManager::requestCsvEntities(const QString &csvFilename)
{
    debugMsg << csvFilename;

    QUrl url =
            m_dropbox->apiToUrl(Dropbox::FILES).toString() +
            QString(DM_DROPBOX_APP_FOLDER) + "/" + csvFilename
            ;

    QNetworkRequest networkRequest(url);

    m_oAuth->signRequest(
                m_userData,
                "GET",
                &networkRequest
                );

    QNetworkReply *networkReplyDownload = m_networkAccessManager->get(networkRequest);
    m_downloadTime.start();
    connect(networkReplyDownload, SIGNAL(downloadProgress(qint64,qint64)),
            this, SLOT(handleDownloadProgress(qint64,qint64)));
    connect(networkReplyDownload, SIGNAL(finished()),
            this, SLOT(handleCsvLoad()));
    m_networkReplyDownload = networkReplyDownload;

    setProgressValue(0);
    setStateLabel("");
}

void DropboxManager::requestAppFolderCreation()
{
    debugMsg;
    QUrl url = m_dropbox->apiToUrl(Dropbox::FILEOPS_CREATEFOLDER);
    url.addQueryItem("root", "dropbox");
    url.addQueryItem("path", QString(DM_DROPBOX_APP_FOLDER) + "/");

    QNetworkRequest networkRequest(url);

    m_oAuth->signRequest(
                m_userData,
                "GET",
                &networkRequest
                );

    m_networkAccessManager->get(networkRequest);
}

void DropboxManager::exportCsvData(QString exportData)
{
    debugMsg << exportData;

    //    QFile csvData("tmp.csv");
    //    if (!csvData.open(QIODevice::WriteOnly))
    //        return;
    //    QTextStream out(&csvData);
    //    out << exportData;
    QByteArray *multipartform = new QByteArray();
    QString crlf("\r\n");
    QString boundaryStr(
                "---------------------------109074266748897678777839994"
                );
    QString boundary = "--" + boundaryStr + crlf;
    QString filename = (m_backupFilename.endsWith(".csv")? m_backupFilename : m_backupFilename + ".csv");
    multipartform->append(boundary.toAscii());
    multipartform->append(
                QString("Content-Disposition: form-data; name=\"file\"; "
                        "filename=\"" + filename.toUtf8() + "\"" + crlf
                        ).toAscii()
                );
    multipartform->append(
                QString("Content-Type: text/plain" + crlf + crlf).toAscii()
                );
    multipartform->append(exportData.toUtf8());
    multipartform->append(
                QString(crlf + "--" + boundaryStr + "--" + crlf).toAscii()
                );
    //prepare request
    QString remoteFolder = QString(DM_DROPBOX_APP_FOLDER) + "/";
    QUrl url =
            m_dropbox->apiToUrl(Dropbox::FILES).toString() +
            remoteFolder
            ;
    url.addQueryItem("file", filename);

    QNetworkRequest networkRequest(url);

    networkRequest.setHeader(
                QNetworkRequest::ContentTypeHeader,
                "multipart/form-data; boundary=" + boundaryStr
                );

    m_oAuth->signRequest(
                m_userData,
                "POST",
                &networkRequest
                );

    //send request
    QNetworkReply *networkReply = m_networkAccessManager->post(
                networkRequest, *multipartform
                );
    connect(networkReply, SIGNAL(uploadProgress(qint64,qint64)),
            this, SLOT(handleUploadProgress(qint64,qint64)));
    m_uploadTime.start();

}

void DropboxManager::handleNetworkReply(QNetworkReply *networkReply)
{
    if (networkReply->error() != QNetworkReply::NoError) {
        debugMsg << "Got error!";
    }

    Dropbox::Api api = m_dropbox->urlToApi(networkReply->url());

    debugMsg << api;

    switch (api) {
    case Dropbox::FILEOPS_CREATEFOLDER:
        handleAppFolderCreation(networkReply);
        break;
    case Dropbox::METADATA:
        handleFilesList(networkReply);
        break;
    case Dropbox::TOKEN:
        handleTokenAndSecret(networkReply);
        break;
    case Dropbox::FILES:
        break;
    default:
        break;
    }
}

void DropboxManager::handleTokenAndSecret(QNetworkReply *networkReply)
{
    debugMsg;
    networkReply->deleteLater();

    if(networkReply->error() != QNetworkReply::NoError)
    {
        if(networkReply->error() == QNetworkReply::AuthenticationRequiredError)
        {
            emit errorMessage(
                        tr("Senha e/ou e-mail invalidos.")
                        );
        }
        else
        {
            emit errorMessage(
                        tr("Ocorreu um erro, por favor tente de novo mais tarde.")
                        );
        }

        return;
    }

    QString jsonData = networkReply->readAll();

    bool ok;
    QVariantMap jsonResult = Json::parse(jsonData, ok).toMap();
    if(!ok)
    {
        emit errorMessage(
                    tr("Ocorreu um erro, por favor tente de novo mais tarde.")
                    );

        return;
    }

    m_userData->token = jsonResult["token"].toString();
    m_userData->secret = jsonResult["secret"].toString();

    if (m_appFolderExists) {
        emit userLogged();
        requestCsvFilesList();
    } else {
        requestAppFolderCreation();
    }
}

void DropboxManager::handleFilesList(QNetworkReply *networkReply)
{

    QString dirJson = networkReply->readAll();

    debugMsg << dirJson;

    bool ok;
    QVariantMap jsonResult = Json::parse(dirJson, ok).toMap();
    if(!ok)
    {
        emit errorMessage(
                    tr("Ocorreu um erro, por favor tente de novo mais tarde.")
                    );
        debugMsg << "There was an error";
        return;
    }

    m_csvFilesList.clear();

    //add files
    foreach(const QVariant &subDirJson, jsonResult["contents"].toList())
    {
        QVariantMap subDir = subDirJson.toMap();

        if(subDir["is_dir"].toBool() == false)
        {
            QString size =  subDir["size"].toString();
            QString subDirPath = subDir["path"].toString();
            QString subDirName = subDirPath.right(
                        (subDirPath.length() - subDirPath.lastIndexOf("/")) - 1
                        );
            QDate date = QDate::fromString(subDir["modified"].toString(),
                                           "Wed, 27 Apr 2011 22:18:51 +0000");

            MoneyCsvResume *csvFile = new MoneyCsvResume;
            csvFile->setFilename(subDirName);
            csvFile->setDate(date);

            m_csvFilesList.append(csvFile);
        }
    }

    emit receivedCsvFiles(m_csvFilesList);
}

void DropboxManager::handleCsvLoad()
{
    //update variables
    setProgressValue(100);
    setStateLabel(
                QString("Download completo!")
                );

    QString contents = m_networkReplyDownload->readAll();
    debugMsg << contents;

    emit receivedCsvContents(contents);
}

void DropboxManager::handleAppFolderCreation(QNetworkReply *networkReply)
{
    debugMsg;
    emit userLogged();
    requestCsvFilesList();
}

void DropboxManager::handleUploadProgress(qint64 sent, qint64 total)
{
    //avoid errors
    if(sent == 0 || total == 0 || m_uploadTime.elapsed() == 0)
        return;

    // calculate the download speed
    double speed = sent * 1000.0 / m_uploadTime.elapsed();
    QString unit;
    if (speed < 1024) {
        unit = "bytes/sec";
    } else if (speed < 1024*1024) {
        speed /= 1024;
        unit = "kB/s";
    } else {
        speed /= 1024*1024;
        unit = "MB/s";
    }

    //update variables
    setProgressValue((sent*100)/total);
    if (sent == 1) {
        setStateLabel(tr("Upload Completo!"));
    } else {
        setStateLabel(
                    QString(tr("Upload a ")) + QString("%1 %2").arg(speed, 3, 'f', 1).arg(unit)
                    );
    }
}

void DropboxManager::handleDownloadProgress(qint64 received, qint64 total)
{
    //avoid errors
    if (received == 0 || total == 0 || m_downloadTime.elapsed() == 0)
        return;

    // calculate the download speed
    double speed = received * 1000.0 / m_downloadTime.elapsed();
    QString unit;
    if (speed < 1024) {
        unit = "bytes/sec";
    } else if (speed < 1024*1024) {
        speed /= 1024;
        unit = "kB/s";
    } else {
        speed /= 1024*1024;
        unit = "MB/s";
    }

    //update variables
    setProgressValue((received*100)/total);
    setStateLabel(
                QString(tr("Download a ")) + QString("%1 %2").arg(speed, 3, 'f', 1).arg(unit)
                );
}

