﻿#ifndef MONEYCSVRESUME_H
#define MONEYCSVRESUME_H

#include <QtCore/QObject>
#include <QtCore/QDate>

class MoneyCsvResume : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setFilename NOTIFY filenameChanged)
    Q_PROPERTY(QDate date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(QList<QObject*> entityList READ entityList WRITE setEntityList
               NOTIFY entityListChanged)
    Q_PROPERTY(qreal totalIn READ totalIn WRITE setTotalIn NOTIFY totalInChanged)
    Q_PROPERTY(qreal totalOut READ totalOut WRITE setTotalOut NOTIFY totalOutChanged)
    Q_PROPERTY(qreal total READ total WRITE setTotal NOTIFY totalChanged)
public:
    QString name() const { return m_filename; }
    QDate date() const { return m_date; }
    QList<QObject*> entityList() const { return m_entityList; }
    qreal totalIn() const { return m_totalIn; }
    qreal totalOut() const { return m_totalOut; }
    qreal total() const { return m_total; }

    void setFilename(QString filename) { m_filename = filename; }
    void setDate(QDate date) { m_date = date; }
    void setEntityList(QList<QObject*> entityList) { m_entityList = entityList; }
    void setTotalIn(const qreal &totalIn) { m_totalIn = totalIn; }
    void setTotalOut(const qreal &totalOut) { m_totalOut = totalOut; }
    void setTotal(const qreal &total) { m_total = total; }
signals:
    void filenameChanged();
    void dateChanged();
    void entityListChanged();
    void totalInChanged();
    void totalOutChanged();
    void totalChanged();
private:
    QString m_filename;
    QDate m_date;
    QList<QObject*> m_entityList;
    qreal m_totalIn;
    qreal m_totalOut;
    qreal m_total;
};

#endif // MONEYCSVRESUME_H
