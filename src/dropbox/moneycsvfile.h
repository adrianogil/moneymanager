#ifndef MONEYCSVFILE_H
#define MONEYCSVFILE_H

#include <QtCore/QObject>
#include "entity.h"

class MoneyCsvFile : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setFilename NOTIFY filenameChanged)
    Q_PROPERTY(QDate date READ date WRITE setDate NOTIFY dateChanged)
    Q_PROPERTY(QList<Entity*> entityList READ entityList WRITE setEntityList
               NOTIFY entityListChanged)
public:
    QString name() const { return m_filename; }
    QDate date() const { return m_date; }
    QList<Entity*> entityList() const { return m_entityList; }

    void setFilename(QString filename) { m_filename = filename; }
    void setDate(QDate date) { m_date = date; }
    void setEntityList(QList<Entity*> entityList) { m_entityList = entityList; }
signals:
    void filenameChanged();
    void dateChanged();
    void entityListChanged();
private:
    QString m_filename;
    QDate m_date;
    QList<Entity*> m_entityList;
};

#endif // MONEYCSVFILE_H
