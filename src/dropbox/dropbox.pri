﻿INCLUDEPATH += $$PWD

HEADERS += \ 
    $$PWD/dropboxmanager.h \
    $$PWD/userdata.h \
    $$PWD/json.h \
    $$PWD/dropbox.h \
    $$PWD/consumerdata.h \
    $$PWD/oauth.h \
    $$PWD/moneycsvresume.h

SOURCES += \
    $$PWD/dropboxmanager.cpp \
    $$PWD/userdata.cpp \
    $$PWD/json.cpp \
    $$PWD/dropbox.cpp \
    $$PWD/oauth.cpp







