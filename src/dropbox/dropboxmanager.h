﻿#ifndef DROPBOXMANAGER_H
#define DROPBOXMANAGER_H

#include <QtCore/QObject>
#include <QtCore/QDate>
#include <QtCore/QStringList>

#include "moneycsvresume.h"
#include "appdebug.h"

//! Forward Declaration
class QNetworkAccessManager;
class ConsumerData;
class OAuth;
class UserData;
class Dropbox;
class QNetworkReply;
class Entity;
class CsvManager;

#define DM_DATE_FORMAT "dd/MM/yy"
#define DM_DROPBOX_APP_FOLDER "/MoneyManager"

class DropboxManager : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString initialDate READ initialDate WRITE setInitialDate
               NOTIFY initialDateChanged)
    Q_PROPERTY(QString finalDate READ finalDate WRITE setFinalDate
               NOTIFY finalDateChanged)
    Q_PROPERTY(QString dropboxOption READ dropboxOption WRITE setDropboxOption
               NOTIFY dropboxOptionChanged)
    Q_PROPERTY(qreal progressValue READ progressValue WRITE setProgressValue
               NOTIFY progressValueChanged)
    Q_PROPERTY(QString stateLabel READ stateLabel WRITE setStateLabel
               NOTIFY stateLabelChanged)
    Q_PROPERTY(QString backupFilename READ backupFilename WRITE setBackupFilename
               NOTIFY backupFilenameChanged)

public:
    explicit DropboxManager(QObject *parent = 0);

    QString initialDate() const { return m_dtInitial.toString(DM_DATE_FORMAT); }
    QString finalDate() const { return m_dtFinal.toString(DM_DATE_FORMAT); }

    void setInitialDate(const QString &initialDate) {
        QDate newDate = QDate::fromString(initialDate,DM_DATE_FORMAT);
        if (newDate != m_dtInitial) {
            m_dtInitial = newDate;
            emit initialDateChanged();
        }
    }
    void setFinalDate(const QString &finalDate) {
        QDate newDate = QDate::fromString(finalDate, DM_DATE_FORMAT);
        if (newDate != m_dtFinal) {
            m_dtFinal = newDate;
            emit finalDateChanged();
        }
    }
    /** Dropbox Options **/
    void setDropboxOption(const QString &dropboxOption)
    {
        if (dropboxOption != m_dropboxOption) {
            m_dropboxOption = dropboxOption;
            emit dropboxOptionChanged();
        }
    }
    QString dropboxOption() const { return m_dropboxOption; }

    qreal progressValue() const { return m_progressValue; }
    void setProgressValue(const qreal &progress) {
        if (progress != m_progressValue) {
            m_progressValue = progress;
            emit progressValueChanged();
        }
    }

    QString stateLabel() const { return m_stateLabel; }
    void setStateLabel(const QString &stateLabel) {
        if (stateLabel != m_stateLabel) {
            m_stateLabel = stateLabel;
            emit stateLabelChanged();
        }
    }

    QString backupFilename() const { return m_backupFilename; }
    void setBackupFilename(const QString &filename) { m_backupFilename = filename; }

    Q_INVOKABLE QStringList csvFilesList() const {
        QStringList csvFilenames;
        foreach (MoneyCsvResume *csvFile, m_csvFilesList) {
            csvFilenames.append(csvFile->name());
        }
        debugMsg << csvFilenames;
        return csvFilenames;
    }

signals:
    void userLogged();
    void errorMessage(QString errorMsg);

    void receivedCsvFiles(QList<MoneyCsvResume*> csvFilesList);
    void receivedCsvContents(const QString &csvFile);

    void initialDateChanged();
    void finalDateChanged();
    void dropboxOptionChanged();
    void progressValueChanged();
    void stateLabelChanged();
    void backupFilenameChanged();

public slots:
    void login(QString email, QString password);
    void handleNetworkReply(QNetworkReply* networkReply);
    void handleTokenAndSecret(QNetworkReply* networkReply);
    void handleFilesList(QNetworkReply* networkReply);
    void handleCsvLoad();
    void handleAppFolderCreation(QNetworkReply* networkReply);
    void handleUploadProgress(qint64 sent,qint64 total);
    void handleDownloadProgress(qint64 received, qint64 total);

    void requestCsvFilesList();
    void requestCsvEntities(const QString &csvFilename);
    void exportCsvData(QString exportData);
    void requestAppFolderCreation();

private:
    QNetworkAccessManager *m_networkAccessManager;
    ConsumerData *m_consumerData;
    OAuth *m_oAuth;
    UserData *m_userData;
    Dropbox *m_dropbox;

    CsvManager *m_csvManager;

    QList<MoneyCsvResume*> m_csvFilesList;

    MoneyCsvResume* m_choosedCsvFile;
    QDate m_dtInitial;
    QDate m_dtFinal;
    QString m_dropboxOption;
    QString m_backupFilename;

    bool m_appFolderExists;

    qreal m_progressValue;
    QString m_stateLabel;
    QTime m_uploadTime;
    QTime m_downloadTime;

    QNetworkReply *m_networkReplyDownload;
};

#endif // DROPBOXMANAGER_H
