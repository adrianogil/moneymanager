#ifndef QMLVIBRAHELPER_H
#define QMLVIBRAHELPER_H

class XQVibra;

#include <QtCore/QObject>

class QMLVibraHelper : public QObject
{
    Q_OBJECT

public:
    QMLVibraHelper();

    Q_INVOKABLE void start(const int &intensity, const int &duration);

private:
    XQVibra *m_vibra;

};

#endif // QMLVIBRAHELPER_H
