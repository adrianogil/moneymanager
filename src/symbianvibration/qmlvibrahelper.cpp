#include "qmlvibrahelper.h"

#if defined(Q_OS_SYMBIAN)
#include "symbianvibration/xqvibra.h"
#else
#include <QtCore/QDebug>
#endif

QMLVibraHelper::QMLVibraHelper()
{
#if defined(Q_OS_SYMBIAN)
    m_vibra = new XQVibra(this);
#endif
}

void QMLVibraHelper::start(const int &intensity, const int &duration)
{
#if defined(Q_OS_SYMBIAN)
    m_vibra->setIntensity(intensity);
    m_vibra->start(duration);
#else
    Q_UNUSED(intensity)
    Q_UNUSED(duration)
    qDebug() << "Platform does not support vibration.";
#endif
}
