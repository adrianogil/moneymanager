﻿#include <QtGui/QApplication>
#include "qmlapplicationviewer.h"

#include <qdeclarative.h>
#include <QtDeclarative/QDeclarativeView>
#include <QtDeclarative/QDeclarativeEngine>
#include <QtDeclarative/QDeclarativeContext>

#include "daomanager.h"
#include "specificsearch.h"
#include "widgetchart.h"

#include "qmlvibrahelper.h"

#include "userdao.h"

#include "aboututils.h"
#include "dropbox/dropboxmanager.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Register C++ types to be used inside QML
    qmlRegisterType<Entity>("Entity", 1, 0, "Entity");
    qmlRegisterType<MoneyCategory>("Entity", 1, 0, "MoneyCategory");
    qmlRegisterType<Money>("Entity", 1, 0, "Money");
    qmlRegisterType<MoneyUser>("Entity", 1, 0, "User");
    qmlRegisterType<MoneyGoal>("Entity", 1, 0, "MoneyGoal");
    qmlRegisterType<MoneyCsvResume>("Entity",1,0,"CsvResume");
    qmlRegisterType<SpecificSearch>("Entity", 1, 0, "SpecificSearch");
    qmlRegisterType<WidgetChart>("Charts", 1, 0, "WidgetChart");

    QmlApplicationViewer viewer;

    DaoManager *daoManager = new DaoManager(true);
    daoManager->login(STANDARD_LOGIN,STANDARD_PASSWORD);

    DropboxManager *dropboxManager = new DropboxManager;

    // Send C++ object to QML
    viewer.rootContext()->setBaseUrl(QUrl("common"));
    viewer.rootContext()->setContextProperty("daoManager", daoManager);
    viewer.rootContext()->setContextProperty("dropboxManager", dropboxManager);
    QMLVibraHelper helper;
    viewer.rootContext()->setContextProperty("vibra", &helper);
    AboutUtils aboutObject;
    viewer.rootContext()->setContextProperty("about", &aboutObject);

    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationLockPortrait);

    viewer.setFrameStyle(QFrame::NoFrame);

//#if defined (Q_WS_MEEGO)
//    viewer.setSource(QUrl("qrc:/qml/MeegoScreen.qml"));
//    viewer.showFullScreen();
//#else
    viewer.setSource(QUrl("qrc:/qml/Main.qml"));
    viewer.showExpanded();
//#endif


    QObject::connect(viewer.engine(), SIGNAL(quit()), qApp, SLOT(quit()));

    return app.exec();
}
