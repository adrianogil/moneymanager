INCLUDEPATH += $$PWD

HEADERS += $$PWD/categorydao.h \
    $$PWD/databasemanager.h \
    $$PWD/moneydao.h \
    $$PWD/moneymanagerdb.h \
    $$PWD/userdao.h \
    $$PWD/goaldao.h \
    $$PWD/specificsearch.h \
    $$PWD/daomanager.h \
    $$PWD/csvmanager.h \
    $$PWD/entitydao.h

SOURCES += $$PWD/categorydao.cpp \
    $$PWD/databasemanager.cpp \
    $$PWD/moneydao.cpp \
    $$PWD/userdao.cpp \
    $$PWD/goaldao.cpp \
    $$PWD/specificsearch.cpp \
    $$PWD/daomanager.cpp \
    $$PWD/csvmanager.cpp \
    $$PWD/entitydao.cpp


