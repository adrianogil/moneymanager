#ifndef SPECIFICSEARCH_H
#define SPECIFICSEARCH_H
#include <QObject>
#include "category.h"
#include <QDate>

#define DATE_FORMAT "dd/MM/yy"

class QSqlQuery;
class Money;

class SpecificSearch : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString dateSince READ dateSince WRITE setDateSince NOTIFY dateSinceChanged)
    Q_PROPERTY(QString dateUntil READ dateUntil WRITE setDateUntil NOTIFY dateUntilChanged)
    Q_PROPERTY(double lessValue READ lessValue WRITE setLessValue NOTIFY lessValueChanged)
    Q_PROPERTY(double greaterValue READ greaterValue WRITE setGreaterValue NOTIFY greaterValueChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)
    Q_PROPERTY(MoneyCategory* category READ category WRITE setCategory NOTIFY categoryChanged)

private:

    QDate sinceDt;
    QDate untilDt;
    double m_lessValue;
    double m_greaterValue;
    QString m_description;
    QList<MoneyCategory*> listMoneyCategory;

    QString m_dateSince;
    QString m_dateUntil;
    MoneyCategory *m_category;

    bool isSinceDt;
    bool isUntilDt;
    bool isLessValue;
    bool isGreaterValue;
    bool isDescription;
    bool isListMoneyCategory;


public:
    SpecificSearch(QObject *parent = 0)
        : QObject(parent)
    {
        isSinceDt = false;
        isUntilDt = false;
        isLessValue = false;
        isGreaterValue = false;
        isDescription = false;
        isListMoneyCategory = false;
    }
    SpecificSearch(QDate sinceDt, QDate untilDt, double lessValue,
                   double greaterValue, QString description,
                   QList<MoneyCategory*> listMoneyCategory, bool isSinceDt,
                   bool isUntilDt, bool isLessValue, bool isGreaterValue,
                   bool isDescription, bool isListMoneyCategory, QObject *parent = 0)
                       : QObject(parent)
    {
        this->sinceDt = sinceDt;
        this->untilDt = untilDt;
        this->m_lessValue = lessValue;
        this->m_greaterValue = greaterValue;
        this->m_description = description;
        this->listMoneyCategory = listMoneyCategory;
        this->isDescription = isDescription;
        this->isGreaterValue = isGreaterValue;
        this->isLessValue = isLessValue;
        this->isListMoneyCategory = isListMoneyCategory;
        this->isSinceDt = isSinceDt;
        this->isUntilDt = isUntilDt;
    }

    QDate getSinceDt() { return sinceDt; }
    QDate getUntilDt() { return untilDt; }
    double getLessValue() { return m_lessValue; }
    double getGreaterValue() { return m_greaterValue; }
    QString getDescription() { return m_description; }
    QList<MoneyCategory*> getListMoneyCategory() { return listMoneyCategory; }

    bool isDescriptionUsed() { return this->isDescription; }
    bool isGreaterValueUsed() { return this->isGreaterValue; }
    bool isLessValueUsed() { return this->isLessValue; }
    bool isListMoneyCategoryUsed() { return this->isListMoneyCategory; }
    bool isSinceDtUsed() { return this->isSinceDt; }
    bool isUntilDtUsed() { return this->isUntilDt; }

    void setSinceDt(QDate sinceDt) { isSinceDt = true; this->sinceDt = sinceDt; }
    void setUntilDt(QDate untilDt) { isUntilDt = true; this->untilDt = untilDt; }
    void setLessValue(double lessValue) { isLessValue = true; this->m_lessValue = lessValue; }
    void setGreaterValue(double greaterValue) { isGreaterValue = true; this->m_greaterValue = greaterValue; }
    void setDescription(QString description) { isDescription = true; this->m_description = description; }
    void setListMoneyCategory(QList<MoneyCategory*> listMoneyCategory) { this->listMoneyCategory = listMoneyCategory; }

    void setActiveSinceDt(bool sinceDt) { this->isSinceDt = sinceDt; }
    void setActiveUntilDt(bool untilDt) { this->isUntilDt = untilDt; }
    void setActiveLessValue(bool lessValue) { this->isLessValue = lessValue; }
    void setActiveGreaterValue(bool greaterValue) { this->isGreaterValue = greaterValue; }
    void setActiveDescription(bool description) { this->isDescription = description; }
    void setActiveListMoneyCategory(bool listMoneyCategory) { this->isListMoneyCategory = listMoneyCategory; }

    QString getFrom();
    QString getConditions();

    QSqlQuery prepareQuery(QSqlQuery query);

    int getNumberActiveFilters();

    QString dateSince()
    {
        return sinceDt.toString(DATE_FORMAT);

    }
    QString dateUntil()
    {

        return untilDt.toString(DATE_FORMAT);

    }

    double lessValue() { return m_lessValue; }
    double greaterValue() { return m_greaterValue; }
    QString description() { return m_description; }
    MoneyCategory* category()
    {
        if (listMoneyCategory.length() > 0) {
            return listMoneyCategory.at(0);
        }
        return 0;
    }

    void setDateSince(QString dateSince)
    {
        isSinceDt = true;
        if (QString::compare(dateSince, m_dateSince) != 0) {
            m_dateSince = dateSince;
            sinceDt = QDate::fromString(m_dateSince, DATE_FORMAT);
            sinceDt = sinceDt.addYears(100);
            emit dateSinceChanged();
        } else  {
            m_dateSince = dateSince;
            sinceDt = QDate::fromString(m_dateSince, DATE_FORMAT);
            sinceDt = sinceDt.addYears(100);
        }
    }
    void setDateUntil(QString dateUntil)
    {
        isUntilDt = true;
        if (QString::compare(dateUntil, m_dateUntil) != 0) {
            m_dateUntil = dateUntil;
            untilDt = QDate::fromString(m_dateUntil, DATE_FORMAT);
            untilDt = untilDt.addYears(100);
            emit dateUntilChanged();
        } else {
            m_dateUntil = dateUntil;
            untilDt = QDate::fromString(m_dateUntil, DATE_FORMAT);
            untilDt = untilDt.addYears(100);
        }
    }
    void setCategory(MoneyCategory* category)
    {

        if (category) {
            isListMoneyCategory = true;
            m_category = category;
            QList<MoneyCategory*> listCat;
            listCat << m_category;
            setListMoneyCategory(listCat);
            emit categoryChanged();
        }
    }

    QList<Money*> filterMoneyList(QList<Money*> listAllMoney);

signals:
    void dateSinceChanged();
    void dateUntilChanged();
    void lessValueChanged();
    void greaterValueChanged();
    void descriptionChanged();
    void categoryChanged();


};

#endif // SPECIFICSEARCH_H
