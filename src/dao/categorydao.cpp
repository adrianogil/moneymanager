﻿#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtCore/QVariant>
#include <QtCore/QDateTime>

#include "category.h"
#include "categorydao.h"
#include "appdebug.h"

MoneyCategory* searchForCategory(QList<MoneyCategory*> listMoneyCategory, int idCategory)
{
    debugMsg << "Received list: " << listMoneyCategory;
    debugMsg << "Search for id " << idCategory << " inside list with " << listMoneyCategory.length() << " categories";
    foreach (MoneyCategory* category, listMoneyCategory) {
        if (category->id() == idCategory) {
            debugMsg << "found category " << category;
            return category;
        }
    }

    debugMsg << " haven't found the category with id " << idCategory;

    return 0;
}

MoneyCategory* getCategoryByName(const QString &categoryName, int id_user)
{
    qDebug() << __PRETTY_FUNCTION__;

    QString strQuery = " SELECT id_category, name, description, dtCreation, dtLastModification "
            "as id_category, name, description, dtCreation, dtLastModification FROM "
            " category where id_user = :id_user AND name LIKE :name_cat ";

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":name_cat", categoryName);
    query.bindValue(":id_user", id_user);
    query.exec();
    MoneyCategory* cat = 0;
    if (query.next()) {
        cat = new MoneyCategory(query.value(query.record().indexOf("id_category")).toInt(),
                                query.value(query.record().indexOf("name")).toString(),
                                query.value(query.record().indexOf("description")).toString(),
                                false);
        cat->setDtCreation(query.value(query.record().indexOf("dtCreation")).toDateTime());
        cat->setDtLastModification(query.value(query.record().indexOf("dtLastModification")).toDateTime());
        QString strQuery02 = " SELECT COUNT(m1.*), COUNT(m2,*) as count_in, count_out FROM money m1, money m2 "
                " WHERE m1.id_user = :id_user AND m2.id_user = :id_user "
                " AND m1.id_category = :id_category AND m2.id_category = :id_category "
                " AND m1.type_money = 0 AND m2.type_money = 1";

        QSqlQuery query02;
        query02.prepare(strQuery02);
        query02.bindValue(":id_category", cat->id());
        query02.bindValue(":id_user", id_user);
        query02.exec();

        if (query02.next()) {
            cat->setTotalExpenses(query02.value(query.record().indexOf("count_out")).toFloat());
            cat->setTotal(query02.value(query.record().indexOf("count_in")).toFloat() -
                          query02.value(query.record().indexOf("count_out")).toFloat());
        }

    } else {
        qDebug() << __PRETTY_FUNCTION__ << "category wasn't found";
    }

    return cat;
}

MoneyCategory* getCategoryByDate(MoneyCategory* incomplete_cat, int id_user)
{
    MoneyCategory *cat = 0;

    debugMsg << __PRETTY_FUNCTION__;

    QString strQuery = " SELECT id_category, name, description, dtCreation, dtLastModification "
            " as id_category, name, description, dtCreation, dtLastModification FROM "
            " category where id_user = :id_user AND dtCreation = :dtCreation AND "
            " dtLastModification = :dtLastModification ";

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":dtCreation", incomplete_cat->dtCreation());
    query.bindValue(":dtLastModification", incomplete_cat->dtLastModification());
    query.bindValue(":id_user", id_user);
    query.exec();

    if (query.next()) {
        cat = new MoneyCategory(query.value(query.record().indexOf("id_category")).toInt(),
                                query.value(query.record().indexOf("name")).toString(),
                                query.value(query.record().indexOf("description")).toString(),
                                false);
        cat->setDtCreation(query.value(query.record().indexOf("dtCreation")).toDateTime());
        cat->setDtLastModification(query.value(query.record().indexOf("dtLastModification")).toDateTime());
        QString strQuery02 = " SELECT COUNT(m1.*), COUNT(m2,*) as count_in, count_out FROM money m1, money m2 "
                " WHERE m1.id_user = :id_user AND m2.id_user = :id_user "
                " AND m1.id_category = :id_category AND m2.id_category = :id_category "
                " AND m1.type_money = 0 AND m2.type_money = 1";

        QSqlQuery query02;
        query02.prepare(strQuery02);if (query.next()) {
            cat = new MoneyCategory(query.value(query.record().indexOf("id_category")).toInt(),
                                    query.value(query.record().indexOf("name")).toString(),
                                    query.value(query.record().indexOf("description")).toString(),
                                    false);
            cat->setDtCreation(query.value(query.record().indexOf("dtCreation")).toDateTime());
            cat->setDtLastModification(query.value(query.record().indexOf("dtLastModification")).toDateTime());
            QString strQuery02 = " SELECT COUNT(m1.*), COUNT(m2,*) as count_in, count_out FROM money m1, money m2 "
                    " WHERE m1.id_user = :id_user AND m2.id_user = :id_user "
                    " AND m1.id_category = :id_category AND m2.id_category = :id_category "
                    " AND m1.type_money = 0 AND m2.type_money = 1";

            QSqlQuery query02;
            query02.prepare(strQuery02);
            query02.bindValue(":id_category", cat->id());
            query02.bindValue(":id_user", id_user);
            query02.exec();

            if (query02.next()) {
                cat->setTotalExpenses(query02.value(query.record().indexOf("count_out")).toFloat());
                cat->setTotal(query02.value(query.record().indexOf("count_in")).toFloat() -
                              query02.value(query.record().indexOf("count_out")).toFloat());
            }

        } else {
            qDebug() << __PRETTY_FUNCTION__ << "category wasn't found";
        }

        query02.bindValue(":id_category", cat->id());
        query02.bindValue(":id_user", id_user);
        query02.exec();

        if (query02.next()) {
            cat->setTotalExpenses(query02.value(query.record().indexOf("count_out")).toFloat());
            cat->setTotal(query02.value(query.record().indexOf("count_in")).toFloat() -
                          query02.value(query.record().indexOf("count_out")).toFloat());
        }

    } else {
        qDebug() << __PRETTY_FUNCTION__ << "category wasn't found";
    }


    return cat;
}

MoneyCategory* getCategory(MoneyCategory* incomplete_cat, int id_user)
{
    qDebug() << __PRETTY_FUNCTION__;

    if (!incomplete_cat)
        return 0;

    MoneyCategory *cat = getCategoryByName(incomplete_cat->name(), id_user);

    if (!cat) {
        cat = getCategoryByDate(incomplete_cat, id_user);
    }

    return cat;
}

MoneyCategory* getTotalByCategory(MoneyCategory* moneyCategory,
                                  int id_user,
                                  SpecificSearch *spPeriod,
                                  Money::money_types typesMoney)
{
    if (!moneyCategory)
        return 0;

    float totalOut = 0;
    float totalIn = 0;
    float total = 0;

    // Getting Total Expenses
    QString strQueryTotalOUT = "";
    QString strQueryTotalIN = "";

    strQueryTotalOUT = " SELECT SUM(money.amount)as count_out FROM money money, money_category mcat1 "
            " WHERE money.id_user = :id_user AND money.id_money = mcat1.id_money "
            " AND mcat1.id_category = :id_category AND money.type_money = 1 "
            " AND ";
    strQueryTotalIN = " SELECT SUM(money.amount)as count_in FROM money money, money_category mcat1 "
            " WHERE money.id_user = :id_user AND money.id_money = mcat1.id_money "
            " AND mcat1.id_category = :id_category AND money.type_money = 0 "
            " AND ";

    spPeriod->setActiveSinceDt(true);
    spPeriod->setActiveUntilDt(true);
    strQueryTotalOUT = strQueryTotalOUT + spPeriod->getConditions();
    strQueryTotalIN = strQueryTotalIN + spPeriod->getConditions();

    QSqlQuery query02;
    query02.prepare(strQueryTotalOUT);
    query02.bindValue(":id_category", moneyCategory->id());
    query02.bindValue(":id_user", id_user);
    spPeriod->prepareQuery(query02);
    query02.exec();

    if (query02.next())
        totalOut = query02.value(query02.record().indexOf("count_out")).toFloat();

    // Getting Total IN

    QSqlQuery query03;
    query03.prepare(strQueryTotalIN);
    query03.bindValue(":id_category", moneyCategory->id());
    query03.bindValue(":id_user", id_user);
    spPeriod->prepareQuery(query03);
    query03.exec();

    if (query03.next())
        totalIn = query03.value(query03.record().indexOf("count_in")).toFloat();

    total = totalIn - totalOut;

    moneyCategory->setTotal(total);
    if (typesMoney == Money::type_in)
        moneyCategory->setTotalExpenses(totalIn);
    else if (typesMoney == Money::type_out)
        moneyCategory->setTotalExpenses(totalOut);

    return moneyCategory;
}

QList<MoneyCategory*> getAllMoneyCategory(int id_user,
                                          SpecificSearch *spPeriod,
                                          Money::money_types typesMoney)
{
    debugMsg;

    QString strQuery = " SELECT id_category, name, description, dtCreation, dtLastModification "
            " as id_category, name, description, dtCreation, dtLastModification FROM "
            " category where id_user = :id_user";

    QSqlQuery query(strQuery);
    query.bindValue(":id_user", id_user);

    QList<MoneyCategory*> listMoneyCategory;
    query.exec();
    while (query.next()) {
        MoneyCategory* moneyCategory = new MoneyCategory(query.value(query.record().indexOf("id_category")).toInt(),
                                                         query.value(query.record().indexOf("name")).toString(),
                                                         query.value(query.record().indexOf("description")).toString(),
                                                         false);
        moneyCategory->setDtCreation(query.value(query.record().indexOf("dtCreation")).toDateTime());
        moneyCategory->setDtLastModification(query.value(query.record().indexOf("dtLastModification")).toDateTime());

        moneyCategory = getTotalByCategory(moneyCategory, id_user, spPeriod, typesMoney);

        listMoneyCategory.append(moneyCategory);
    }

    qDebug() << __PRETTY_FUNCTION__ << "Getting " << listMoneyCategory.size() << " categories";

    return listMoneyCategory;
}

QList<MoneyCategory*> getAllMoneyCategory(int id_user)
{
    debugMsg;

    QString strQuery = " SELECT id_category, name, description, dtCreation, dtLastModification "
            " as id_category, name, description, dtCreation, dtLastModification FROM "
            " category where id_user = :id_user";

    QSqlQuery query(strQuery);
    query.bindValue(":id_user", id_user);

    QList<MoneyCategory*> listMoneyCategory;
    query.exec();
    while (query.next()) {
        MoneyCategory* moneyCategory = new MoneyCategory(query.value(query.record().indexOf("id_category")).toInt(),
                                                         query.value(query.record().indexOf("name")).toString(),
                                                         query.value(query.record().indexOf("description")).toString(),
                                                         false);
        moneyCategory->setDtCreation(query.value(query.record().indexOf("dtCreation")).toDateTime());
        moneyCategory->setDtLastModification(query.value(query.record().indexOf("dtLastModification")).toDateTime());

        listMoneyCategory.append(moneyCategory);
    }

    qDebug() << __PRETTY_FUNCTION__ << "Getting " << listMoneyCategory.size() << " categories";

    return listMoneyCategory;
}

bool deleteMoneyCategory(MoneyCategory* category)
{
    if (!category)
        return false;

    qDebug() << __PRETTY_FUNCTION__;

    QString strQuery = " DELETE FROM ";
    strQuery = strQuery + "category WHERE id_category = :id_category";

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":id_category", category->id());
    return query.exec();
}

bool updateMoneyCategory(MoneyCategory* category)
{
    if (!category)
        return false;

    qDebug() << __PRETTY_FUNCTION__;
    QString strQuery = " UPDATE category SET name = :name, description = :description,"
            "dtCreation = :dtCreation, dtLastModification = :dtLastModification "
            " WHERE id_category = :id_category";


    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":id_category", category->id());
    query.bindValue(":name", category->name());
    query.bindValue(":description", category->description());
    query.bindValue(":dtCreation", category->dtCreation());
    query.bindValue(":dtLastModification", QDateTime::currentDateTime());

    return query.exec();
}

bool registerMoneyCategory(MoneyCategory* category, int id_user)
{
    if (!category || getCategoryByName(category->name(), id_user))
        return false;

    QString strQuery = " INSERT INTO category (name, description, dtCreation, dtLastModification, id_user)"
            "values (:name, :description, :dtCreation, :dtLastModification, :id_user) ";

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":name", category->name());
    query.bindValue(":description", category->description());
    query.bindValue(":dtCreation", QDateTime::currentDateTime());
    query.bindValue(":dtLastModification", QDateTime::currentDateTime());
    query.bindValue(":id_user", id_user);

    return query.exec();
}
