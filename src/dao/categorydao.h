#ifndef CATEGORYDAO_H
#define CATEGORYDAO_H

#include <QtCore/QString>
#include <QtCore/QList>

#include "specificsearch.h"
#include "category.h"
#include "money.h"

class MoneyCategory;

MoneyCategory* searchForCategory(QList<MoneyCategory*> listMoneyCategory, int idCategory);

MoneyCategory* getCategory(MoneyCategory* incompleteCat, int id_user);

MoneyCategory* getCategoryByName(const QString &categoryName, int id_user);

QList<MoneyCategory*> getAllMoneyCategory(int id_user);

QList<MoneyCategory*> getAllMoneyCategory(int id_user,
                                          SpecificSearch *spPeriod,
                                          Money::money_types typesMoney);

bool deleteMoneyCategory(MoneyCategory* category);

bool updateMoneyCategory(MoneyCategory* category);

bool registerMoneyCategory(MoneyCategory* category, int id_user);

#endif // CATEGORYDAO_H
