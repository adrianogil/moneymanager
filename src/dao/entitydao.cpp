#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtSql/QSqlError>
#include <QtCore/QVariant>

#include "entitydao.h"
#include "appdebug.h"

struct QueryCondition
{
    QString condition;
    QStringList placeHolders;
    QVariantList values;
};

EntityDao::EntityDao(QObject *parent) :
    QObject(parent)
{
}

bool EntityDao::persist(Entity *entity)
{
    if (!entity || m_dbParameters.length() <= 0 ||
            m_dbParameters.length() != m_objParameters.length())
        return false;

    entity->setDtCreation(QDateTime::currentDateTime());
    entity->setDtLastModification(QDateTime::currentDateTime());

    QString strQuery = "INSERT INTO " + m_tableName + " (";

    for(int i = 0; i < m_dbParameters.length(); i++) {
        QString dbParameter = m_dbParameters.at(i);
        strQuery = strQuery + dbParameter + (i == m_dbParameters.length()-1? "" : ",");
    }

    strQuery = strQuery + ") VALUES (";

    for(int i = 0; i < m_dbParameters.length(); i++) {
        QString dbPlaceholder = ":" + m_dbParameters.at(i);
        strQuery = strQuery + dbPlaceholder + (i == m_dbParameters.length()-1? "" : ",");
    }

    strQuery = strQuery + ")";

    debugMsg << strQuery;

    QSqlQuery query;
    query.prepare(strQuery);

    for(int i = 0; i < qMin(m_dbParameters.length(), m_objParameters.length()); i++) {
        QString dbPlaceholder = ":" + m_dbParameters.at(i);
        QString objParameter = m_objParameters.at(i);
        QVariant objValue = entity->property(objParameter.toAscii());

        query.bindValue(dbPlaceholder, objValue);
    }

    bool result = query.exec();

    if (!result)
        debugMsg << query.lastError().text();

    return result;
}

bool EntityDao::remove(Entity *entity)
{
    if (!entity || m_dbParameters.length() <= 0 ||
            m_dbParameters.length() != m_objParameters.length())
        return false;

    Entity *oldEntity = getEntity(entity);

    if (!oldEntity)
        return false;

    entity->setId(oldEntity->id());
    entity->setDtCreation(oldEntity->dtCreation());

    QString strQuery = " DELETE FROM " + m_tableName +
            " WHERE " + m_idPrimaryKey + " = :" + m_idPrimaryKey;

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":" + m_idPrimaryKey, entity->id());

    bool result = query.exec();
    if (!result)
        query.lastError().text();
    return result;
}

bool EntityDao::update(Entity *entity)
{
    if (!entity || m_dbParameters.length() <= 0 ||
            m_dbParameters.length() != m_objParameters.length())
        return false;

    Entity *oldEntity = getEntity(entity);

    if (!oldEntity)
        return false;

    entity->setId(oldEntity->id());
    entity->setDtCreation(oldEntity->dtCreation());

    QString strQuery = " UPDATE " + m_tableName + " SET ";

    for(int i = 0; i < m_dbParameters.length(); i++) {
        QString dbParameter = m_dbParameters.at(i);
        strQuery = strQuery + dbParameter + " = :" + dbParameter +
                (i == m_dbParameters.length()-1? "" : ",");
    }

    strQuery = strQuery +
            " WHERE " + m_idPrimaryKey + " = :" + m_idPrimaryKey;


    QSqlQuery query;
    query.prepare(strQuery);

    for(int i = 0; i < m_dbParameters.length(); i++) {
        QString dbPlaceholder = ":" + m_dbParameters.at(i);
        QString objParameter = m_objParameters.at(i);
        QVariant objValue = entity->property(objParameter.toAscii());

        query.bindValue(dbPlaceholder, objValue);
    }

    query.bindValue(":" + m_idPrimaryKey, entity->id());

    bool result = query.exec();
    if (!result)
        debugMsg << query.lastError().text();
    return result;
}

QList<Entity *> EntityDao::getAll()
{
    if (m_dbParameters.length() <= 0 ||
            m_dbParameters.length() != m_objParameters.length())
        return QList<Entity*>();

    debugMsg << "START";

    QString strQuery = "SELECT ";

    strQuery = strQuery + m_tableName.toLower() + "." + m_idPrimaryKey +
            (m_dbParameters.length() > 0? "," : "");

    for(int i = 0; i < m_dbParameters.length(); i++) {
        QString dbParameter = m_dbParameters.at(i);
        strQuery = strQuery + m_tableName.toLower() + "." + dbParameter +
                (i == m_dbParameters.length()-1? "" : ",");
    }

    strQuery = strQuery + " FROM " + m_tableName + " " + m_tableName.toLower();

    if (m_queryConditions.length() > 0)
        strQuery = strQuery + " WHERE ";

    foreach (QueryCondition *queryCondition, m_queryConditions) {
        strQuery = strQuery + queryCondition->condition +
                (queryCondition == m_queryConditions.last()? "" : " AND ");
    }

    QSqlQuery query;
    query.prepare(strQuery);

    foreach (QueryCondition *queryCondition, m_queryConditions) {
        for(int i = 0; i < qMin(queryCondition->placeHolders.length(),
                                queryCondition->values.length()); i++) {
            QString placeHolder = queryCondition->placeHolders.at(i);
            QVariant value = queryCondition->values.at(i);
            query.bindValue(placeHolder, value);
        }
    }

    bool result = query.exec();

    if (!result)
        debugMsg << query.lastError().text() << strQuery;

    QList<Entity*> entityList;

    while (query.next()) {
        QSqlRecord record = query.record();
        Entity *newEntity = buildEntity(&record);
        if (newEntity)
            entityList.append(newEntity);
    }

    debugMsg << "END" << entityList.length();

    return entityList;
}

/*
  Recover id value from an incomplete Entity
  */
Entity* EntityDao::getEntity(Entity *entity)
{
    if (!entity || m_dbParameters.length() <= 0 ||
            m_dbParameters.length() != m_objParameters.length())
        return 0;

    QString strQuery = "SELECT ";

    strQuery = strQuery + m_tableName.toLower() + "." + m_idPrimaryKey +
            (m_dbParameters.length() > 0? "," : "");

    for(int i = 0; i < m_dbParameters.length(); i++) {
        QString dbParameter = m_dbParameters.at(i);
        strQuery = strQuery + m_tableName.toLower() + "." + dbParameter +
                (i == m_dbParameters.length()-1? "" : ",");
    }

    strQuery = strQuery + " FROM " + m_tableName + " " + m_tableName.toLower() +
            " WHERE " +  m_tableName.toLower() + ".dtCreation = :dtCreation AND " +
            m_tableName.toLower() + ".dtLastModification = :dtLastModification ";

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":dtCreation", entity->dtCreation());
    query.bindValue(":dtLastModification", entity->dtLastModification());

    query.exec();

    Entity *newEntity = 0;

    if (query.next()) {
        QSqlRecord record = query.record();
        newEntity = buildEntity(&record);
    } else {
        debugMsg << query.lastError() << strQuery << entity->id()
                 << entity->dtCreation() << entity->dtLastModification();
    }

    return newEntity;
}

Entity *EntityDao::buildEntity(QSqlRecord *record)
{
    Entity *entity = new Entity(this);

    for(int i = 0; i < m_dbParameters.length(); i++) {
        QString dbParameter = m_dbParameters.at(i);
        QString objParameter = m_objParameters.at(i);
        QVariant objValue = record->value(record->indexOf(dbParameter));

        entity->setProperty(objParameter.toAscii(), objValue);
    }

    entity->setId(record->value(record->indexOf(m_idPrimaryKey)).toInt());

    return entity;
}

void EntityDao::resetQueryConditions()
{
    foreach (QueryCondition* condition, m_queryConditions) {
        delete condition;
    }
    m_queryConditions.clear();
    debugMsg << m_queryConditions;
}

void EntityDao::appendQueryConditions(const QString &condition, const QStringList &placeHolders, const QVariantList &values)
{
    debugMsg << "1";
    QueryCondition *queryCondition = new QueryCondition;
    debugMsg << "2";
    queryCondition->condition = condition;
    queryCondition->placeHolders = placeHolders;
    queryCondition->values = values;
    m_queryConditions.append(queryCondition);
    debugMsg << m_queryConditions.length();
}
