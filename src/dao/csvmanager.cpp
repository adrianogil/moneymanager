#include "csvmanager.h"
#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtCore/QStringList>
#include <QtCore/QDebug>
#include "appdebug.h"

CsvManager::CsvManager(QObject *parent) :
    QObject(parent)
{
    MetaField idField = {0,"id",QVariant::Int};
    MetaField moneyTypeField = {1,"moneyType",QVariant::Int};
    MetaField categoryField = {2,"categoryByName",QVariant::String};
    MetaField descriptionField = {3,"description",QVariant::String};
    MetaField amountField = {4,"amount",QVariant::Double};
    MetaField moneyDateField = {5,"moneyDate",QVariant::String};
    MetaField dtCreationField = {6,"strDtCreation",QVariant::String};
    MetaField dtLastModificationField = {7,"strDtLastModification",QVariant::String};

    m_metaFieldList.append(idField);
    m_metaFieldList.append(moneyTypeField);
    m_metaFieldList.append(categoryField);
    m_metaFieldList.append(descriptionField);
    m_metaFieldList.append(amountField);
    m_metaFieldList.append(moneyDateField);
    m_metaFieldList.append(dtCreationField);
    m_metaFieldList.append(dtLastModificationField);

    //    foreach (MetaField mf, m_metaFieldList)
    //        debugMsg << mf.index << mf.name;
}

QString CsvManager::createCsvFromEntities(QList<Entity *> entityList, QStringList fields,
                                          QStringList fieldsNames)
{
    debugMsg << entityList.length() << fields;

    QString csvContents;
    QTextStream out(&csvContents);

    //QString crlf("\r\n");

    //Fields Name
    foreach (QString fieldName, fieldsNames)
        out << fieldName << ",";
    out << "\n";

    foreach (Entity* entity, entityList) {
        foreach (QString field, fields) {
            QVariant value = entity->property(field.toAscii());
            out << value.toString() << ",";
        }
        out << "\n";
    }
    out << "\n";

    return csvContents;
}

QList<Entity*> CsvManager::loadEntitiesFromCsv(QString filepath)
{
    QFile file(filepath);
    QList<Entity*> entitiesList;

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        return entitiesList;
    }

    QTextStream in(&file);

    return loadEntitiesFromCsv(&in);
}

QList<Entity*> CsvManager::loadEntitiesFromCsv(QTextStream *in)
{

    QList<Entity*> entitiesList;
    QString line;
    QStringList listLine;

    int lineNull = 0;
    if (in->readLine().isEmpty())
        lineNull = 1;
    while(lineNull == 0){
        line=in->readLine();

        if(!line.isEmpty()){
            Entity* entity = new Entity(this);
            listLine=line.split(",");

            bool validEntity = true;
            foreach(MetaField mField, m_metaFieldList){
                if (listLine.length() > mField.index) {
                    entity->setProperty(mField.name, QVariant(listLine.at(mField.index)));
                } else {
                    validEntity = false;
                }
            }
            if (validEntity)
                entitiesList.append(entity);

        } else {
            lineNull=1;
        }

    }

    return entitiesList;
}

QList<Entity *> CsvManager::loadEntitiesFromCsvContents(const QString &contents)
{
    QString csvContents(contents);
    QTextStream tx(&csvContents);
    return loadEntitiesFromCsv(&tx);
}

void CsvManager::syncEntitiesList(QList<Entity*> csvList, QList<Entity*> dbList)
{
    debugMsg << m_importPolicy << csvList.length() << dbList.length();

    QList<Entity*> deleteEntitiesList;
    QList<Entity*> persistEntitiesList;
    QList<Entity*> updateEntitiesList;
    QList<Entity*> conflitsDbEntitiesList;
    QList<Entity*> conflitsCvsEntitiesList;

    QList<Entity*> remainDbList;
    remainDbList << dbList;

    foreach (Entity* csvEntity, csvList) {
        bool conflits = false;

        // Update DB List removing old conflits
        dbList.clear();
        dbList << remainDbList;

        // Comparing current entity from CSV with all entities of the DB
        foreach (Entity* dbEntity, dbList) {
            // Get conflits
            //            debugMsg << "Test conflit between dBEntity(" << dbEntity->id() << "," << dbEntity->strDtCreation() << ")"
            //                     << " and csvEntity " << csvEntity->id() << "," << csvEntity->strDtCreation() << ")";
            if (!conflits && csvEntity->id() == dbEntity->id() &&
                    csvEntity->dtCreation() == dbEntity->dtCreation()) {
                conflitsDbEntitiesList.append(dbEntity);
                conflitsCvsEntitiesList.append(csvEntity);
                conflits = true;
                remainDbList.removeOne(dbEntity);
                // Compare by last modification
                if (m_importPolicy.testFlag(CsvManager::KeepLastVersion)) {
                    if (csvEntity->dtLastModification() > dbEntity->dtLastModification()) {
                        // Last Version is from CSV
                        updateEntitiesList.append(csvEntity);
                    } else if (csvEntity->dtLastModification() < dbEntity->dtLastModification()) {
                        // Last Version is from DB
                    } else {
                        // Registers are the same!
                    }
                } else if (m_importPolicy.testFlag(CsvManager::KeepDBVersion)) {
                } else if (m_importPolicy.testFlag(CsvManager::KeepCSVVersion)) {
                    updateEntitiesList.append(csvEntity);
                }
            }
        }
        // If there is some Entities from CSV that aren't in DB
        //debugMsg << csvEntity->id() << m_importPolicy << conflits;
        if (!m_importPolicy.testFlag(CsvManager::KeepOnlyDBRegisters) && (
                    m_importPolicy.testFlag(CsvManager::KeepAllRegisters) ||
                    m_importPolicy.testFlag(CsvManager::KeepOnlyCSVRegisters) ||
                    m_importPolicy.testFlag(CsvManager::GraphiclyChoose))) {
            if (!conflits)
                persistEntitiesList.append(csvEntity);
        }
    }

    if (m_importPolicy.testFlag(CsvManager::KeepOnlyCSVRegisters)) {
        foreach (Entity* entity, remainDbList) {
            deleteEntitiesList.append(entity);
        }
    }

    emit deleteEntities(deleteEntitiesList);
    emit persistEntities(persistEntitiesList);
    emit updateEntities(updateEntitiesList);

    if (m_importPolicy.testFlag(CsvManager::GraphiclyChoose)) {
        emit conflitsEntities(conflitsDbEntitiesList, conflitsCvsEntitiesList);
    }

}
