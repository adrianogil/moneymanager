#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtCore/QVariant>
#include <QtGui/QMessageBox>
#include <QtSql/QSqlError>

#include "money.h"
#include "specificsearch.h"
#include "categorydao.h"
#include "moneydao.h"
#include "appdebug.h"

/** ABOUT MONEY <-> CATEGORY **/
QList<MoneyCategory*> getAllCategoryByMoney(Money* money, QList<MoneyCategory*> listAllMoneyCategory) {
    if (!money || listAllMoneyCategory.isEmpty())
        return QList<MoneyCategory*>();

    debugMsg;


    QList<MoneyCategory*> listCategory;

    QString strQuery = "SELECT id_category as id_category FROM money_category "
                       " WHERE id_money = :id_money";
    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":id_money", money->id());

    query.exec();

    while (query.next()) {
        MoneyCategory* cat = searchForCategory(listAllMoneyCategory,
                                              query.value(query.record().indexOf("id_category")).toInt());
        listCategory.append(cat);
    }

    return listCategory;
}

bool deleteAllCategoryByMoney(Money* money) {
    if (!money)
        return false;

    qDebug() << __PRETTY_FUNCTION__;

    QString strQuery = " DELETE FROM money_category "
               " WHERE id_money = :id_money";

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":id_money", money->id());
    return query.exec();
}

bool MoneyDao::registerCategoryByMoney(int id_category, int id_money) {
    debugMsg << id_category << id_money;

    QString strQuery = " INSERT INTO money_category (id_money, id_category) "
                       " VALUES (:id_money, :id_category)";
    QSqlQuery query;
    query.prepare(strQuery);

    query.bindValue(":id_category", id_category);
    query.bindValue(":id_money", id_money);
    return query.exec();
}

bool MoneyDao::registerCategoryByMoney(QString categoryName, int id_money) {

    MoneyCategory *cat = new MoneyCategory;
    cat->setName(categoryName);
    registerMoneyCategory(cat, m_idUser);

    cat = getCategory(cat, m_idUser);

    if (!cat)
        return 0;

    return registerCategoryByMoney(cat->id(),id_money);
}

bool MoneyDao::registerAllCategoryByMoney(Money* money) {
    if (!money)
        return false;

    debugMsg << money->listMoneyCategory() << money->categoryByName();

    if (money->listMoneyCategory().length() == 0 &&
            money->categoryByName() != "") {
        return registerCategoryByMoney(money->categoryByName(), money->id());
    }
    foreach (MoneyCategory* category, money->listMoneyCategory())
        if (!registerCategoryByMoney(category->id(), money->id()))
            return false;

    return true;
}

/** ABOUT MONEY **/

Money* MoneyDao::getMoney(Money* incomplete_money, QList<MoneyCategory*> listAllMoneyCategory)
{

    qDebug() << __PRETTY_FUNCTION__;

    QString strQuerySelect = " SELECT money.id_money, type_money, amount, dt_transaction, description, "
                             " timestamp, dtCreation, dtLastModification "
                             " as id_money, type_money, amount, dt_transaction, description, "
                             " timestamp, dtCreation, dtLastModification ";
    QString strQueryFrom = " FROM ";
    strQueryFrom = strQueryFrom + " money money ";

    QString strQueryCondition = " WHERE money.id_user = :id_user AND money.timestamp LIKE :timestamp AND "
            " money.amount = :amount AND money.dtCreation = :dtCreation AND money.description LIKE :description ";
    QString strQuery = strQuerySelect + strQueryFrom + strQueryCondition;

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":id_user", m_idUser);
    query.bindValue(":timestamp", incomplete_money->timestamp());
    query.bindValue(":amount", incomplete_money->amount());
    query.bindValue(":dtCreation", incomplete_money->dtCreation());
    query.bindValue(":description", incomplete_money->description());

    query.exec();

    Money* money = 0;

    debugMsg << m_idUser << incomplete_money->timestamp() << incomplete_money->amount()
             << incomplete_money->dtCreation() << incomplete_money->description()
             << strQuery;

    if(query.next()) {
        money = new Money();
        money->setId(query.value(query.record().indexOf("id_money")).toInt());
        money->setTypeMoney(query.value(query.record().indexOf("type_money")).toInt() == 0 ?
                           Money::type_in : Money::type_out );
        money->setAmount((float)query.value(query.record().indexOf("amount")).toDouble());
        money->setDtTransaction(query.value(query.record().indexOf("dt_transaction")).toDate());
        money->setDescription(query.value(query.record().indexOf("description")).toString());
        money->setListMoneyCategory(getAllCategoryByMoney(money, listAllMoneyCategory));
        money->setTimestamp(query.value(query.record().indexOf("timestamp")).toString());
        money->setDtCreation(query.value(query.record().indexOf("dtCreation")).toDateTime());
        money->setDtLastModification(query.value(query.record().indexOf("dtLastModification")).toDateTime());
    } else {
        debugMsg << "It didn't find the Money: " << query.lastError().text();
    }
    return money;
}

QList<Money*> MoneyDao::getAllMoney(QList<MoneyCategory*> listAllMoneyCategory,
                                    SpecificSearch* ss,
                                    Money::money_types typeMoney)
{
    debugMsg;

    QString strQuery = " ";

    QString strQuerySelect = " SELECT money.id_money, type_money, amount, dt_transaction, description, "
                             " timestamp, dtCreation, dtLastModification, frequency, details "
                             " as id_money, type_money, amount, dt_transaction, description, "
                             " timestamp, dtCreation, dtLastModification, frequency, details ";
    QString strQueryFrom = " FROM ";
    strQueryFrom = strQueryFrom + " money money ";


    QString strQueryCondition = " WHERE money.id_user = :id_user ";

    QString strQueryOrder = " order by money.dt_transaction ";

    QSqlQuery query;

    if (typeMoney == Money::type_none ) {

        //qDebug() << __PRETTY_FUNCTION__ << "Search ALL";

        strQuery = strQuerySelect + strQueryFrom;

        if (ss && ss->getNumberActiveFilters() > 0) {
            if (ss->isListMoneyCategoryUsed())
                strQueryFrom = strQueryFrom + "," + "money_category cat ";
            strQuery = strQuerySelect + strQueryFrom;
            strQuery = strQuery + strQueryCondition + " AND " + ss->getConditions();

            strQuery = strQuery + strQueryOrder;
            query.prepare(strQuery);
            query = ss->prepareQuery(query);
        } else {
            strQuery = strQuery + strQueryCondition;
            strQuery = strQuery + strQueryOrder;
            query.prepare(strQuery);
        }

        //qDebug() << strQuery;

    } else {
        strQueryCondition = strQueryCondition + "AND money.type_money = :type_money ";
        strQuery = strQuerySelect + strQueryFrom + strQueryCondition;
        if (ss && ss->getNumberActiveFilters() > 0) {
            if (ss->isListMoneyCategoryUsed())
                strQueryFrom = strQueryFrom + "," + " money_category cat ";
            strQuery = strQuerySelect + strQueryFrom + strQueryCondition;
            strQuery = strQuery + " AND " + ss->getConditions();
            query.prepare(strQuery);
            strQuery = strQuery + strQueryOrder;
            query = ss->prepareQuery(query);
        } else {
            strQuery = strQuery + strQueryOrder;
            query.prepare(strQuery);
        }
        //qDebug() << strQuery;
        //qDebug() << __PRETTY_FUNCTION__ << "Binding TypeMoney " << typeMoney;
        query.bindValue(":type_money", typeMoney == Money::type_in ? 0 : 1);
    }

    //qDebug() << __PRETTY_FUNCTION__ << "Binding idUser " << m_idUser;
    query.bindValue(":id_user", m_idUser);
    query.exec();

    QList<Money*> listMoney;

    while (query.next()) {
        Money* money = new Money;

        money->setId(query.value(query.record().indexOf("id_money")).toInt());
        money->setTypeMoney(query.value(query.record().indexOf("type_money")).toInt() == 0 ?
                           Money::type_in : Money::type_out );
        money->setAmount((float)query.value(query.record().indexOf("amount")).toDouble());
        money->setDtTransaction(query.value(query.record().indexOf("dt_transaction")).toDate());
        money->setDescription(query.value(query.record().indexOf("description")).toString());
        money->setListMoneyCategory(getAllCategoryByMoney(money, listAllMoneyCategory));
        money->setCategoryByName(money->listMoneyCategory().length() > 0?
                                     money->listMoneyCategory().at(0)->name() : "");
        money->setFrequency((int)query.value(query.record().indexOf("frequency")).toInt());
        money->setDetails(query.value(query.record().indexOf("details")).toString());
        money->setTimestamp(query.value(query.record().indexOf("timestamp")).toString());
        money->setDtCreation(query.value(query.record().indexOf("dtCreation")).toDateTime());
        money->setDtLastModification(query.value(query.record().indexOf("dtLastModification")).toDateTime());

        debugMsg << listAllMoneyCategory;

        listMoney.append(money);
    }

    debugMsg << "getting " << listMoney.length() << " money register ";

    return listMoney;
}

bool MoneyDao::deleteMoney(Money* money)
{
    if (!money)
        return false;

    debugMsg;

    // Deleting category related
    deleteAllCategoryByMoney(money);

    QString strQuery = " DELETE FROM money "
               " WHERE id_money = :id_money";

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":id_money", money->id());

    return query.exec();
}

bool MoneyDao::updateMoney(Money* money)
{
    if (!money)
        return false;

    qDebug() << __PRETTY_FUNCTION__;

    // Deleting category related
    deleteAllCategoryByMoney(money);

    QString strQuery = " UPDATE ";
    strQuery = strQuery + " money SET  amount = :amount, type_money = :type_money, "
               " dt_transaction = :dt_transaction, description = :description, "
               " frequency = :frequency, details = :details,"
               " dtLastModification = :dtLastModification "
               " WHERE id_money = :id_money ";


    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":id_money", money->id());
    query.bindValue(":dt_transaction", money->dtTransaction());
    query.bindValue(":type_money", money->typeMoney() == Money::type_in ? 0 : 1);
    query.bindValue(":description", money->description());
    query.bindValue(":details", money->details());
    query.bindValue(":frequency", money->frequency());
    query.bindValue(":amount", money->amount());
    query.bindValue(":dtLastModification", QDateTime::currentDateTime());
    bool result = query.exec();

    // Reinserting the categories
    registerAllCategoryByMoney(money);

    return result;
}

bool MoneyDao::registerMoney(Money* money)
{
    if (!money)
        return false;

    debugMsg << money << money->category();

    money->setTimestamp(QDateTime::currentDateTime().toString());

    QString strQuery = " INSERT INTO ";
    strQuery = strQuery + " money (amount, type_money, dt_transaction, description, "
            " timestamp, frequency, details, id_user,"
            " dtCreation, dtLastModification) "
            " VALUES "
            " (:amount, :type_money, :dt_transaction, :description, "
            "  :timestamp, :frequency, :details, :id_user, "
            "  :dtCreation, :dtLastModification) ";

    //qDebug() << strQuery;

    money->setDtCreation(QDateTime::currentDateTime());

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":dt_transaction", money->dtTransaction());
    query.bindValue(":type_money", money->typeMoney() == Money::type_in ? 0 : 1);
    query.bindValue(":description", money->description());
    query.bindValue(":amount", money->amount());
    query.bindValue(":timestamp", money->timestamp());
    query.bindValue(":details", money->details());
    query.bindValue(":frequency", money->frequency());
    query.bindValue(":id_user", m_idUser);
    query.bindValue(":dtCreation", money->dtCreation());
    query.bindValue(":dtLastModification", QDateTime::currentDateTime());

    if( query.exec() ) {
        QList<MoneyCategory*> listCategory = money->listMoneyCategory();
        QString categoryName = money->categoryByName();
        money = getMoney(money, getAllMoneyCategory(m_idUser));
        money->setListMoneyCategory(listCategory);
        money->setCategoryByName(categoryName);
        return registerAllCategoryByMoney(money);
    }
    else return false;

    return true;
}

bool MoneyDao::removeCategoryFromMoney(const QString &category)
{
    debugMsg << category << "Start";

    QList<Money*> moneyList = getAllMoney(getAllMoneyCategory(m_idUser));
    foreach(Money* money, moneyList) {
        debugMsg << money->strCategory() << money->categoryByName() << category;
        if (money->categoryByName() == category) {
            money->setCategoryByName("Sem Categoria");
            updateMoney(money);
        }
    }

    debugMsg << "End";

    return true;
}

