﻿#include <QtCore/QDebug>
#include <QtCore/QDateTime>

#include <math.h>

#include "daomanager.h"
#include "dropbox/moneycsvresume.h"

#include "databasemanager.h"
#include "specificsearch.h"
#include "csvmanager.h"

#include "appdebug.h"
#include "utils.h"

DaoManager::DaoManager(bool debugActivated)
    : m_newUser(new MoneyUser)
    , m_newGoal(new MoneyGoal)
    , m_money2BeEdited(new Money)
    , m_money2Sync(new MoneyCsvResume)
    , m_waiting(false)
    , m_calendarActivated(false)
    , m_catEditEnabled(false)
{
    debugMsg;

    // Preparing the database to connections
    DatabaseManager *dbManager = new DatabaseManager();
    dbManager->openDB();

    m_typeDate = 1;
    m_typeMoney = -1;
    m_semaphorMoney = 0;
    m_semaphorCsvImporter = 0;
    m_currentSelectedDate = QDate::currentDate();

    userDao = new UserDao;

    createFrequencyList();

    // Just for Debug
    if (debugActivated)
        login(STANDARD_LOGIN, STANDARD_PASSWORD);

    // Colors related to the Category Chart.
    m_colorList << QColor(255,221,0) << QColor(243,132,0)
                << QColor(166,14,14) << QColor(2,58,101)
                << QColor(0,173,239);
    m_colorNameList << QString("#FFDD00") << QString("#F38400")
                    << QString("#A60E0E") << QString("#053A65")
                    << QString("#00ADEF");

    m_resumeOptions << tr("Resumo de Entrada") << tr("Resumo de Saida") << tr("Resumo do Fluxo de Caixa");
    m_resumeType = tr("Resumo de Entrada");
}

bool DaoManager::openDropDown(QVariant list, QString propertyName)
{
    emit startDropDown(list, propertyName);

    return true;
}

bool DaoManager::openMessageBox(QString title,
                                QString message,
                                QVariant options,
                                QVariant viewList,
                                QVariant stateList,
                                QVariant actionList)
{
    emit startMessageBox(title, message, options, viewList, stateList, actionList);

    return true;
}

void DaoManager::setDropDownValue(QString property, QString value)
{
    debugMsg << property << value <<
                setProperty(property.toAscii(), QVariant::fromValue(value));
}

void DaoManager::setMessageBoxValue(QString view, QString state, QString action)
{
    emit switchFlow(view, state, action);
}

bool DaoManager::login(QString username, QString password)
{
    debugMsg;
    userDao->createStandardUser();
    m_user = new MoneyUser(username, password, "", "");
    if (userDao->userLogin(m_user)) {
        m_user = userDao->getUser(m_user);
        m_moneyDao = new MoneyDao(m_user->id());
        m_goalDao = new GoalDao(m_user->id(), this);
        m_spSearch = new SpecificSearch();
        updatePeriod();
        updateDatePeriodList();
        emit categoryListChanged();
        emit moneyListChanged();
        emit goalListChanged();
        return true;
    } else return false;
}

bool DaoManager::checkUser(QString username)
{
    return userDao->verifyUserLogin(username);;
}

bool DaoManager::createUser(QString username, QString password)
{
    debugMsg;
    m_user = new MoneyUser(username, password);
    if (userDao->registerUser(m_user)) {
        m_user = userDao->getUser(m_user);
        m_moneyDao = new MoneyDao(m_user->id());
        m_goalDao = new GoalDao(m_user->id(), this);
        m_spSearch = new SpecificSearch();
        updatePeriod();
        updateDatePeriodList();

        emit categoryListChanged();
        emit moneyListChanged();
        emit goalListChanged();

        return true;
    } else  return false;


    return false;
}

bool DaoManager::createNewUser()
{
    debugMsg;
    m_newUser->setQuestion(m_secretQuestion);
    if (userDao->registerUser(m_newUser)) {
        m_user = userDao->getUser(m_newUser);
        debugMsg << m_user;
        debugMsg << (m_user? QString::number(m_user->id()) : "-1");

        m_moneyDao = new MoneyDao(m_user->id());
        m_newUser = new MoneyUser;
        m_spSearch = new SpecificSearch();

        updatePeriod();
        updateDatePeriodList();

        emit categoryListChanged();
        emit moneyListChanged();
        emit goalListChanged();
        emit newUserChanged();

        return true;
    }

    return false;
}

QString DaoManager::getQuestion(QString userName)
{
    return userDao->getUserQuestion(userName);
}

bool DaoManager::verifyUserAccountAnswer(QString login, QString answer)
{
    MoneyUser *user = new MoneyUser(login, "", "", answer);
    return userDao->verifyUserAnswer(user);
}

bool DaoManager::updatePassword(QString userName,QString password){
    debugMsg;
    return userDao->setNewPassword(userName,password);
}

bool DaoManager::isValidUser(QString userName){
    debugMsg;
    return userDao->validUserLogin(userName);
}

QStringList DaoManager::defaultSecretQuestions()
{
    debugMsg;

    QStringList defaultSecretQuestions;
    defaultSecretQuestions << QString(tr("Qual o nome do seu primeiro animal de estimacao?"))
                           << QString(tr("Qual o nome do seu seriado preferido?"));

    debugMsg << defaultSecretQuestions;

    return defaultSecretQuestions;
}

/************************ CATEGORIES ******************************/

QList<QObject*> DaoManager::getAllCategories()
{
    debugMsg;
    // Just use the function provided by categorydao
    if (m_user) {
        m_listCategory =  getAllMoneyCategory(m_user->id(),
                                              m_spSearch,
                                              (m_resumeType == m_resumeOptions.at(0))? Money::type_in : Money::type_out);
        updateTotalByCategory();
        updateCategoryColors();

        if (!m_catEditEnabled) {
            QList<MoneyCategory*> catList = m_listCategory;
            foreach(MoneyCategory *cat, catList)
                if (cat->name() == "") {
                    debugMsg << "Deleting cat: " << cat;
                    deleteMoneyCategory(cat);
                    m_listCategory.removeOne(cat);
                }
        }

        return convert2ObjList(m_listCategory);
    }
    else return QList<QObject*>();
}

QList<QObject*> DaoManager::addCategory(QString category)
{
    debugMsg << category;
    MoneyCategory* newCat = new MoneyCategory(-1, category, QDateTime::currentDateTime().toString(), true);
    registerMoneyCategory(newCat, m_user->id());

    emit categoryListChanged();

    return convert2ObjList(m_listCategory);
}

void DaoManager::addNewEmptyCategory()
{
    bool emptyCategoryExists = false;
    foreach (MoneyCategory* cat, m_listCategory)
        if (cat && cat->name() == "")
            emptyCategoryExists = true;

    if (!emptyCategoryExists) {
        m_catEditEnabled = true;
        addCategory("");
        emit categoryListChanged();
    }
}

QList<QObject*> DaoManager::updateCategory(int index, QString name)
{
    if (index < 0 || index >= m_listCategory.length())
        return QList<QObject*>();
    m_catEditEnabled = false;
    MoneyCategory *cat = m_listCategory.at(index);
    MoneyCategory *catMoney = getCategory(cat, m_user->id());

    catMoney->setName(name);
    updateMoneyCategory(catMoney);

    emit categoryListChanged();

    return convert2ObjList(m_listCategory);
}

QList<QObject*> DaoManager::removeCategory(int index)
{
    debugMsg;

    MoneyCategory* notWantedCat = m_listCategory.at(index);
    MoneyCategory* cat2BeDeleted = getCategory(notWantedCat, m_user->id());

    if (!cat2BeDeleted)
        return QList<QObject*>();

    debugMsg << "Start remove from Money";
    m_moneyDao->removeCategoryFromMoney(cat2BeDeleted->name());
    debugMsg << "End remove from Money";

    deleteMoneyCategory(cat2BeDeleted);

    emit categoryListChanged();
    emit moneyListChanged();
    emit goalListChanged();

    return convert2ObjList(m_listCategory);
}

QString DaoManager::today()
{
    debugMsg;
    return QDate::currentDate().toString("dd/MM/yy");
}

QString DaoManager::getCurrentDateString(int typeDate, SpecificSearch *spSearch)
{
    if (typeDate == 0)
        return m_currentSelectedDate.toString("dd/MM/yyyy");
    else if (typeDate == 1)
        return m_currentSelectedDate.toString("MMMM, yyyy");
    else if (typeDate == 2)
        return m_currentSelectedDate.toString("yyyy");
    else if (spSearch)
        return spSearch->dateSince() + " - " + spSearch->dateUntil();
    return "";
}

QString DaoManager::currentPeriod()
{
    debugMsg;
    return getCurrentDateString(m_typeDate, m_spSearch);
}

void DaoManager::setCurrentPeriod(const QString &currentPeriod)
{
    for (int i = 0; i < m_listPeriods.length(); i++) {
        if (currentPeriod == m_listPeriods.at(i)) {
            setTypeDate(i);
            emit moneyListChanged();
            break;
        }
    }
}

QVariant DaoManager::listPeriods()
{
    return QVariant::fromValue(m_listPeriods);
}

QString DaoManager::currentDate()
{
    debugMsg;

    return getCurrentDateString(m_typeDate, m_spSearch);
}

QString DaoManager::nextDate()
{
    debugMsg;

    if (m_typeDate == 0)
        m_currentSelectedDate = m_currentSelectedDate.addDays(1);
    else if (m_typeDate == 1)
        m_currentSelectedDate = m_currentSelectedDate.addMonths(1);
    else if (m_typeDate == 2)
        m_currentSelectedDate = m_currentSelectedDate.addYears(1);
    else {
        int days = m_spSearch->getSinceDt().daysTo(m_spSearch->getUntilDt());
        m_spSearch->setSinceDt(m_spSearch->getSinceDt().addDays(days));
        m_spSearch->setUntilDt(m_spSearch->getUntilDt().addDays(days));
    }

    updatePeriod();
    updateDatePeriodList();

    emit currentPeriodChanged();
    emit moneyListChanged();

    return currentDate();
}

QString DaoManager::previousDate()
{
    debugMsg;

    if (m_typeDate == 0)
        m_currentSelectedDate = m_currentSelectedDate.addDays(-1);
    else if (m_typeDate == 1)
        m_currentSelectedDate = m_currentSelectedDate.addMonths(-1);
    else if (m_typeDate == 2)
        m_currentSelectedDate = m_currentSelectedDate.addYears(-1);
    else {
        int days = m_spSearch->getSinceDt().daysTo(m_spSearch->getUntilDt());
        m_spSearch->setSinceDt(m_spSearch->getSinceDt().addDays(-days));
        m_spSearch->setUntilDt(m_spSearch->getUntilDt().addDays(-days));
    }

    updatePeriod();
    updateDatePeriodList();

    emit currentPeriodChanged();
    emit moneyListChanged();

    return currentDate();
}

/************************ MONEY ******************************/
QList<QObject*> DaoManager::getAllMoneyRegister()
{
    debugMsg << "Type of Money: " << m_typeMoney;

    if (m_semaphorMoney == 0) {
        m_semaphorMoney = 1;
        m_listCategory = getAllMoneyCategory(m_user->id(), m_spSearch,
                                             m_resumeType == m_resumeOptions.at(0)? Money::type_in : Money::type_out);
        Money::money_types tt = Money::type_none;
        if ( m_typeMoney == 0)
            tt = Money::type_in;
        else if (m_typeMoney == 1)
            tt = Money::type_out;
        debugMsg << m_listCategory << m_spSearch;
        m_listCurrentSeenMoney = m_moneyDao->getAllMoney(m_listCategory, m_spSearch, tt);
        debugMsg << m_listCategory << m_spSearch;

        m_semaphorMoney = 0;

        emit totalIncomesChanged();
        emit totalOutcomesChanged();
        emit totalCashFlowChanged();

    }


    return convert2ObjList(m_listCurrentSeenMoney);
}

int DaoManager::addNewMoney()
{
    bool registeredMoney = m_moneyDao->registerMoney(m_money2BeEdited);
    debugMsg << registeredMoney;
    if (registeredMoney) {
        m_money2BeEdited = m_moneyDao->getMoney(m_money2BeEdited, m_listCategory);
        int id = m_money2BeEdited? m_money2BeEdited->id() : -1;
        m_money2BeEdited = new Money;
        emit moneyListChanged();
        emit newMoneyChanged();
        emit goalListChanged();
        return id;
    }
    return -1;
}

QList<QObject*> DaoManager::addMoney(int type, int indexCategory, QString description, double amount, QString date,int frequency, QString details)
{
    debugMsg << type << indexCategory << description << amount << date ;

    Money *newMoney = new Money();
    newMoney->setMoneyType(type);
    if (indexCategory >= 0 && indexCategory < m_listCategory.length()) {
        debugMsg << " Category index" << m_listCategory.at(indexCategory)->id() ;
        newMoney->setCategory(m_listCategory.at(indexCategory));
    }
    newMoney->setDescription(description);
    newMoney->setAmount(amount);
    newMoney->setMoneyDate(date);
    newMoney->setFrequency(frequency);
    newMoney->setDetails(details);
    m_moneyDao->registerMoney(newMoney);

    return convert2ObjList(m_listCurrentSeenMoney);
}

void DaoManager::databaseRandomPopulate()
{
    for(int i = 0; i < 5; i++)
        this->addMoney(qrand() % 2,
                       qrand() % getAllCategories().length(),
                       "VALOR", qrand() % 100,
                       QDate::currentDate().addDays((qrand() % 10)- 5).toString(FORMAT_DATE),1,"details");
    updateTotalByCategory();

    emit moneyListChanged();
}

QList<QObject*> DaoManager::updateMoneyRegister(int type, int indexCategory, QString description, double amount, QString date,int frequency, QString details)
{
    debugMsg << description;
    Money *money;
    money = m_moneyDao->getMoney(m_money2BeEdited, m_listCategory);
    if (money == 0)
        return QList<QObject*>();
    money->setMoneyType(type);
    if (indexCategory >= 0 && indexCategory < m_listCategory.length())
        money->setCategory(m_listCategory.at(indexCategory));
    money->setDescription(description);
    money->setAmount(amount);
    money->setMoneyDate(date);
    money->setFrequency(frequency);
    money->setDetails(details);
    m_moneyDao->updateMoney(money);

    return convert2ObjList(m_listCurrentSeenMoney);
}

bool DaoManager::updateMoney()
{
    debugMsg << m_money2BeEdited;
    bool editedMoney = false;
    if (m_money2BeEdited) {
        editedMoney = m_moneyDao->updateMoney(m_money2BeEdited);
        m_money2BeEdited = new Money;
        emit newMoneyChanged();
        emit moneyListChanged();
    }

    return editedMoney;
}

void DaoManager::removeMoneyRegister(int index)
{
    debugMsg << index;

    if (index < 0 || index > m_listCurrentSeenMoney.length())
        return;

    Money *money2BeDeleted = m_listCurrentSeenMoney.takeAt(index);
    money2BeDeleted = m_moneyDao->getMoney(money2BeDeleted, m_listCategory);

    if (money2BeDeleted)
        m_moneyDao->deleteMoney(money2BeDeleted);

    emit moneyListChanged();
    emit goalListChanged();
}

void DaoManager::removeMassiveMoneyRegisters(QVariant list)
{
    QVariantList indexList = list.toList();
    debugMsg << indexList;
    foreach(QVariant varIndex, indexList) {
        int index = varIndex.toInt();
        debugMsg << index;
        if (index < 0 || index > m_listCurrentSeenMoney.length())
            return;

        Money *money2BeDeleted = m_listCurrentSeenMoney.at(index);
        money2BeDeleted = m_moneyDao->getMoney(money2BeDeleted, m_listCategory);

        if (money2BeDeleted)
            m_moneyDao->deleteMoney(money2BeDeleted);
    }

    emit moneyListChanged();
}

QList<QObject*> DaoManager::getAllTypeMoney()
{
    debugMsg;

    QStringList typeList;
    typeList << "Todos" << "Entrada" << "Saida";
    QList<MoneyCategory*> listTypes;
    for (int i = 0; i < typeList.length(); i++) {
        listTypes.append(new MoneyCategory(i, typeList.at(i), "", false));
    }
    return convert2ObjList(listTypes);
}

void DaoManager::setMoney2BeEdited(int index)
{
    debugMsg << "Index in CashFlowList " << index;
    if (index >= 0 && index < m_listCurrentSeenMoney.length()) {
        m_money2BeEdited = m_listCurrentSeenMoney.at(index);
        debugMsg << m_money2BeEdited->description();
        debugMsg << (m_money2BeEdited->category() ?
                         m_money2BeEdited->category()->name() :
                         "Sem categoria");
//        m_money2BeEdited->setCategoryByName(m_money2BeEdited->category() ?
//                                                m_money2BeEdited->category()->name() :
//                                                "Sem categoria");

        emit newMoneyChanged();
    }

}

Money* DaoManager::money2BeEdited()
{
    debugMsg << m_money2BeEdited;
    return m_money2BeEdited;
}


qreal DaoManager::totalCashFlow()
{

    qreal total = 0;
    foreach(Money* money, m_listCurrentSeenMoney)
        total = total + (2 * (1 - money->moneyType()) - 1) * money->amount();

    debugMsg << total;

    return total;
}


qreal DaoManager::totalIncomes()
{
    qreal total = 0;
    foreach(Money* money, m_listCurrentSeenMoney)
        if (money->typeMoney() == Money::type_in)
            total = total + money->amount();

    debugMsg << total;

    return total;
}


qreal DaoManager::allTimeSale()
{
    qreal sumAmounts = 0;
    m_allMoney = m_moneyDao->getAllMoney(m_listCategory);

    foreach (Money* money, m_allMoney) {
        sumAmounts += (money->typeMoney()) ? -money->amount() : money->amount();
    }

    return sumAmounts;

}


qreal DaoManager::totalOutcomes()
{
    qreal total = 0;
    foreach(Money* money, m_listCurrentSeenMoney)
        if (money->typeMoney() == Money::type_out)
            total = total + money->amount();

    debugMsg << total;

    return total;
}

void DaoManager::updateTotalByCategory()
{
    //debugMsg;

    float total = 0;
    foreach (MoneyCategory* cat, m_listCategory) {
        total = total + cat->totalExpenses();
    }

    foreach (MoneyCategory* cat, m_listCategory) {
        if (total != 0)
            cat->setPercentage( (floor((cat->totalExpenses()/total)*10000))/100 );
        else  cat->setPercentage(0);

    }

}

void DaoManager::setMoneyType(int typeMoney)
{
    debugMsg << typeMoney;
    if (typeMoney != m_typeMoney) {
        m_typeMoney = typeMoney;
    }
}

void DaoManager::setTypeDate(int typeDate)
{
    debugMsg << typeDate;
    if (typeDate != m_typeDate) {
        m_typeDate = typeDate;
        updatePeriod();
        emit currentPeriodChanged();
    } else {
        m_typeDate = typeDate;
    }
}


void DaoManager::setSpSearch(SpecificSearch *sp)
{
    debugMsg << sp->category();
    //    if (sp->category())
    //        debugMsg << sp->category()->name();
    m_spSearch = sp;
    if (m_spSearch) {
        updateDatePeriodList();
    }
}

SpecificSearch* DaoManager::spSearch()
{
    debugMsg;
    return m_spSearch;
}

void DaoManager::updatePeriod()
{
    debugMsg;
    m_spSearch = updateSpSearch(m_typeDate, m_spSearch);
    emit currentPeriodChanged();
}


SpecificSearch* DaoManager::updateSpSearch(int typeDate, SpecificSearch *spSearch)
{
    debugMsg;

    if (!spSearch)
        spSearch = new SpecificSearch();
    if (typeDate == 0) {
        spSearch->setActiveSinceDt(true);
        spSearch->setActiveUntilDt(true);

        spSearch->setSinceDt(m_currentSelectedDate);
        spSearch->setUntilDt(m_currentSelectedDate);
    } else if (typeDate == 1) {

        spSearch->setActiveSinceDt(true);
        spSearch->setActiveUntilDt(true);

        QDate beginDate, endDate;
        beginDate.setDate(m_currentSelectedDate.year(), m_currentSelectedDate.month(), 1);
        endDate.setDate(m_currentSelectedDate.year(), m_currentSelectedDate.month(), m_currentSelectedDate.daysInMonth());

        spSearch->setSinceDt(beginDate);
        spSearch->setUntilDt(endDate);
    } else if (typeDate == 2) {

        spSearch->setActiveSinceDt(true);
        spSearch->setActiveUntilDt(true);

        QDate beginDate, endDate;
        beginDate.setDate(m_currentSelectedDate.year(), 1, 1);
        endDate.setDate(m_currentSelectedDate.year(), 12, 31);

        spSearch->setSinceDt(beginDate);
        spSearch->setUntilDt(endDate);
    }

    return spSearch;
}


QList<QObject*> DaoManager::getAllDatePeriods()
{
    debugMsg;

    return convert2ObjList(m_listDatePeriods);
}

void DaoManager::updateDatePeriodList()
{
    debugMsg;

    SpecificSearch* sp = 0;

    m_listPeriods.clear();

    for (int typeDate = 0; typeDate <= 2; typeDate++) {
        if (typeDate != 3)
            sp = updateSpSearch(typeDate, sp);
        else {
            sp = new SpecificSearch;
            sp->setDateSince(m_spSearch->dateSince());
            sp->setDateUntil(m_spSearch->dateUntil());
        }
        m_listPeriods << getCurrentDateString(typeDate, sp);
    }

    m_listDatePeriods.clear();
    for (int i = 0; i < m_listPeriods.length(); i++) {
        m_listDatePeriods.append(new MoneyCategory(i, m_listPeriods.at(i), "", false));
    }
}

/** About CSV **/
QString DaoManager::exportCSVEntities()
{
    QString exportData;

    Money::money_types tt = Money::type_none;
    SpecificSearch *sp = new SpecificSearch;
    sp->setSinceDt(m_dtInitialExport);
    sp->setUntilDt(m_dtFinalExport);
    sp->setActiveSinceDt(true);
    sp->setActiveUntilDt(true);
    m_listCategory = getAllMoneyCategory(m_user->id(),sp,tt);
    QList<Money*> listMoney2BeExported = m_moneyDao->getAllMoney(m_listCategory, m_spSearch, tt);

    CsvManager *csvManager = new CsvManager(this);
    QStringList fields, fieldsName;
    fields << "strId" << "moneyType" << "strCategory" << "description" << "strAmount"
           << "moneyDate" << "details" << "frequencyName" << "strDtCreation"
           << "strDtLastModification";
    fieldsName << tr("Id") << tr("Tipo") << tr("Categoria") << tr("Descricao") << tr("Valor")
               << tr("Data") << tr("Detalhes") << tr("frequencia") << tr("Data de Criaçao")
               << tr("Data da ultima modificaçao");
    exportData = csvManager->createCsvFromEntities(convert2EntityList(listMoney2BeExported), fields, fieldsName);

    return exportData;
}


void DaoManager::exportCsvFile(QString filepath)
{
    debugMsg << filepath;
    QFile file(filepath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;

    QTextStream out(&file);

    CsvManager *csvManager = new CsvManager;
    QStringList fields, fieldsName;
    fields << "strId" << "moneyType" << "strCategory" << "description" << "strAmount"
           << "moneyDate" << "details" << "frequencyName" << "strDtCreation"
           << "strDtLastModification";
    fieldsName << tr("Id") << tr("Tipo") << tr("Categoria") << tr("Descricao") << tr("Valor")
               << tr("Data") << tr("Detalhes") << tr("frequencia") << tr("Data de Criaçao")
               << tr("Data da ultima modificaçao");
    out << csvManager->createCsvFromEntities(convert2EntityList(m_moneyDao->getAllMoney(m_listCategory)),
                                             fields, fieldsName);

    file.close();
}

void DaoManager::importCsvContents(const QString &contents, const QString &filename)
{
    if (m_semaphorCsvImporter == 0) {
        m_semaphorCsvImporter = 1;

        debugMsg;

        CsvManager *csvManager = new CsvManager;
        CsvManager::ImportPolicy policy = CsvManager::KeepAllRegisters;
        csvManager->setImportPolicy(policy);
        QList<Entity*> listCsvEntities = csvManager->loadEntitiesFromCsvContents(contents);
        debugMsg << "Got " << listCsvEntities.length() << " Entities";
        m_money2Sync = new MoneyCsvResume;
        m_money2Sync->setEntityList(convert2ObjList(listCsvEntities));
        qreal total = 0;
        qreal totalIn = 0;
        qreal totalOut = 0;
        foreach (QObject* entity, m_money2Sync->entityList()) {
            if (!entity->property("amount").isNull() &&
                    !entity->property("moneyType").isNull()) {
                qreal amount = entity->property("amount").toReal();
                int moneyType = entity->property("moneyType").toInt();

                if (moneyType == 0)
                    totalIn = totalIn + amount;
                else totalOut = totalOut + amount;
                total = total + (moneyType == 0? 1 : -1) * amount;
            }
        }
        m_money2Sync->setFilename(filename);
        m_money2Sync->setTotal(total);
        m_money2Sync->setTotalIn(totalIn);
        m_money2Sync->setTotalOut(totalOut);
        emit money2SyncChanged();
    }
}

void DaoManager::doSync()
{
    if (m_money2Sync) {
        CsvManager *csvManager = new CsvManager(this);
        connect(csvManager, SIGNAL(deleteEntities(QList<Entity*>)),
                this, SLOT(getMoney2BeDeleted(QList<Entity*>)));
        connect(csvManager, SIGNAL(persistEntities(QList<Entity*>)),
                this, SLOT(getMoney2BePersisted(QList<Entity*>)));
        connect(csvManager, SIGNAL(updateEntities(QList<Entity*>)),
                this, SLOT(getMoney2BeUpdated(QList<Entity*>)));
        connect(csvManager, SIGNAL(conflitsEntities(QList<Entity*>,QList<Entity*>)),
                this, SLOT(getMoney2BeGraphiclyChoosed(QList<Entity*>,QList<Entity*>)));
        CsvManager::ImportPolicy policy = CsvManager::KeepAllRegisters;
        csvManager->setImportPolicy(policy);

        QList<Entity*> listDBEntities = Money::convert2Entities(m_moneyDao->getAllMoney(m_listCategory));
        csvManager->syncEntitiesList(convert2EntityList(m_money2Sync->entityList()), listDBEntities);
        m_semaphorCsvImporter = 0;
        setDropboxSyncLv(3);
    }
}

void DaoManager::importCsvFile(QString filepath, QVariantList listPolicyChecks)
{
    if (m_semaphorCsvImporter == 0) {
        m_semaphorCsvImporter = 1;
        if (listPolicyChecks.length() < 8)
            return;
        debugMsg << filepath << listPolicyChecks;
        CsvManager *csvManager = new CsvManager;
        connect(csvManager, SIGNAL(deleteEntities(QList<Entity*>)),
                this, SLOT(getMoney2BeDeleted(QList<Entity*>)));
        connect(csvManager, SIGNAL(persistEntities(QList<Entity*>)),
                this, SLOT(getMoney2BePersisted(QList<Entity*>)));
        connect(csvManager, SIGNAL(updateEntities(QList<Entity*>)),
                this, SLOT(getMoney2BeUpdated(QList<Entity*>)));
        connect(csvManager, SIGNAL(conflitsEntities(QList<Entity*>,QList<Entity*>)),
                this, SLOT(getMoney2BeGraphiclyChoosed(QList<Entity*>,QList<Entity*>)));
        CsvManager::ImportPolicy policy = CsvManager::None;
        if (listPolicyChecks.at(0).toBool()) {
            policy = policy | CsvManager::KeepAllRegisters;
        }
        if (listPolicyChecks.at(1).toBool()) {
            policy = policy | CsvManager::KeepOnlyCSVRegisters;
        }
        if (listPolicyChecks.at(2).toBool()) {
            policy = policy | CsvManager::KeepOnlyDBRegisters;
        }
        if (listPolicyChecks.at(3).toBool()) {
            policy = policy | CsvManager::KeepCSVVersion;
        }
        if (listPolicyChecks.at(4).toBool()) {
            policy = policy | CsvManager::KeepDBVersion;
        }
        if (listPolicyChecks.at(5).toBool()) {
            policy = policy | CsvManager::KeepLastVersion;
        }
        if (listPolicyChecks.at(6).toBool()) {
            policy = policy | CsvManager::GraphiclyChoose;
        }
        if (listPolicyChecks.at(7).toBool()) {
            policy = policy | CsvManager::GraphiclyAnalyze;
        }
        csvManager->setImportPolicy(policy);
        QList<Entity*> listCsvEntities = csvManager->loadEntitiesFromCsv(filepath);
        debugMsg << "Got " << listCsvEntities.length() << " Entities";
        QList<Entity*> listDBEntities = Money::convert2Entities(m_moneyDao->getAllMoney(m_listCategory));
        csvManager->syncEntitiesList(listCsvEntities, listDBEntities);
        m_semaphorCsvImporter = 0;
    }
}

bool DaoManager::getNextSyncEntity()
{
    debugMsg << m_listGraphiclyChoosedDbEntity.length();
    if (m_syncEntityCount < m_listGraphiclyChoosedDbEntity.length()) {
        emit syncChoiceBetweenEntities(m_listGraphiclyChoosedDbEntity.at(m_syncEntityCount),
                                       new Money(*m_listGraphiclyChoosedCsvEntity.at(m_syncEntityCount)));
        m_syncEntityCount++;
    }
    else return false;

    return true;
}

bool DaoManager::deleteMoneyEntity(QObject *object)
{
    Money *money = (Money*) object;
    return m_moneyDao->deleteMoney(money);
}

bool DaoManager::persistMoneyEntity(QObject *object)
{
    Money *money = (Money*) object;
    return m_moneyDao->registerMoney(money);
}

void DaoManager::getMoney2BeGraphiclyChoosed(QList<Entity *> listDbEntities,
                                             QList<Entity *> listCsvEntities)
{
    debugMsg << listDbEntities.length() << listCsvEntities.length();
    m_listGraphiclyChoosedDbEntity << listDbEntities;
    m_listGraphiclyChoosedCsvEntity << listCsvEntities;
    m_syncEntityCount = 0;
    emit startGraphicEntitySync();
}

void DaoManager::getMoney2BeDeleted(QList<Entity *> listEntities)
{
    debugMsg << "Got " << listEntities.length() << " Entities";
    QList<Money*> listMoneys = Money::convertFromEntities(listEntities);
    foreach (Money* money, listMoneys)
        m_moneyDao->deleteMoney(money);
    emit moneyListChanged();
    setDropboxSyncLv(dropboxSyncLv() + 1);
}

void DaoManager::getMoney2BeUpdated(QList<Entity *> listEntities)
{
    debugMsg << "Got " << listEntities.length() << " Entities";
    QList<Money*> listMoneys = Money::convertFromEntities(listEntities);
    foreach (Money* money, listMoneys)
        m_moneyDao->updateMoney(money);
    emit moneyListChanged();
    setDropboxSyncLv(dropboxSyncLv() + 1);
}

void DaoManager::getMoney2BePersisted(QList<Entity *> listEntities)
{
    debugMsg << "Got " << listEntities.length() << " Entities";
    QList<Money*> listMoneys = Money::convertFromEntities(listEntities);
    foreach (Money* money, listMoneys)
        m_moneyDao->registerMoney(money);
    emit moneyListChanged();
    setDropboxSyncLv(dropboxSyncLv() + 1);
}
/************************ GOALS ******************************/

QList<QObject*> DaoManager::getAllGoals()
{
    debugMsg << m_user->id();
    m_listGoal = m_goalDao->getAll();
    foreach(MoneyGoal *goal, m_listGoal) {
        debugMsg << goal->id();
    }

    return convert2ObjList(m_listGoal);
}

QList<QObject*> DaoManager::addGoal(int type, float target, QString initialDate, QString finalDate, QString description)
{
    debugMsg << type << target << initialDate << finalDate << description;
    MoneyGoal *newGoal = new MoneyGoal();
    newGoal->setGoalType(type);
    newGoal->setTarget(target);
    newGoal->setInitialDate(initialDate);
    newGoal->setFinalDate(finalDate);
    newGoal->setDescription(description);
    m_goalDao->persist(newGoal);

    return convert2ObjList(m_listGoal);
}


void DaoManager::removeGoal(int index, bool massive)
{
    debugMsg << index;
    if (index < 0 || index > m_listGoal.length())
        return;

    MoneyGoal *goal = m_listGoal.at(index);
    m_goalDao->remove(goal);

    if (!massive)
        emit goalListChanged();
}

QList<QObject*> DaoManager::updateGoal(int type, float target, QString initialDate, QString finalDate, QString description, int index)
{
    debugMsg << type << target << initialDate << finalDate << description;
    MoneyGoal *goal = m_listGoal.at(index);
    if (goal) {
        goal->setGoalType(type);
        goal->setTarget(target);
        goal->setInitialDate(initialDate);
        goal->setFinalDate(finalDate);
        goal->setDescription(description);
        m_goalDao->update(goal);
    }
    return convert2ObjList(m_listGoal);
}

QList<QObject*> DaoManager::getAllTypeGoal()
{
    debugMsg;

    QStringList typeList;
    typeList << "Total" << "Entrada" << "Saida";
    QList<MoneyCategory*> listTypes;
    for (int i = 0; i < typeList.length(); i++) {
        listTypes.append(new MoneyCategory(i, typeList.at(i), "", false));
    }
    return convert2ObjList(listTypes);
}

bool DaoManager::addNewGoal()
{
    if (m_goalDao->persist(m_newGoal)) {
        m_newGoal = new MoneyGoal;

        emit newGoalChanged();
        emit goalListChanged();

        return true;
    }

    return false;
}

void DaoManager::setGoal2BeEdited(const int &index)
{
    debugMsg << index;
    if (index >= 0 && index < m_listGoal.length()) {

        m_newGoal = m_listGoal.at(index);
        debugMsg << m_newGoal;

        emit newGoalChanged();
    }
}

bool DaoManager::updateEditedGoal()
{
    bool editedGoal = m_goalDao->update(m_newGoal);
    if (editedGoal) {
        m_newGoal = new MoneyGoal;

        emit newGoalChanged();
        emit goalListChanged();
    }

    return editedGoal;
}

/*************************************************************/

void DaoManager::updateCategoryColors()
{
    debugMsg << "START";

    for(int i = 0; i < m_listCategory.length(); i++) {
        MoneyCategory* cat = m_listCategory.at(i);
//        if (i < m_colorList.length() && i < m_colorNameList.length()) {
////            cat->setColor(m_colorNameList.at(i));
//            cat->setColorRgb(m_colorList.at(i));
//        } else {
//            int j = qrand() % m_colorNameList.length();
////            cat->setColor(m_colorNameList.at(j));
//              cat->setColor( QColor(0,173,239));
//              cat->setColorRgb(m_colorList.at(j));
//        }
        int d = i*256/(3*m_listCategory.length());
        QColor color;
        if(i%3==0)
            color.setRgb(2*d,3*d,255-d);
        else if(i%3==1)
             color.setRgb(3*d,255-d,3*d);
        else
             color.setRgb(255-d,3*d,2*d);

        cat->setColorRgb(color);
        cat->setColor(cat->colorRgb().name());
        m_listCategory.replace(i, cat);
    }

    debugMsg << "END";
}
