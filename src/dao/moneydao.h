#ifndef MONEYDAO_H
#define MONEYDAO_H

#include <QString>
#include <QList>
#include "money.h"


class SpecificSearch;

class MoneyDao
{
public:
    MoneyDao(const int &idUser) { m_idUser = idUser; }
    Money* getMoney(Money* incomplete_money, QList<MoneyCategory*> listAllMoneyCategory);

    QList<Money*> getAllMoney(QList<MoneyCategory*> listAllMoneyCategory,
                              SpecificSearch* ss = 0,
                              Money::money_types typeMoney = Money::type_none);

    bool deleteMoney(Money* money);

    bool updateMoney(Money* money);

    bool registerMoney(Money* money);

    bool removeCategoryFromMoney(const QString &category);

    void setIdUser(int idUser) { m_idUser = idUser; }

private:
    bool registerCategoryByMoney(QString categoryName, int id_money);
    bool registerCategoryByMoney(int id_category, int id_money);
    bool registerAllCategoryByMoney(Money* money);

    int m_idUser;
};

#endif // MONEYDAO_H
