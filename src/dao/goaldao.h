#ifndef GOALDAO_H
#define GOALDAO_H

#include <QString>
#include <QList>
#include "../entity/goal.h"

#include "entitydao.h"

MoneyGoal::goal_types selectTypeGoal(int typeGoal);

int selectTypeGoal(MoneyGoal::goal_types typeGoal);

bool updateMoneyGoal(MoneyGoal* goal);

MoneyGoal* getGoal(MoneyGoal* incomplete_goal, int id_user);

class GoalDao : public EntityDao
{
public:
    GoalDao(int userId, QObject *parent = 0);

    bool persist(MoneyGoal *goal);
    bool remove(MoneyGoal *goal);
    bool update(MoneyGoal *goal);

    QList<MoneyGoal*> getAll();
private:
    int m_userId;
};

#endif // GOALDAO_H
