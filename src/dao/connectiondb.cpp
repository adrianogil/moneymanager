
#ifndef CONNECTIONDB_H
#define CONNECTIONDB_H

#include "connectionDB.h"
#include <QMessageBox>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QDebug>

bool createConnection(const QString newDatabase)
{
    qDebug() << "Trying to create connection";
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(newDatabase);
    if (!db.open()) {
        QMessageBox::critical(0, "ERRO!",
                        "Nao foi possivel estabelecer uma ligacao com a base de dados.\n"
                     ""
                     "Clique em Cancel para sair.", QMessageBox::Cancel);
        return false;
    }
    return true;
}

#endif // CONNECTIONDB_H
