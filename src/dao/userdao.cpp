#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtCore/QVariant>

#include <QtCore/QDateTime>

#include "userdao.h"
#include "categorydao.h"

#include "appdebug.h"
#include <QTextCodec>

bool UserDao::userLogin(MoneyUser *user)
{
    if(!user)
        return false;

    QString strQuery = " SELECT id_user as id_user FROM user           "
                       " WHERE login = :login and password = :password ";
    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":login", user->login().toLower());
    query.bindValue(":password", user->password());

    query.exec();

    return (query.next());
}

bool UserDao::validUserLogin(QString login)
{
    QString strQuery = " SELECT id_user as id_user FROM user           "
                       " WHERE login = :login ";
    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":login", login.toLower());

    query.exec();

    return (query.next());
}

bool UserDao::verifyUserAnswer(MoneyUser *verify) {
    if (!verify)
        return false;

    QString srtQuery = " SELECT id_user as id_user FROM User                                "
                       " WHERE login = :login and answer = :answer ";
    QSqlQuery query;
    query.prepare(srtQuery);
    query.bindValue(":login", verify->login().toLower());
    query.bindValue(":answer", verify->answer());

    query.exec();

    return (query.next());
}

bool UserDao::verifyUserLogin(QString login)
{
    QString srtQuery = " SELECT EXISTS(SELECT id_user FROM User                                "
                       " WHERE login = :login) as exist";
    QSqlQuery query;
    query.prepare(srtQuery);
    query.bindValue(":login", login.toLower());
    query.exec();
    query.next();

    bool exists = query.value(query.record().indexOf("exist")).toBool();
    return exists;

}

QString UserDao::getUserQuestion(QString login)
{
    QString question = "";

      QString srtQuery = " SELECT question as question FROM user                                "
                       " WHERE login = :login ";
    QSqlQuery query;
    query.prepare(srtQuery);
    query.bindValue(":login", login);

    query.exec();

    if (query.next()) {
        question = query.value(query.record().indexOf("question")).toString();
    }

    return question;
}

bool UserDao::updateRegisterUser(MoneyUser* user)
{
    if(!user)
        return false;

    debugMsg;
    QString strQuery = " UPDATE user SET password = :password, question = :question "
                       " WHERE id_user = :id_user";

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue("id_user", user->id());
    query.bindValue(":password", user->password());
    query.bindValue(":question", user->question());

    return query.exec();
}

bool UserDao::setNewPassword(QString userName,QString password)
{
    debugMsg;
    QString strQuery = " UPDATE user SET password = '"+ password +"'" +
            " WHERE login = '"+ userName.toLower() + "'";

    QSqlQuery query;
    query.prepare(strQuery);

    return query.exec();
}


bool createTableUser()
{
    QString strQuery = "create table user ( "
                                "id_user INTEGER,      "
                                "login VARCHAR(50),    "
                                "password VARCHAR(255), "
                                "question VARCHAR(300),  "
                                "answer VARCHAR(100),   "
                                "dtCreation datetime,                  "
                                "dtLastModification datetime,          "
                                "primary key(id_user)  "
                       ")";
    QSqlQuery query;
    query.prepare(strQuery);

    return query.exec();
}

bool createAllUserTables()
{
    QString strQuery1 = "create table money (                          "
                                "id_money INTEGER,                     "
                                "id_user INTEGER,                      "
                                "amount DECIMAL(13,2),                 "
                                "dt_transaction Date,                  "
                                "description VARCHAR(255),             "
                                "timestamp VARCHAR(255),               "
                                "frequency INTEGER,                    "
                                "details   VARCHAR(255),                "
                                "type_money INTEGER(1),                "
                                "dtCreation datetime,                  "
                                "dtLastModification datetime,          "
                                "primary key (id_money),               "
                                "foreign key (id_user) references      "
                                "user(id_user)                         "
                                "ON UPDATE CASCADE ON DELETE RESTRICT  "
                        ")";

    QString strQuery2 = "create table category (       "
                                "id_category INTEGER,      "
                                "id_user INTEGER,          "
                                "name VARCHAR(50),         "
                                "description VARCHAR(255), "
                                "dtCreation datetime,                  "
                                "dtLastModification datetime,          "
                                "primary key(id_category),             "
                                "foreign key (id_user) references      "
                                "user(id_user)                         "
                                "ON UPDATE CASCADE ON DELETE RESTRICT  "
                        ")";

    QString strQuery3 = " create table money_category (   "
                            "id_money INTEGER NOT NULL, "
                            "id_category INTEGER NOT NULL, "
                            "dtCreation datetime,                  "
                            "dtLastModification datetime,          "
                            "foreign key (id_money) references money(id_money),"
                            "foreign key (id_category) references category(id_category))";

    QString strQuery4 = " create table goal (                                    "
                                " id_goal INTEGER,                               "
                                " id_user INTEGER,                               "
                                " description VARCHAR(255),                      "
                                " dt_initial Date,                               "
                                " dt_final Date,                                 "
                                " target_value DECIMAL(13,2),                    "
                                " type_goal INTEGER(2),                          "
                                " dtCreation datetime,                            "
                                " dtLastModification datetime,                    "
                                " primary key (id_goal),                         "
                                " foreign key (id_user) references user(id_user) "
                                " ON UPDATE CASCADE ON DELETE RESTRICT           "
                        ")";

    debugMsg << strQuery1;
    debugMsg << strQuery2;
    debugMsg << strQuery3;
    debugMsg << strQuery4;

    QSqlQuery query1;
    query1.prepare(strQuery1);

    QSqlQuery query2;
    query2.prepare(strQuery2);

    QSqlQuery query3;
    query3.prepare(strQuery3);

    QSqlQuery query4;
    query4.prepare(strQuery4);

    return query1.exec() && query2.exec() && query3.exec() && query4.exec();

}

void UserDao::createSampleData(MoneyUser *user)
{
    debugMsg;

    QTextCodec *linuxCodec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForTr(linuxCodec);
    QTextCodec::setCodecForCStrings(linuxCodec);
    QTextCodec::setCodecForLocale(linuxCodec);

    QStringList categories;
    categories << "Sem categoria" << "Alimentação" << "Educação" << "Impostos" << "Investimentos"
               << "Lazer" << "Moradia" << "Saúde" << "Transporte" << "Outros";

    foreach(QString cat_name, categories) {
       MoneyCategory *cat = new MoneyCategory(0, cat_name, "",true);
       registerMoneyCategory(cat, user->id());
}

}

bool UserDao::registerUser(MoneyUser *user)
{
    debugMsg;

    bool sucess = false;

    if (user && !userLogin(user)) {

    QString strQuery = "INSERT INTO user (login, password, dtCreation, dtLastModification, question, answer)"
                       " values (:login, :password,:dtCreation, :dtLastModification, :question, :answer)";

        QSqlQuery query;
        query.prepare(strQuery);
        query.bindValue(":login", user->login().toLower());
        query.bindValue(":password",user->password());
        query.bindValue(":question", user->question());
        query.bindValue(":answer", user->answer());
        query.bindValue(":dtCreation", QDateTime::currentDateTime());
        query.bindValue(":dtLastModification", QDateTime::currentDateTime());

        sucess = query.exec();

        if (sucess) {
            user = getUser(user);
            if (user)
                createSampleData(user);
        }
    }

    return sucess;
}

MoneyUser* UserDao::createStandardUser()
{
    debugMsg;
    MoneyUser* admin_user  = new MoneyUser(STANDARD_LOGIN, STANDARD_PASSWORD, "", "");

    if (!userLogin(admin_user)) {
        createTableUser();
        createAllUserTables();

        registerUser(admin_user);

        admin_user = getUser(admin_user);
        createSampleData(admin_user);
    }

    return admin_user;
}

MoneyUser* UserDao::getUser(MoneyUser* incomplete_user) {
    if(!incomplete_user)
        return 0;

    debugMsg;

    QString strQuery = "SELECT id_user, login, password, dtCreation, dtLastModification, question, answer "
            "as id_user, login, password, dtCreation, dtLastModification, question, answer FROM user "
                       " WHERE login = :login and password = :password";
    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":login", incomplete_user->login().toLower());
    query.bindValue(":password", incomplete_user->password());

    query.exec();

    MoneyUser* user = new MoneyUser();

    if (query.next()) {
        user->setId(query.value(query.record().indexOf("id_user")).toInt());
        user->setLogin(query.value(query.record().indexOf("login")).toString());
        user->setPassword(query.value(query.record().indexOf("password")).toString());
	user->setAnswer(query.value(query.record().indexOf("answer")).toString());
	user->setAnswer(query.value(query.record().indexOf("question")).toString());
        user->setDtCreation(query.value(query.record().indexOf("dtCreation")).toDateTime());
        user->setDtLastModification(query.value(query.record().indexOf("dtLastModification")).toDateTime());
    } else return 0;

    return user;
}
