#ifndef ENTITYDAO_H
#define ENTITYDAO_H

#include <QObject>
#include <entity.h>

class QSqlRecord;
struct QueryCondition;

class EntityDao : public QObject
{
    Q_OBJECT
public:
    explicit EntityDao(QObject *parent = 0);

    bool persist(Entity *entity);
    bool remove(Entity *entity);
    bool update(Entity *entity);

    //!
    /*! Get all entities according query conditions
      */
    QList<Entity*> getAll();

    QStringList dbParameters() const { return m_dbParameters; }
    QStringList objParameters() const { return m_objParameters; }

    void setDbParameters(const QStringList &dbParameters) {
        m_dbParameters.clear();
        m_dbParameters.append(dbParameters);
    }
    void setObjParameters(const QStringList &objParameters) {
        m_objParameters.clear();
        m_objParameters.append(objParameters);
    }
    void setTableName(const QString &tableName) { m_tableName = tableName; }

    void resetQueryConditions();
    void appendQueryConditions(const QString &condition, const QStringList &placeHolders, const QVariantList &values);

    void setPrimaryKey(const QString &primaryKey) { m_idPrimaryKey = primaryKey; }
    QString primaryKey() const { return m_idPrimaryKey; }

signals:
    
public slots:

protected:
    Entity* getEntity(Entity *entity);
    Entity* buildEntity(QSqlRecord *record);

private:
    QString m_tableName;
    QString m_idPrimaryKey;
    QStringList m_dbParameters;
    QStringList m_objParameters;

    QList<QueryCondition*> m_queryConditions;
};

#endif // ENTITYDAO_H
