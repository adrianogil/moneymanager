#ifndef CONNECTIONDB_H
#define CONNECTIONDB_H

class QString;

bool createConnection(const QString newDatabase);

#endif // CONNECTIONDB_H
