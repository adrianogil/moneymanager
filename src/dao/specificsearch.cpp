﻿#include <QtGui>

#include "specificsearch.h"
#include "money.h"
#include <QtSql/QSqlQuery>

QString addAnd2Conditions(QString conditions) {
        if (conditions.length() < 2 || conditions.isEmpty())
           return conditions;
        return conditions + " and ";
}

int SpecificSearch::getNumberActiveFilters() {
        int numberFilters = 0;

        if (isDescription)
            numberFilters++;
        if (isSinceDt)
            numberFilters++;
        if (isUntilDt)
            numberFilters++;
        if (isListMoneyCategory)
            numberFilters++;
        if (isLessValue)
            numberFilters++;
        if (isGreaterValue)
            numberFilters++;

        return numberFilters;
    }

QString SpecificSearch::getConditions() {

    QString conditions = "";

    if (isDescription)
        conditions = conditions + " money.description LIKE :description ";

    if (isSinceDt) {
        conditions = addAnd2Conditions(conditions);
        conditions = conditions + " money.dt_transaction >= :dt_since ";
    }
    if (isUntilDt) {
        conditions = addAnd2Conditions(conditions);
        conditions = conditions + " money.dt_transaction <= :dt_until ";
    }
    if (isGreaterValue) {
        conditions = addAnd2Conditions(conditions);
        conditions = conditions + " money.amount >= :greater_value ";
    }
    if (isLessValue) {
        conditions = addAnd2Conditions(conditions);
        conditions = conditions + " money.amount <= :less_value ";
    }
    if (isListMoneyCategory and listMoneyCategory.length() > 0) {
        conditions = addAnd2Conditions(conditions);
        int count = 0;
        conditions = conditions + " cat.id_money = money.id_money AND ";
        foreach(MoneyCategory* category, listMoneyCategory) {
            Q_UNUSED(category);
            conditions = conditions + " cat.id_category = :id_cat" + QString::number(count);
            count++;
        }
    }

    return conditions;

}

QSqlQuery SpecificSearch::prepareQuery(QSqlQuery query) {
        if (getNumberActiveFilters() == 0)
            return query;


        if (isListMoneyCategory) {
            int count = 0;
            foreach (MoneyCategory* cat , listMoneyCategory) {
                //qDebug() << __PRETTY_FUNCTION__ << "Binding " << ":id_cat" << QString::number(count) << cat->id();
                query.bindValue(":id_cat" + QString::number(count), cat->id());
                count++;
            }
        }
        if (isSinceDt) {
            //qDebug() << __PRETTY_FUNCTION__ << "Binding SinceDt" << sinceDt;
            query.bindValue(":dt_since", sinceDt);
        }
        if (isUntilDt) {
            //qDebug() << __PRETTY_FUNCTION__ << "Binding UntilDt" << untilDt;
            query.bindValue(":dt_until", untilDt);
        }
        if (isDescription) {
            //qDebug() << __PRETTY_FUNCTION__ << "Binding Description" << m_description;
            query.bindValue(":description", "%" + m_description + "%");
        }

        if (isLessValue) {
            //qDebug() << __PRETTY_FUNCTION__ << "Binding LessValue" << m_lessValue;
            query.bindValue(":less_value", m_lessValue);
        }
        if (isGreaterValue) {
            //qDebug() << __PRETTY_FUNCTION__ << "Binding GreaterValue" << m_greaterValue;
            query.bindValue(":greater_value", m_greaterValue);
        }

        return query;
    }

QList<Money*> SpecificSearch::filterMoneyList(QList<Money*> listAllMoney)
{
    qDebug() << __PRETTY_FUNCTION__ << listAllMoney.size();

    QList<Money*> listMoney;
    QList<Money*> listCurrentMoney;
    listCurrentMoney.clear();
    listCurrentMoney.append(listAllMoney);

    if (getNumberActiveFilters() == 0)
        return listMoney;


    if (isListMoneyCategory) {
        bool foundCat = true;
        listMoney.clear();
        listMoney.append(listCurrentMoney);
        listCurrentMoney.clear();
        foreach (Money* money, listMoney) {
            foundCat = true;
            foreach (MoneyCategory* cat , listMoneyCategory) {
                if (!(money->category() && money->category()->id() == cat->id()))
                    foundCat = false;
            }
            if (foundCat)
                listCurrentMoney.append(money);
        }
        //qDebug() << __PRETTY_FUNCTION__ << "filtering category " << listCurrentMoney.size();
    }

    if (isSinceDt) {
        listMoney.clear();
        listMoney.append(listCurrentMoney);
        listCurrentMoney.clear();
        foreach (Money* money, listMoney) {
            if (money->dtTransaction() >=  sinceDt)
                listCurrentMoney.append(money);
        }
        //qDebug() << __PRETTY_FUNCTION__ << "filtering sinceDt " << listCurrentMoney.size();
    }
    if (isUntilDt) {
        listMoney.clear();
        listMoney.append(listCurrentMoney);
        listCurrentMoney.clear();
        foreach (Money* money, listMoney) {
            if (money->dtTransaction() <=  untilDt)
                listCurrentMoney.append(money);
        }
        //qDebug() << __PRETTY_FUNCTION__ << "filtering untilDt " << listCurrentMoney.size();
    }
    if (isDescription) {
        listMoney.clear();
        listMoney.append(listCurrentMoney);
        listCurrentMoney.clear();
        foreach (Money* money, listMoney) {
            if (money->description().contains(m_description))
                listCurrentMoney.append(money);
        }
        //qDebug() << __PRETTY_FUNCTION__ << "filtering description " << listCurrentMoney.size();
    }

    if (isLessValue) {
        listMoney.clear();
        listMoney.append(listCurrentMoney);
        listCurrentMoney.clear();
        foreach (Money* money, listMoney) {
            if (money->amount() <= m_lessValue)
                listCurrentMoney.append(money);
        }
        //qDebug() << __PRETTY_FUNCTION__ << "filtering lessValue " << listCurrentMoney.size();
    }
    if (isGreaterValue) {
        listMoney.clear();
        listMoney.append(listCurrentMoney);
        listCurrentMoney.clear();
        foreach (Money* money, listMoney) {
            if (money->amount() >=  m_greaterValue)
                listCurrentMoney.append(money);
        }
        //qDebug() << __PRETTY_FUNCTION__ << "filtering greaterValue " << listCurrentMoney.size();
    }

    return listCurrentMoney;
}
