﻿#ifndef DAOMANAGER_H
#define DAOMANAGER_H

#include <QtCore/QObject>

#include "categorydao.h"
#include "moneydao.h"
#include "userdao.h"
#include "goaldao.h"
#include "QSettings"

class MoneyCsvResume;
class SpecificSearch;

class DaoManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(SpecificSearch* spSearch READ spSearch WRITE setSpSearch NOTIFY spSearchChanged)
    Q_PROPERTY(QString currentPeriod READ currentPeriod WRITE setCurrentPeriod NOTIFY currentPeriodChanged)
    Q_PROPERTY(QVariant listPeriods READ listPeriods NOTIFY listPeriodsChanged)
    Q_PROPERTY(qreal totalIncomes READ totalIncomes NOTIFY totalIncomesChanged)
    Q_PROPERTY(qreal totalOutcomes READ totalOutcomes NOTIFY totalOutcomesChanged)
    Q_PROPERTY(qreal totalCashFlow READ totalCashFlow NOTIFY totalCashFlowChanged)
    Q_PROPERTY(qreal allTimeSale READ allTimeSale NOTIFY allTimeSaleChanged)

    Q_PROPERTY(QList<QObject*> moneyList READ getAllMoneyRegister NOTIFY moneyListChanged)
    Q_PROPERTY(QList<QObject*> categoryList READ getAllCategories NOTIFY categoryListChanged)
    Q_PROPERTY(QList<QObject*> goalList READ getAllGoals NOTIFY goalListChanged)

    Q_PROPERTY(QStringList frequencyList READ getAllFrequency)
    Q_PROPERTY(QString frequencyType READ frequencyType WRITE setFrequencyType
               NOTIFY frequencyTypeChanged)

    Q_PROPERTY(Money* newMoney READ money2BeEdited NOTIFY newMoneyChanged)
    Q_PROPERTY(QString newMoneyCategory READ newMoneyCategory WRITE setNewMoneyCategory
               NOTIFY newMoneyCategoryChanged)

    Q_PROPERTY(QString secretQuestion READ secretQuestion WRITE setSecretQuestion
               NOTIFY secretQuestionChanged)
    Q_PROPERTY(MoneyUser* newUser READ newUser NOTIFY newUserChanged)

    Q_PROPERTY(MoneyGoal* newGoal READ newGoal NOTIFY newGoalChanged)

    Q_PROPERTY(QString keyboardValue READ keyboardValue WRITE setKeyboardValue
               NOTIFY keyboardValueChanged)
    Q_PROPERTY(bool keyboardActivated READ keyboardActivated WRITE setKeyboardActivated
               NOTIFY keyboardActivatedChanged)

    Q_PROPERTY(QString calendarValue READ calendarValue WRITE setCalendarValue
               NOTIFY calendarValueChanged)
    Q_PROPERTY(bool calendarActivated READ calendarActivated WRITE setCalendarActivated
               NOTIFY calendarActivatedChanged)

    Q_PROPERTY(int calculatorValue READ calculatorValue WRITE setCalculatorValue
               NOTIFY calculatorValueChanged)
    Q_PROPERTY(bool calculatorActivated READ calculatorActivated WRITE setCalculatorActivated
               NOTIFY calculatorActivatedChanged)

    Q_PROPERTY(QStringList resumeOptions READ resumeOptions NOTIFY resumeOptionsChanged)
    Q_PROPERTY(QString resumeType READ resumeType WRITE setResumeType NOTIFY resumeTypeChanged)

    Q_PROPERTY(QString dropboxOption READ dropboxOption WRITE setDropboxOption
               NOTIFY dropboxOptionChanged)
    Q_PROPERTY(QString finalExportDate READ finalExportDate WRITE setFinalExportDate
               NOTIFY finalExportDateChanged)
    Q_PROPERTY(QString initialExportDate READ initialExportDate WRITE setInitialExportDate
               NOTIFY initialExportDateChanged)
    Q_PROPERTY(QString csvFilename READ csvfilename WRITE setCsvFilename NOTIFY csvFilenameChanged)
    Q_PROPERTY(MoneyCsvResume* money2Sync READ money2Sync NOTIFY money2SyncChanged)
    Q_PROPERTY(qreal dropboxSyncLv READ dropboxSyncLv NOTIFY dropboxSyncLvChanged)

    Q_PROPERTY(bool waitingMode READ waitingMode WRITE setWaitingMode NOTIFY waitingChanged)

    Q_PROPERTY(bool catEditEnabled READ isCategoryEditEnabled
               WRITE setCategoryEditEnabled NOTIFY categoryEditEnabledChanged )

    /***  Save Login and Passwords ***/
    Q_PROPERTY(QVariant savedLoginPassword READ savedLoginPassword WRITE setSavedLoginPassword)
    Q_PROPERTY(QVariant savedLoginName READ savedLoginName WRITE setSavedLoginName)
    Q_PROPERTY(QVariant savedDropboxPassword READ savedDropboxPassword WRITE setSavedDropboxPassword)
    Q_PROPERTY(QVariant savedDropboxName READ savedDropboxName WRITE setSavedDropboxName)

public:
    DaoManager(bool debugActivated = false);
    SpecificSearch* spSearch();
    void setSpSearch(SpecificSearch* sp);

    Q_INVOKABLE int appWidth() {
        int width = 360;
        //#ifdef Q_WS_MEEGO
        //        width = 480;
        //#endif
        return width;
    }

    Q_INVOKABLE int appHeight() {
        int height = 640;
        //#ifdef Q_WS_MEEGO
        //        height = 854;
        //#endif
        return height;
    }

    /** Screen Manager **/
    Q_INVOKABLE bool openDropDown(QVariant list, QString propertyName);
    Q_INVOKABLE void setDropDownValue(QString property, QString value);
    Q_INVOKABLE bool openMessageBox(QString title,
                                    QString message,
                                    QVariant options,
                                    QVariant viewList,
                                    QVariant stateList,
                                    QVariant actionList);
    Q_INVOKABLE void setMessageBoxValue(QString view, QString state, QString action);

    /** About User **/
    Q_INVOKABLE bool login(QString username, QString password);
    Q_INVOKABLE bool checkUser(QString username);
    Q_INVOKABLE bool createUser(QString username, QString password);
    Q_INVOKABLE QString getQuestion(QString username);
    Q_INVOKABLE bool verifyUserAccountAnswer(QString login, QString answer);
    Q_INVOKABLE bool isValidUser(QString username);
    Q_INVOKABLE bool updatePassword(QString,QString);
    Q_INVOKABLE QStringList defaultSecretQuestions();
    QString secretQuestion() const { return m_secretQuestion; }
    void setSecretQuestion(const QString &secretAnswer) {
        if (secretAnswer != m_secretQuestion) {
            m_secretQuestion = secretAnswer;
            emit secretQuestionChanged();
        }
    }
    MoneyUser* newUser() { return m_newUser; }
    Q_INVOKABLE bool createNewUser();
    /** About Categories **/
    Q_INVOKABLE QList<QObject*> getAllCategories();
    Q_INVOKABLE QList<QObject*> addCategory(QString category);
    Q_INVOKABLE QList<QObject*> removeCategory(int index);
    Q_INVOKABLE QList<QObject*> updateCategory(int index, QString name);
    Q_INVOKABLE void updateTotalByCategory();
    Q_INVOKABLE void addNewEmptyCategory();
    bool isCategoryEditEnabled() const { return m_catEditEnabled; }
    void setCategoryEditEnabled(const bool &enabled) { m_catEditEnabled = enabled; }
    /** About Date **/
    Q_INVOKABLE QString today();
    Q_INVOKABLE QString currentDate();
    Q_INVOKABLE QString previousDate();
    Q_INVOKABLE QString nextDate();
    Q_INVOKABLE void setTypeDate(int typeDate);
    Q_INVOKABLE QList<QObject*> getAllDatePeriods();
    QString currentPeriod();
    void setCurrentPeriod(const QString &currentPeriod);
    QVariant listPeriods();
    /** About Money **/
    Q_INVOKABLE QList<QObject*> getAllMoneyRegister();
    Q_INVOKABLE int addNewMoney();
    Q_INVOKABLE bool updateMoney();
    Q_INVOKABLE QList<QObject*> addMoney(int tipo, int idCategoria, QString description, double amount, QString date, int frequency, QString details);
    Q_INVOKABLE QList<QObject*> updateMoneyRegister(int type, int idCategory, QString description, double amount, QString date, int frequency, QString details);
    Q_INVOKABLE void removeMoneyRegister(int index);
    Q_INVOKABLE void removeMassiveMoneyRegisters(QVariant list);
    Q_INVOKABLE QList<QObject*> getAllTypeMoney();
    Q_INVOKABLE void setMoney2BeEdited(int index);
    Money* money2BeEdited();
    QString newMoneyCategory() { return (m_money2BeEdited? m_money2BeEdited->categoryByName() : "Sem categoria"); }
    QString frequencyType() { return (m_money2BeEdited? m_money2BeEdited->frequencyName() : "0"); }
    void setFrequencyType(QString frequencyType) {
        if (m_money2BeEdited) {
            m_money2BeEdited->setFrequencyName(frequencyType);
            qDebug() <<  frequencyType  << m_money2BeEdited->frequencyName();
            emit frequencyTypeChanged();
            emit newMoneyChanged();
        }
    }
    Q_INVOKABLE QStringList getAllFrequency(){ return m_frequencyNameList;}
    void setNewMoneyCategory(const QString &category) {
        if (m_money2BeEdited) {
            m_money2BeEdited->setCategoryByName(category);
            emit newMoneyCategoryChanged();
            emit newMoneyChanged();
        }
    }

    qreal totalIncomes();
    qreal totalOutcomes();
    qreal totalCashFlow();
    Q_INVOKABLE qreal allTimeSale();
    Q_INVOKABLE void setMoneyType(int typeMoney);
    /** About Goals **/
    Q_INVOKABLE QList<QObject*> getAllGoals();
    Q_INVOKABLE QList<QObject*> addGoal(int type, float target, QString initialDate, QString finalDate, QString description);
    Q_INVOKABLE void removeGoal(int index, bool massive = false);
    Q_INVOKABLE QList<QObject*> updateGoal(int type, float target, QString initialDate, QString finalDate, QString description, int index);
    Q_INVOKABLE QList<QObject*> getAllTypeGoal();
    MoneyGoal* newGoal() { return m_newGoal; }
    Q_INVOKABLE bool addNewGoal();
    Q_INVOKABLE void setGoal2BeEdited(const int &index);
    Q_INVOKABLE bool updateEditedGoal();
    /** About CSV  **/
    Q_INVOKABLE QString exportCSVEntities();
    Q_INVOKABLE void exportCsvFile(QString filepath);
    Q_INVOKABLE void importCsvContents(const QString &contents, const QString &filename);
    Q_INVOKABLE void doSync();
    Q_INVOKABLE void importCsvFile(QString filepath, QVariantList listPolicyChecks);
    Q_INVOKABLE bool getNextSyncEntity();
    Q_INVOKABLE bool deleteMoneyEntity(QObject *object);
    Q_INVOKABLE bool persistMoneyEntity(QObject *object);
    /** Test of Data Insertion **/
    Q_INVOKABLE void databaseRandomPopulate();

    /** Keyboard **/
    Q_INVOKABLE bool isUsingKeyboard() {
        bool usingKeyboard = false;
        //#ifdef Q_OS_SYMBIAN
        //        usingKeyboard = true;
        //#elif defined (Q_WS_MEEGO)
        //        usingKeyboard = true;
        //#endif
        return usingKeyboard;
    }

    QString keyboardValue() const { return m_keyboardValue; }
    void setKeyboardValue(const QString &keyboardValue) {
        if (keyboardValue != m_keyboardValue) {
            m_keyboardValue = keyboardValue;
            emit keyboardValueChanged();
        }
    }

    bool keyboardActivated() const { return m_keyboardActivated; }
    void setKeyboardActivated(const bool &activated) {
        if (activated != m_keyboardActivated) {
            m_keyboardActivated = activated;
            emit keyboardActivatedChanged();
        }
    }

    /** Calendar **/
    QString calendarValue() const { return m_calendarValue; }
    void setCalendarValue(const QString &calendarValue) {
        if (calendarValue != m_calendarValue) {
            m_calendarValue = calendarValue;
            emit calendarValueChanged();
        }
    }

    bool calendarActivated() const { return m_calendarActivated; }
    void setCalendarActivated(const bool &activated) {
        if (activated != m_calendarActivated) {
            m_calendarActivated = activated;
            emit calendarActivatedChanged();
        }
    }

    /** Calculator **/
    bool calculatorActivated() const { return m_calculatorActivated; }
    void setCalculatorActivated(const bool &activated) {
        if (activated != m_calculatorActivated) {
            m_calculatorActivated = activated;
            emit calculatorActivatedChanged();
        }
    }

    int calculatorValue() const { return m_calculatorValue; }
    void setCalculatorValue(const int &value) {
        if (value != m_calculatorValue) {
            m_calculatorValue = value;
            emit calculatorValueChanged();
        }
    }


    /** Resume Options **/
    QStringList resumeOptions() const { return m_resumeOptions; }

    void setResumeType(const QString &resumeType) {
        m_resumeType = resumeType;
        emit resumeTypeChanged();
        emit categoryListChanged();

    }
    QString resumeType() const { return m_resumeType; }

    /** Dropbox Options **/
    void setDropboxOption(const QString &dropboxOption)
    {
        if (dropboxOption != m_dropboxOption) {
            m_dropboxOption = dropboxOption;
            setDropboxSyncLv(0);
            emit dropboxOptionChanged();
        }
    }

    QString dropboxOption() const { return m_dropboxOption; }
    Q_INVOKABLE QStringList dropboxOptionsList() const
    {
        QStringList dropboxOptions;
        dropboxOptions << tr("Sincronizar") << tr("Salvar");

        return dropboxOptions;
    }

    qreal dropboxSyncLv() const { return m_dropboxSyncLv; }
    void setDropboxSyncLv(const qreal &dropboxSyncLv) {
        if (m_dropboxSyncLv != dropboxSyncLv) {
            m_dropboxSyncLv = dropboxSyncLv;
            emit dropboxSyncLvChanged();
        }
    }

    QString initialExportDate() const { return m_dtInitialExport.toString(DATE_FORMAT); }
    QString finalExportDate() const { return m_dtFinalExport.toString(DATE_FORMAT); }

    void setInitialExportDate(const QString &initialDate) {
        QDate newDate = QDate::fromString(initialDate,DATE_FORMAT);
        if (newDate != m_dtInitialExport) {
            m_dtInitialExport = newDate;
            emit initialExportDateChanged();
        }
    }
    void setFinalExportDate(const QString &finalDate) {
        QDate newDate = QDate::fromString(finalDate, DATE_FORMAT);
        if (newDate != m_dtFinalExport) {
            m_dtFinalExport = newDate;
            emit finalExportDateChanged();
        }
    }

    QString csvfilename() const { return m_csvfilenameChoosed; }
    void setCsvFilename(const QString &csvFilename) {
        if (csvFilename != m_csvfilenameChoosed) {
            m_csvfilenameChoosed = csvFilename;
            emit csvFilenameChanged();
        }
    }

    MoneyCsvResume* money2Sync() { return m_money2Sync; }

    bool waitingMode() const { return m_waiting; }
    void setWaitingMode(const bool &waiting) {
        if (waiting != m_waiting) {
            m_waiting = waiting;
            emit waitingChanged();
        }
    }

    //save login and password of app
    void setSavedLoginPassword(QVariant password){ m_savedData.setValue("loginPassword",password); debugMsg << savedLoginPassword();}
    void setSavedLoginName(QVariant login){m_savedData.setValue("loginName",login); debugMsg << m_savedData.value("loginName");}
    QVariant savedLoginPassword(){return m_savedData.value("loginPassword");}
    QVariant savedLoginName(){return m_savedData.value("loginName");}

    //save login and password of dropbox
    void setSavedDropboxPassword(QVariant password){ m_savedData.setValue("dropboxPassword",password); debugMsg << savedDropboxPassword();}
    void setSavedDropboxName(QVariant login){m_savedData.setValue("dropboxEmail",login); debugMsg << savedDropboxName();}
    QVariant savedDropboxPassword(){return m_savedData.value("dropboxPassword");}
    QVariant savedDropboxName(){return m_savedData.value("dropboxEmail");}

signals:
    void spSearchChanged();
    void currentPeriodChanged();
    void listPeriodsChanged();
    void totalIncomesChanged();
    void totalOutcomesChanged();
    void totalCashFlowChanged();
    void allTimeSaleChanged();

    void moneyListChanged();
    void categoryListChanged();
    void goalListChanged();

    void newMoneyChanged();
    void newMoneyCategoryChanged();
    void frequencyTypeChanged();
    void secretQuestionChanged();
    void newUserChanged();
    void newGoalChanged();

    void startDropDown(QVariant list, QString propertyName);
    void startMessageBox(QString title,
                         QString message,
                         QVariant options,
                         QVariant viewList,
                         QVariant stateList,
                         QVariant actionList);

    void switchFlow(QString viewIdentifier, QString viewState, QString action);

    void startGraphicEntitySync();
    void syncChoiceBetweenEntities(QObject* dbEntity, QObject* csvEntity);

    void keyboardValueChanged();
    void keyboardActivatedChanged();

    void calendarValueChanged();
    void calendarActivatedChanged();

    void calculatorValueChanged();
    void calculatorActivatedChanged();

    void resumeOptionsChanged();
    void resumeTypeChanged();

    void dropboxOptionChanged();
    void initialExportDateChanged();
    void finalExportDateChanged();
    void csvFilenameChanged();
    void money2SyncChanged();
    void dropboxSyncLvChanged();

    void waitingChanged();

    void categoryEditEnabledChanged();
private:
    /** About User **/
    UserDao *userDao;
    MoneyUser *m_user;
    MoneyUser *m_newUser;
    QString m_secretQuestion;

    MoneyGoal *m_newGoal;

    MoneyDao *m_moneyDao;
    GoalDao *m_goalDao;

    Money* m_money2BeEdited;
    QDate m_currentSelectedDate;
    QList<MoneyCategory*> m_listCategory;
    QList<Money*> m_listCurrentSeenMoney;
    QList<Money*> m_allMoney;
    QList<MoneyGoal*> m_listGoal;
    QList<float> m_totalList;
    int m_typeMoney;
    int m_typeDate;
    int m_semaphorMoney;
    SpecificSearch* m_spSearch;
    QList<MoneyCategory*> m_listDatePeriods;
    QStringList m_listPeriods;

    QList<QColor> m_colorList;
    QList<QString> m_colorNameList;

    QString m_keyboardValue;
    bool m_keyboardActivated;

    QString m_calendarValue;
    bool m_calendarActivated;

    QStringList m_resumeOptions;
    QString m_resumeType;

    int m_calculatorValue;
    bool m_calculatorActivated;

    void updatePeriod();
    void updateCategoryColors();
    void updateDatePeriodList();
    void createFrequencyList(){
        m_frequencyNameList.append("Nenhum");
        m_frequencyNameList.append("Diaria");
        m_frequencyNameList.append("Semanal");
        m_frequencyNameList.append("Mensal");
        m_frequencyNameList.append("Bimestral");
        m_frequencyNameList.append("Semestral");
        m_frequencyNameList.append("Anual");
    }
    SpecificSearch* updateSpSearch(int typeDate, SpecificSearch* sp_Search = 0);
    QString getCurrentDateString(int typeDate,  SpecificSearch* sp_Search = 0);

    /** About CSV Importer **/
    int m_semaphorCsvImporter;
    int m_syncEntityCount;
    QList<Entity*> m_listGraphiclyChoosedDbEntity;
    QList<Entity*> m_listGraphiclyChoosedCsvEntity;
    QStringList m_frequencyNameList;

    /** About Dropbox **/
    QString m_dropboxOption;
    QDate m_dtInitialExport;
    QDate m_dtFinalExport;
    QString m_csvfilenameChoosed;
    MoneyCsvResume *m_money2Sync;

    bool m_waiting;

    qreal m_dropboxSyncLv;

    /** About Category Editing **/
    bool m_catEditEnabled;

    QSettings m_savedData;


private slots:
    /** About CSV **/
    void getMoney2BeDeleted(QList<Entity*> listEntities);
    void getMoney2BeUpdated(QList<Entity*> listEntities);
    void getMoney2BePersisted(QList<Entity*> listEntities);
    void getMoney2BeGraphiclyChoosed(QList<Entity*> listDbEntities, QList<Entity*> listCsvEntities);
};

#endif // DAOMANAGER_H
