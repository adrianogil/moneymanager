#ifndef USERDAO_H
#define USERDAO_H

#include "user.h"

#define STANDARD_LOGIN "adriano"
#define STANDARD_PASSWORD "adm123"
#define STANDARD_QUESTION "Cidade em que você nasceu?"
#define STANDARD_ANSWER "Manaus"

class UserDao {

public:
    bool userLogin(MoneyUser *user);

    MoneyUser* createStandardUser();

    void createSampleData(MoneyUser *user);

    bool registerUser(MoneyUser *user);
    bool setNewPassword(QString userName,QString password);

    MoneyUser* getUser(MoneyUser* incomplete_user);

    bool verifyUserAnswer(MoneyUser *verify);
    bool verifyUserLogin(QString login);

    bool updateRegisterUser(MoneyUser* user);

    QString getUserQuestion(QString login);
    bool validUserLogin(QString login);
};

#endif // USERDAO_H
