#ifndef LOCATIONDAO_H
#define LOCATIONDAO_H

#include <QString>
#include <QList>
#include "../entity/Location.h"

MoneyLocation searchForLocation(QList<MoneyLocation> listMoneyLocation, int idLocation);

QList<MoneyLocation> getAllMoneyLocation(QString user_prefix);

bool deleteMoneyLocation(MoneyLocation* location, QString user_prefix);

bool updateMoneyLocation(MoneyLocation* location, QString user_prefix);

bool registerMoneyLocation(MoneyLocation* location, QString user_prefix);

#endif // LOCATIONDAO_H
