#ifndef CSVMANAGER_H
#define CSVMANAGER_H

#include <QObject>
#include <QtCore/QVariant>
#include <QFile>
#include "entity.h"

struct MetaField
{
    int index;
    char* name;
    QVariant::Type type;
};

class CsvManager : public QObject
{
    Q_OBJECT
    Q_FLAGS(CsvImportPolicy)
public:
    explicit CsvManager(QObject *parent = 0);

    enum CsvImportPolicyFlags {
        None = 0x00,
        KeepAllRegisters = 0x01,
        KeepOnlyDBRegisters = 0x02,
        KeepOnlyCSVRegisters = 0x04,
        KeepDBVersion = 0x08,
        KeepCSVVersion = 0x0F,
        KeepLastVersion = 0x10,
        GraphiclyChoose = 0x20,
        GraphiclyAnalyze = 0x40
    };
    Q_DECLARE_FLAGS(ImportPolicy, CsvImportPolicyFlags)

    // The list fields must be filled with the property names that will be exported
    QString createCsvFromEntities(QList<Entity*> entityList, QStringList fields, QStringList fieldsNames);
    QList<Entity*> loadEntitiesFromCsv(QString filepath);
    QList<Entity*> loadEntitiesFromCsv(QTextStream *inText);
    QList<Entity*> loadEntitiesFromCsvContents(const QString &contents);
    void syncEntitiesList(QList<Entity*> csvList, QList<Entity*> dbList);

    void setMetaFieldList(QList<MetaField> metaFieldList){ m_metaFieldList = metaFieldList; }
    QList<MetaField> metaFieldList(){ return m_metaFieldList; }

    void setImportPolicy(ImportPolicy importPolicy) { m_importPolicy = importPolicy; }

private:
    QList<MetaField> m_metaFieldList;

signals:
    void deleteEntities(QList<Entity*> entities);
    void persistEntities(QList<Entity*> entities);
    void updateEntities(QList<Entity*> entities);
    void conflitsEntities(QList<Entity*> dbEntities, QList<Entity*> csvEntities);
public slots:
private:
    ImportPolicy m_importPolicy;

};

Q_DECLARE_OPERATORS_FOR_FLAGS(CsvManager::ImportPolicy)

#endif // CSVMANAGER_H
