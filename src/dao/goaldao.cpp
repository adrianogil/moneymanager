﻿#include <QtSql/QSqlQuery>
#include <QtSql/QSqlRecord>
#include <QtCore/QVariant>
#include <QtCore/QVariantList>

#include <math.h>

#include "goaldao.h"
#include "appdebug.h"

MoneyGoal::goal_types selectTypeGoal(int typeGoal)
{
    switch(typeGoal) {
    case 0:
        return MoneyGoal::IN_greater_than;
        break;
    case 1:
        return MoneyGoal::OUT_less_than;
        break;
    case 2:
        return MoneyGoal::Total_greater_than;
        break;
    }

    return MoneyGoal::Total_greater_than;
}

int selectTypeGoal(MoneyGoal::goal_types typeGoal)
{
    if (typeGoal == MoneyGoal::IN_greater_than)
        return 0;
    else if (typeGoal == MoneyGoal::OUT_less_than)
        return 1;

    return 2;
}

MoneyGoal* getPercentageByGoal(MoneyGoal* goal, int id_user)
{
    if (!goal)
        return 0;

    int percentage = 0;
    QString goalType = "";
    if (goal->typeGoal() == MoneyGoal::IN_greater_than)
        goalType = " AND m1.type_money = 0";
    else if (goal->typeGoal() == MoneyGoal::OUT_less_than)
        goalType = " AND m1.type_money = 1";
    QString strQuery = " SELECT SUM(m1.amount) as total FROM money m1 "
                       " WHERE m1.id_user = :id_user AND m1.dt_transaction >= :dt_since "
                       " AND m1.dt_transaction <= :dt_until " + goalType;

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":id_user", id_user);
    query.bindValue(":dt_since", goal->getDtInitial());
    query.bindValue(":dt_until", goal->getDtFinal());
    query.exec();

    if (query.next()) {
        float total = query.value(query.record().indexOf("total")).toFloat();
        if (goal->target() != 0)
            percentage = floor(100 *  total / goal->target());
        if (percentage < 0)
            percentage = 0;
        else if (percentage > 100)
            percentage = 100;
        qDebug() << __PRETTY_FUNCTION__ << "Setting percentage" << percentage << total << goal->target();
    }

    goal->setPercentage(percentage);

    return goal;
}

bool updateMoneyGoal(MoneyGoal* goal)
{
    qDebug() << __PRETTY_FUNCTION__;

    if (!goal)
        return false;

    QString strQuery = " UPDATE goal SET dt_initial = :dt_initial, dt_final = :dt_final, "
                       " type_goal = :type_goal, target_value = :target_value, description = :description "
                       " WHERE id_goal = :id_goal";


    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":id_goal", goal->id());
    query.bindValue(":dt_initial", goal->getDtInitial());
    query.bindValue(":dt_final", goal->getDtFinal());
    query.bindValue(":type_goal", selectTypeGoal(goal->typeGoal()));
    query.bindValue(":target_value", goal->target());
    query.bindValue(":description", goal->description());

    return query.exec();
}

MoneyGoal* getGoal(MoneyGoal *incomplete_goal, int id_user)
{
    qDebug() << __PRETTY_FUNCTION__;

    QString strQuery = " SELECT id_goal, dt_initial, dt_final, target_value, type_goal, description as "
                       " id_goal, dt_initial, dt_final, target_value, type_goal, description, dtCreation, "
                       " dtLastModification FROM      "
                       " goal where id_user = :id_user AND dt_initial = :dt_initial       "
                       " AND dt_final = :dt_final AND target_value = :target_value        "
                       " AND type_goal = :type_goal AND description LIKE :description     ";

    QSqlQuery query;
    query.prepare(strQuery);
    query.bindValue(":id_user", id_user);
    query.bindValue(":dt_initial", incomplete_goal->getDtInitial());
    query.bindValue(":dt_final", incomplete_goal->getDtFinal());
    query.bindValue(":type_goal", selectTypeGoal(incomplete_goal->typeGoal()));
    query.bindValue(":target_value", incomplete_goal->target());
    query.bindValue(":description", incomplete_goal->description());

    MoneyGoal* goal = 0;
    query.exec();
    if (query.next()) {
        goal = new MoneyGoal(query.value(query.record().indexOf("id_goal")).toInt(),
                                   selectTypeGoal(query.value(query.record().indexOf("type_goal")).toInt()),
                                    query.value(query.record().indexOf("target_value")).toFloat(),
                                    query.value(query.record().indexOf("dt_initial")).toDate(),
                                    query.value(query.record().indexOf("dt_final")).toDate(),
                                    query.value(query.record().indexOf("description")).toString());
        goal->setDtCreation(query.value(query.record().indexOf("dtCreation")).toDateTime());
        goal->setDtCreation(query.value(query.record().indexOf("dtLastModification")).toDateTime());
    } else qDebug() << __PRETTY_FUNCTION__ << "Goal not found";

    return goal;
}

GoalDao::GoalDao(int userId, QObject *parent)
    : EntityDao(parent)
    , m_userId(userId)
{
    setTableName("goal");

    QStringList dbParams;
    dbParams << "description" << "dt_initial" << "dt_final" << "id_user"
             << "target_value" << "type_goal" << "dtCreation" << "dtLastModification";
    setDbParameters(dbParams);

    QStringList objParams;
    objParams << "description" << "dtInitial" << "dtFinal" << "idUser"
             << "target" << "goalType" << "dtCreation" << "dtLastModification";
    setObjParameters(objParams);

    setPrimaryKey("id_goal");
}

bool GoalDao::persist(MoneyGoal *goal)
{
    debugMsg << goal;

    goal->setProperty("idUser", m_userId);
    return EntityDao::persist((Entity*) goal);
}

bool GoalDao::remove(MoneyGoal *goal)
{
    debugMsg << goal;

    return EntityDao::remove((Entity*) goal);
}

bool GoalDao::update(MoneyGoal *goal)
{
    goal->setProperty("idUser", m_userId);
    return EntityDao::update((Entity*) goal);
}

QList<MoneyGoal*> GoalDao::getAll()
{
    debugMsg << "START";
    resetQueryConditions();
    QVariantList valuesList;
    valuesList << m_userId;
    appendQueryConditions("goal.id_user = :id_user", QStringList(":id_user"), valuesList);
    QList<MoneyGoal*> goalList = MoneyGoal::toGoalList(EntityDao::getAll());
    resetQueryConditions();
    debugMsg << "END" << goalList.length();
    return goalList;
}

