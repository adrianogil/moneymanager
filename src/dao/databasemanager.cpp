﻿#ifndef DATABASEMANAGER_CPP
#define DATABASEMANAGER_CPP

#include "databasemanager.h"
#include "moneymanagerdb.h"
#include <QDir>

DatabaseManager::DatabaseManager(QObject *parent)
    : QObject(parent)
{

}

DatabaseManager::~DatabaseManager()
{

}

bool DatabaseManager::openDB()
{
    // Find QSLite driver
    db = QSqlDatabase::addDatabase("QSQLITE");

    #ifdef Q_OS_LINUX
    // NOTE: We have to store database file into user home folder in Linux
    QString path(QDir::home().path());
    path.append(QDir::separator()).append(MONEY_DB_NAME);
    path = QDir::toNativeSeparators(path);
    db.setDatabaseName(path);
    #else
    // NOTE: File exists in the application private folder, in Symbian Qt implementation
    db.setDatabaseName(MONEY_DB_NAME);
    #endif

    // Open databasee
    return db.open();
}

QSqlError DatabaseManager::lastError()
{
    // If opening database has failed user can ask
    // error description by QSqlError::text()
    return db.lastError();
}

bool DatabaseManager::deleteDB()
{
    // Close database
    //db.close();

    #ifdef Q_OS_LINUX
    // NOTE: We have to store database file into user home folder in Linux
    QString path(QDir::home().path());
    path.append(QDir::separator()).append(MONEY_DB_NAME);
    path = QDir::toNativeSeparators(path);
    return QFile::remove(path);
    #else

    // Remove created database binary file
    return QFile::remove(MONEY_DB_NAME);
    #endif
}

#endif // DATABASEMANAGER_CPP
