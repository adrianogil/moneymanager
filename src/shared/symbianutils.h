#ifndef SYMBIANUTILS_H
#define SYMBIANUTILS_H

#include <sysutil.h>
#include <apgcli.h>
#include <apgtask.h>
#include <eikenv.h>

#include <QtCore/QStringList>
#include <QtCore/QFile>
#include <QtCore/QTextStream>

namespace SymbianUtils {

    static QString firmware()
    {
        QString versionText;
        TBuf<KSysUtilVersionTextLength> versionBuf;
        if (SysUtil::GetSWVersion(versionBuf) == KErrNone) {
            versionText = QString::fromUtf16(versionBuf.Ptr(), versionBuf.Length());
        }
		versionText = versionText.split("\n").at(0);
        return versionText.replace("v ", "").replace("v","");
    }

    static QString model(QString fileName = "z:/resource/versions/product.txt")
    {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            return QString("");
        } else {
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString line(in.readLine());
                if (line.contains("Model")) {
                    return line.split("=").at(1);
                }
            }
        }
    }

    static QString rm(QString fileName = "z:/resource/versions/sw.txt")
    {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            return QString("");
        } else {
            QTextStream in(&file);
            while (!in.atEnd()) {
                QString line(in.readLine());
                if (line.contains("RM-")) {
                    return line.mid(line.indexOf("RM-"), 6);
                }
            }
        }
    }

    static void openWindowByUID(TInt windowUID)
    {
        HBufC* param = HBufC::NewLC(0);
//        const TInt windowUID = 0x101f6de5;
        TUid id(TUid::Uid(windowUID));

        TApaTaskList taskList(CEikonEnv::Static()->WsSession());
        TApaTask task = taskList.FindApp(id);

        if (task.Exists()) {
            HBufC8* param8 = HBufC8::NewLC(param->Length());
            param8->Des().Append(*param);
            task.SendMessage(TUid::Uid(0), *param8); // Uid is not used
            CleanupStack::PopAndDestroy(); // param8
        } else {
            RApaLsSession appArcSession;
            User::LeaveIfError(appArcSession.Connect()); // connect to AppArc server
            TThreadId id;
            appArcSession.StartDocument(*param, TUid::Uid(windowUID), id);
            appArcSession.Close();
        }
        CleanupStack::PopAndDestroy(); // param
    }

};

#endif // SYMBIANUTILS_H
