INCLUDEPATH += $$PWD

SOURCES += $$PWD/linegraph.cpp \
        $$PWD/nightcharts.cpp \
        $$PWD/aboututils.cpp

HEADERS += $$PWD/linegraph.h \
        $$PWD/nightcharts.h \
        $$PWD/symbianutils.h \
        $$PWD/aboututils.h
