#ifndef ABOUTMANAGER_H
#define ABOUTMANAGER_H

#include <QObject>

class AboutUtils : public QObject
{
    Q_OBJECT
public:
    explicit AboutUtils(QObject *parent = 0);
    Q_INVOKABLE void setUpAboutMessage();
    Q_INVOKABLE QString getAboutMessage();
    Q_INVOKABLE void updateSoftware();
    Q_INVOKABLE bool showUpdateButton();

private:
    QString m_model;
    QString m_firmware;
    QString m_homofirmware;
    QString m_message;
    bool m_showUpdateButton;

};

#endif // ABOUTMANAGER_H
