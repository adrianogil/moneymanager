#include <QDebug>
#include <QHash>


#include "aboututils.h"

#if defined (Q_OS_SYMBIAN)
#include "symbianutils.h"
#elif defined (Q_WS_MEEGO)
#include <QSystemInfo>
#include <QSystemDeviceInfo>
#include <QProcess>
#endif


AboutUtils::AboutUtils(QObject *parent) :
    QObject(parent),
    m_showUpdateButton(false)
{
    //    setUpAboutMessage();
}

void AboutUtils::setUpAboutMessage(){

        QHash<QString,QString> m_modelList;
        m_modelList["N9"] = QString("10.2011");
        m_modelList["N950"] = QString("10.2011");
        m_modelList["N8-00"] = QString("011.012");
        m_modelList["N97"] = QString("22.2.110");
        m_modelList["C7-00"] = QString("013.016");
        m_modelList["E7-00"] = QString("013.016");

        QHash<QString,QString> m_rmList;
        m_rmList["N9"] = QString("RM-696");
        m_rmList["N950"] = QString("RM-680");
        m_rmList["N8-00"] = QString("RM-596");
        m_rmList["N97"] = QString("RM-507");
        m_rmList["C7-00"] = QString("RM-675");
        m_rmList["E7-00"] = QString("RM-626");

        QString rm = "";

#if defined (Q_OS_SYMBIAN)
   m_model = SymbianUtils::model();
   rm = SymbianUtils::rm();
   m_firmware = SymbianUtils::firmware();

   int firstDigit = m_firmware.indexOf(QRegExp("[0-9]"));
   int lastDot = 0;
   if (m_firmware.count('.') > 1)
       lastDot = m_firmware.lastIndexOf('.');
   m_firmware = m_firmware.mid(firstDigit, m_firmware.length() - lastDot);
#elif defined(Q_WS_MEEGO)
   QtMobility::QSystemInfo s;
   m_firmware = s.version(QtMobility::QSystemInfo::Firmware);
   QRegExp firmwareRegex("\\d\\d\\.\\d\\d\\d\\d");
   int pos = firmwareRegex.indexIn(m_firmware);
   if (pos != -1)
       m_firmware = firmwareRegex.capturedTexts().at(0);

   QtMobility::QSystemDeviceInfo sd;
   m_model = sd.model();
   rm = sd.productName();
#endif

   if (m_modelList.contains(m_model)) {
       m_homofirmware = m_modelList[m_model];
       if (QString::compare(m_firmware, m_homofirmware) < 0) {
           m_message =
             "<p align='justify'>"
                 "A vers&#227;o do software do seu Nokia <strong>" +
                    m_model + ", " + rm + "</strong> &#233; a "
                 "<strong>" + m_firmware + "</strong> e esta "
                    "aplica&#231;&#227;o foi projetada "
                 "para a vers&#227;o <strong>" + m_modelList[m_model] +
                "</strong> com o <strong>" + m_rmList[m_model] + "</strong> e superiores."
             "</p>"
             "<p align='justify'>"
                 "Para um bom funcionamento sugerimos a "
                 "atualiza&#231;&#227;o do software de seu aparelho.<br />"
                 "Caso deseje fazer isso agora, pressione o "
                 "bot&#227;o ATUALIZAR e na tela escolha "
                 "OP&#199;&#213;ES e CHECAR ATUALIZA&#199;&#213;ES."
             "</p>";
       }
       else if (QString::compare(m_firmware, m_homofirmware) == 0) {
           m_message =
             "<p align='justify'>"
                 "A vers&#227;o do software do seu Nokia <strong>" +
                 m_model + ", " + rm + "</strong> &#233; a "
                 "<strong>" + m_firmware + "</strong>, vers&#227;o para " +
                "a qual esta aplica&#231;&#227;o foi projetada."
             "</p>";
       }
       else {
           m_message =
             "<p align='justify'>"
                 "A vers&#227;o do software do seu Nokia <strong>" +
                  m_model + ", " + rm + "</strong> &#233; a "
                 "<strong>" + m_firmware + "</strong>, vers&#227;o " +
                    "acima da qual sua aplica&#231;&#227;o foi projetada, <strong>" +
                    m_modelList[m_model] + "</strong>."
             "</p>";
       }
   }
   else {
       m_message =
         "<p align='justify'>"
             "Esta aplica&#231;&#227;o n&#227;o foi homologada para o seu aparelho Nokia <strong>" +
               m_model + ", " + rm + "</strong>.Eventuais problemas poder&#227;o ocorrer durante a sua "
             "execu&#231;&#227;o."
         "</p>";
   }
        qDebug()<< __PRETTY_FUNCTION__ << "aboutMessage";
    }

void AboutUtils::updateSoftware(){
#if defined (Q_OS_SYMBIAN)
    SymbianUtils::openWindowByUID(0x101f6de5);
#elif defined(Q_WS_MEEGO)
    QProcess::startDetached("/usr/bin/invoker",
                                QStringList() << "--type=e" <<
                                "--single-instance" << "/usr/bin/package-manager-ui");
#endif
}


QString AboutUtils::getAboutMessage(){
    setUpAboutMessage();
    return m_message;
}

bool AboutUtils::showUpdateButton(){
    return m_showUpdateButton;
}
