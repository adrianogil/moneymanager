﻿#ifndef UTILS_H
#define UTILS_H

#include <QtCore/QObject>
#include "money.h"
#include "category.h"
#include "goal.h"

QList<QObject*> convert2ObjList(QList<Entity*> entList)
{
    QList<QObject*> objList;
    foreach(Entity* entity, entList)
        objList.append(entity);
    return objList;
}

QList<QObject*> convert2ObjList(QList<MoneyCategory*> catList)
{
    QList<QObject*> objList;
    foreach(MoneyCategory* cat, catList)
        objList.append(cat);
    return objList;
}

QList<QObject*> convert2ObjList(QList<Money*> moneyList)
{
    QList<QObject*> objList;
    foreach(Money* money, moneyList)
        objList.append(money);
    return objList;
}

QList<QObject*> convert2ObjList(QList<MoneyGoal*> goalList)
{
    QList<QObject*> objList;
    foreach(MoneyGoal* goal, goalList)
        objList.append(goal);
    return objList;
}

QList<Entity*> convert2EntityList(QList<Money*> moneyList) {
    QList<Entity*> entList;
    foreach (QObject *obj, convert2ObjList(moneyList))
        entList.append((Entity*) obj);
    return entList;
}

QList<Entity*> convert2EntityList(QList<QObject*> objList) {
    QList<Entity*> entList;
    foreach (QObject *obj, objList)
        entList.append((Entity*) obj);
    return entList;
}

#endif // UTILS_H
