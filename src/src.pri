INCLUDEPATH += $$PWD

include($$PWD/entity/entity.pri)
include($$PWD/dao/dao.pri)
include($$PWD/shared/shared.pri)
include($$PWD/gui/gui.pri)
include($$PWD/utils/utils.pri)
include($$PWD/dropbox/dropbox.pri)
include($$PWD/symbianvibration/symbianvibration.pri)

SOURCES += $$PWD/main.cpp

HEADERS += $$PWD/appdebug.h

OTHER_FILES += \
    src/resources/qml/MeegoScreen.qml \
    src/resources/qml/mmportraitcommon/View.qml \
    src/resources/qml/mmportraitcommon/TitleBox.qml \
    src/resources/qml/mmportraitcommon/TargetsView.qml \
    src/resources/qml/mmportraitcommon/TargetsAdd.qml \
    src/resources/qml/mmportraitcommon/SuperiorMenuButton.qml \
    src/resources/qml/mmportraitcommon/SuperiorMenu.qml \
    src/resources/qml/mmportraitcommon/StringSlider.qml \
    src/resources/qml/mmportraitcommon/SplashView.qml \
    src/resources/qml/mmportraitcommon/ResumeView.qml \
    src/resources/qml/mmportraitcommon/ResumeListItem.qml \
    src/resources/qml/mmportraitcommon/QuestionButton.qml \
    src/resources/qml/mmportraitcommon/ProgressIndicator.qml \
    src/resources/qml/mmportraitcommon/ProgressBar.qml \
    src/resources/qml/mmportraitcommon/PopUpButton.qml \
    src/resources/qml/mmportraitcommon/PeriodSelector.qml \
    src/resources/qml/mmportraitcommon/PasswordRecoveryView.qml \
    src/resources/qml/mmportraitcommon/NewAccountView.qml \
    src/resources/qml/mmportraitcommon/MoneyTypeSelector.qml \
    src/resources/qml/mmportraitcommon/MoneyAddView.qml \
    src/resources/qml/mmportraitcommon/MessageBox.qml \
    src/resources/qml/mmportraitcommon/LoginBox.qml \
    src/resources/qml/mmportraitcommon/ListMoneyItem.qml \
    src/resources/qml/mmportraitcommon/LineGraph.qml \
    src/resources/qml/mmportraitcommon/LineEdit.qml \
    src/resources/qml/mmportraitcommon/KeyboardInputScreen.qml \
    src/resources/qml/mmportraitcommon/ItemGoalList.qml \
    src/resources/qml/mmportraitcommon/HelpView.qml \
    src/resources/qml/mmportraitcommon/ExpansiveListItem.qml \
    src/resources/qml/mmportraitcommon/DropDownList.qml \
    src/resources/qml/mmportraitcommon/DropboxView.qml \
    src/resources/qml/mmportraitcommon/ContactView.qml \
    src/resources/qml/mmportraitcommon/Chart.qml \
    src/resources/qml/mmportraitcommon/CategoryPopupButton.qml \
    src/resources/qml/mmportraitcommon/CategoriesView.qml \
    src/resources/qml/mmportraitcommon/CategoriesListItem.qml \
    src/resources/qml/mmportraitcommon/CashFlowView.qml \
    src/resources/qml/mmportraitcommon/CashFlowResume.qml \
    src/resources/qml/mmportraitcommon/CashFlowList.qml \
    src/resources/qml/mmportraitcommon/ButtonMenu.qml \
    src/resources/qml/mmportraitcommon/Button.qml \
    src/resources/qml/mmportraitcommon/BottomMenu.qml \
    src/resources/qml/mmportraitcommon/AboutView.qml \
    src/resources/qml/mmportraitcommon/GImage.qml \
    src/resources/qml/viewmanager.js \
    src/resources/qml/utils.js \
    src/resources/qml/pageslider.js \
    src/resources/qml/moneymanager.ts \
    src/resources/qml/TextSignalError.qml \
    src/resources/qml/TextError.qml \
    src/resources/qml/PageSlider.qml \
    src/resources/qml/MainView.qml \
    src/resources/qml/Main.qml





