﻿var viewObjs = {}
var registeredViews = []

var lastViewObj
var currentViewObj

var currentViewId = ""

// Designed to be acessed in the following way:
//          lastSubViewByState['State'] = 'subview'
var lastSubViewByState = {}

function registerView(viewObj)
{
    console.log("ViewManager.js - Register View " + viewObj +
                " with id " + viewObj.viewId)
    viewObjs[viewObj.viewId] = viewObj;
    registeredViews[registeredViews.length] = viewObj.viewId
    if (registeredViews.length >= 8)
        mainView.allViewRegistered()
}

function hideAllViews(listExceptions)
{
    if (listExceptions == null)
        listExceptions = []
    console.log("ViewManager.js - Hide All Views except " + listExceptions);
    for (var i = 0; i < registeredViews.length; i++) {
        //        console.log("ViewManager.js - Hide All Views - Trying to Hiding " + registeredViews[i] +
        //                    " but got " + listExceptions.indexOf(registeredViews[i]));
        //        if (listExceptions.indexOf(registeredViews[i]) == -1) {
        //            console.log("ViewManager.js - Hide All Views - Hiding " + registeredViews[i]);
        viewObjs[registeredViews[i]].opacity = 0;
        //        }
    }
}

function doPageSlider(nextViewId)
{
    console.log("ViewManager.js - doPageSlider - Next: " + nextViewId +
                " Last: " + currentViewId)

    if (nextViewId == currentViewId || nextViewId == "" ||
            registeredViews.indexOf(nextViewId) == -1)
        return;

    console.log("ViewManager.js - doPageSlider - Next: " + nextViewId +
                " Last: " + currentViewId + " REALLY DOING!")

    for (var i = 0; i < registeredViews.length; i++) {
        if (registeredViews[i] != currentViewId &&
                registeredViews[i] != nextViewId)
            viewObjs[registeredViews[i]].opacity = 0;
    }

    lastViewObj = currentViewObj;
    currentViewObj = viewObjs[nextViewId];

    currentViewId = nextViewId

    for (var i = 0; i < pageSlider.children.length; i++)
        console.log("ViewManager.js - doPageSlider - View " + pageSlider.children[i].viewId +
                    " at x = " + pageSlider.children[i].x);

    if (lastViewObj) {
        lastViewObj.opacity = 1;
        console.log("ViewManager.js - doPageSlider - Setting pageSlider.x = " +
                    (-(lastViewObj.x)));
    }
    if (currentViewObj) {
        currentViewObj.opacity = 1;
        if (lastViewObj)
            pageSlider.x = -(lastViewObj.x)
        console.log("ViewManager.js - doPageSlider - Setting pageSliderAnimation.newX = " +
                    (-(currentViewObj.x)));
        pageSliderAnimation.newX = -(currentViewObj.x)
        pageSliderAnimation.stop()
        pageSliderAnimation.restart();
        console.log("ViewManager.js - doPageSlider - " + nextViewId +
                    " Doing Page Slider Animation with the lastView " +
                    (lastViewObj? lastViewObj.viewId : "undefined") + " and the currentView " +
                    currentViewObj.viewId)
    }
}
