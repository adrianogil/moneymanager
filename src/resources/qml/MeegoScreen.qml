import QtQuick 1.1
import com.nokia.meego 1.0

PageStackWindow {
   id: rootWindow
   showStatusBar: false
   showToolBar: false
   inPortrait: true
   anchors.margins: 0
   initialPage: mainPage

   Page {
       id: mainPage
       anchors.margins: 0
       orientationLock: PageOrientation.LockPortrait

       Main {}
   }

}
