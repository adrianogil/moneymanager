<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<context>
    <name>AboutView</name>
    <message>
        <location filename="mmportraitcommon/AboutView.qml" line="35"/>
        <source>Sobre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/AboutView.qml" line="108"/>
        <source>Atualizar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ButtonMenu</name>
    <message>
        <location filename="mmportraitcommon/ButtonMenu.qml" line="23"/>
        <source>Sobre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ButtonMenu.qml" line="34"/>
        <source>Ajuda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ButtonMenu.qml" line="46"/>
        <source>Exportar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ButtonMenu.qml" line="56"/>
        <source>Fale Conosco</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ButtonMenu.qml" line="68"/>
        <source>Sair da Conta</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalendarComponent</name>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="77"/>
        <source>Janeiro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="77"/>
        <source>Fevereiro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="78"/>
        <source>Abril</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="78"/>
        <source>Maio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="78"/>
        <source>Junho</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="79"/>
        <source>Julho</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="79"/>
        <source>Agosto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="79"/>
        <source>Setembro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="80"/>
        <source>Outubro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="80"/>
        <source>Novembro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarComponent.qml" line="80"/>
        <source>Dezembro</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CalendarView</name>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarView.qml" line="44"/>
        <source>Calendário</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarView.qml" line="56"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/calendar/CalendarView.qml" line="66"/>
        <source>Cancelar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CashFlowList</name>
    <message>
        <location filename="mmportraitcommon/CashFlowList.qml" line="131"/>
        <source>Recebido</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/CashFlowList.qml" line="145"/>
        <source>Pago</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/CashFlowList.qml" line="159"/>
        <source>Total</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/CashFlowList.qml" line="171"/>
        <source>Create</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/CashFlowList.qml" line="179"/>
        <source>Não existem registros cadastrados</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CashFlowResume</name>
    <message>
        <location filename="mmportraitcommon/CashFlowResume.qml" line="6"/>
        <source>Resumo de Fluxo de Caixa</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CategoriesListItem</name>
    <message>
        <location filename="mmportraitcommon/CategoriesListItem.qml" line="147"/>
        <source>Nova Categoria</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CategoriesView</name>
    <message>
        <location filename="mmportraitcommon/CategoriesView.qml" line="25"/>
        <source>Categorias cadastradas</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CategoryPopupButton</name>
    <message>
        <location filename="mmportraitcommon/CategoryPopupButton.qml" line="20"/>
        <source>Adicionar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/CategoryPopupButton.qml" line="28"/>
        <source>Remover</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ContactView</name>
    <message>
        <location filename="mmportraitcommon/ContactView.qml" line="32"/>
        <source>Fale Conosco</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ContactView.qml" line="86"/>
        <source>Saiba mais sobre os aplicativos desenvolvidos pelo INDT através do perfil Apps Guru no Twitter e Facebook.
Acompanhe nossos canais de comunicação para estar atualizado sobre novidades e lançamentos, enviar sugestões e muito mais. Qualquer dúvida é só falar com a gente!&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DropboxView</name>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="40"/>
        <source>Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="55"/>
        <source>Faça o login com sua conta Dropbox:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="65"/>
        <source>Email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="71"/>
        <source>Senha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="95"/>
        <source>Escolha o tipo de operação:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="104"/>
        <source>Operação no Dropbox</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="117"/>
        <source>Selecionar todos os dados cadastrados entre o período:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="124"/>
        <source>Data de início</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="127"/>
        <location filename="mmportraitcommon/DropboxView.qml" line="143"/>
        <location filename="mmportraitcommon/DropboxView.qml" line="152"/>
        <location filename="mmportraitcommon/DropboxView.qml" line="252"/>
        <location filename="mmportraitcommon/DropboxView.qml" line="273"/>
        <location filename="mmportraitcommon/DropboxView.qml" line="283"/>
        <location filename="mmportraitcommon/DropboxView.qml" line="286"/>
        <source>Sincronizar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="129"/>
        <location filename="mmportraitcommon/DropboxView.qml" line="180"/>
        <location filename="mmportraitcommon/DropboxView.qml" line="270"/>
        <source>Salvar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="136"/>
        <source>Data final</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="144"/>
        <source>Escolha um arquivo para Sync:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="154"/>
        <source>Nome do Arquivo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="182"/>
        <source>Nome do arquivo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="189"/>
        <source>Obs: A sincronização ou exportação de arquivos nesse processo  pode acorretar na perda parcial de dados.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="208"/>
        <source>Arquivo encontrado: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="212"/>
        <source>Registros cadastrados: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="216"/>
        <source>Total entrada: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="220"/>
        <source>Total saída: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="224"/>
        <source>Total: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="239"/>
        <source>Sincronização completa!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="252"/>
        <source>Voltar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="252"/>
        <location filename="mmportraitcommon/DropboxView.qml" line="352"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="279"/>
        <source>Campo obrigatorio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="294"/>
        <source>Sair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="294"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/DropboxView.qml" line="323"/>
        <source>Progresso da Operação</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HelpView</name>
    <message>
        <location filename="mmportraitcommon/HelpView.qml" line="36"/>
        <source>Ajuda</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/HelpView.qml" line="153"/>
        <source>Continuar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/HelpView.qml" line="153"/>
        <source>Sair</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ItemGoalList</name>
    <message>
        <location filename="mmportraitcommon/ItemGoalList.qml" line="80"/>
        <source>Prazo:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ItemGoalList.qml" line="80"/>
        <source>à</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ItemGoalList.qml" line="87"/>
        <source>Valor na data final:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListMoneyItem</name>
    <message>
        <location filename="mmportraitcommon/ListMoneyItem.qml" line="39"/>
        <source>Categoria:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ListMoneyItem.qml" line="53"/>
        <source>Frequência:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ListMoneyItem.qml" line="67"/>
        <source>Descrição:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginBox</name>
    <message>
        <location filename="mmportraitcommon/LoginBox.qml" line="28"/>
        <source>Nome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/LoginBox.qml" line="37"/>
        <source>Senha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/LoginBox.qml" line="43"/>
        <source>Nome ou senha incorretos</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoneyAddView</name>
    <message>
        <location filename="mmportraitcommon/MoneyAddView.qml" line="64"/>
        <source>Descrição</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/MoneyAddView.qml" line="77"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/MoneyAddView.qml" line="87"/>
        <source>Valor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/MoneyAddView.qml" line="99"/>
        <source>Categoria</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/MoneyAddView.qml" line="115"/>
        <source>Frequência</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/MoneyAddView.qml" line="130"/>
        <source>Detalhes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/MoneyAddView.qml" line="162"/>
        <source>* Dados Inválidos</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewAccountView</name>
    <message>
        <location filename="mmportraitcommon/NewAccountView.qml" line="47"/>
        <source>Nome</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/NewAccountView.qml" line="61"/>
        <source>Senha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/NewAccountView.qml" line="76"/>
        <source>Confirmar senha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/NewAccountView.qml" line="92"/>
        <source>Pergunta Secreta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/NewAccountView.qml" line="117"/>
        <source>Resposta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/NewAccountView.qml" line="131"/>
        <location filename="mmportraitcommon/NewAccountView.qml" line="151"/>
        <source>* Somente números e letras, no mínimo 2 dígitos</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/NewAccountView.qml" line="141"/>
        <source>* Usuário já cadastrado</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/NewAccountView.qml" line="161"/>
        <source>* Senha incorreta.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/NewAccountView.qml" line="171"/>
        <location filename="mmportraitcommon/NewAccountView.qml" line="182"/>
        <source>* Campo vazio.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordRecoveryView</name>
    <message>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="41"/>
        <source>Recuperar senha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="66"/>
        <source>Nome do usuário:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="80"/>
        <source>Usuário não encontrado</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="114"/>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="186"/>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="277"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="137"/>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="208"/>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="310"/>
        <source>Cancelar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="221"/>
        <source>Resposta Incorreta!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="239"/>
        <source>Insira sua nova senha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="252"/>
        <source>Nova senha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="263"/>
        <source>Confirmar nova senha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/PasswordRecoveryView.qml" line="293"/>
        <source>Senha não é igual</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ResumeView</name>
    <message>
        <location filename="mmportraitcommon/ResumeView.qml" line="6"/>
        <source>Resumo de totais
por categoria (em gráfico)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ResumeView.qml" line="68"/>
        <source>Resumo de Entrada</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ResumeView.qml" line="69"/>
        <source>Sem registros de Entrada associados a Categorias</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/ResumeView.qml" line="106"/>
        <location filename="mmportraitcommon/ResumeView.qml" line="127"/>
        <source>Resumo do Fluxo de Caixa</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TargetsAdd</name>
    <message>
        <location filename="mmportraitcommon/TargetsAdd.qml" line="89"/>
        <source>Descrição</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/TargetsAdd.qml" line="101"/>
        <source>Data inicial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/TargetsAdd.qml" line="114"/>
        <source>Data final</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/TargetsAdd.qml" line="125"/>
        <source>Valor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/TargetsAdd.qml" line="141"/>
        <source>Investimento Mensal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/TargetsAdd.qml" line="151"/>
        <source>Saldo atual</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TargetsView</name>
    <message>
        <location filename="mmportraitcommon/TargetsView.qml" line="43"/>
        <source>Lista de Metas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mmportraitcommon/TargetsView.qml" line="131"/>
        <source>Não existem metas cadastradas</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>viewmanager</name>
    <message>
        <location filename="viewmanager.js" line="100"/>
        <location filename="viewmanager.js" line="108"/>
        <source>Modificar Senha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="101"/>
        <source>Senha alterada com sucesso</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="102"/>
        <location filename="viewmanager.js" line="110"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="109"/>
        <source>Erro! Tente outra vez</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="312"/>
        <source>Data inicial vazia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="315"/>
        <source>Data final vazia</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="318"/>
        <source>Data Final menor que data inicial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="322"/>
        <source>Valor precisa ser maior que 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="388"/>
        <source>Criar novo Registro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="389"/>
        <source>Deseja realmente adicionar os dados inseridos?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="390"/>
        <location filename="viewmanager.js" line="403"/>
        <location filename="viewmanager.js" line="418"/>
        <location filename="viewmanager.js" line="428"/>
        <location filename="viewmanager.js" line="438"/>
        <location filename="viewmanager.js" line="457"/>
        <location filename="viewmanager.js" line="466"/>
        <location filename="viewmanager.js" line="475"/>
        <location filename="viewmanager.js" line="522"/>
        <location filename="viewmanager.js" line="535"/>
        <location filename="viewmanager.js" line="554"/>
        <location filename="viewmanager.js" line="567"/>
        <location filename="viewmanager.js" line="591"/>
        <source>Sim</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="401"/>
        <source>Editar Registro</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="426"/>
        <source>Criar Meta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="427"/>
        <source>Deseja realmente salvar a nova meta?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="436"/>
        <source>Editar Meta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="520"/>
        <location filename="viewmanager.js" line="530"/>
        <location filename="viewmanager.js" line="552"/>
        <location filename="viewmanager.js" line="562"/>
        <source>Excluir dados</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="521"/>
        <location filename="viewmanager.js" line="553"/>
        <source>Deseja realmente remover o item selecionado?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="531"/>
        <location filename="viewmanager.js" line="563"/>
        <source>Deseja realmente remover oDeseja realmente remover o</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="534"/>
        <location filename="viewmanager.js" line="566"/>
        <source> ite iteselecionadoselecionado</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="589"/>
        <source>Sair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="viewmanager.js" line="590"/>
        <source>Deseja realmente sair do aplicativo?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
