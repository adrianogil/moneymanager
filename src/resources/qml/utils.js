var errorMsgObjs = []

var textErrorComponent = Qt.createComponent("qrc:/qml/TextError.qml")
var textSignalErrorComponent = Qt.createComponent("qrc:/qml/TextSignalError.qml") 

function removeAllErrorMessages() {
    for (var i = 0; i < errorMsgObjs.length; i++) {
        if (errorMsgObjs[i]) {
            errorMsgObjs[i].opacity = 0
            errorMsgObjs[i].destroy(1000)
        }
    }
}

function addErrorMessage(errorMsg, targetEdit, parent, addSignal) {
    if (addSignal == null)
        addSignal = true;
    if (textErrorComponent.status == Component.Ready) {
        var text1Obj = textErrorComponent.createObject(parent);
        text1Obj.targetEdit = targetEdit;
        text1Obj.text = errorMsg;
        errorMsgObjs[errorMsgObjs.length] = text1Obj;
        if (addSignal) {
            var text2Obj = textSignalErrorComponent.createObject(parent);
            text2Obj.targetEdit = targetEdit;
            errorMsgObjs[errorMsgObjs.length] = text2Obj;
        }
    } else {
        console.log("utils.js:addErrorMessage - " + textErrorComponent.errorString())
    }
}

function monthsDiff(initialDate, finalDate)
{
    var firstDate = new Date (parseInt(initialDate.slice(6,8)),
                              parseInt(initialDate.slice(3,5)) -1,
                              parseInt(initialDate.slice(0,2)))

    var lastDate = new Date (parseInt(finalDate.slice(6,8)),
                             parseInt(finalDate.slice(3,5)) -1,
                             parseInt(finalDate.slice(0,2)))

    var d = (lastDate.getMonth() + 1 + lastDate.getYear() * 12) -
            (firstDate.getMonth() + 1 + firstDate.getYear() * 12)

    var days = (lastDate.getDay() - 30) + (30 - firstDate.getDay())
    if (days > 25) d = d + 1

    console.log("Valor da diferença"+d)

    return d;
}

function setTimerFunction(interval, callback, parent)
{
    var timerObj  = Qt.createQmlObject("import QtQuick 1.0; Timer {} ",
                                             parent, 'customTimer')
    timerObj.interval = interval;
    timerObj.triggered.connect(function() {
                                   callback();
                                   timerObj.destroy(1000);
                               })
    timerObj.start()
}
