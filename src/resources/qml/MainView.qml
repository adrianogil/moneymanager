﻿import QtQuick 1.0
import "mmportraitcommon" as Common
import "mmportraitcommon/calendar" as Calendar
import "mmportraitcommon/calculator" as Calculator
import "viewmanager.js" as ViewManager

Common.View {
    id: mainView

    property string subview: ""

    signal allViewRegistered
    signal closeOthersViews()

    function startView() {
        pageSlider.init()
        superiorMenu.state = "login";
    }

    viewId: "mainView"
//    width: 360; height: 640 // Layout_Symbian
//    width: 480; height: 842 //Layout_Meego
    onAllViewRegistered: startView()
    onActionActivated: {
        switch(action) {
        case "cashflow-list":
            ViewManager.cancelAddNewMoney();
            break;
        case "money-agreed2add":
            ViewManager.addNewMoney();
            break;
        case "money-agreed2edit":
            ViewManager.editMoney();
            break;
        case "money-agreed2remove":
            ViewManager.removeSelectedMoney();
            break;
        case "money-agreed2remove-list":
            ViewManager.removeSelectedMoneyList();
            break;
        case "login-after-account-creation":
            ViewManager.createNewUser();
            break;
        case "cancel-new-account":
            ViewManager.cancelNewAccount();
            break;
        case "goal-agreed2add":
            ViewManager.addNewGoal();
            break;
        case "goal-agreed2edit":
            ViewManager.updateGoal();
            break;
        case "goal-cancel":
            ViewManager.cancelAddGoal();
            break;
        case "goal-agreed2remove":
            ViewManager.removeSelectedGoal();
            break;
        case "goal-agreed2remove-list":
            ViewManager.removeSelectedGoalList();
            break;
        case "agreed2exit":
            Qt.quit();
            break;
        default:
            break;
        }
    }
    Common.SuperiorMenu {
        id: superiorMenu
        anchors.top: mainView.top
        onStateChanged: {
            mainView.state = state
            buttonMenu.state = "hide"
        }
    }
    PageSlider {
        id: pageSlider
        x: 0; y: superiorMenu.height;
        width: mainView.width

        Common.NewAccountView {
            id: newAccountView
        }
        Common.LoginBox {
            id: loginView
        }
        Common.CashFlowView {
            id: cashFlowView
        }
        Common.MoneyAddView {
            id: moneyAddView
            subview: mainView.subview
        }
        Common.ResumeView {
            id: resumeView
        }
        Common.CategoriesView {
            id: categoriesView
        }
        Common.TargetsView {
            id: targetView
        }
        Common.TargetsAdd {
            id: targetAdd
        }
    }
    Column {
        anchors.bottom: mainView.bottom
        spacing: -1
        Common.BottomMenu {
            id: bottomMenu
            onClicked: {
                console.log("main.qml - " + btnClicked + " " + bottomMenu.state)
                ViewManager.manageMenuActions(btnClicked);
             }
        }
        Common.ButtonMenu {
            id: buttonMenu
            onClicked: ViewManager.managerOthersView(action)
        }
    }
    MouseArea {
        width: mainView.width; height: mainView.height - buttonMenu.height
        enabled: buttonMenu.state == "show"
        onClicked: buttonMenu.state = "hide"
    }

    onCloseOthersViews: ViewManager.changeEnable()


    Common.HelpView {
        id:helpview
        visible:false
        onCloseHelp:closeOthersViews()
    }
    Common.AboutView {
        id:aboutview
        visible:false
        onCloseAbout: closeOthersViews()
    }
    Common.ContactView{
        id:contactview
        visible:false
        onCloseContactView: closeOthersViews()
    }
    Common.DropboxView {
        id: dropboxView
        visible: false
        onCloseDropboxView: closeOthersViews()
    }
    Common.PasswordRecoveryView {
        id: recoveryPassword
        anchors.centerIn: mainView
        visible: false
        onChangedPassword: ViewManager.changePassword(login,newPassword)
        onExit: ViewManager.enable()
    }
    Common.KeyboardInputScreen {
        id: keyboard
        opacity: 0
    }
    Calendar.CalendarView {
        id: calendar
        opacity: 0
    }
    Calculator.CalculatorView {
        id: calculator
        opacity: 0
    }
    Common.ProgressIndicator {
        id: progressIndicator
    }
    Connections {
        target: daoManager
        onStartDropDown: ViewManager.createDropDown(list, propertyName)
        onStartMessageBox: ViewManager.createMessageBox(title,
                                                        message,
                                                        options,
                                                        viewList,
                                                        stateList,
                                                        actionList)
    }
    onStateChanged: {
        ViewManager.flowManager(state,subview);
    }
    onSubviewChanged: {
        ViewManager.flowManager(state,subview);
    }


}
