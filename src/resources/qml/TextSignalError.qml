import QtQuick 1.0

Text {
    id: signalErrorMsg

    property QtObject targetEdit

    y: targetEdit.y; anchors.left: targetEdit.right
    anchors.leftMargin: 10
    width: targetEdit.width
    color: "red"; text: "*"
}
