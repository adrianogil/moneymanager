import QtQuick 1.0

Rectangle{
    id: popUpButton
    width: 130; height:44
    color: "#808080"

    property string name: "Editar"

    signal clicked()

    Text{
        id: textButton
        text: name
        color:"white"
        anchors.left: parent.left
        anchors.margins: 5
        anchors.verticalCenter: parent.verticalCenter
    }

    MouseArea{
        anchors.fill: popUpButton
        onClicked: popUpButton.clicked()
        onPressed: popUpButton.state="pressed"
        onExited: popUpButton.state="released"
        onReleased: popUpButton.state="released"

    }

    states: [
    State{
            name: "pressed"
            PropertyChanges {target:textButton; color:"#00adef"}
        },
    State{
             name: "released"
            PropertyChanges {target:textButton; color:"white"}
        }
    ]

    state: "released"
}
