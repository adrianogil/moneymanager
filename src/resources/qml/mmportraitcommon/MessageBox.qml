import QtQuick 1.0

Rectangle {
    id: messageBox
    color: "transparent"

    property string title: ""
    property string message: ""
    property variant options: []
    property variant viewList: []
    property variant stateList: []
    property variant actionList: []

    signal choosedOption(int optionValue)

    //width: 360; height: 640
    anchors.fill: parent

    onChoosedOption: {
        if (optionValue >= 0 && optionValue < viewList.length &&
                optionValue < stateList.length &&
                optionValue < actionList.length) {
            daoManager.setMessageBoxValue(viewList[optionValue],
                                          stateList[optionValue],
                                          actionList[optionValue])
            messageBox.opacity = 0;
            messageBox.visible = false;
        }
    }
    Rectangle {
        anchors.fill: messageBox
        color: "black"; opacity: 0.9
        MouseArea {anchors.fill: parent; }
    }
    Rectangle {
        width: 345; height: 173
        anchors.centerIn: messageBox

        Rectangle {
            id: titleHeader

            width: 345; height: 53
            color: Qt.rgba(204/255,204/255,204/255,1)
            Text {
                textFormat: Text.RichText
                anchors.fill: titleHeader
                anchors.leftMargin: 10
                horizontalAlignment: Text.AlignLeft
                verticalAlignment: Text.AlignVCenter
                text: messageBox.title
                font {pixelSize: 17; weight: Font.Bold}
            }
        }

        Item {
            id: messageItem

            y: 54
            width: 345; height: messageText.height + 30
            Text {
                id: messageText

                y: 15; x: 15
                textFormat: Text.RichText
                width: messageItem.width - 30
                text: messageBox.message
                wrapMode: Text.WordWrap
                font {pixelSize: 16}
            }
        }

        Item {
            id: buttonsItem

            x: 7; y: messageItem.height + 54
            width: 330; height: 41
            ListView {
                anchors.fill: buttonsItem
                model: messageBox.options
                spacing: 10; interactive: false
                orientation: ListView.Horizontal
                delegate: Button {
                    text: modelData
                    onClicked: messageBox.choosedOption(index)
                }
            }
        }
    }
}
