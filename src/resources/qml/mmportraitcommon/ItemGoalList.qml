import QtQuick 1.0
import Entity 1.0
import "../utils.js" as Common

ExpansiveListItem {
    id: itemGoalList

    property string name: goal? goal.description : ""
    property int value: goal ? goal.percentage : 0
    property QtObject goal

    function startAnimation() {
        openAnimation.restart()
    }

    x: 0;
    width: parent.width;
    backgroundPath: "qrc:/images/cashflow/item_background.png"
    backgroundPath_a: "qrc:/images/goal/item_background.png"
    backgroundPath_expanded: "qrc:/images/goal/item_background_expanded.png"
    backgroundPath_expanded_a: "qrc:/images/goal/item_background_expanded.png"
    backgroundPath_shadow: "qrc:/images/cashflow/item_background_shadow.png"
    backgroundPath_shadow_a: "qrc:/images/cashflow/item_background_shadow_a.png"

    NumberAnimation {
        id: openAnimation
        running: false
        target: progressBar
        property: "value"
        to: itemGoalList.value
        from: 0
    }
    onValueChanged: {
        console.log("itemGoalList item " + index + "- valueChanged " + value)
        //progressBarAnimationTimer.start()
        progressBar.value = itemGoalList.value
    }
    Timer {
        id: progressBarAnimationTimer
        running: false
        interval: 200
        repeat: false
        onTriggered: progressBar.value = itemGoalList.value
    }
    Text {
        x: 10; y: 20

        text: name + "   " + value + "%"
        font { pixelSize: 12; weight: Font.DemiBold; }
    }
    ProgressBar {
        id: progressBar

        function getBarColor(progressValue) {
            if (progressValue < 40) {
                return "#e53f3f"
            } else if (progressValue < 70) {
                return "#00adef"
            } else if (progressValue < 99) {
                return "#0e18e7"
            }

            return "#22e722"
        }

        x: 10; y: 40
        height: 10

        barColor: getBarColor(value)

        Behavior on value {
            NumberAnimation {
                duration: 600
            }
        }
    }
    Text {
        x: 10; y: 66

        text: "<b>"+qsTr("Prazo:")+"</b>" + goal.initialDate + "à" + goal.finalDate
//        text: "<b>"+qsTr("Prazo:")+"</b>" + goal.initialDate + qsTr("à") + goal.finalDate
        font { pixelSize: 12; weight: Font.Normal; }
        visible: expanded
    }
    Text {
        x: 10; y: 86

        text: "<b>"+qsTr("Valor na data final:")+"</b>" + goal.target
        font { pixelSize: 12; weight: Font.Normal; }
        visible: expanded
    }
    Text {
        id: labelInvestiment
        x: 10; y: 106

        font { pixelSize: 12; weight: Font.Normal; }
        visible: expanded
    }
    Text {
        id: labelSale
        x: 10; y: 126

        font { pixelSize: 12; weight: Font.Normal; }
        visible: expanded
    }

//    onExpandedChanged: {
//        if (expanded) {
//            var sale = daoManager.allTimeSale

//            var percentage =  (100*sale) / goal.target;
//            console.log("888888888888888888888888888888888  "+percentage)
//            goal.percentage = percentage;

//            var investimentValue = (parseFloat(goal.target) - sale)/
//                Common.monthsDiff(new String(goal.initialDate), new String(goal.finalDate))
//            investimentValue = parseInt(investimentValue * 100)/100

//            labelInvestiment.text = "<b>Parcela mensal:</b>  " + investimentValue
//            labelSale.text = "<b>Saldo atual:</b>  " + sale

//        }
//    }
    Component.onCompleted: {
        var sale = daoManager.allTimeSale

        var percentage =  (100*sale) / goal.target;
        console.log("888888888888888888888888888888888  "+percentage)
        if (percentage < 0) percentage = 0
        else if (percentage > 100) percentage = 100

        goal.percentage = percentage;

        var investimentValue = (parseFloat(goal.target) - sale)/
            Common.monthsDiff(new String(goal.initialDate), new String(goal.finalDate))
        investimentValue = (investimentValue <= 0) ? 0 : parseInt(investimentValue * 100)/100

        labelInvestiment.text = "<b>Parcela mensal:</b>  " + investimentValue
        labelSale.text = "<b>Saldo atual:</b>  " + sale
                               }
}

