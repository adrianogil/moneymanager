import QtQuick 1.0

View {
    id: superiorMenu

    viewId: "superior-menu-view"
    width: flowButton.width + resumoButton.width + metasButton.width;
    height: flowButton.height

    Rectangle {
        id: background
        anchors.fill: superiorMenu
        color: "#535252"
    }
    SuperiorMenuButton{
        id: flowButton

        normalIconPath: 'qrc:/images/menu/caixa_cinza.png'
        selectedIconPath: 'qrc:/images/menu/caixa_azul.png'
        selectedImagePath: 'qrc:/images/menu/caixa_azul.png'
        backgroundImagePath: 'qrc:/images/menu/caixa_cinza.png'
        anchors.left: parent.left
        onClicked: superiorMenu.state = 'cashflow'
    }
    SuperiorMenuButton{
        id: resumoButton
        normalIconPath: 'qrc:/images/menu/resumo_cinza.png'
        selectedIconPath: 'qrc:/images/menu/resumo_azul.png'
        selectedImagePath: 'qrc:/images/menu/resumo_azul.png'
        backgroundImagePath: 'qrc:/images/menu/resumo_cinza.png'
        anchors.left: flowButton.right
        onClicked: superiorMenu.state = 'resume'
    }
    SuperiorMenuButton{
        id: metasButton
        normalIconPath: 'qrc:/images/menu/metas_cinza.png'
        selectedIconPath: 'qrc:/images/menu/metas_azul.png'
        selectedImagePath: 'qrc:/images/menu/metas_azul.png'
        backgroundImagePath: 'qrc:/images/menu/metas_cinza.png'
        anchors.left: resumoButton.right
        onClicked: superiorMenu.state = 'goal'
    }

    SuperiorMenuButton{
        id: loginButton
        normalIconPath: 'qrc:/images/menu/entrar_cinza.png'
        selectedIconPath: 'qrc:/images/menu/entrar_azul.png'
        backgroundImagePath: 'qrc:/images/menu/entrar_cinza.png'
        selectedImagePath: 'qrc:/images/menu/entrar_azul.png'
        anchors.right: parent.right
        onClicked: superiorMenu.state = 'login'
    }

    SuperiorMenuButton{
        id: newAccButton
        normalIconPath: 'qrc:/images/menu/novaconta_cinza.png'
        selectedIconPath: 'qrc:/images/menu/novaconta_azul.png'
        selectedImagePath: 'qrc:/images/menu/novaconta_azul.png'
        backgroundImagePath: 'qrc:/images/menu/novaconta_cinza.png'
        anchors.left: parent.left
        onClicked: superiorMenu.state = 'new-account'
   }


    states: [
        State { name: 'login';
            PropertyChanges { target: loginButton; state: 'selected' }
            PropertyChanges { target: newAccButton; state: 'normal' }
        },
        State { name: 'new-account';
            PropertyChanges { target: newAccButton; state: 'selected' }
            PropertyChanges { target: loginButton; state: 'normal' }
        },
        State { name: 'cashflow';
            PropertyChanges { target: flowButton; state: 'selected' }
            PropertyChanges { target: resumoButton; state: 'normal' }
            PropertyChanges { target: metasButton; state: 'normal' }
            PropertyChanges { target: loginButton; visible: false }
            PropertyChanges { target: newAccButton; visible: false }
        },
        State { name: 'resume';
            PropertyChanges { target: resumoButton; state: 'selected' }
            PropertyChanges { target: flowButton; state: 'normal' }
            PropertyChanges { target: metasButton; state: 'normal' }
            PropertyChanges { target: loginButton; visible: false }
            PropertyChanges { target: newAccButton; visible: false }
        },
        State { name: 'goal';
            PropertyChanges { target: resumoButton; state: 'normal' }
            PropertyChanges { target: flowButton; state: 'normal' }
            PropertyChanges { target: metasButton; state: 'selected' }
            PropertyChanges { target: loginButton; visible: false }
            PropertyChanges { target: newAccButton; visible: false }
        }
    ]
}
