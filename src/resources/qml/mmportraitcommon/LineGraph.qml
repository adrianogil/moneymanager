import QtQuick 1.0
import Charts 1.0

Flickable {
    width:  parent.width; height: 400
    contentWidth: lineGraph.width; contentHeight: lineGraph.height
    clip: true

    WidgetChart {
        id: lineGraph
        visible: true
        typeChart: 0
        moneyList: daoManager.moneyList
        width:  600; height: 404
    }

}
