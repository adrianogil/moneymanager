import QtQuick 1.0
import "calendar.js" as CalendarManager

Rectangle {
    id: calendarcomponent

    property int pixelSize: 23

    property alias day: days.currentIndex
    property alias month: months.currentIndex
    property alias year: years.currentIndex

    signal sendMonth(string selectedMonth)
    signal sendDay(string selectedDay)
    signal sendYear(string selectedYear)

    signal sendFormatedDate(string formatDate) // in format DD/MM/YY

    function setDate(d,m,y) {
        day = d - 1;
        month = m - 1;
        year = y - 1;
        correctDaysModel();
    }
    function setDateFromString(strdate) { // in format DD/MM/YY
        var dateParts = strdate.split("/");
        setDate(dateParts[0],dateParts[1],dateParts[2]);
    }
    function setCurrentDate() {
        var today = new Date()
        day = today.getDate();
        month = today.getMonth();
        year = today.getFullYear() - 2001;
        correctDaysModel();
    }

    function getFormatedDate() {
        var strDay = CalendarManager.getSelectedDay()
        if (strDay < 10)
            strDay = '0' + strDay
        var strMonth = CalendarManager.getSelectedMonth()
        if (strMonth < 10)
            strMonth = '0' + strMonth
        var strYear = CalendarManager.getSelectedYear()
        if (strYear < 10)
            strYear = '0' + strYear
        var strDate = strDay + "/" + strMonth + "/" + strYear

        return strDate;
    }

    function correctDaysModel() {
        CalendarManager.getMaxDays(months.currentIndex,
                                   yearsModel.get(years.currentIndex).numberYear)
    }

    width: 340; height: 240

    ListModel {
        id: daysModel
    }
    ListModel {
        id: monthsModel
    }
    ListModel {
        id: yearsModel
    }

    onOpacityChanged: sendFormatedDate(getFormatedDate())

    Item {
        id: calandercolumns

        Row {
            MonthsView {
                id: months
                property variant listMonthsNames: [ qsTr("Janeiro"), qsTr("Fevereiro"), "Março",
                    qsTr("Abril"), qsTr("Maio"), qsTr("Junho"),
                    qsTr("Julho"), qsTr("Agosto"), qsTr("Setembro"),
                    qsTr("Outubro"), qsTr("Novembro"), qsTr("Dezembro") ]
//                property variant listMonthsNames: [ qsTr("Janeiro"), qsTr("Fevereiro"), "Março",
//                    qsTr("Abril"), qsTr("Maio"), qsTr("Junho"),
//                    qsTr("Julho"), qsTr("Agosto"), qsTr("Setembro"),
//                    qsTr("Outubro"), qsTr("Novembro"), qsTr("Dezembro") ]
                width: calendarcomponent.width/2; height: calendarcomponent.height
                model: monthsModel
                onMovementEnded: correctDaysModel()
                itemHeight: 35
                delegate: Component {
                    id: monthscomponent;
                    Text {
                        font.pixelSize: calendarcomponent.pixelSize;
                        text: listMonthsNames[numberMonth-1];
                        height: 50; verticalAlignment: Text.AlignVCenter
                    }
                }
            }
            Rectangle {
                width: 5; height: calendarcomponent.height
                color: "#00adef"
            }
            DaysView {
                id: days
                width: calendarcomponent.width/5; height: calendarcomponent.height
                model: daysModel
                currentIndex: 0
                itemHeight: 35
                delegate:Component {
                    id: dayscomponent;
                    Text {
                        font.pixelSize: calendarcomponent.pixelSize;
                        text: numberDay; height: 50;
                        verticalAlignment: Text.AlignVCenter
                    }
                }
            }
            Rectangle {
                width: 5; height: calendarcomponent.height
                color: "#00adef"
            }
            YearsView {
                id: years
                width: calendarcomponent.width - 10 - months.width - days.width;
                height: calendarcomponent.height
                model: yearsModel
                currentIndex: 0
                itemHeight: 35
                delegate:Component{ id: yearcomponent;
                    Text {
                        font.pixelSize: calendarcomponent.pixelSize;
                        text:numberYear; height: 50; verticalAlignment: Text.AlignVCenter
                    }
                }
            }
        }

        Component.onCompleted: {
            CalendarManager.getMonths()
            CalendarManager.getYears()
            CalendarManager.getDays()
            CalendarManager.actualDay()
        }

    }


}
