function getYears() {
    var years = 2001
    yearsModel.clear()
    for (years; years <= 2050; years++){
        yearsModel.append({"numberYear": years})
    }
}

function getDays(maxDays) {
    if (maxDays == null)
        maxDays = 31;
    var days = 1
    daysModel.clear()
    for (days; days <= maxDays; days++){
        daysModel.append({"numberDay": days})
    }
}

function getMaxDays(m,y) {
    // Find out how much days have such month
    var overDate = new Date(y, m, 32)
    var maxDays = 32 - overDate.getDate();

    var currentDay = days.currentIndex;

    console.log("calendar.js - Setting max days to " +
                "(" + y + "," + m + ") - " + overDate + " : " + maxDays)

    //getDays(maxDays);

    var currentMaxDays = daysModel.get(daysModel.count-1).numberDay
    var diffMaxDays = maxDays - currentMaxDays;

    if (diffMaxDays < 0) {
        for (var i = 0; i < Math.abs(diffMaxDays); i++)
            daysModel.remove(daysModel.count-1)
    } else if (diffMaxDays > 0) {
        for (var i = currentMaxDays+1; i <= maxDays; i++)
            daysModel.append({"numberDay" : i})
    }


    if (currentDay <= (maxDays-1))
        days.currentIndex = currentDay
    else days.currentIndex = (maxDays-1)
}

function getMonths() {
    var month = 1
    monthsModel.clear()
    for (month; month <= 12; month++){
        monthsModel.append({"numberMonth": month})
    }
}

function actualDay() {
    var actualDate = new Date()
    days.currentIndex =  actualDate.getDate() -1
    months.currentIndex = actualDate.getMonth()
    years.currentIndex = actualDate.getFullYear() -1
}

function getSelectedMonth(){
    return months.currentIndex +1;
}

function getSelectedDay(){
    return days.currentIndex +1;
}

function getSelectedYear(){
    return years.currentIndex +1;
}
