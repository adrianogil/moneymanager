import QtQuick 1.0
import "../" as Common

Item {
    id: calendarView

    signal exitCalendarView

    width: parent.width; height: parent.height

//    onOpacityChanged: console.log("CalendarView.qml:onOpacityChanged - " +
//                                  opacity)
    onExitCalendarView: {
        console.log("CalendarView.qml:onExitCalendarView - " + calendar.getFormatedDate())
        daoManager.calendarValue = ""
        daoManager.calendarValue = calendar.getFormatedDate()
        daoManager.calendarActivated = true
        daoManager.calendarActivated = false
        calendarView.opacity = 0
    }

    Rectangle {
        id: background
        anchors.fill: parent
        color: "black"; opacity: 0.7
    }
    MouseArea {
        anchors.fill: calendarView
        onClicked: {
            calendarView.exitCalendarView()
        }
    }
    Rectangle {
        id: calendarRect

        width: 344; height: 370
        anchors.centerIn: calendarView

        Column {
            Rectangle {
                id: titleBar
                height: 52; width: calendarRect.width
                color: "#00adef"
                Text { x:12; y: 22; text: "Calendário"; color: "white"; font.pixelSize: 18; }
//                Text { x:12; y: 22; text: qsTr("Calendário"); color: "white"; font.pixelSize: 18; }
            }
            CalendarComponent {
                id: calendar
                width: calendarRect.width; height: 263
            }
            Rectangle {
                id: buttonBar
                height: 54; width: calendarRect.width
                Common.Button {
                    id: okButton
                    x: 6; y: 6
                    text: qsTr("Ok"); textImgVisible: true
                    buttonImage: "qrc:/images/dropbox/menu_dropbox_blue.png"
                    pressedImage: "qrc:/images/dropbox/menu_dropbox_gray.png"
                    onClicked: {
                        calendarView.exitCalendarView();
                    }
                }
                Common.Button {
                    id: cancelButton
                    x: 177; y: 6
                    text: qsTr("Cancelar"); textImgVisible: true
                    buttonImage: "qrc:/images/dropbox/menu_dropbox_blue.png"
                    pressedImage: "qrc:/images/dropbox/menu_dropbox_gray.png"
                    onClicked: {
                        daoManager.calendarActivated = false
                        calendarView.opacity = 0
                    }
                }
            }
        }
    }
    Connections {
        target: daoManager
        onCalendarActivatedChanged: {
            console.log("CalendarView.qml - " + daoManager.calendarActivated)
            if (daoManager.calendarActivated) {
                // calendarValue must be formatted as 'dd/MM/yyyy'
                calendar.setDateFromString(daoManager.calendarValue)
                calendarView.opacity = 1
            }
        }
    }

}

