import QtQuick 1.0

Rectangle{
    id: titleBox
    width: 360 ; height:50
    color:"lightGray"

    property string text1:""

    Text{
        color : "darkGray"
        text:  text1
        font { pixelSize: 20; family: "Series 60 Sans"; weight: Font.DemiBold }
        horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter
        anchors.fill: parent
    }
}
