import QtQuick 1.0

Rectangle {
    id: popupButtons0
    height: 55
    width: 80
    color: "#00ADEF"
    radius: 6

    //property bool toggled: false
    property int itemNumber: 0

    signal firstButtonClicked
    signal secondButtonClicked

    Text {
        id: firstTextButton
        width: parent.width; height: parent.height/2
        color: "white"
        text: qsTr("Adicionar")
        font.pixelSize: 12
        horizontalAlignment: "AlignHCenter"
        verticalAlignment: "AlignVCenter"
    }
    Text {
        width: parent.width; height: parent.height/2
        color: "white"
        text: qsTr("Remover")
        anchors.top: firstTextButton.bottom
        font.pixelSize: 12
        horizontalAlignment: "AlignHCenter"
        verticalAlignment: "AlignVCenter"
    }
    Rectangle {
        id: separatePopupButton
        x: 0; y: parent.height/2
        width: parent.width; height: 1
        color: "#4EC6F4"
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (mouseY >= 0 && mouseY <= parent.height/2) {
                console.debug("Adding");
                popupButtons0.firstButtonClicked();
            }
            else {
                console.debug("Removing");
                popupButtons0.secondButtonClicked();
            }
        }
    }
}

