﻿import QtQuick 1.0
import "../utils.js" as Utils

View {
    id: dropboxView

    property bool progressDialogActive: false

    signal closeDropboxView

    width: parent.width; height: parent.height
    viewId: "dropbox-view"

    onCloseDropboxView: {
        dropboxView.state = "login"
        visible = false;
        Utils.removeAllErrorMessages()
    }

    Rectangle {
        id: darkBackground
        anchors.fill: parent
        color: "black"; opacity: 0.7
        MouseArea {
            anchors.fill: parent
        }
    }
    Item {
        id: dropboxItem

        anchors.centerIn: dropboxView
        width: parent.width * 0.95; height: parent.height * 0.8

        Column {
            Rectangle {
                id: title
                width: dropboxItem.width; height: 53
                color: "#00adef"
                Text {
                    x: 12
                    height: title.height
                    text: qsTr("Dropbox")
                    color: "white";
                    font { pixelSize: 20; }
                    verticalAlignment: Text.AlignVCenter
                }
            }
            Rectangle {
                id: loginRectangle
                visible: dropboxView.state == "login"
                width: dropboxItem.width;
                height: dropboxItem.height - title.height
                color: "white"

                Text {
                    x: 24; y: 114
                    text: "Faça o login com sua conta Dropbox:"
//                    text: qsTr("Faça o login com sua conta Dropbox:")
                    font{pixelSize: 16}
                }

                Column {
                    id: loginColumn
                    anchors.centerIn:loginRectangle
                    spacing: 20
                    LineEdit {
                        id: emailLineEdit
                        width: 240; height: 50
                        label: qsTr("Email")
                        text: ""
                    }
                    LineEdit {
                        id: passwordLineEdit
                        width: 240; height: 50
                        label: qsTr("Senha")
                        echoMode: TextInput.Password
                        text: ""
                    }

                    Rectangle{
                        id: mouseAreaRect
                        width: checkSaveData.width*2; height: checkSaveData.height*2
                        color: "transparent"

                    Rectangle{
                        id:checkSaveData
                        width: 25; height: 25
                        anchors.centerIn: mouseAreaRect
                        radius: 30; smooth: true
                        border.color: "#999999"
                    }

                    Rectangle {
                        id: checkImage
                        visible: false
                        width: checkSaveData.width - 10 ; height: checkSaveData.height - 10
                        color:  "#00adef"
                        radius: 30; smooth: true
                        anchors.centerIn: checkSaveData
                    }

                    Text {
                        id: checkText
                        text: "Lembrar Senha"
                        color: "#00adef"
                        anchors.verticalCenter: mouseAreaRect.verticalCenter
                        anchors.left: checkSaveData.left
                        anchors.leftMargin: checkSaveData.width +5
                    }

                    MouseArea{
                       anchors.fill: mouseAreaRect
                        onClicked: {
                            checkImage.visible = !checkImage.visible
                            if(checkImage.visible == true){
                                daoManager.savedDropboxPassword = passwordLineEdit.text
                                daoManager.savedDropboxName = emailLineEdit.text
                            }
                            else{
                                daoManager.savedDropboxPassword = ""
                                daoManager.savedDropboxName = ""
                            }
                        }

                    }
                    }
                }
                Text {
                    id: loginErrorMsg;
                    anchors.top: loginColumn.bottom
                    anchors.topMargin: 10
                    width: loginRectangle.width
                    color: "red"
                    visible: false
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                }
            }
            Rectangle {
                id: operationsView
                visible: dropboxView.state == "user-logged"
                width: dropboxItem.width;
                height: dropboxItem.height - title.height;
                color: "white"

                Text {
                    x: 30; y: 27
                    text: "Escolha o tipo de operação:"
//                    text: qsTr("Escolha o tipo de operação:")
                    font { pixelSize: 15 }
                }
                LineEdit {
                    id: operationEdit

                    x: 30; y: 50
                    width: 200;

                    label: "Operação no Dropbox"
//                    label: qsTr("Operação no Dropbox")
                    text: daoManager.dropboxOption
                    MouseArea {
                        anchors.fill: operationEdit
                        onClicked: {
                            daoManager.openDropDown(daoManager.dropboxOptionsList(),
                                                    "dropboxOption")
                        }
                    }
                }
                Text {
                    x: 30; y: 121
                    width: 268
                    text: "Selecionar todos os dados cadastrados entre o período:"
//                    text: qsTr("Selecionar todos os dados cadastrados entre o período:")
                    font { pixelSize: 15 }
                    wrapMode: Text.WordWrap
                }
                LineEdit {
                    id: textInitialDate;
                    x: 20; y: 169; width: 145;
                    label: "Data de início";
//                    label: qsTr("Data de início");
                    type: 1
                    onTextFilled: {
                        if (dropboxManager.dropboxOption == qsTr("Sincronizar"))
                            dropboxManager.initialDate = text;
                        else if (dropboxManager.dropboxOption == qsTr("Salvar"))
                            daoManager.initialExportDate = text;
                    }
                }
                LineEdit {
                    id: textFinalDate;
                    x: 180; y: 169; width: 145;
                    type: 1; label: qsTr("Data final");
                    onTextFilled: {
                        dropboxManager.finalDate = text;
                    }
                }
                Text {
                    x: 30; y: 235
                    visible: daoManager.dropboxOption == qsTr("Sincronizar")
                    text: qsTr("Escolha um arquivo para Sync:")
                    font { pixelSize: 15 }
                }
                LineEdit {
                    id: fileChooseInput

                    x: 30; y: 255
                    width: 200;
                    visible: daoManager.dropboxOption == qsTr("Sincronizar")

                    label: qsTr("Nome do Arquivo")
                    text: daoManager.csvFilename
                    MouseArea {
                        anchors.fill: fileChooseInput
                        onClicked: {
                            daoManager.openDropDown(dropboxManager.csvFilesList(),
                                                    "csvFilename")
                        }
                    }
                }
                Text {
                    id: csvFileNameErrorMsg
                    anchors.top: fileChooseInput.bottom
                    anchors.topMargin: 10
                    width: fileChooseInput.width
                    color: "red"
                    visible: false
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                }
                Text {
                    id: csvFileNameEmptyErrorMsg
                    y: fileChooseInput.y; anchors.left: fileChooseInput.right
                    anchors.leftMargin: 10
                    width: fileChooseInput.width
                    color: "red"; text: "*"
                    visible: false
                }
                Text {
                    id: dateIntervalErrorMsg
                    anchors.top: fileChooseInput.bottom
                    anchors.topMargin: 10
                    width: fileChooseInput.width
                    color: "red"
                    visible: false
                    wrapMode: Text.WordWrap
                    horizontalAlignment: Text.AlignHCenter
                }
                Text {
                    id: dateIntervalSignalErrorMsg
                    y: fileChooseInput.y; anchors.left: fileChooseInput.right
                    anchors.leftMargin: 10
                    width: fileChooseInput.width
                    color: "red"; text: "*"
                    visible: false
                }

                LineEdit {
                    id: filenameLineEdit

                    x: 30; y: 255
                    width: 200;
                    visible: daoManager.dropboxOption == qsTr("Salvar")

                    label: qsTr("Nome do arquivo")
                    text: dropboxManager.backupFilename
                    onTextFilled: dropboxManager.backupFilename = text;
                }
                Text {
                    x: 30; y: 335
                    width: 267
                    text: "Obs: A sincronização ou exportação de arquivos neste processo " +
                               " pode acarretar a perda parcial de dados."
//                    text: qsTr("Obs: A sincronização ou exportação de arquivos neste processo " +
//                               " pode acarretar a perda parcial de dados.")
                    font { pixelSize: 12 }
                    opacity: 0.7
                    wrapMode: Text.WordWrap
                }
            }
            Rectangle {
                id: csvResume
                visible: dropboxView.state == "sync-csv-resume"
                width: dropboxItem.width;
                height: dropboxItem.height - title.height;
                color: "white"

                Column {
                    anchors.centerIn: csvResume
                    spacing: 10
                    Text {
                        height: 20; width: 300; wrapMode: Text.WordWrap
                        text: qsTr("Arquivo encontrado: ") + daoManager.money2Sync.name
                    }
                    Text {
                        height: 20; width: 300; wrapMode: Text.WordWrap
                        text: qsTr("Registros cadastrados: ") + daoManager.money2Sync.entityList.length
                    }
                    Text {
                        height: 20; width: 300; wrapMode: Text.WordWrap
                        text: qsTr("Total entrada: ") + (Math.round(daoManager.money2Sync.totalIn*100)/100)
                    }
                    Text {
                        height: 20; width: 300; wrapMode: Text.WordWrap
                        text: qsTr("Total saída: ") + (Math.round(daoManager.money2Sync.totalOut*100)/100)
                    }
                    Text {
                        height: 20; width: 300; wrapMode: Text.WordWrap
                        text: qsTr("Total: ") + (Math.round(daoManager.money2Sync.total*100)/100)
                    }
                }
            }
            Rectangle {
                id: syncResume
                visible: dropboxView.state == "sync-resume"
                width: dropboxItem.width;
                height: dropboxItem.height - title.height;
                color: "white"

                Text {
                    anchors.fill: parent
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    text: qsTr("Sincronização completa!")
                }
            }

            Rectangle {
                id: buttonsRect
                y:  dropboxItem.height - buttonsRect.height -30; x:dropboxItem.width/2 - okButton.width
                height: Math.max(okButton.height, cancelButton.height)
                color: "white"
                Button {
                    id: okButton
//                    x: 6;
                    visible: dropboxView.state != "sync-csv-resume" || (
                                 daoManager.dropboxSyncLv != 3 &&
                                 daoManager.money2Sync.entityList.length > 0
                                 )
                    text: dropboxView.state == "sync-csv-resume"?
                              qsTr("Sincronizar") : (dropboxView.state == "sync-resume"? qsTr("Voltar") : qsTr("Ok"));
                    textImgVisible: true
                    buttonImage: "qrc:/images/dropbox/menu_dropbox_blue.png"
                    pressedImage: "qrc:/images/dropbox/menu_dropbox_gray.png"
                    onClicked: {
                        Utils.removeAllErrorMessages()
                        if (dropboxView.state == "login") {
                            if (emailLineEdit.text != "" && passwordLineEdit.text != "") {
                                daoManager.waitingMode = true;
                                daoManager.csvFilename = ""
                                dropboxManager.login(emailLineEdit.text, passwordLineEdit.text);
                                daoManager.dropboxSyncLv = 0;
                            }
                            else {
                                Utils.addErrorMessage(qsTr("Campos não preenchidos"),
                                                      loginColumn, loginRectangle, false);
                            }
                        } else if (dropboxView.state == "user-logged") {
                            if (dropboxManager.dropboxOption == qsTr("Salvar")) {
                                dropboxManager.exportCsvData(daoManager.exportCSVEntities())
                                progressDialogActive = true;
                            } else if (dropboxManager.dropboxOption == qsTr("Sincronizar")) {
                                if (daoManager.csvFilename != "") {
                                    dropboxManager.requestCsvEntities(daoManager.csvFilename)
                                    progressDialogActive = true;
                                } else {
                                    Utils.addErrorMessage(qsTr("Campo obrigatorio"),
                                                          fileChooseInput, operationsView);
                                }
                            } else {
                                Utils.addErrorMessage(qsTr("Campo obrigatorio"),
                                                      operationEdit, operationsView);
                            }
                        } else if (dropboxView.state == "sync-csv-resume" &&
                                   dropboxManager.dropboxOption == qsTr("Sincronizar")) {
                            daoManager.doSync();
                        } else if (dropboxView.state == "sync-resume" &&
                                   dropboxManager.dropboxOption == qsTr("Sincronizar")) {
                            dropboxView.state = "user-logged"
                        }
                    }
                }
                Button {
                    id: cancelButton
                    x: okButton.width + 5;
                    text: daoManager.dropboxSyncLv == 3? qsTr("Sair") : qsTr("Cancel"); textImgVisible: true
                    buttonImage: "qrc:/images/dropbox/menu_dropbox_blue.png"
                    pressedImage: "qrc:/images/dropbox/menu_dropbox_gray.png"
                    onClicked: {
                        dropboxView.closeDropboxView()
                    }
                }
            }
        }
    }

    Rectangle {
        id: darkBackground2
        anchors.fill: parent
        color: "black"; opacity: 0.7
        visible: progressDialogActive
        MouseArea { anchors.fill: darkBackground2}
    }
    Rectangle {
        id: progressDialog

        width: 300; height: 220; color: "white"
        anchors.centerIn: dropboxView
        border {width: 1; color: "#00adef"}
        radius: 10
        visible: progressDialogActive
        Text {
            y: 10
            width: progressDialog.width

            text: qsTr("Progresso da Operação");
            font { weight: Font.Bold; pixelSize: 16}
            horizontalAlignment: Text.AlignHCenter
        }
        Column {
            spacing: 15
            anchors.centerIn: progressDialog
            Text { text: dropboxManager.stateLabel }
            Text {
                property int progressValue: dropboxManager.progressValue
                Behavior on progressValue {
                    NumberAnimation {
                        duration: 300
                        easing.type: Easing.InCurve
                    }
                }
                text: progressValue + "%"
            }
            ProgressBar {
                barColor: "#00adef"; backgroundColor: "white"
                width: progressDialog.width - 20; height: 20
                value: dropboxManager.progressValue
            }
        }
        Button {
            id: confirmButton
            anchors.horizontalCenter: progressDialog.horizontalCenter
            anchors.bottom: progressDialog.bottom
            anchors.bottomMargin: 15
            text: qsTr("Ok"); textImgVisible: true
            buttonImage: "qrc:/images/dropbox/menu_dropbox_blue.png"
            pressedImage: "qrc:/images/dropbox/menu_dropbox_gray.png"
            onClicked: {
                progressDialogActive = false
                dropboxManager.progressValue = 0;
                dropboxManager.stateLabel = ""
                if (dropboxManager.dropboxOption == qsTr("Salvar")) {
                    dropboxView.closeDropboxView()
                }
            }
        }
    }


    Connections {
        target: dropboxManager
        onUserLogged: {
            console.log("DropboxView.qml - user logged");
            dropboxView.state = "user-logged"
            daoManager.waitingMode = false
        }
        onErrorMessage: {
            daoManager.waitingMode = false
            console.log("DropboxView.qml - error: " + errorMsg);
            if (dropboxView.state == "login") {
                Utils.addErrorMessage(errorMsg, loginColumn, loginRectangle, false);
            }
        }
        onReceivedCsvContents: {
            console.log("DropboxView.qml - received Csv Contents")
            daoManager.importCsvContents(csvFile, daoManager.csvFilename);
            dropboxView.state = "sync-csv-resume"
        }
    }
    // Workaround: DropDown can't refer to 'dropboxManager'
    Connections {
        target: daoManager
        onDropboxOptionChanged: dropboxManager.dropboxOption = daoManager.dropboxOption
    }

    state: "login"

    Component.onCompleted: {

        if(daoManager.savedDropboxPassword !== ""){
            passwordLineEdit.text = daoManager.savedDropboxPassword;
            checkImage.visible=true
        }
        if(daoManager.savedDropboxName !== ""){
            emailLineEdit.text = daoManager.savedDropboxName;
            checkImage.visible=true
        }
    }
}
