import QtQuick 1.0

Item {
    id: buttonmenu

    signal clicked (string action)
    property bool logoutEnabled: false

    width:about.width
    height:about.height
    clip:true

    Behavior on height{
        NumberAnimation{duration:500}
    }
    Column {
        id: column
        spacing: -1

        Button{
            id:about

            visible: true
            text: qsTr("Sobre")
            textImgVisible: true
            textColor: "#808080"
            buttonImage: "qrc:/images/bottom-menu/logout_Background.png"
            pressedImage: "qrc:/images/bottom-menu/logout_Background_pressed.png"
            onClicked: buttonmenu.clicked("showAbout")
        }
        Button{
            id:help

            visible:true
            text:qsTr("Ajuda")
            textImgVisible: true
            textColor: "#808080"
            buttonImage: "qrc:/images/bottom-menu/logout_Background.png"
            pressedImage: "qrc:/images/bottom-menu/logout_Background_pressed.png"
            onClicked: buttonmenu.clicked("showHelp")

        }
        Button{
            id:exporter

            visible: logoutEnabled
            text: qsTr("Exportar")
            textImgVisible: true
            textColor: "#808080"
            buttonImage: "qrc:/images/bottom-menu/logout_Background.png"
            pressedImage: "qrc:/images/bottom-menu/logout_Background_pressed.png"
            onClicked: buttonmenu.clicked("showDropBox")
        }
        Button{
            id:contactUs

            text: qsTr("Fale Conosco")
            textImgVisible: true
            textColor: "#808080"
            buttonImage: "qrc:/images/bottom-menu/logout_Background.png"
            pressedImage: "qrc:/images/bottom-menu/logout_Background_pressed.png"
            onClicked: buttonmenu.clicked("showContact")

        }
        Button{
            id:logout

            visible: logoutEnabled
            text:qsTr("Sair da Conta")
            textImgVisible: true
            textColor: "#808080"
            buttonImage: "qrc:/images/bottom-menu/logout_Background.png"
            pressedImage: "qrc:/images/bottom-menu/logout_Background_pressed.png"
            onClicked: buttonmenu.clicked("logout")
        }
    }

    states:[
        State{
            name:"show"
            PropertyChanges{ target:buttonmenu; height: column.height; }
        },
        State{
            name:"hide"
            PropertyChanges{ target:buttonmenu; height: 0; }
        }
    ]

    state:"hide"
}

