import QtQuick 1.0

Rectangle {
    id: categoriesListItem

    width: itemImage.width; height: itemImage.height + (caractereInvalidVisible? 25 : 0)
    z: menuVisible? 100 : 0
    color: "transparent"

    property QtObject category
    property bool selected: false
    property bool editModeEnabled: category ? (category.name.length < 2) : false
    property alias menuVisible: popupButton.visible
    property alias caractereInvalidVisible: caracterInvalidText.visible

    signal addedItem
    signal removedItem
    signal selectedItem
    signal editedItem(string newCategoryName)

    function removeFocusFromLineEdit() {
        categoryLineEdit.textFilled()
    }

    Image {
        id: itemImage
        source: categoriesListItem.selected?
               "qrc:/images/categories/resume_item_a.png" :
         "qrc:/images/categories/resume_item.png"
    }
    Image {
        source: "qrc:/images/categories/sombreado_lista.png"
    }
    Text {
        id: categoryText

        width: categoriesListItem.width - optionsButton.width
        height: categoriesListItem.height
        anchors.left: itemImage.left
        anchors.leftMargin: 13
        anchors.verticalCenter: itemImage.verticalCenter
        color:  "#5a5e60"
        font { pixelSize: 18; weight: Font.DemiBold; family: "Series 60 Sans"}
        text: category.name
        verticalAlignment: Text.AlignVCenter
        visible: !editModeEnabled
    }

    MouseArea {
        anchors.fill: categoriesListItem

        onClicked: {
            categoriesListItem.selectedItem()
            if (editModeEnabled)
                removeFocusFromLineEdit()
        }
    }

    Item {
        id: popupButton

        anchors {
            right: categoriesListItem.right
            top: categoriesListItem.top
            topMargin: 8
            rightMargin: 12
        }

        width: popupButtonBackground.width;
        height: popupButtonBackground.height

        visible: false

        Rectangle {
            id: popupButtonBackground
            width: 140; height: 130
            color: "#3e4345"
            radius: 15
        }
        Column {
            PopUpButton {
                id: addButton
                height: 40
                name: "Adicionar"
                color: "transparent"
                anchors.leftMargin: 10
                onClicked: {
                    categoriesListItem.addedItem()
                }
            }
            PopUpButton {
                id: editButton
                height: 40
                name: "Editar"
                color: "transparent"
                anchors.leftMargin: 10
                onClicked: {
                    categoriesListItem.editModeEnabled = true
                    categoryLineEdit.text = category.name
                    popupButton.visible = false
                }
            }
            PopUpButton {
                id: removeButton
                height: 40
                name: "Remover"
                color: "transparent"
                anchors.leftMargin: 10
                onClicked: {
                    categoriesListItem.removedItem()
                }
            }
        }
    }
    Button {
        id: optionsButton

        x: 308; y: 15;
        anchors.verticalCenter: itemImage.verticalCenter

        width: 50; height: 50

        buttonImage: popupButton.visible?
                         "qrc:/images/common/arrow_left_a.png":
        "qrc:/images/common/arrow_right.png"
        pressedImage: "qrc:/images/common/arrow_right_a.png"

        onClicked: {
            popupButton.visible = !popupButton.visible;

            categoriesListItem.selectedItem()
        }
    }

    LineEdit {
        id: categoryLineEdit

        height: 40; width: 230;
        anchors.verticalCenter: itemImage.verticalCenter
        x: width/23; y: (categoriesListItem.height - height)/2
        text: "";
        visible: editModeEnabled
        fontPixelSize: 12;
        label: qsTr("Nova Categoria")

        onTextFilled: {
            console.log("CategoriesListItem.qml - LineEdit received signal TextFilled - " + text)
            category.name = text;
            categoriesListItem.editedItem(text);
        }
    }
    Text {
        id: caracterInvalidText
        anchors.top: categoryLineEdit.bottom
        anchors.topMargin: 5
        text: " * Menos que 2 caracteres"
        visible: false; color: "red";
        font.pixelSize: 14
    }
}
