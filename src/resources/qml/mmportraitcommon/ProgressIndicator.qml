﻿import QtQuick 1.0

/**
 * An item that indicates something is happening.
 */
Item {
    id: progressIndicator
    width: parent.width; height: parent.height

    visible: indicator.running

    Rectangle {
        id: darkBackground
        anchors.fill: parent
        color: "black"; opacity: 0.7
        MouseArea {anchors.fill: parent}
    }
    Image {
        id: indicator
        anchors.centerIn: progressIndicator

        property bool running: daoManager.waitingMode

        source: "qrc:/images/common/progress-indicator.png"
        opacity: 1; scale: 10
        smooth: true;

        NumberAnimation {
            id: rotateAnimation
            target: indicator
            property: "rotation"
            from: 0
            to: 360
            duration: 1000
            loops: Animation.Infinite
        }

        states: [
            State {
                name: "running"
                when: indicator.running
                PropertyChanges {
                    target: rotateAnimation
                    running: true
                }
                PropertyChanges {
                    target: indicator
                    opacity: 1
                    scale: 2
                }
            }
        ]

        transitions: [
            Transition {
                from: "running"
                SequentialAnimation {
                    NumberAnimation {
                        properties: "opacity,scale"
                        duration: 500
                        easing.type: Easing.InOutQuad
                    }
                    PropertyAction {
                        property: "running"
                    }
                }
            },
            Transition {
                to: "running"
                SequentialAnimation {
                    NumberAnimation {
                        properties: "opacity,scale"
                        duration: 1000
                        easing.type: Easing.InOutQuad
                    }
                }
            }
        ]
    }
}
