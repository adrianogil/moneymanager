import QtQuick 1.0

Item {
    id: progressbar

    property real minimum: 0
    property real maximum: 100
    property real value: 0
    property color backgroundColor: "black"
    property color barColor: "#00adef"


    width: parent.width*0.85; height: 5

    onValueChanged: {
        console.log("ProgressBar.qml - onValueChanged - " + value )
        highlight.width = Math.abs((progressbar.width * (value - minimum)) / (maximum - minimum))
    }
    Rectangle {
        id: border
        width: parent.width; height: parent.height
        color: backgroundColor;
    }
    Rectangle {
        id: highlight
        height: progressbar.height
        width: 0;
        Behavior on width { SmoothedAnimation { velocity: 1200 } }
        color: barColor
    }

}
