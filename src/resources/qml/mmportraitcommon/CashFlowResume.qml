import QtQuick 1.0

View {
    width: background.width; height: background.height

    property string titleText: qsTr("Resumo de Fluxo de Caixa")

    Image {
        id: background

        source:  "qrc:/images/categories/backgroundResume.png"
    }

    Rectangle {
        id: titleSpace
        width: parent.width; height: title.font.pixelSize * 4
        color:  "transparent"

        Text {
            id: title

            text: titleText
            width: parent.width - parent.width/3; height: parent.height
            anchors.centerIn: parent
            font { pixelSize: 18; bold: true }
            horizontalAlignment: "AlignHCenter"
            verticalAlignment: "AlignVCenter"
        }
    }

    LineGraph {
        anchors.centerIn: parent
    }

}
