﻿import QtQuick 1.0

StringSlider {
    id: periodSelector

    currentString: ""

    onSelectedValue: {
        daoManager.openDropDown(daoManager.listPeriods, "currentPeriod");
    }
    onChangedString2Next: daoManager.nextDate()
    onChangedString2Previous: daoManager.previousDate()

    Component.onCompleted: {
        currentString = daoManager.currentPeriod
    }

    Connections {
        target: daoManager
        onCurrentPeriodChanged: {
            currentString = daoManager.currentPeriod
        }
    }
}
