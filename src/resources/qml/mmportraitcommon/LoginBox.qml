import QtQuick 1.0

View {
    id: loginBox

    property string userName: nameLineEdit.text
    property string userPassword: passwordLineEdit.text

    property bool errorLoginMessage: false
    property int fontPixelSize: loginBox.width/30


    function clearLogin(){
        nameLineEdit.text = "";
        passwordLineEdit.text = "";
        errorLoginMessage = false
    }

    viewId: "login-view"
    width: bg.width; height: bg.height


    Rectangle {
        id: background
        anchors.fill: loginBox
    }

    Image {
        id:bg
        source: "qrc:/images/common/background.png"
        //        anchors.fill: loginBox
    }



    LineEdit {
        id: nameLineEdit
        y: 157; anchors.horizontalCenter: loginBox.horizontalCenter
        width: loginBox.width * 0.7291; height: loginBox.width * 0.15
        label: qsTr("Nome")
        text: ""
        fontPixelSize: loginBox.fontPixelSize +4
        onTextChanged: {
            loginBox.errorLoginMessage = false
            if(checkImage.visible == true){
                daoManager.savedLoginName = text
            }

        }
    }
    LineEdit {
        id: passwordLineEdit
        width: nameLineEdit.width; height: nameLineEdit.height
        echoMode: TextInput.Password
        label: qsTr("Senha")
        anchors.topMargin: 35
        anchors.top: nameLineEdit.bottom
        anchors.horizontalCenter: nameLineEdit.horizontalCenter
        fontPixelSize: loginBox.fontPixelSize +4
        text: ""
        onTextChanged: {
            loginBox.errorLoginMessage = false
            if(checkImage.visible == true){
                daoManager.savedLoginPassword = text
            }

        }

    }

    Rectangle{
        id: mouseAreaRect
        width: checkSaveData.width*2; height: checkSaveData.height*2
        anchors.top: passwordLineEdit.bottom
        anchors.left: passwordLineEdit.left
        anchors.leftMargin: 10
        color: "transparent"

    Rectangle{
        id:checkSaveData
        width: 25; height: 25
        anchors.centerIn: mouseAreaRect
        radius: 30; smooth: true
        border.color: "#999999"
    }

    Rectangle {
        id: checkImage
        visible: false
        width: checkSaveData.width - 10 ; height: checkSaveData.height - 10
        color:  "#00adef"
        radius: 30; smooth: true
        anchors.centerIn: checkSaveData
    }

    Text {
        id: checkText
        text: "Lembrar Senha"
        color: "#00adef"
        anchors.verticalCenter: mouseAreaRect.verticalCenter
        anchors.left: checkSaveData.left
        anchors.leftMargin: checkSaveData.width +5
    }

    MouseArea{
       anchors.fill: mouseAreaRect
        onClicked: {
            checkImage.visible = !checkImage.visible
            if(checkImage.visible == true){
                daoManager.savedLoginPassword = passwordLineEdit.text
                daoManager.savedLoginName = nameLineEdit.text
            }
            else{
                daoManager.savedLoginPassword = ""
                daoManager.savedLoginName = ""
            }
        }

    }
    }
    Text{
        text: qsTr("* Nome ou senha incorretos");
        color: "red"
        anchors.top:mouseAreaRect.bottom
        anchors.horizontalCenter: loginBox.horizontalCenter
        anchors.topMargin: 20
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        visible: errorLoginMessage
        font {pixelSize: loginBox.fontPixelSize; }
    }

    Component.onCompleted: {

        if(daoManager.savedLoginPassword !== ""){
            passwordLineEdit.text = daoManager.savedLoginPassword;
            checkImage.visible=true
        }
        if(daoManager.savedLoginName !== ""){
            nameLineEdit.text = daoManager.savedLoginName;
            checkImage.visible=true
        }
    }
}

