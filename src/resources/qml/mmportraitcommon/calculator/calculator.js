// Store all buttons
var buttonsObjs = new Array(20);

// Used for inserting new decimal elements
var decimal_mode = 100;

// Number that was stored for next calculation
var number1 = 0;
// Type of operator: +,-,/,*,=,.
var operator_type = "";
// Indicates that the display must be reseted and
// a new number must be inserted
var new_number_mode = true
// Indicates if number1 can be used
var stored_number = false

/**
  @function Allow store each button
  */
function initButton(index,obj)
{
    if (index >= 0 && index < buttonsObjs.length) {
        console.log("calculator.js - Init button " + index)
        buttonsObjs[index] = obj;
    }
}

/**
  @function launch logic script when button is clicked
  */
function clickButton(index)
{
    console.log("calculator.js - Click button " + index)
    for (var i = 0; i < buttonsObjs.length; i++) {
        if (buttonsObjs[i] != null &&
                buttonsObjs[i] != undefined) {
            if (i == index) {
                buttonsObjs[i].pressed = true
                if (buttonsObjs[i].type == "num") {
                    if (new_number_mode) {
                        decimal_mode = 100;
                        number1 = calculator.displayValue
                        stored_number = true
                        calculator.displayValue = 0;
                        new_number_mode = false
                    }
                    if (calculator.state == "insert-number") {
                        decimal_mode = 100;
                        if (calculator.displayValue != 0) {
                            calculator.displayValue = calculator.displayValue*10 +
                                    parseInt(buttonsObjs[i].name)*100;
                        } else {
                            calculator.displayValue = parseInt(buttonsObjs[i].name)*100;
                        }
                    } else if (calculator.state == "insert-number-decimal") {
                        if (decimal_mode > 1) {
                            decimal_mode = decimal_mode / 10;
                            calculator.displayValue = calculator.displayValue +
                                    parseInt(buttonsObjs[i].name)*decimal_mode;
                        }
                    } else if (calculator.state == "number-result") {
                        calculator.state = "insert-number"
                        calculator.displayValue = parseInt(buttonsObjs[i].name)*100;
                    }
                } else if (buttonsObjs[i].type == "op") {
                    switch(buttonsObjs[i].name) {
                    case '=':
                        calculator.displayValue = doCurrentCalculation();
                        calculator.state = "insert-number"
                        stored_number = true
                        number1 = calculator.displayValue;
                        new_number_mode = true
                        break;
                    case '.':
                        calculator.state = "insert-number-decimal";
                        break;
                    case 'Go':
                        calculator.finishCalculation()
                        break;
                    case "CE":
                        calculator.state = "insert-number"
                        stored_number = false;
                        new_number_mode = true;
                        calculator.displayValue = 0;
                        decimal_mode = 100;
                        break;
                    case "C":
                        console.log("calculator.js - " + (calculator.displayValue % 100))
                        if (calculator.displayValue % 100 != 0) {
                            if (calculator.displayValue % 10 != 0) {
                                decimal_mode = 10;
                                calculator.displayValue = (calculator.displayValue - (calculator.displayValue % 10));
                            }
                            else {
                               decimal_mode = 100;
                               calculator.displayValue = (calculator.displayValue - (calculator.displayValue % 100));
                               calculator.state = "insert-number"
                            }
                        } else {
                            calculator.displayValue = (calculator.displayValue - (calculator.displayValue % 1000)) / 10;
                        }

                        if (calculator.displayValue == 0) {
                            new_number_mode = true;
                            calculator.state = "insert-number"
                        }

                        break;
                    default:
                        if (!new_number_mode) {
                            if (operator_type != "" &&
                                    calculator.state == "insert-number") {
                                calculator.displayValue = doCurrentCalculation();
                            }
                            operator_type = buttonsObjs[i].name;
                            calculator.state = "insert-number"
                            new_number_mode = true
                        } else {
                            if (stored_number) {
                                operator_type = buttonsObjs[i].name;
                                calculator.state = "insert-number"
                                new_number_mode = true
                            }
                        }

                        break;
                    }

                }
            }
            else buttonsObjs[i].pressed = false
        }
    }
}

/**
  @function Calculate the current operation according
            stored number
 */
function doCurrentCalculation()
{
    var result = 0

    if (stored_number) {
        switch (operator_type) {
        case '+':
            result = number1 + calculator.displayValue;
            break;
        case '-':
            result = number1 - calculator.displayValue;
            break;
        case 'x':
            result = number1 * calculator.displayValue;
            result = result / 100;
            break;
        case '/':
            result = number1 / calculator.displayValue;
            result = result * 100;
            break;
        case '%':
            result = number1 * calculator.displayValue / 100;
            result = result / 100;
            break;
        default:
            result = calculator.displayValue;
            break;

        }
        number1 = result;
        stored_number = true;
        operator_type = ""
    } else {
        result = calculator.displayValue;
    }

    return result;
}
