import QtQuick 1.0

Rectangle {
    id: cButton

    property bool pressed: false
    property string name: ""
    property string type: ""
    property color defaultColor: "#00adef"
    property color pressedColor: "black"

    signal clicked

    width: 50; height: 50
    color: defaultColor
    radius: 10;
    Text {
        id: buttonText
        text: cButton.name
        color: "white"
        font {pixelSize: 16; weight: Font.DemiBold}
        anchors.fill: cButton
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }
    MouseArea {
        anchors.fill: cButton

        onPressed: {
            cButton.color = cButton.pressedColor
        }
        onReleased: {
            cButton.clicked()
            cButton.color = cButton.defaultColor
        }
    }
}
