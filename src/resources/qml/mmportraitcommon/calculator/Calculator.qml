import QtQuick 1.0
import "calculator.js" as CalcLogic

Rectangle {
    id: calculator

    property string backgroundImg: ""
    property string buttonsImg: ""
    property int marginBetweenElements: 8
    property int displayValue: 0
    property string display: displayText.text

    signal finishCalculation;

    Behavior on opacity {
        NumberAnimation { duration: 300 }
    }
    width: 300; height: 300; radius: 20
    gradient: Gradient {
        GradientStop {
            position: 0.00;
            color: "#2f1d1d";
        }
        GradientStop {
            position: 1.00;
            color: "#10417a";
        }
    }

    Image {
        id: background
        anchors.fill: calculator
        source: calculator.backgroundImg
    }

    Rectangle {
        id: displayBackground

        height: 50
        anchors {
            rightMargin: marginBetweenElements
            leftMargin: marginBetweenElements
            topMargin: marginBetweenElements

            right: calculator.right;
            left: calculator.left;
            top: calculator.top;
        }
        radius: 10; border {color: "black"; width: 2}
        //        visible: background.source == ""
        Text {
            id: displayText
            font.pixelSize: 18
            text: "" + calculator.displayValue / 100 + "   "
            anchors.fill: displayBackground
            horizontalAlignment: Text.AlignRight
            verticalAlignment: Text.AlignVCenter
            onTextChanged: console.log(text)
        }
    }

    Rectangle {
        id: buttonsRect

        color: "transparent"
        anchors {
            rightMargin: marginBetweenElements
            leftMargin: marginBetweenElements
            topMargin: marginBetweenElements
            bottomMargin: marginBetweenElements

            right: calculator.right;
            left: calculator.left;
            top: displayBackground.bottom;
            bottom: calculator.bottom;
        }
        Grid {
            id: calcButtons


            anchors.centerIn: buttonsRect
            rows: 4; columns: 5
            spacing: marginBetweenElements

            ListModel {
                id: calcModel
                ListElement { buttonName: "1"; buttonType: "num"}
                ListElement { buttonName: "2"; buttonType: "num"}
                ListElement { buttonName: "3"; buttonType: "num"}
                ListElement { buttonName: "/"; buttonType: "op"}
                ListElement { buttonName: "CE"; buttonType: "op"}
                ListElement { buttonName: "4"; buttonType: "num"}
                ListElement { buttonName: "5"; buttonType: "num"}
                ListElement { buttonName: "6"; buttonType: "num"}
                ListElement { buttonName: "-"; buttonType: "op"}
                ListElement { buttonName: "C"; buttonType: "op"}
                ListElement { buttonName: "7"; buttonType: "num"}
                ListElement { buttonName: "8"; buttonType: "num"}
                ListElement { buttonName: "9"; buttonType: "num"}
                ListElement { buttonName: "+"; buttonType: "op"}
                ListElement { buttonName: "%"; buttonType: "op"}
                ListElement { buttonName: "."; buttonType: "op"}
                ListElement { buttonName: "0"; buttonType: "num"}
                ListElement { buttonName: "="; buttonType: "op"}
                ListElement { buttonName: "x"; buttonType: "op"}
                ListElement { buttonName: "Go"; buttonType: "op"}
            }

            Repeater {
                model: calcModel
                CalculatorButton {
                    id: cButton
                    name: buttonName
                    type: buttonType
                    visible: name != "nope"
                    onClicked: {
                        CalcLogic.clickButton(index);
                    }
                    Component.onCompleted: {
                        CalcLogic.initButton(index,cButton);
                    }
                }
            }
        }
    }
    states: [
        State {name: "insert-number"},
        State {name: "insert-number-decimal"},
        State {name: "number-result"}
    ]
    state: "insert-number"
    onStateChanged: console.log("Calculator.qml - State Changed to " + state)
}

