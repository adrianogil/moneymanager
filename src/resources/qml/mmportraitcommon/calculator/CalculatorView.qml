import QtQuick 1.0

Item {
    id: calculatorView

    signal exitCalculatorView

    width: parent.width; height: parent.height

    onOpacityChanged: {
        console.log("CalendarView.qml:onOpacityChanged - " + opacity)
        if(opacity==1)
            if(calculator.display.indexOf(".")==-1)
                calculator.state = "insert-number"
            else
                calculator.state = "insert-number-decimal"
    }
    onExitCalculatorView: {
        daoManager.calculatorValue = calculator.displayValue
        daoManager.calculatorActivated = false
        calculatorView.opacity = 0
    }
    Rectangle {
        id: background
        anchors.fill: parent
        color: "black"; opacity: 0.7
    }
    MouseArea {
        anchors.fill: calculatorView
        onClicked: {
            calculatorView.exitCalculatorView()
        }
    }
    Calculator {
        id: calculator
        anchors.centerIn: calculatorView
        onFinishCalculation: calculatorView.exitCalculatorView()
    }
    Connections {
        target: daoManager
        onCalculatorActivatedChanged: {
            if (daoManager.calculatorActivated) {
                if(calculator.display.indexOf(".")==-1)
                    calculator.state = "insert-number"
                else
                    calculator.state = "insert-number-decimal"
                calculator.displayValue = daoManager.calculatorValue
                calculatorView.opacity = 1
            }
        }
    }

}

