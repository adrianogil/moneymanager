import QtQuick 1.0
import Charts 1.0

WidgetChart {
    signal withoutPieces(bool hasPiece)
    id: widgetChart
    visible: true
    categoryList: daoManager.categoryList
    typeChart: 1
    onPieces:{console.log("nopiece");
        if(numberOfpieces == 0)
            withoutPieces(true);
        else
            withoutPieces(false);
    }
}
