import QtQuick 1.0
import "keyboard" as Keyboard

Item {
    id: keyboardInputScreen

    signal exitKeyboardScreen

    width: parent.width; height: parent.height

    onOpacityChanged: console.log("KeyboardInputScreen.qml:onOpacityChanged - " +
                                  opacity)
    onExitKeyboardScreen: {
        daoManager.keyboardActivated = false
        keyboard.opacity = 1;
        keyboardInputScreen.opacity = 0
    }

    Rectangle {
        id: background
        anchors.fill: keyboardInputScreen
        color: "black"; opacity: 0.7
    }
    MouseArea {
        anchors.fill: keyboardInputScreen
        onClicked: exitKeyboardScreen()
    }

    Column {
        Rectangle {
            id: lineEdit

            radius: 13; smooth: true
            width: keyboardInputScreen.width; height: keyboardInputScreen.height / 15

            gradient: Gradient {
                GradientStop {
                    position: 0.00
                    color: "#777777"
                }
                GradientStop {
                    position: 1.00
                    color: "#3e3e3e"
                }
            }

            Text {
                id: textInput

                text: keyboard.text

                color: "white"
                font.pixelSize: keyboardInputScreen.height / 30
                anchors {
                    left: parent.left; right: parent.right; leftMargin: 18; rightMargin: 18
                    verticalCenter: parent.verticalCenter
                }
            }
        }
        Keyboard.Keyboard {
            id: keyboard
            height: 360
            onTextChanged: {
                daoManager.keyboardValue = text;
            }
            onOpacityChanged: {
                if (opacity == 0) {
                    keyboardInputScreen.exitKeyboardScreen()
                }
            }
        }
    }
    Connections {
        target: daoManager
        onKeyboardActivatedChanged: {
            if (daoManager.keyboardActivated) {
                keyboard.text = daoManager.keyboardValue
                keyboardInputScreen.opacity = 1
            }
        }
    }

}
