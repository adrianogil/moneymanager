import QtQuick 1.0

View {
    id: targetsView

    property int selectedItem: -1
    property variant selectedItemIndexList: []

    signal startedOpenAnimation

    viewId: "targets-view"
    width: parent.width; height: parent.height

    Image {
        id: background
        source: "qrc:/images/cashflow/background.png"
    }

    onVisibleChanged: {
        if (visible)
            progressBarAnimationTimer.start()
    }
    Timer {
        id: progressBarAnimationTimer
        running: false
        interval: 1000
        repeat: false
        onTriggered: targetsView.startedOpenAnimation()
    }
    Rectangle {
        id: targetsArea
        x:width/2 - height/2 ; y:height/2 -width/2 +40
        width:650; height:650
        rotation: -90
        gradient: Gradient {
            GradientStop {
                position: 0.00;
                color: "#ebebeb";
            }
            GradientStop {
                position: 1.00;
                color: "#e8e8e8";
            }
        }
    }
    TitleBox {
        id: titleBox
        text1: qsTr("Lista de Metas")
        width: background.width; height: backgroundTitle.height
    }

    Image {
        id: backgroundTitle
        x: 0; y: 0
        source: "qrc:/images/periodselector/background.png"
        visible: false
    }
    Component {
        id: delegate

        ItemGoalList {
            id: itemGoalList
            goal: model.modelData
            selected: ((targetsView.state == "item-list" ||
                        targetsView.state == "item-remove") &&
                       targetsView.selectedItem == index)
                      ||
                      ((targetsView.state == "item-multiple-remove" ||
                        targetsView.state == "item-remove") &&
                       targetsView.selectedItemIndexList.indexOf(index) != -1)
            state: targetView.state
            onSelectedItem: {
                if (targetsView.state == "item-list") {
                    if (!selected)
                        targetsView.selectedItem = index;
                    else targetsView.selectedItem = -1;
                } else if (targetsView.state == "item-multiple-remove" &&
                           selectedItemIndexList.indexOf(index) == -1) {
                    var indexList = selectedItemIndexList;
                    indexList[indexList.length] = index;
                    selectedItemIndexList = indexList;
                } else if (targetsView.state == "item-multiple-remove" &&
                           selectedItemIndexList.indexOf(index) != -1) {
                    var indexList2 = [];
                    for (var i = 0; i < selectedItemIndexList.length; i++)
                        if (selectedItemIndexList[i] != index)
                            indexList2[indexList2.length] = selectedItemIndexList[i];
                    selectedItemIndexList = indexList2;
                }
            }
            onRemovedItem: {
                if (selectedItemIndexList.length > 0) {
                    var indexList2 = [];
                    for (var i = 0; i < selectedItemIndexList.length; i++)
                        if (selectedItemIndexList[i] != index)
                            indexList2[indexList2.length] = selectedItemIndexList[i];
                    selectedItemIndexList = indexList2;
                    targetsView.selectedItem = -1;
                    if (selectedItemIndexList.length == 0) {
                        targetsView.state = 'item-list'
                        daoManager.removeGoal(index);
                    } else {
                        daoManager.removeGoal(index, true);
                    }
                } else {
                    var item2beRemoved = targetsView.selectedItem
                    targetsView.selectedItem = -1
                    targetView.state = "item-list"
                    daoManager.removeGoal(item2beRemoved)
                }
            }
            delay2remove: selectedItemIndexList.length > 0 ?
                              selectedItemIndexList.indexOf(index) * 200 : 1

            Component.onCompleted: {
                targetsView.startedOpenAnimation.connect(startAnimation);
            }

        }
    }
    ListView {
        id: listViewTarget
        x:10;y:80
        width: parent.width;height: parent.height-10
        clip: true; spacing: 5
        orientation: ListView.Vertical
        delegate: delegate
        currentIndex: -1



    }
    Connections {
        target: daoManager
        onGoalListChanged: {
            listViewTarget.model = daoManager.goalList
        }
    }
    Component.onCompleted: {
        listViewTarget.model = daoManager.goalList
    }

    Text {
        width: parent.width
        text: "Não existem metas cadastradas"
//        text: qsTr("Não existem metas cadastradas")
        font{pixelSize:16}
        anchors {
            top: titleBox.bottom;
            topMargin: 10
        }
        horizontalAlignment: Text.AlignHCenter
        visible: (listViewTarget.count > 0) ? false : true
    }

    state: "item-list"
}

