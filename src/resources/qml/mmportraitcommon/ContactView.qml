import QtQuick 1.0

View {
    id:contactViewMain
    anchors.fill:parent
    signal closeContactView()

    //Background
    Rectangle {
        anchors.fill: contactViewMain
        color: "black"
        opacity: 0.8
        MouseArea {
            anchors.fill: parent
        }
    }

    Item {
        id:contactview

        width: headerImage.width
        height: headerImage.height + bodyImage.height
        anchors.centerIn: contactViewMain

        Column {
            Image {
                id: headerImage
                source: "qrc:/images/common/popup_cabecalho.png"

                Text {
                    id: headerText
                    text: qsTr("Fale Conosco")
                    color: "white"
                    anchors.verticalCenter: headerImage.verticalCenter
                    anchors.left: headerImage.left
                    anchors.leftMargin: 10
                    font { pixelSize: 16; weight: Font.DemiBold; family: "Series 60 Sans" }
                }
                Item {
                    id: exitButton
                    width: 50; height: 50
                    anchors.verticalCenter: headerImage.verticalCenter
                    anchors.right: headerImage.right
                    Rectangle {
                        width: 5; height:20
                        anchors.centerIn: parent
                        rotation: 45
                        color: "white"
                        radius: 3
                        smooth: true
                    }
                    Rectangle {
                        width: 5; height:20
                        anchors.centerIn: parent
                        rotation: -45
                        color: "white"
                        radius: 3
                        smooth: true
                    }
                    MouseArea{
                        anchors.fill: exitButton
                        onClicked: {
                            contactViewMain.visible = false
                            contactViewMain.closeContactView()
                        }
                    }
                }

            }
            Image {
                id: bodyImage
                source: "qrc:/images/common/popup_bg_cinza.png"

                Column {
                    width: bodyImage.width
                    spacing: contactview.width/20
                    Image {
                        id: gnuImage
                        source: "qrc:/images/contact/marca-appsguru.png"
                        anchors.horizontalCenter:  parent.horizontalCenter
                    }
                    Text {
                        id: mainText
                        width: 280
                        wrapMode: Text.WordWrap
                        text: "<p align='justify'>" + "Saiba mais sobre os aplicativos desenvolvidos pelo INDT através " +
                                   "do perfil Apps Guru no Twitter e Facebook.\n" +
                                   "Acompanhe nossos canais de comunicação para estar atualizado " +
                                   "sobre novidades e lançamentos, enviar sugestões e muito mais. " +
                                                          "Qualquer dúvida é só falar com a gente!" + "</p>"
//                        text: "<p align='justify'>" + qsTr("Saiba mais sobre os aplicativos desenvolvidos pelo INDT através " +
//                                   "do perfil Apps Guru no Twitter e Facebook.\n" +
//                                   "Acompanhe nossos canais de comunicação para estar atualizado " +
//                                   "sobre novidades e lançamentos, enviar sugestões e muito mais. " +
//                                                          "Qualquer dúvida é só falar com a gente!" + "</p>")
                        font { pixelSize: 14; weight: Font.DemiBold; family: "Series 60 Sans" }
                        anchors.top: gnuImage.bottom
                        anchors.horizontalCenter:  parent.horizontalCenter
                        anchors.margins: 10

                    }
                    Button{
                        id: facebookButton
//                        width: 321; height: 67
                        anchors.horizontalCenter:  parent.horizontalCenter
                        buttonImage: "qrc:/images/contact/facebook.png"
                        pressedImage: "qrc:/images/contact/facebook.png"
                        onClicked: Qt.openUrlExternally("http://m.facebook.com/indtappsguru")
                    }
                    Button{
                        id: twitterButton
//                        width: 322; height: 68
                        anchors.horizontalCenter:  parent.horizontalCenter
                        buttonImage: "qrc:/images/contact/twitter.png"
                        pressedImage: "qrc:/images/contact/twitter.png"
                        onClicked:Qt.openUrlExternally("http://mobile.twitter.com/indtappsguru")
                    }
                }
            }

        }
    }
}
