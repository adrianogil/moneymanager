﻿import QtQuick 1.0

Rectangle {
    id: stringSlider

    property string currentString: ""
    property string animationMode: "right"

    signal changedString2Next
    signal changedString2Previous
    signal selectedValue

    width: background.width; height: background.height
    color: "transparent"

    onCurrentStringChanged:  {
        console.log("onCurrentStringChanged - " + currentString)
        //        if (animationMode == "right") {
        displayStringSelected.currentValue = stringSlider.currentString
        //        } else if (animationMode == "left") {
        //            displayStringSelected.currentValue = stringSlider.currentString
        //        }
    }
    SequentialAnimation {
        id: nextAnimation

        running: false
        NumberAnimation {
            target: displayStringSelected
            property: "x";
            from: stringSlider.width/4
            to: -(3*stringSlider.width/4)-10
            duration: 400

        }
        ScriptAction { script: displayStringSelected.currentValue = stringSlider.currentString }
        //        PauseAnimation { duration: 300  }
        NumberAnimation {
            target: displayStringSelected
            property: "x";
            from: stringSlider.width + 10
            to: stringSlider.width/4
            duration: 400

        }
    }
    SequentialAnimation {
        id: previousAnimation

        running: false
        NumberAnimation {
            target: displayStringSelected;
            property: "x";
            from: stringSlider.width/4;
            to: stringSlider.width + 10
            duration: 400

        }
        ScriptAction { script: displayStringSelected.currentValue = stringSlider.currentString }
        //          PauseAnimation { duration: 300  }
        NumberAnimation {
            target: displayStringSelected;
            property: "x";
            from: -(3*stringSlider.width/4)-10
            to: stringSlider.width/4;
            duration: 400

        }
    }

    Image {
        id: background
        x: 0; y: 0
        source: "qrc:/images/periodselector/background.png"
    }
    Row{
        Button {
            id: rowPrevious

            width: stringSlider.width/4; height: stringSlider.height
            buttonImage: "qrc:/images/common/arrow_left.png"
            pressedImage: "qrc:/images/common/arrow_left_a.png"

            onClicked: {
                stringSlider.animationMode = "left"
                stringSlider.changedString2Previous()
                previousAnimation.start()

            }
        }
        Item {
            id: displayStringSelected

            property string currentValue: ""
            width: stringSlider.width/2; height: stringSlider.height

            Text {
                height: stringSlider.height
                text: "<p align='center'>" + displayStringSelected.currentValue + "</p>"
                //anchors.fill: displayStringSelected
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font { pixelSize: 16; weight: Font.Bold }
                anchors.horizontalCenter: displayStringSelected.horizontalCenter
            }
            MouseArea {
                anchors.fill: displayStringSelected
                onClicked: {
                    stringSlider.selectedValue()
                }
            }

        }
        Button {
            id: rowNext
            width: stringSlider.width/4; height: stringSlider.height
            buttonImage: "qrc:/images/common/arrow_right.png"
            pressedImage: "qrc:/images/common/arrow_right_a.png"


            onClicked: {
                stringSlider.animationMode = "right"
                stringSlider.changedString2Next()
                nextAnimation.start()

            }
        }
    }

}

