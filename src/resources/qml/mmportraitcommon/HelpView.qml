import QtQuick 1.0

Rectangle {
    id: helpView
    anchors.fill:parent
    color: "transparent"

    signal closeHelp()
    property int dotSize: 8
    property bool isContinue: true

    //Background
    Rectangle {
        anchors.fill: helpView
        color: "black"
        opacity: 0.8
    }
    MouseArea {
        anchors.fill: helpView
        onClicked: {}
    }
    Rectangle{
        id: helpContentView
        width: title.width
        height: title.height + bgContent.height
        anchors.centerIn: helpView
        color: "transparent"

        // Title Bar
        Image {
            id: title
           // height: 53; width: helpContentView.width
            source: "qrc:/images/common/popup_cabecalho.png"
            Text {
                text: qsTr("Ajuda")
                color: "white"
                anchors.verticalCenter: title.verticalCenter
                anchors.left: title.left
                anchors.leftMargin: 10

                verticalAlignment: Text.AlignVCenter
                font { pixelSize: 16; weight: Font.DemiBold; family: "Series 60 Sans" }
            }

            Item {
                id: exitButton
                width: 50; height: 50
                anchors.verticalCenter: title.verticalCenter
                anchors.right: title.right
                Rectangle {
                    width: 5; height:20
                    anchors.centerIn: parent
                    rotation: 45
                    color: "white"
                    radius: 3
                    smooth: true
                }
                Rectangle {
                    width: 5; height:20
                    anchors.centerIn: parent
                    rotation: -45
                    color: "white"
                    radius: 3
                    smooth: true
                }
                MouseArea{
                    anchors.fill: exitButton
                    onClicked: {
                        helpView.visible = false
                        helpView.closeHelp()
                    }
                }
            }
        }

        Image {
            id: bgContent

            source: "qrc:/images/common/popup_bg_cinza.png"
            anchors {horizontalCenter: helpView.horizontalCenter; top: title.bottom}
        }

        //Images

        ListModel{
            id: model
            ListElement { imageSource: "qrc:images/help/ajuda1.png" }
            ListElement { imageSource: "qrc:images/help/ajuda2.png" }
            ListElement { imageSource: "qrc:images/help/ajuda3.png" }

        }


        ListView{
            id: listView
            width: bgContent.width - 20; height: bgContent.height - 100
            anchors {
                top: title.bottom
                horizontalCenter: helpContentView.horizontalCenter
                margins: 5
            }
            highlightRangeMode: ListView.StrictlyEnforceRange
            clip:  true
            orientation: "Horizontal"
            spacing: 2
            snapMode: "SnapToItem"

            model: model

            delegate:
                Image {
                source: imageSource
            }

            onCurrentIndexChanged: isContinue = (currentIndex < 2) ? true : false;



        }

        // Dots


        Rectangle {
            id: first
            height: dotSize; width: dotSize
            anchors.right: second.left
            anchors.margins: 10
            color: listView.currentIndex == 0 ? "#00ADEF" : "#000000"
            anchors.bottom: button.top
        }
        Rectangle {
            id: second
            height: dotSize; width: dotSize
            // anchors.left: first.right
            anchors.horizontalCenter: helpContentView.horizontalCenter
            anchors.margins: 10
            color: listView.currentIndex == 1 ? "#00ADEF" : "#000000"
            anchors.bottom: button.top
        }
        Rectangle {
            id: third
            height: dotSize; width: dotSize
            anchors.left: second.right
            anchors.margins: 10
            color: listView.currentIndex == 2 ? "#00ADEF" : "#000000"
            anchors.bottom: button.top
        }

        Button {
            id: button
            text: (isContinue) ? qsTr("Continuar"): qsTr("Sair")
            defaultColor: "#808080"
            pressedColor: "#00adef"
            width: 240; height: 42
            color: "#808080"
            radius: 16

            anchors {
                bottom: helpContentView.bottom
                bottomMargin: 10
                horizontalCenter: helpContentView.horizontalCenter
            }
            onClicked: {

                if(isContinue)
                    listView.incrementCurrentIndex()
                else {
                    closeHelp()
                    listView.currentIndex = 0
                    helpView.visible = false
                }
            }

        }
    }

}
