import QtQuick 1.0
import Entity 1.0
import "calculator" as Calculator
import "calendar" as Calendar
import "../utils.js" as Utils

View {
    id: moneyAddView

    property string subview: ""
    property string errorDescriptionText: ""
    property bool errorRequiredFields: false
    property int errorSourceType: -1
    property int fontPixelSize: moneyAddView.width/30

    function showView() {
        if (subview == "money-add")
            moneyTypeSelector.changedString2Next();
        visible = true;
    }
    function closeView() {
        visible = false;
    }
    function clearFields(){
        descriptionLineEdit.text = ""
        dateLineEdit.text = ""
        valueLineEdit.text = ""
        categoryLineEdit.text=""
        frequencyLineEdit.text = ""
        detailsLineEdit.text = ""
        errorRequiredFields = false
        daoManager.calculatorValue = 0
        moneyAddView.focus = true
    }

    Rectangle {
        id:background
        anchors.fill: moneyAddView
    }

    Image {
        id: listBackground
        y: moneyTypeSelector.height
        source: "qrc:/images/cashflow/background_add.png"
    }


    viewId: "money-add-view"
    width: listBackground.width; height: listBackground.height
    onVisibleChanged: {
        if (visible) {
            showView()
        }
    }

    MoneyTypeSelector {
        id: moneyTypeSelector

        onMoneyTypeValueChanged: {
            daoManager.newMoney.moneyType = moneyTypeValue;
        }
    }

    Column {
        x: 10; y: 92
        spacing: 20
        anchors.horizontalCenter: moneyAddView.horizontalCenter
        anchors.verticalCenter: moneyAddView.verticalCenter

        LineEdit {
            id: descriptionLineEdit

            width: moneyAddView.width * 0.7291; height: moneyAddView.width * 0.15
            label: "Descrição"
            //        label: qsTr("Descrição")
            maxLength: 20
            onTextChanged: {
                errorRequiredFields = false
                console.log("descriptionLineEdit - " + text)
                daoManager.newMoney.description = text;
            }
        }
        Row {
            id:rowDateValueEdit
            spacing: 20
            height: descriptionLineEdit.height
            LineEdit {
                id: dateLineEdit

                width: descriptionLineEdit.width/2-10;height: descriptionLineEdit.height
                label: qsTr("Data")
                type: 1
                onTextChanged: daoManager.newMoney.moneyDate = text
            }
            LineEdit {
                id: valueLineEdit

                width: descriptionLineEdit.width/2-10;height: descriptionLineEdit.height
                label: qsTr("Valor")
                type: 2

                onTextChanged: daoManager.newMoney.amount = parseFloat(text);
            }
        }
        LineEdit {
            id: categoryLineEdit

            width: descriptionLineEdit.width; height: descriptionLineEdit.height
            label: qsTr("Categoria")
            text : daoManager.newMoney.categoryByName

            MouseArea {
                anchors.fill: categoryLineEdit
                onClicked: {
                    daoManager.openDropDown(daoManager.categoryList, "newMoneyCategory")

                }
            }
        }
        LineEdit {
            id: frequencyLineEdit

            width: descriptionLineEdit.width; height: descriptionLineEdit.height
            label: "Frequência";
            //        label: qsTr("Frequência");
            visible: false
            MouseArea {
                anchors.fill: frequencyLineEdit
                onClicked: {
                    daoManager.openDropDown(daoManager.frequencyList, "frequencyType")
                    frequencyLineEdit.textFocus = false
                }
            }

        }
        LineEdit {
            id: detailsLineEdit
            // x: 10; y: 340 // If using frequency -> 427
            width: descriptionLineEdit.width; height: descriptionLineEdit.height
            label: qsTr("Detalhes")

            onTextChanged: {
                daoManager.newMoney.details = text
            }
        }


        Text {
            text: "*"
            color: "red"
            anchors.top: errorSourceType == 1 ? rowDateValueEdit.top: descriptionLineEdit.top
            anchors.left: errorSourceType == 1 ? rowDateValueEdit.right: descriptionLineEdit.right
            anchors.margins: 5
            visible: errorRequiredFields
            font.pixelSize: moneyAddView.fontPixelSize + 4
        }
    }

    Connections {
        target:  daoManager
        onNewMoneyChanged: {
            console.log("Connections:onNewMoneyChanged - " + subview + " " +
                        daoManager.newMoney.description);
            if (moneyAddView.subview == "money-edit"){
                valueLineEdit.text = daoManager.newMoney.amount
                dateLineEdit.text = daoManager.newMoney.moneyDate;
                descriptionLineEdit.text = daoManager.newMoney.description
                moneyTypeSelector.setValue(daoManager.newMoney.moneyType)
                moneyTypeSelector.changedString2Next();
                moneyTypeSelector.changedString2Next();
                frequencyLineEdit.text = daoManager.newMoney.frequencyName
                detailsLineEdit.text = daoManager.newMoney.details
                categoryLineEdit.text = daoManager.newMoney.categoryByName
            }
            else
                categoryLineEdit.text = daoManager.newMoney.categoryByName
        }
    }

    // Required Fields Message

    Text {
        text: errorDescriptionText != ""? errorDescriptionText : "* Dados Inválidos"
        //        text: errorDescriptionText != ""? errorDescriptionText : qsTr("* Dados Inválidos")
        color: "red"
        anchors.bottom: moneyAddView.bottom
        anchors.bottomMargin: 10
        anchors.horizontalCenter: moneyAddView.horizontalCenter
        horizontalAlignment: Text.AlignHCenter
        visible: errorRequiredFields
    }
    Component.onCompleted: daoManager.newMoney.moneyDate = daoManager.today()
}
