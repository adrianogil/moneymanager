import QtQuick 1.0

Item {
    id: dropDownList

    property variant model: []
    property string choosedValue: ""
    property string property2BeAssigned: ""

    signal choosed

    function closeView() {
        visible = false
    }

    width: parent.width; height: parent.height
    onChoosed: {
        daoManager.setDropDownValue(property2BeAssigned, choosedValue)
    }

    Rectangle {
        anchors.fill: dropDownList
        color: "black"; opacity: 0.5
    }
    MouseArea {
        anchors.fill: dropDownList
        onClicked: dropDownList.closeView()
    }
    Item {

        height: Math.min(356, model.length * 62); width: 360
        anchors.centerIn: dropDownList
        clip: true

        ListView {
            anchors.fill: parent
            model: dropDownList.model

            clip: false
            delegate: Rectangle {
                property string roleName: model.modelData.name? model.modelData.name : modelData
                property bool choosed: dropDownList.choosedValue == roleName

                width: 360; height: 62
                color: choosed? "#00adef":"white"
                Text {
                    anchors.fill: parent
                    anchors.leftMargin: 28
                    text: roleName
                    font { pixelSize: 16}
                    color: parent.choosed? "white" : "black"
                    horizontalAlignment: Text.AlignLeft
                    verticalAlignment: Text.AlignVCenter
                    wrapMode: Text.WordWrap
                }
                MouseArea {
                    anchors.fill: parent
                    onPressed: {
                        dropDownList.choosedValue = roleName;
                    }
                    onReleased: {
                        dropDownList.choosed();
                    }
                }
            }
        }
    }

}
