import QtQuick 1.0

Item {
    id: viewComponent

    property string viewId: "view"

    signal actionActivated(string action)

//    Behavior  on opacity {
//        NumberAnimation { duration: 500 }
//    }

//    onOpacityChanged: {
//        console.log("View.qml - View " + viewId + " changed opacity to " +
//                    opacity);
//    }

    Connections {
        target: daoManager
        onSwitchFlow: {
            if (viewIdentifier == viewComponent.viewId) {
                viewComponent.state = viewState;
                viewComponent.actionActivated(action)
            }
        }
    }
}
