import QtQuick 1.0

View {
    id: cashFlowList

    property variant moneyList: []
    property variant selectedItemIndexList: []
    property variant removedItemIndexList: []
    property int selectedItemIndex: -1
    property int idNewItem: -1

    function showView() {
        visible = true;
    }
    function closeView() {
        visible = false;
    }
    function removeCurrentMoneyRegister() {
        if (selectedItemIndex != -1)
            cashFlowList.state = 'item-remove'
    }
    function removeSelectedMoneyRegisters() {
        if (selectedItemIndexList.length > 0)
            cashFlowList.state = 'item-remove'
    }

    width: listBackground.width; height: listBackground.height

    Image {
        id: listBackground
        source: "qrc:/images/cashflow/background.png"
    }
    ListView {
        id: listView
        anchors.fill: listBackground
        anchors.margins: 11
        model: moneyList
        spacing: 10; clip: true
        delegate: ListMoneyItem {
            state: cashFlowList.state
            money: model.modelData
            visible: removedItemIndexList.indexOf(index) == -1
            selected: ((cashFlowList.state == "item-list" ||
                        cashFlowList.state == "item-remove") &&
                       cashFlowList.selectedItemIndex == index)
                      ||
                      ((cashFlowList.state == "item-multiple-remove" ||
                        cashFlowList.state == "item-remove") &&
                       cashFlowList.selectedItemIndexList.indexOf(index) != -1)
            onSelectedItem: {
                if (cashFlowList.state == "item-list") {
                    if (!selected)
                        cashFlowList.selectedItemIndex = index;
                    else cashFlowList.selectedItemIndex = -1;
                } else if (cashFlowList.state == "item-multiple-remove" &&
                           selectedItemIndexList.indexOf(index) == -1) {
                    var indexList = selectedItemIndexList;
                    indexList[indexList.length] = index;
                    selectedItemIndexList = indexList;
                } else if (cashFlowList.state == "item-multiple-remove" &&
                           selectedItemIndexList.indexOf(index) != -1) {
                    var indexList2 = [];
                    for (var i = 0; i < selectedItemIndexList.length; i++)
                        if (selectedItemIndexList[i] != index)
                            indexList2[indexList2.length] = selectedItemIndexList[i];
                    selectedItemIndexList = indexList2;
                }
            }
            onRemovedItem: {
                if (selectedItemIndexList.length > 0) {
                    var indexList2 = [];
                    for (var i = 0; i < selectedItemIndexList.length; i++)
                        if (selectedItemIndexList[i] != index)
                            indexList2[indexList2.length] = selectedItemIndexList[i];
                    selectedItemIndexList = indexList2;
                    console.log("CashFlowList.qml - call massive money remove: "
                                + removedItemIndexList + " id: " + index)
                    var removedList = removedItemIndexList
                    removedList[removedList.length] = index;
                    removedItemIndexList = removedList;
                    if (selectedItemIndexList.length == 0) {
                        console.log("CashFlowList.qml - call massive money remove: "
                                    + removedItemIndexList)
                        daoManager.removeMassiveMoneyRegisters(removedItemIndexList);
                        removedItemIndexList = []
                        cashFlowList.state = 'item-list'
                    } else {
                        listView.positionViewAtIndex(selectedItemIndexList[0],
                                                     ListView.Contain)
                    }
                } else {
                    var item2BeRemoved = cashFlowList.selectedItemIndex;
                    cashFlowList.selectedItemIndex = -1;
                    daoManager.removeMoneyRegister(item2BeRemoved)
                    cashFlowList.state = 'item-list'
                }
            }
            delay2remove: selectedItemIndexList.length > 0 ?
                              cashFlowList.selectedItemIndexList.indexOf(index) * 400 : 1
            onItemCreationCompleted: {
                cashFlowList.idNewItem = -1;
            }
            Component.onCompleted: {
                cashFlowList.idNewItemChanged.connect(
                            function() {
                                if (money != null && money.id == idNewItem)
                                    startEnterAnimation()
                            }
                            )
            }
        }
    }
    Item {
        id: totalBar

        y: cashFlowList.height
        width: imageTotalBar.width; height: imageTotalBar.height

        Image {
            id: imageTotalBar
            anchors.centerIn: totalBar
            source: "qrc:/images/cashflow/total_bar.png"
        }

        Flickable {
            id:totalBarFlick
            x: 30;
            width: listBackground.width; height: 60
            contentWidth: row.width; contentHeight: 60
            interactive: false; clip: true
            Row {
                id: row

                Column {
                    Text {
                        id: text1
                        width: totalBarFlick.width/3;
                        text: qsTr("Recebido")
                        font {pixelSize: 16; weight: Font.Bold}
                        color: "#00adef"
                    }
                    Text {
                        id: text2
                        width: totalBarFlick.width/3
                        text: (Math.round(daoManager.totalIncomes*100)/100)
                        font {pixelSize: 20; weight: Font.Bold}
                        color: "#00adef"
                    }
                }
                Column {
                    Text {
                        width: totalBarFlick.width/3
                        text: qsTr("Pago")
                        font {pixelSize: 16; weight: Font.Bold}
                        color: "#00adef"
                    }
                    Text {
                        width: totalBarFlick.width/3
                        text: (Math.round(daoManager.totalOutcomes*100)/100)
                        font {pixelSize: 20; weight: Font.Bold}
                        color: "#00adef"
                    }
                }
                Column {
                    Text {
                        width: totalBarFlick.width/3
                        text: qsTr("Total")
                        font {pixelSize: 16; weight: Font.Bold}
                        color: "#00adef"
                    }
                    Text {
                        width: totalBarFlick.width/3
                        text: (Math.round(daoManager.totalCashFlow*100)/100)
                        font {pixelSize: 20; weight: Font.Bold}
                        color: "#00adef"
                    }
                }
                Button {
                    text: qsTr("Create")
                    onClicked: daoManager.databaseRandomPopulate()
                }
            }
        }
    }

    Text {
        text: "Não existem registros cadastrados"
//        text: qsTr("Não existem registros cadastrados");
        horizontalAlignment: Text.AlignHCenter
        textFormat: Text.RichText
        font {pixelSize: 16; }
        anchors.top: cashFlowList.top
        anchors.horizontalCenter: cashFlowList.horizontalCenter
        anchors.topMargin: 10
        visible: moneyList.length > 0 ? false : true
    }

    state: 'item-list'
}
