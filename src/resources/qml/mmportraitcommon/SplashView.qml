import QtQuick 1.0

View {
    id: mainContainer
    width: initialScreen.width
    height: initialScreen.height

    state: 'splash'
    signal loadView

    Image {
        id: initialScreen
        source: "qrc:/images/splash/tela_inicial.png"

        Behavior on opacity {
            NumberAnimation { duration: 1000 }
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(splashTimer.interval = 0)
                    initialScreen.opacity = 1
            }
        }
    }

    Timer {
        id: splashTimer
        interval: 2000
        repeat: false
        onTriggered: {
            splashAnimation.start()
            mainContainer.loadView()
        }
    }

    NumberAnimation {
        id: splashAnimation
        running: false
        target: mainContainer;
        property: "opacity";
        duration: 3000;
        easing.type: Easing.InCubic
        from: 1; to: 0
    }

    states: [
        State {
            name: "splash"
            PropertyChanges { target: initialScreen; opacity: 1 }
            PropertyChanges { target: splashTimer; running: true;}

        }
    ]
}
