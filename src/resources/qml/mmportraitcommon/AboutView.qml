import QtQuick 1.0

Rectangle {

    id: aboutView
    width: 360
    height: 640
    anchors.fill: parent
    color: "transparent"
    signal closeAbout()


    Rectangle {
        anchors.fill: aboutView
        color: "black"
        opacity: 0.8
    }
    MouseArea {
        anchors.fill: aboutView
        onClicked: {}
    }

    Rectangle {
        id:aboutViewContent
        width: headerImage.width
        height: headerImage.height + bodyImage.height
        anchors.horizontalCenter: aboutView.horizontalCenter
        anchors.verticalCenter: aboutView.verticalCenter

        Image {
            id: headerImage
            source: "qrc:/images/common/popup_cabecalho.png"
            Text {
                id: headerText
                text: qsTr("Sobre")
                color: "white"
                anchors.verticalCenter: headerImage.verticalCenter
                anchors.left:headerImage.left
                anchors.leftMargin: 10
                font { pixelSize: 20; weight: Font.DemiBold; family: "Series 60 Sans" }
            }

            Item {
                id: exitButton
                width: 50; height: 50
                anchors.verticalCenter: headerImage.verticalCenter
                anchors.right: headerImage.right
                Rectangle {
                    width: 5; height:20
                    anchors.centerIn: parent
                    rotation: 45
                    color: "white"
                    radius: 3
                    smooth: true
                }
                Rectangle {
                    width: 5; height:20
                    anchors.centerIn: parent
                    rotation: -45
                    color: "white"
                    radius: 3
                    smooth: true
                }
                MouseArea{
                    anchors.fill: exitButton
                    onClicked: {
                        aboutView.visible = false
                        aboutView.closeAbout()
                    }
                }


            }
        }
        Image {
            id: bodyImage
            source: "qrc:/images/common/popup_bg_cinza.png"
            anchors {
                top: headerImage.bottom
            }
        }

        Text {
            id: version
            color: "black"
            text: "Versão 1.0 - 25.11.2011"
            anchors.horizontalCenter:bodyImage.horizontalCenter
            anchors.top: headerImage.bottom
            anchors.topMargin: 10
            anchors.verticalCenter: aboutView.verticalCenter
            font { pixelSize: 14; weight: Font.DemiBold; family: "Series 60 Sans" }
        }

        Text {
            id: orientationText
            width:300;
            wrapMode: Text.Wrap
            color: "black"
//            text: qsTr("Esta aplicação rodará apenas na ") +
//                  "<strong>" + qsTr("orientação vertical") +
//                  "</strong>"
            text: "Esta aplicação rodará apenas na <strong>orientação vertical</strong>"
            anchors.horizontalCenter:bodyImage.horizontalCenter
            anchors.top: version.bottom
            anchors.topMargin: 10
            anchors.verticalCenter: aboutView.verticalCenter
            font { pixelSize: 16; weight: Font.DemiBold; family: "Series 60 Sans" }
        }

        Text {
            width:300 ; height: 250
            wrapMode: Text.Wrap
            text: about.getAboutMessage();
            font { pixelSize: 16; weight: Font.DemiBold; family: "Series 60 Sans" }
            color: "black"
            anchors.horizontalCenter: bodyImage.horizontalCenter
            anchors.top: orientationText.bottom
            anchors.topMargin: 20
        }

        Button {
            id: btnDownload
            height: 42
            text: qsTr("Atualizar")
            visible:  about.showUpdateButton()
            onClicked: about.updateSoftware();
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: btnUpdate.top

        }

        Button {
            id: btnUpdate
         //   text: "UPDATE"
            buttonImage: "qrc:/images/about/selo.png"
            onClicked: Qt.openUrlExternally("http://bit.ly/atRQmu")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
        }
}
}
