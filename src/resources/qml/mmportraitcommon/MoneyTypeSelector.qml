import QtQuick 1.0

StringSlider {
    id: moneyTypeSelector

    property variant moneyTypes: ["Recebimento", "Pagamento"]
    property int moneyTypeValue: 1

    function setValue(typeValue) {
        moneyTypeValue = Math.abs(typeValue) % moneyTypes.length
    }

    currentString: ""

    onMoneyTypeValueChanged: {
        currentString = moneyTypes[Math.abs(moneyTypeValue) % moneyTypes.length];
    }

    onChangedString2Next:  moneyTypeValue = Math.abs(moneyTypeValue-1) % moneyTypes.length
    onChangedString2Previous: moneyTypeValue = Math.abs(moneyTypeValue+1) % moneyTypes.length

    Component.onCompleted: {
        currentString = moneyTypes[1];
    }
}
