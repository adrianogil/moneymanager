import QtQuick 1.0

Image {
    id: gImage

    QtObject {
        id: props
        property real scaleXFactor: 720 / 360
        property real scaleYFactor: 1024 / 640
    }

    onProgressChanged: {
        if (progress == 1) {
            gImage.width = props.scaleXFactor * gImage.width;
            gImage.height = props.scaleYFactor * gImage.height;
        }
    }
}
