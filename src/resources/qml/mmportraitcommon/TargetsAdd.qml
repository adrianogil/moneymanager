import QtQuick 1.0
import "calendar" as Calendar
import "../utils.js" as Common

View {
    id:targetsAdd

    property string subview: ""
    property int diff: 0
    property alias descriptionError: errorDescription.visible
    property alias initialDateError: errorInitialDate.visible
    property alias finalDateError: errorFinalDate.visible
    property alias targetError: errorTarget.visible
    property alias goalError: errorGoalMessage.visible
    property alias messageError: errorGoalMessage.text
    property alias initialDateValue: textInitialDate.text
    property alias finalDateValue: textFinalDate.text
    property alias targetValue: textValue.text
    property int fontPixelSize: newAccountView.width/30

    function clearFields(){
        targetAdd.focus = true
        textDescription.text = ""
        textFinalDate.text = ""
        textInitialDate.text = ""
        textValue.text = ""
        textInvestiment.text = ""
        textSale.text = ""
        clearErrors()
        daoManager.calculatorValue = 0
    }

    function clearErrors() {
        descriptionError = false
        initialDateError = false
        finalDateError = false
        targetError = false
        goalError = false
    }

    function generateInvestiment(){
        var sale = parseInt(daoManager.allTimeSale * 100)/100
        textSale.label = "Saldo Atual : " + sale

        if(textInitialDate.text != "" && textFinalDate != "")
            diff = Common.monthsDiff(textInitialDate.text, textFinalDate.text)

        if(diff != 0 && textValue.text != ""){
            var value = parseFloat(textValue.text)
            var investimentValue = (parseFloat(textValue.text) - sale)/diff

            investimentValue = parseInt(investimentValue * 100)/100
            if (investimentValue < 0) investimentValue = 0

            //textInvestiment.text = investimentValue
            textInvestiment.label = "Investimento Mensal : " + investimentValue

        } else
            //textInvestiment.text = textValue.text
            textInvestiment.label = "Investimento Mensal"



    }

    viewId: "targets-add-view"
    width:  backgroundImage.width; height:  backgroundImage.height

    Image {
        id: backgroundTitle
        x: 0; y: 0
        source: "qrc:/images/periodselector/background.png"
        visible: false
    }

    Rectangle {
        id: background
        anchors.fill: targetAdd
    }
    TitleBox {
        id: titleAdd
        y: 0
        text1: "Adicionar Metas"
        color:"Gray"
        width: backgroundImage.width; height: backgroundTitle.height

    }
    Rectangle{
        id: targetsAddArea

        anchors.top: titleAdd.bottom
        width:  backgroundImage.width; height:  backgroundImage.height

        Image{
            id: backgroundImage
            source: "qrc:/images/cashflow/background_add.png"
        }
        LineEdit {
            id: textDescription; label: "Descrição";
//            id: textDescription; label: qsTr("Descrição");
            maxLength: 20
            height: targetAdd.width * 0.15
            anchors {top: parent.top; topMargin: 30}
            anchors { left: parent.left; leftMargin: 10
                      right:parent.right; rightMargin: 30  }

            onTextChanged: {
                clearErrors()
                daoManager.newGoal.description = text;
            }
        }
        LineEdit {
            id: textInitialDate; label: qsTr("Data inicial"); width: 150;height: targetAdd.width * 0.15
            anchors { top: textDescription.bottom; left: parent.left
                topMargin: 30; leftMargin: 10}
            type: 1
            onTextFilled: {
                clearErrors()
                generateInvestiment()
                daoManager.newGoal.initialDate = text;
            }
        }
        LineEdit {
            id: textFinalDate;
            width: 150;height: targetAdd.width * 0.15
            type: 1; label: qsTr("Data final");
            anchors {top: textDescription.bottom; left: textInitialDate.right
                topMargin: 30; margins: 20 }

            onTextFilled: {
                clearErrors()
                generateInvestiment()
                daoManager.newGoal.finalDate = text;
            }
        }
        LineEdit {
            id: textValue; label: qsTr("Valor");height: targetAdd.width * 0.15
            anchors {top: textFinalDate.bottom; topMargin: 30}
            anchors { left: parent.left; leftMargin: 10
                      right:parent.right; rightMargin: 30  }
            type: 2


            onTextChanged: {
                clearErrors()
                daoManager.newGoal.target = parseFloat(text);
                generateInvestiment()
            }
            onTextFilled:
                generateInvestiment()
        }
        LineEdit {
            id: textInvestiment; label: qsTr("Investimento Mensal");height: targetAdd.width * 0.15
            isEnabled: false
            activeFocus: false

            anchors { top: textValue.bottom; topMargin: 30 }
            anchors { left: parent.left; leftMargin: 10
                      right:parent.right; rightMargin: 30  }

        }
        LineEdit {
            id: textSale; label: qsTr("Saldo atual"); y: 379 ; width: 320;height: targetAdd.width * 0.15
            isEnabled: false
            activeFocus: false
            anchors { top: textInvestiment.bottom; topMargin: 30 }
            anchors { left: parent.left; leftMargin: 10
                      right:parent.right; rightMargin: 30  }
        }

        Text{
            id: errorDescription
            anchors {top: textDescription.top; left: textDescription.right; margins: 5}
            color: "red"
            text: "*"
            font{pixelSize:targetAdd.fontPixelSize}
        }

        Text{
            id: errorInitialDate
            anchors {top: textInitialDate.top; left: textInitialDate.right; margins: 5}
            color: "red"
            text: "*"
            font{pixelSize:targetAdd.fontPixelSize}
        }

        Text{
            id: errorFinalDate
            anchors {top: textFinalDate.top; left: textFinalDate.right; margins: 5}
            color: "red"
            text: "*"
            font{pixelSize:targetAdd.fontPixelSize}
        }

        Text{
            id: errorTarget
            anchors {top: textValue.top; left: textValue.right; margins: 5}
            color: "red"
            text: "*"
            font{pixelSize:targetAdd.fontPixelSize}
        }

        Text{
            id: errorGoalMessage
            anchors {top: textSale.bottom; horizontalCenter: parent.horizontalCenter; margins: 20}
            color: "red"
            font{pixelSize:targetAdd.fontPixelSize}
        }


        Connections {
            target: daoManager
            onNewGoalChanged: {
                if (subview == "goal-edit") {
                    textDescription.text = daoManager.newGoal.description
                    textInitialDate.text = daoManager.newGoal.initialDate
                    textFinalDate.text = daoManager.newGoal.finalDate
                    textValue.text = daoManager.newGoal.target
                }
            }
        }
    }
}
