import QtQuick 1.0
import Entity 1.0

Item {
    id: expansiveListItem

    property bool expanded: false
    property bool selected: false
    property string backgroundPath_expanded
    property string backgroundPath_expanded_a
    property string backgroundPath_shadow_a
    property string backgroundPath_shadow
    property string backgroundPath_a
    property string backgroundPath
    property int delay2remove: 1

    signal selectedItem
    signal removedItem
    signal itemCreationCompleted

    function startRemoveAnimation() {
        delayTimer.start();
    }
    function startEnterAnimation() {
        expansiveListItem.x = -1.5*expansiveListItem.width;
        enterAnimation.start()
    }

    width: background.width; height: background.height

    Behavior on height {
        NumberAnimation { duration: 300 }
    }

    onStateChanged: {
        if (state == "item-remove" && selected)
            startRemoveAnimation()
    }
    onSelectedChanged: {
        if (!selected)
            expanded = false
    }

    SequentialAnimation {
        id: removeAnimation

        running: false
        NumberAnimation {
            target: expansiveListItem
            property: "x"
            from: 0; to: 400
            duration: 700
        }
        NumberAnimation {
            target: expansiveListItem;
            property: "height";
            to: 0;
            duration: 300
        }
        onCompleted: {
            expansiveListItem.removedItem()
        }
    }
    SequentialAnimation {
        id: enterAnimation

        running: false
        NumberAnimation {
            target: expansiveListItem;
            property: "height";
            from: 0; to: 62;
            duration: 1500
        }
        NumberAnimation {
            target: expansiveListItem
            property: "x"
            from: -1.5*expansiveListItem.width; to: 0
            duration: 700
        }
        onCompleted: {
            expansiveListItem.itemCreationCompleted()
        }
    }
    Timer {
        id: delayTimer

        running: false; repeat: false
        interval: delay2remove
        onTriggered: removeAnimation.start()
    }
    Item {
        Image {
            id: background
            source: expansiveListItem.expanded ?
                        (expansiveListItem.selected ?
                             backgroundPath_expanded_a :
                             backgroundPath_expanded
                         ) :
                           (expansiveListItem.selected ?
                                backgroundPath_a :
                                backgroundPath
                            )
        }
        Image {
            id: background_shadow
            visible: !expansiveListItem.expanded
            source: (expansiveListItem.selected ?
                         backgroundPath_shadow_a :
                         backgroundPath_shadow
                    )
        }
    }

    MouseArea {
        anchors.fill: expansiveListItem
        onClicked: {
            expansiveListItem.selectedItem()
        }
        onPressAndHold: {
            if (!selected) {
                expansiveListItem.selectedItem()
            }
            expanded = !expanded
        }
    }
    state: "item-list"
}

