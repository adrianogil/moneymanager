import QtQuick 1.0

Rectangle {
    id: button

    property string buttonText: ''
    property string normalIconPath: ''
    property string selectedImagePath: ''
    property string selectedIconPath: ''
    property string backgroundImagePath: ''
    signal clicked()

    Image {
        id: backgroundImage;
        source: backgroundImagePath
       // anchors.fill: parent
    }

    width: backgroundImage.width
    height: backgroundImage.height

    Image {
        id: selectedImage;
        source: selectedImagePath
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Image {
        id: normalIcon;
        source: normalIconPath
        anchors.centerIn: selectedImage
    }

    Image {
        id: selectedIconImage;
        source: selectedIconPath
        anchors.centerIn: selectedImage
    }

    Text {
        id: text
        color: 'white'
        horizontalAlignment: Text.AlignHCenter
        anchors.centerIn: parent
        text: buttonText
    }


    states: [
        State {
            name: 'normal'
            PropertyChanges { target: selectedImage; visible: false }
            PropertyChanges { target: selectedIconImage; visible: false }
        },
        State {
            name: 'selected'
            PropertyChanges { target: selectedImage; visible: true }
            PropertyChanges { target: selectedIconImage; visible: true }
        }
    ]

    MouseArea {
        anchors.fill: parent
        onClicked: {
            button.state = 'selected'
            button.clicked()
        }
    }


}
