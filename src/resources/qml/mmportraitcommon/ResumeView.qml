import QtQuick 1.0

View {
    id: resumeView

    property string titleText: "Resumo de totais\npor categoria (em gráfico)"
//    property string titleText: qsTr("Resumo de totais\npor categoria (em gráfico)")
    property bool emptyGraph: false

    signal clicked(string btnClicked);

    viewId: "resume-view"
    width: core.width; height: periodSelector.height + core.height

    PeriodSelector {
        id: periodSelector
    }

    Rectangle {
        id: core

        width: backgroundCoreImage.width; height: backgroundCoreImage.height
        anchors.top:  periodSelector.bottom

        Image {
            id: backgroundCoreImage

            source: "qrc:/images/categories/backgroundResume.png"
        }

        Item {
            id: titleSpace
            width: parent.width; height: title.font.pixelSize * 4
            anchors.top: parent.top
            anchors.topMargin: 0

            Text {
                id: title

                text: titleText
                anchors.horizontalCenterOffset: -22
                width: parent.width - parent.width/3; height: parent.height
                anchors.centerIn: parent
                font { pixelSize: 18; bold: true }
                horizontalAlignment: "AlignHCenter"
                verticalAlignment: "AlignVCenter"
            }

            Button{
                id: resumeShowButton

                visible:true
                anchors.verticalCenterOffset: 0
                anchors.leftMargin: 0
                anchors.left: title.right
                anchors.verticalCenter: title.verticalCenter
                buttonImage: 'qrc:/images/menu/resumo_exibir_click.png'
                pressedImage: 'qrc:/images/menu/resumo_exibir.png'

                onClicked:  daoManager.openDropDown(daoManager.resumeOptions, "resumeType")
            }
        }

        Text {
            id: emptyTextWarning
            function updateEmptyText(resumeType) {
                var newText = ""
                switch (resumeType) {
                case qsTr("Resumo de Entrada"):
                    newText = qsTr("Sem registros de Entrada associados a Categorias")
                    break;
                case "Resumo de Saída":
                    newText = "Sem registros de Saída associados a Categorias"
                    break;
                default:
                    break;
                }
                return "<p align='center'>" + newText + "</p>";
            }
            width: 300; height: 60
            wrapMode: Text.WordWrap
            visible: emptyGraph
            text: updateEmptyText(daoManager.resumeType)
            anchors.centerIn: core
            font: {pixelSize:16;}
        }
        Chart {
            id: graph
            width:  250; height: 200
            anchors.top: titleSpace.bottom
            anchors.horizontalCenter: backgroundCoreImage.horizontalCenter
            //            anchors.horizontalCenter: titleSpace.horizontalCenter
            visible: true
            onWithoutPieces: emptyGraph = hasPiece
        }

        LineGraph {
            id:lineGraph
            anchors.top: titleSpace.bottom
            visible: false
        }

        Connections{
            target: daoManager
            onResumeTypeChanged: {
                console.log(daoManager.resumeType)
                emptyTextWarning.updateEmptyText()
                if(daoManager.resumeType == qsTr("Resumo do Fluxo de Caixa"))
                {
                    graph.visible = false
                    lineGraph.visible = true
                }
                else
                {
                    lineGraph.visible = false
                    graph.visible = true
                }
            }
        }


        Item {
            id: rectangleList
            width: limitTable.width
            height: parent.height - titleSpace.height - graph.height - parent.height/25
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: emptyGraph ? lineGraph.visible ? lineGraph.bottom
                                                        : titleSpace.bottom :  graph.bottom
            visible: daoManager.resumeType != qsTr("Resumo do Fluxo de Caixa") && !emptyGraph
            Image {
                id: limitTable
                y:  -limitTable.height/3
                z:10
                source: "qrc:/images/categories/limite_tabelagrafico.png"
            }
            ListView {
                id: categories
                anchors.fill: parent
                z: 5; clip: true
                model:  daoManager.categoryList
                delegate: ResumeListItem {
                    id: resumeItem
                    indexItem: index
                    category:  model.modelData
                    onClicked: {
                        categories.currentIndex = index;
                    }
                }
            }
        }
    }
}

