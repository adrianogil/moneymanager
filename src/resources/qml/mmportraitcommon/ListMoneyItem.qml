import QtQuick 1.0
import Entity 1.0

ExpansiveListItem {
    id: listMoneyItem

    property QtObject money

    backgroundPath: "qrc:/images/cashflow/item_background.png"
    backgroundPath_a: "qrc:/images/cashflow/item_background_a.png"
    backgroundPath_expanded: "qrc:/images/cashflow/item_background_expanded.png"
    backgroundPath_expanded_a: "qrc:/images/cashflow/item_background_expanded_a.png"
    backgroundPath_shadow: "qrc:/images/cashflow/item_background_shadow.png"
    backgroundPath_shadow_a: "qrc:/images/cashflow/item_background_shadow_a.png"

    Text {
        x: 30; y: 17
        width: 100
        text: money.description
        elide: Text.ElideRight
        font {pixelSize: 13; weight: Font.Bold}
    }
    Text {
        x: 30; y: 36
        width: 100
        text: money.moneyDate
        font {pixelSize: 11; weight: Font.Light}
    }
    Text {
        x: 221; y: 17
        width: 100
        text: (money.moneyType? "-" : "+") + (Math.round(money.amount*100)/100)
        font {pixelSize: 20; weight: Font.Normal}
    }
    Text {
        x: 30; y: 55
        width: 105
        visible: expanded
        text: qsTr("Categoria:")
        font {pixelSize: 14; weight: Font.Bold}
    }
    Text {
        x: 105; y: 55
        width: 100
        visible: expanded
        text: money.categoryByName
        font {pixelSize: 14; weight: Font.Normal}
    }
    Text {
        x: 185; y: 55
        width: 100
        visible: false //expanded
        text: "Frequência:"
//        text: qsTr("Frequência:")
        font {pixelSize: 14; weight: Font.Bold}
    }
    Text {
        x: 275; y: 55
        width: 100
        visible: false //expanded
        text: money.frequencyName
        font {pixelSize: 14; weight: Font.Normal}
    }
    Text {
        x: 30; y: 75
        width: 300
        visible: expanded
        text: "Descrição:"
//        text: qsTr("Descrição:")
        font {pixelSize: 14; weight: Font.Bold}
    }
    Text {
        x: 110; y: 75
        width: 300
        visible: expanded
        text: money.details
        font {pixelSize: 14; weight: Font.Normal}
    }

    state: "item-list"
}

