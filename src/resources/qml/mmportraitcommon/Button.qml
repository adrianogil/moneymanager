﻿import QtQuick 1.0


Rectangle {
    id: button

    width: buttonImg.progress? buttonImg.width : 160;
    height: buttonImg.progress? buttonImg.height : 41;

    color: buttonImg.progress? "transparent" : defaultColor
    radius: buttonImg.progress? 0 : 10; border.width: 0

    property bool toggleEnable: false
    property bool isToggled: false
    property bool textImgVisible: false

    property string text: ""

    property color textColor: "white"
    property color textColorToggled: "white"
    property color textColorPressed : "white"

    property int textPixelSize: 18

    property color pressedColor: "#3e4345"
    property color defaultColor: "#00adef"

    property url buttonImage: ""
    property url pressedImage: ""

    property bool forcedPress: false
    property bool forcedValue: false

    signal clicked

    function forcePressValue(value) {
        buttonImg.pressed = value;
        forcedPress = true;
        forcedValue = value;
    }
    function removeForcedValue(value) {
        forcedPress = false;
        buttonImg.pressed = value;
    }

    Image {
        id: buttonImg

        anchors.centerIn: button
        property bool pressed: false
        onPressedChanged: {
            if (forcedPress) {
                pressed = forcedValue;
            }
        }

        source: pressed && button.pressedImage != "" ? button.pressedImage : button.buttonImage

    }
    Text {
        id: buttonText

        visible: textImgVisible || buttonImg.progress == 0
        color: buttonImg.pressed? button.textColorPressed : button.textColor
        text: button.text; anchors.fill: button
        font { pixelSize: button.textPixelSize; weight: Font.DemiBold }
        horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter;
    }
    ColorAnimation on color {
        id: animation2Press
        running: false
        from: button.defaultColor;
        to: button.pressedColor;
        duration: 200
    }
    ColorAnimation on color {
        id: animation2Release
        running: false
        from: button.pressedColor;
        to: button.defaultColor;
        duration: 200
    }
    onIsToggledChanged: {
        if (button.isToggled) {
            if (buttonImg.progress != 0) {
                buttonImg.pressed = true;
            } else {
                animation2Press.start();
            }
        } else {
            if (buttonImg.progress != 0) {
                buttonImg.pressed = false;
            } else {
                animation2Release.start();
            }
        }
    }
    MouseArea {
        id: mouseArea

        anchors.fill: parent
        onPressed: {
            if (buttonImg.progress == 0) {
                animation2Press.start();
            } else {
                buttonImg.pressed = true;
            }

        }
        onReleased: {
            if (buttonImg.progress == 0) {
                if (!button.toggleEnable) {
                    animation2Press.stop();
                    animation2Release.start();
                }
            } else buttonImg.pressed = false;

            if (button.toggleEnable) {
                isToggled = !isToggled;
            }
            button.clicked()
        }
    }
}


