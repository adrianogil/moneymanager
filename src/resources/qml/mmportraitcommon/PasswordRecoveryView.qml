import QtQuick 1.0


Rectangle {
    id: recoveryView

    width: 344
    height: 243
    border.color: "#00adef"


    property string question: ""
    property bool sucess: true

    signal exit
    signal changedPassword(string login, string newPassword)

    function reset(){
        userNameLineEdit.text = ''
        answerLineEdit.text = ''
        passwordEdit.text = ''
        confirmPassowrdEdit.text = ''
        wrongAnswerMessage.visible = false
        loginNotFoundLabel.visible = false

        recoveryView.state = 'get-username'
        recoveryView.visible = false
        exit()
    }


    Rectangle{
        id: backgroundMainLabel
        width: parent.width
        height: 55
        color: "#00adef"


        Text{
            id: mainLabel
            text: qsTr("Recuperar senha")
            width: parent.width
            height: 29
            color: "white"
            anchors.verticalCenter: backgroundMainLabel.verticalCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.margins: 10
            font { pixelSize: 20; weight: Font.DemiBold; family: "Series 60 Sans" }

        }

    }


    //Seleciona nome do usuario, verifica se existe no banco
    Item {
        id: getUsernameItems
        width: userLabel.width
        anchors.top: backgroundMainLabel.bottom
        anchors.horizontalCenter: recoveryView.horizontalCenter

        Text {
            id: userLabel
            width: recoveryQuestionItems.width
            height: 35
            text: qsTr("Nome do usuário:")
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            horizontalAlignment: Text.AlignHCenter
            font { pixelSize: 16; weight: Font.DemiBold; family: "Series 60 Sans" }

        }

        Text {
            id: loginNotFoundLabel
            width: recoveryQuestionItems.width
            height: 29
            anchors.top: getUserButton.bottom
            anchors.margins: 10
            text: 'Usuário não encontrado'
//            text: qsTr('Usuário não encontrado')
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            horizontalAlignment: Text.AlignHCenter
            color: 'red'
            visible:  false
            font { pixelSize: 16; weight: Font.DemiBold; family: "Series 60 Sans" }

        }

        LineEdit {
            id: userNameLineEdit
            anchors.top: userLabel.bottom
            anchors.horizontalCenter: getUsernameItems.horizontalCenter
            anchors.margins: 5

            width: 300 ; height: 35
            text: ''

            onFocusChanged: {
                wrongAnswerMessage.visible = false
                userNameLineEdit.text = ''
            }
        }

        Button {
            id: getUserButton
            width: 148
            height: 38
            anchors.top: userNameLineEdit.bottom
            anchors.right: exitButton.left
            anchors.margins: 15
            radius: 20

            text:  qsTr('Ok')

            onClicked: {
                if (daoManager.isValidUser(userNameLineEdit.text)) {
                    console.log("user valid")
                    question = daoManager.getQuestion(userNameLineEdit.text)
                    recoveryView.state = 'ask-question'
                } else {
                    loginNotFoundLabel.visible = true
                }

            }
        }


        Button {
            id: exitButton
            width: 148
            height: 38
            radius: 20
            anchors.top: userNameLineEdit.bottom
            color: "#999999"
            anchors.margins: 15
            text: qsTr('Cancelar')
            onClicked: reset()

        }

    }


    Item {
        id: recoveryQuestionItems
        anchors.top: backgroundMainLabel.bottom
        anchors.horizontalCenter: recoveryView.horizontalCenter

        Text {
            id: questionLabel
            width: recoveryQuestionItems.width
            height: 29
            text: question
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            horizontalAlignment: Text.AlignHCenter
            font { pixelSize: 16; weight: Font.DemiBold; family: "Series 60 Sans" }

        }

        LineEdit {
            id: answerLineEdit
            anchors.top: questionLabel.bottom
            anchors.horizontalCenter: recoveryQuestionItems.horizontalCenter
            anchors.margins: 5

            width: 160
            height: 34
            text: ''

            onFocusChanged:  wrongAnswerMessage.visible = false


        }

        Button {
            id: recoveryButton
            width: 148
            height: 38
            radius: 20
            smooth: true
            anchors.margins: 15
            anchors.top: answerLineEdit.bottom
            anchors.right: exitButton1.left
            text: qsTr('Ok')

            onClicked: {

                if (daoManager.verifyUserAccountAnswer(userNameLineEdit.text, answerLineEdit.text)){
                    recoveryView.state = 'new-password'
                }
                else {
                    wrongAnswerMessage.visible = true
                }
            }
        }

        Button {
            id: exitButton1
            width: 148
            height: 38
            radius: 20
            color: "#999999"
            smooth: true
            anchors.margins: 15
            anchors.top: answerLineEdit.bottom
            text: qsTr('Cancelar')
            onClicked: reset()

        }

        Text {
            id: wrongAnswerMessage
            anchors.top: recoveryButton.bottom
            anchors.margins: 5
            width: recoveryQuestionItems.width
            horizontalAlignment: Text.AlignHCenter
            visible: false
            color: 'red'
            text: qsTr('Resposta Incorreta!')
            font { pixelSize: 16; weight: Font.DemiBold; family: "Series 60 Sans"; }
        }

    }

    Item {
        id: setNewPasswordItems
        anchors.top: backgroundMainLabel.bottom
        anchors.horizontalCenter: recoveryView.horizontalCenter
        visible: false

        Text {
            id: titleLabel
            anchors.margins: 10
            width: setNewPasswordItems.width
            horizontalAlignment: Text.AlignHCenter

            text: qsTr('Insira sua nova senha')
            font { pixelSize: 16; weight: Font.DemiBold; family: "Series 60 Sans" }

        }

        LineEdit {
            id: passwordEdit
            width: 300 ; height: 35
            anchors.margins: 5
            anchors.horizontalCenter: setNewPasswordItems.horizontalCenter
            anchors.top: titleLabel.bottom
            echoMode :TextInput.Password

            label: qsTr("Nova senha")

        }

        LineEdit {
            id: confirmPassowrdEdit
            width: 300 ; height: 35
            anchors.margins: 5
            anchors.horizontalCenter: setNewPasswordItems.horizontalCenter
            anchors.top: passwordEdit.bottom

            label: qsTr("Confirmar nova senha")
            echoMode : TextInput.Password

        }


        Button {
            id: setNewPasswordButton
            width: 148
            height: 38
            radius: 20
            anchors.top: confirmPassowrdEdit.bottom
            anchors.right: exitButton2.left
            anchors.margins: 15
            text: qsTr('Ok')

            onClicked: {
                passwordDontMatchLabel.visible = false
                if((passwordEdit.text === confirmPassowrdEdit.text)&&(passwordEdit.text != "")){
                    recoveryView.changedPassword(userNameLineEdit.text,passwordEdit.text)
                    recoveryView.visible = false
                } else {
                    passwordDontMatchLabel.visible = true
                }
            }
        }

        Text {
            id: passwordDontMatchLabel
            anchors.margins: 5
            text: 'Senha não é igual'
//            text: qsTr('Senha não é igual')
            color: "red"
            visible: false
            anchors.horizontalCenter: setNewPasswordItems.horizontalCenter
            anchors.top : setNewPasswordButton.bottom

            font { pixelSize: 7; weight: Font.DemiBold; family: "Series 60 Sans" }

        }

        Button {
            id: exitButton2
            width: 148
            height: 38
            radius: 20
            anchors.top: confirmPassowrdEdit.bottom
            anchors.margins: 15
            text: qsTr('Cancelar')
            color: "#999999"
            onClicked: reset()

        }
    }


    states : [
        State {
            name: 'get-username'
            PropertyChanges { target: recoveryQuestionItems; visible: false }
            PropertyChanges { target: setNewPasswordItems; visible: false }
            PropertyChanges { target: getUsernameItems; visible: true }



        },
        State {
            name: 'ask-question'
            PropertyChanges { target: recoveryQuestionItems; visible: true }
            PropertyChanges { target: setNewPasswordItems; visible: false }
            PropertyChanges { target: getUsernameItems; visible: false }


        },
        State {
            name: 'new-password'
            PropertyChanges { target: recoveryQuestionItems; visible: false }
            PropertyChanges { target: setNewPasswordItems; visible: true }
            PropertyChanges { target: getUsernameItems; visible: false }



        }
    ]

    state: 'get-username'


}
