﻿import QtQuick 1.0

View {
    id: newAccountView

    viewId: "new-account-view"
    //anchors.fill:parent
    width: bg.width; height: bg.height

    property bool allFieldsCheck: false
    property alias nameError: errorLogin.visible
    property alias passwordError: errorPassword.visible
    property alias confirmPasswordError: errorConfirmPassword.visible
    property alias loginError: errorLoginNewUser.visible
    property alias answerError: errorAnswer.visible
    property alias confirmPasswordText: confirmPasswordLineEdit.text
    property alias passwordText: passwordLineEdit.text
    property alias secretQuestionError: errorSecretQuestion.visible
    property alias secretQuestionText: secretQuestionLineEdit.text
    property int fontPixelSize: newAccountView.width/30

    function eraseFields(){

        nameLineEdit.text=""
        nameLineEdit.textFocus =false

        passwordLineEdit.text=""
        passwordLineEdit.textFocus =false

        confirmPasswordLineEdit.text=""
        confirmPasswordLineEdit.textFocus =false

        answerLineEdit.text=""
        answerLineEdit.textFocus= false

        daoManager.secretQuestion = ""
    }

    Rectangle {
        id: background
        anchors.fill: newAccountView
    }

    Image {
        id: bg
        source: "qrc:/images/common/background.png"
    }

    LineEdit {
        id: nameLineEdit
        anchors.horizontalCenter: newAccountView.horizontalCenter
        anchors.bottom: newAccountView.center
        y: 50
        fontPixelSize: newAccountView.fontPixelSize +4
        width: newAccountView.width * 0.7291; height: newAccountView.width * 0.15
        label: qsTr("Nome")
//        inputMask: "NNNNNNNNNN"
        onTextChanged: {
            errorLoginNewUser.visible = daoManager.isValidUser(text)
            daoManager.newUser.login = text
            newAccountView.nameError = false
        }

    }
    LineEdit {
        id: passwordLineEdit
        anchors.horizontalCenter: newAccountView.horizontalCenter
        anchors.top: nameLineEdit.bottom
        anchors.topMargin: 35
        fontPixelSize: newAccountView.fontPixelSize +4
        width: nameLineEdit.width; height: nameLineEdit.height
        label: qsTr("Senha")
        echoMode: TextInput.Password
        //inputMask: "NNNNNNNNNN"

        onTextChanged: {
            if (confirmPasswordLineEdit.text == text)
                daoManager.newUser.password = text
                passwordError = false
        }
    }
    LineEdit {
        id: confirmPasswordLineEdit

        anchors {
            top:  passwordLineEdit.bottom
            topMargin:35
            horizontalCenter: newAccountView.horizontalCenter
        }
        fontPixelSize: newAccountView.fontPixelSize +4
        width: nameLineEdit.width; height: nameLineEdit.height
        label: qsTr("Confirmar senha")
        echoMode: TextInput.Password
        //inputMask: "NNNNNNNNNN"

        onTextChanged: {
            if (passwordLineEdit.text == text)
                daoManager.newUser.password = text
                confirmPasswordError = false
        }
    }
    LineEdit {
        id: secretQuestionLineEdit

        anchors {
            top:  confirmPasswordLineEdit.bottom
            topMargin:35
            horizontalCenter: newAccountView.horizontalCenter
        }
        fontPixelSize: newAccountView.fontPixelSize -2

        width: nameLineEdit.width; height: nameLineEdit.height

        label: qsTr("Pergunta Secreta")
        text: daoManager.secretQuestion
         onTextChanged: {
            errorMessage.visible = false
            secretQuestionError = false
        }

        MouseArea {
            anchors.fill: secretQuestionLineEdit
            onClicked: {

                daoManager.openDropDown(daoManager.defaultSecretQuestions(), "secretQuestion")
                secretQuestionError =false
            }

        }
    }


    LineEdit {
        id: answerLineEdit

        anchors {
            top:  secretQuestionLineEdit.bottom
            topMargin:35
            horizontalCenter: newAccountView.horizontalCenter
        }

        width: nameLineEdit.width; height: nameLineEdit.height

        label: qsTr("Resposta")
        fontPixelSize: newAccountView.fontPixelSize +4
        onTextChanged: {
            daoManager.newUser.answer = text
            answerError = false
        }
    }
    Text{
        id: errorLogin
        visible: false
        anchors.horizontalCenter: newAccountView.horizontalCenter
        anchors.margins: 10
        anchors.top: nameLineEdit.bottom
        color: "red"
        text: "* Somente números e letras, no mínimo 2 dígitos"
//        text: qsTr("* Somente números e letras, no mínimo 2 dígitos")
        font {pixelSize: fontPixelSize; }
    }

    Text{
        id: errorLoginNewUser
        visible: false
        anchors.horizontalCenter: newAccountView.horizontalCenter
        anchors.margins: 10
        anchors.top: nameLineEdit.bottom
        color: "red"
        text: "* Usuário já cadastrado!"
        font {pixelSize: fontPixelSize; }
//        text: qsTr("* Usuário já cadastrado!")
    }
    Text{
        id: errorPassword
        visible: false
        anchors.horizontalCenter: newAccountView.horizontalCenter
        anchors.margins: 10
        anchors.top: passwordLineEdit.bottom
        color: "red"
        text: "* Somente números e letras, no mínimo 2 dígitos"
//        text: qsTr("* Somente números e letras, no mínimo 2 dígitos")
        font {pixelSize: fontPixelSize; }
    }
    Text{
        id: errorConfirmPassword
        visible: false
        anchors.horizontalCenter: newAccountView.horizontalCenter
        anchors.margins: 10
        anchors.top: confirmPasswordLineEdit.bottom
        color: "red"
        font {pixelSize: fontPixelSize; }
        text: qsTr("* Senha incorreta.")
    }
    Text{
        id: errorAnswer
        visible: false
        anchors.horizontalCenter: newAccountView.horizontalCenter
        anchors.margins: 10
        anchors.top: answerLineEdit.bottom
        color: "red"
        font {pixelSize: fontPixelSize; }
        text: qsTr("* Campo vazio.")
    }

    Text{
        id: errorSecretQuestion
        visible: false
        anchors.horizontalCenter: newAccountView.horizontalCenter
        anchors.margins: 10
        anchors.top: secretQuestionLineEdit.bottom
        color: "red"
        font {pixelSize: fontPixelSize; }
        text: qsTr("* Campo vazio.")
    }
}
