import QtQuick 1.0

Rectangle {
    id: lineEdit

    width: 340; height: 42
    color: "white"
    radius: 10
    border { width: 2; color: "darkGray"; }

    property alias text: textInput.text
    property string label: ""
    property alias echoMode: textInput.echoMode
    property int fontPixelSize: 16
    property alias textFocus: textInput.focus
    property bool owned: false
    property alias maxLength: textInput.maximumLength
    property bool isEnabled : true
    property alias activeFocus : textInput.activeFocusOnPress

    // type of LineEdit
    //      0  ->  Using Keyboard
    //      1  ->  Using Calendar
    //      2  ->  Using Calculator
    property int type: 0
    property alias inputMask: textInput.inputMask

    signal textFilled
    signal gainedFocus

    Text {
        visible: textInput.text == "" && !textInput.focus
        text: label
        font {pixelSize: fontPixelSize; weight: Font.Bold}
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignLeft
        anchors.fill: lineEdit
        anchors.margins: 20
    }

    Connections {
        target: daoManager
        onKeyboardActivatedChanged: {
            if (owned && !daoManager.keyboardActivated) {
                owned = false;
                console.log("LineEdit.qml - call signal textFilled");
                lineEdit.textFilled();
            }
        }
        onKeyboardValueChanged: {
            if (owned) {
                if (type == 0)
                    textInput.text = daoManager.keyboardValue
            }
        }
        onCalendarActivatedChanged: {
            console.log("LineEdit.qml:onCalendarActivatedChanged - owned = " + owned);
            if (owned && !daoManager.calendarActivated) {
                owned = false;
                console.log("LineEdit.qml - call signal textFilled");
                lineEdit.textFilled();
            }
        }
        onCalendarValueChanged: {
            if (owned) {
                if (type == 1)
                    textInput.text = daoManager.calendarValue
            }
        }
        onCalculatorActivatedChanged: {
            if (owned && !daoManager.calculatorActivated) {
                owned = false;
                console.log("LineEdit.qml - call signal textFilled");
                lineEdit.textFilled();
            }
        }
        onCalculatorValueChanged: {
            if (owned) {
                if (type == 2)
                    textInput.text = daoManager.calculatorValue / 100.0
            }
        }
    }

    TextInput {
        id: textInput
        enabled: type != 2 && type != 1 && !daoManager.isUsingKeyboard()

        text: lineEdit.text
        width: parent.width; height: parent.height
        anchors { margins: 10; fill: parent; verticalCenter: parent.verticalCenter }
        color: "black"
        font { pixelSize: lineEdit.fontPixelSize; weight: Font.DemiBold; }

        property bool alreadyFilled: false

        onFocusChanged: {
            if (focus == true) {
                alreadyFilled = true;
            } else {
                if (alreadyFilled) {
                    lineEdit.textFilled()
                }
            }
        }
    }
    MouseArea {
        anchors.fill: lineEdit
        enabled: (isEnabled) ? type == 2 || type == 1 || daoManager.isUsingKeyboard() : false
        onClicked: {
            console.log("LineEdit.qml - MouseArea:onClicked - ownedKeyboard")
            owned = true;
            if (type == 0) {

                daoManager.keyboardValue = text;
                daoManager.keyboardActivated = true;
            } else if (type == 1) {
                if (text == "")
                    daoManager.calendarValue = daoManager.today();
                else daoManager.calendarValue = text;
                console.log("LineEdit.qml - MouseArea:onClicked - trying to activate calendar: " +
                            daoManager.calendarActivated)
                daoManager.calendarActivated = true;
            } else if (type == 2) {
                if (text != "")
                    daoManager.calculatorValue = parseFloat(text)*100;
                else
                    daoManager.calculatorValue = 0;
                daoManager.calculatorActivated = true;
            }
        }
    }
}
