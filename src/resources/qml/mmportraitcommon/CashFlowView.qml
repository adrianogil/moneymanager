import QtQuick 1.0

View {
    id: cashFlowView

    property int selectedItemIndex: cashFlowList.selectedItemIndex
    property variant selectedItemIndexList: cashFlowList.selectedItemIndexList
    property bool multipleRemoveEnabled: false

    function removeCurrentMoneyRegister() {
        cashFlowList.removeCurrentMoneyRegister()
    }
    function removeSelectedMoneyRegisters() {
        cashFlowList.removeSelectedMoneyRegisters();
    }
    function createNewMoneyRegister(idNewMoney) {
        cashFlowList.idNewItem = idNewMoney
    }

    viewId: "cashflow-view"
    width: bg.width; height: bg.height

    Rectangle {
        id: background
        anchors.fill: cashFlowView
    }

    Image {
        id:bg
        source: "qrc:/images/cashflow/background.png"
    }


    onMultipleRemoveEnabledChanged: {
        if (multipleRemoveEnabled &&
                cashFlowList.selectedItemIndex == -1) {
            cashFlowList.removedItemIndexList = []
            cashFlowList.state = "item-multiple-remove"
        } else if (!multipleRemoveEnabled) {
            cashFlowList.state = "item-list"
        }
    }

    Column {
        PeriodSelector {
            id: periodSelector
        }
        CashFlowList {
            id: cashFlowList
            moneyList: daoManager.moneyList
        }
    }
}
