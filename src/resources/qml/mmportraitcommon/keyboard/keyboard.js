var component
var popup

function createPopup(val, parentItem) {

    component = Qt.createComponent("PopUp.qml");

    if (component.status == Component.Ready) {
        popup = component.createObject(parentItem);
        popup.text = val
        if (popup == null) {
            // Error Handling
            console.log("Error creating object");
        }
    } else if (component.status == Component.Error) {
        // Error Handling
        console.log("Error loading component:", component.errorString());
    }
}

function destroyPopup() {
    popup.destroy()
}

var row1Length = 10
var row2Length = 9
var row3Length = 7
var row4Length = 2

//new Array("Letters", "SHFTLetters", "Numbers", "+Numbers", "SpecialLetters", "SHFTSpecialLetters");
//§$£€¥¤<>[] {}^\~®©¦| "@?!;:,  %# //+Numbers
//1234567890 ()+*=&_-' "@?!;:,  %# //Numbers
//åäáàãâéèêë öóòôøíìîï úùûüñýç  ßæ //SpecialLetters
//ÅÄÁÀÃÂÉÈÊË ÖÓÒÔØÍÌÎÏ ÚÙÛÜÑÝÇ  ßÆ //SHFTSpecialLetters
//qwertyuiop asdfghjkl zxcvbnm  ./ //Letters
//QWERTYUIOP ASDFGHJKL ZXCVBNM  ./ //SHFTLetters

var values = new Array(
                //row1
                new Array("\u0071","\u0051","\u0031","\u00A7","\u00E5","\u00C5"),
                new Array("\u0077","\u0057","\u0032","\u0024","\u00E4","\u00C4"),
                new Array("\u0065","\u0045","\u0033","\u00A3","\u00E1","\u00C1"),
                new Array("\u0072","\u0052","\u0034","\u20AC","\u00E0","\u00C0"),
                new Array("\u0074","\u0054","\u0035","\u00A5","\u00E3","\u00C3"),
                new Array("\u0079","\u0059","\u0036","\u00A4","\u00E2","\u00C2"),
                new Array("\u0075","\u0055","\u0037","\u003C","\u00E9","\u00C9"),
                new Array("\u0069","\u0049","\u0038","\u003E","\u00E8","\u00C8"),
                new Array("\u006F","\u004F","\u0039","\u005B","\u00EA","\u00CA"),
                new Array("\u0070","\u0050","\u0030","\u005D","\u00EB","\u00CB"),
                //row2
                new Array("\u0061","\u0041","\u0028","\u007B","\u00F6","\u00D6"),
                new Array("\u0073","\u0053","\u0029","\u007D","\u00F3","\u00D3"),
                new Array("\u0064","\u0044","\u002B","\u005E","\u00F2","\u00D2"),
                new Array("\u0066","\u0046","\u002A","\u002F","\u00F4","\u00D4"),
                new Array("\u0067","\u0047","\u003D","\u007E","\u00F8","\u00D8"),
                new Array("\u0068","\u0048","\u0026","\u00AE","\u00ED","\u00CD"),
                new Array("\u006A","\u004A","\u005F","\u00A9","\u00EC","\u00CC"),
                new Array("\u006B","\u004B","\u002D","\u00A6","\u00EE","\u00CE"),
                new Array("\u006C","\u004C","\u0027","\u007C","\u00EF","\u00CF"),
                //row3
                new Array("\u007A","\u005A","\u0022","\u0022","\u00FA","\u00DA"),
                new Array("\u0078","\u0058","\u0040","\u0040","\u00F9","\u00D9"),
                new Array("\u0063","\u0043","\u003F","\u003F","\u00FB","\u00DB"),
                new Array("\u0076","\u0056","\u0021","\u0021","\u00FC","\u00DC"),
                new Array("\u0062","\u0042","\u003B","\u003B","\u00F1","\u00D1"),
                new Array("\u006E","\u004E","\u003A","\u003A","\u00FD","\u00DD"),
                new Array("\u006D","\u004D","\u002C","\u002C","\u00E7","\u00C7"),
                //row4
                new Array("\u0020","\u0020","\u0020","\u0020","\u0020","\u0020"),
                new Array("\u002E","\u002E","\u0025","\u0025","\u00DF","\u00DF"),
                new Array("\u002F","\u002F","\u0023","\u0023","\u00E6","\u00C6")
                )

var shiftImgPath = "resources/arrow-up.png"
var backspaceImgPath = "resources/arrow-left.png"
var numText = "123"
var specText = "\u00E4\u00E9"
