import QtQuick 1.0

import "keyboard.js" as KeyboardEngine

Rectangle {
    id: main

    width: parent.width
    height: 254

    property string text: ""

    Behavior on opacity { NumberAnimation { duration: 200 } }

    Column {
        id: column

        spacing: 2

        width: main.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.fill: parent
        anchors.topMargin: 3

        Row {
            id: row1

            spacing: 2
            anchors.horizontalCenter: parent.horizontalCenter

            KeyButton { normalValue: KeyboardEngine.values[0][0]; normalShiftedValue: KeyboardEngine.values[0][1]; numValue: KeyboardEngine.values[0][2]; numShiftedValue: KeyboardEngine.values[0][3]; specValue: KeyboardEngine.values[0][4]; specShiftedValue: KeyboardEngine.values[0][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[1][0]; normalShiftedValue: KeyboardEngine.values[1][1]; numValue: KeyboardEngine.values[1][2]; numShiftedValue: KeyboardEngine.values[1][3]; specValue: KeyboardEngine.values[1][4]; specShiftedValue: KeyboardEngine.values[1][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[2][0]; normalShiftedValue: KeyboardEngine.values[2][1]; numValue: KeyboardEngine.values[2][2]; numShiftedValue: KeyboardEngine.values[2][3]; specValue: KeyboardEngine.values[2][4]; specShiftedValue: KeyboardEngine.values[2][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[3][0]; normalShiftedValue: KeyboardEngine.values[3][1]; numValue: KeyboardEngine.values[3][2]; numShiftedValue: KeyboardEngine.values[3][3]; specValue: KeyboardEngine.values[3][4]; specShiftedValue: KeyboardEngine.values[3][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[4][0]; normalShiftedValue: KeyboardEngine.values[4][1]; numValue: KeyboardEngine.values[4][2]; numShiftedValue: KeyboardEngine.values[4][3]; specValue: KeyboardEngine.values[4][4]; specShiftedValue: KeyboardEngine.values[4][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[5][0]; normalShiftedValue: KeyboardEngine.values[5][1]; numValue: KeyboardEngine.values[5][2]; numShiftedValue: KeyboardEngine.values[5][3]; specValue: KeyboardEngine.values[5][4]; specShiftedValue: KeyboardEngine.values[5][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[6][0]; normalShiftedValue: KeyboardEngine.values[6][1]; numValue: KeyboardEngine.values[6][2]; numShiftedValue: KeyboardEngine.values[6][3]; specValue: KeyboardEngine.values[6][4]; specShiftedValue: KeyboardEngine.values[6][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[7][0]; normalShiftedValue: KeyboardEngine.values[7][1]; numValue: KeyboardEngine.values[7][2]; numShiftedValue: KeyboardEngine.values[7][3]; specValue: KeyboardEngine.values[7][4]; specShiftedValue: KeyboardEngine.values[7][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[8][0]; normalShiftedValue: KeyboardEngine.values[8][1]; numValue: KeyboardEngine.values[8][2]; numShiftedValue: KeyboardEngine.values[8][3]; specValue: KeyboardEngine.values[8][4]; specShiftedValue: KeyboardEngine.values[8][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[9][0]; normalShiftedValue: KeyboardEngine.values[9][1]; numValue: KeyboardEngine.values[9][2]; numShiftedValue: KeyboardEngine.values[9][3]; specValue: KeyboardEngine.values[9][4]; specShiftedValue: KeyboardEngine.values[9][5]; onClicked: main.text += value }
        }
        Row {
            id: row2

            spacing: 2
            anchors.horizontalCenter: parent.horizontalCenter

            KeyButton { normalValue: KeyboardEngine.values[10][0]; normalShiftedValue: KeyboardEngine.values[10][1]; numValue: KeyboardEngine.values[10][2]; numShiftedValue: KeyboardEngine.values[10][3]; specValue: KeyboardEngine.values[10][4]; specShiftedValue: KeyboardEngine.values[10][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[11][0]; normalShiftedValue: KeyboardEngine.values[11][1]; numValue: KeyboardEngine.values[11][2]; numShiftedValue: KeyboardEngine.values[11][3]; specValue: KeyboardEngine.values[11][4]; specShiftedValue: KeyboardEngine.values[11][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[12][0]; normalShiftedValue: KeyboardEngine.values[12][1]; numValue: KeyboardEngine.values[12][2]; numShiftedValue: KeyboardEngine.values[12][3]; specValue: KeyboardEngine.values[12][4]; specShiftedValue: KeyboardEngine.values[12][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[13][0]; normalShiftedValue: KeyboardEngine.values[13][1]; numValue: KeyboardEngine.values[13][2]; numShiftedValue: KeyboardEngine.values[13][3]; specValue: KeyboardEngine.values[13][4]; specShiftedValue: KeyboardEngine.values[13][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[14][0]; normalShiftedValue: KeyboardEngine.values[14][1]; numValue: KeyboardEngine.values[14][2]; numShiftedValue: KeyboardEngine.values[14][3]; specValue: KeyboardEngine.values[14][4]; specShiftedValue: KeyboardEngine.values[14][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[15][0]; normalShiftedValue: KeyboardEngine.values[15][1]; numValue: KeyboardEngine.values[15][2]; numShiftedValue: KeyboardEngine.values[15][3]; specValue: KeyboardEngine.values[15][4]; specShiftedValue: KeyboardEngine.values[15][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[16][0]; normalShiftedValue: KeyboardEngine.values[16][1]; numValue: KeyboardEngine.values[16][2]; numShiftedValue: KeyboardEngine.values[16][3]; specValue: KeyboardEngine.values[16][4]; specShiftedValue: KeyboardEngine.values[16][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[17][0]; normalShiftedValue: KeyboardEngine.values[17][1]; numValue: KeyboardEngine.values[17][2]; numShiftedValue: KeyboardEngine.values[17][3]; specValue: KeyboardEngine.values[17][4]; specShiftedValue: KeyboardEngine.values[17][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[18][0]; normalShiftedValue: KeyboardEngine.values[18][1]; numValue: KeyboardEngine.values[18][2]; numShiftedValue: KeyboardEngine.values[18][3]; specValue: KeyboardEngine.values[18][4]; specShiftedValue: KeyboardEngine.values[18][5]; onClicked: main.text += value }
        }
        Row {
            id: row3

            spacing: 2
            anchors.horizontalCenter: parent.horizontalCenter

            ShiftButton { id: shiftButton; width: 50; image: true; value: KeyboardEngine.shiftImgPath; onToggled: main.shift() }

            KeyButton { normalValue: KeyboardEngine.values[19][0]; normalShiftedValue: KeyboardEngine.values[19][1]; numValue: KeyboardEngine.values[19][2]; numShiftedValue: KeyboardEngine.values[19][3]; specValue: KeyboardEngine.values[19][4]; specShiftedValue: KeyboardEngine.values[19][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[20][0]; normalShiftedValue: KeyboardEngine.values[20][1]; numValue: KeyboardEngine.values[20][2]; numShiftedValue: KeyboardEngine.values[20][3]; specValue: KeyboardEngine.values[20][4]; specShiftedValue: KeyboardEngine.values[20][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[21][0]; normalShiftedValue: KeyboardEngine.values[21][1]; numValue: KeyboardEngine.values[21][2]; numShiftedValue: KeyboardEngine.values[21][3]; specValue: KeyboardEngine.values[21][4]; specShiftedValue: KeyboardEngine.values[21][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[22][0]; normalShiftedValue: KeyboardEngine.values[22][1]; numValue: KeyboardEngine.values[22][2]; numShiftedValue: KeyboardEngine.values[22][3]; specValue: KeyboardEngine.values[22][4]; specShiftedValue: KeyboardEngine.values[22][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[23][0]; normalShiftedValue: KeyboardEngine.values[23][1]; numValue: KeyboardEngine.values[23][2]; numShiftedValue: KeyboardEngine.values[23][3]; specValue: KeyboardEngine.values[23][4]; specShiftedValue: KeyboardEngine.values[23][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[24][0]; normalShiftedValue: KeyboardEngine.values[24][1]; numValue: KeyboardEngine.values[24][2]; numShiftedValue: KeyboardEngine.values[24][3]; specValue: KeyboardEngine.values[24][4]; specShiftedValue: KeyboardEngine.values[24][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[25][0]; normalShiftedValue: KeyboardEngine.values[25][1]; numValue: KeyboardEngine.values[25][2]; numShiftedValue: KeyboardEngine.values[25][3]; specValue: KeyboardEngine.values[25][4]; specShiftedValue: KeyboardEngine.values[25][5]; onClicked: main.text += value }

            Button { id: bsButton; image: true; value: KeyboardEngine.backspaceImgPath; onClicked: main.text = main.text.substring(0, main.text.length-1) }

        }
        Row {
            id: row4

            spacing: 2
            anchors.horizontalCenter: parent.horizontalCenter

            ShiftButton { id: numButton; width: 40; value: KeyboardEngine.numText                
                onToggled: {
                    if (specButton.state == "toggled")
                        specButton.state = ""
                    if (shiftButton.state == "toggled")
                        shiftButton.state = ""

                    main.shift()
                }
            }
            ShiftButton { id: specButton; width: 40; value: KeyboardEngine.specText
                onToggled: {
                    if (numButton.state == "toggled")
                        numButton.state = ""
                    if (shiftButton.state == "toggled")
                        shiftButton.state = ""

                    main.shift()
                }
            }

            KeyButton { width: 100; normalValue: KeyboardEngine.values[26][0]; normalShiftedValue: KeyboardEngine.values[26][1]; numValue: KeyboardEngine.values[26][2]; numShiftedValue: KeyboardEngine.values[26][3]; specValue: KeyboardEngine.values[26][4]; specShiftedValue: KeyboardEngine.values[26][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[27][0]; normalShiftedValue: KeyboardEngine.values[27][1]; numValue: KeyboardEngine.values[27][2]; numShiftedValue: KeyboardEngine.values[27][3]; specValue: KeyboardEngine.values[27][4]; specShiftedValue: KeyboardEngine.values[27][5]; onClicked: main.text += value }
            KeyButton { normalValue: KeyboardEngine.values[28][0]; normalShiftedValue: KeyboardEngine.values[28][1]; numValue: KeyboardEngine.values[28][2]; numShiftedValue: KeyboardEngine.values[28][3]; specValue: KeyboardEngine.values[28][4]; specShiftedValue: KeyboardEngine.values[28][5]; onClicked: main.text += value }

            Button { width: 60; value: "Go"; onClicked: main.opacity = 0 }
        }
    }

    Timer {
        id: bsAutoRepeater

        interval: 100; repeat: true
        running: false

        onTriggered: {
            bsButton.clicked()
            vibra.start(25, 50)
        }
    }

    MouseArea {
        anchors.fill: parent

        onPressAndHold: {
            if (contains(mouse, row3.children[row3.children.length - 1]))
                bsAutoRepeater.running = true
        }

        onMousePositionChanged: {
            var i

            for (i = 0; i < row1.children.length; i++) {
                if (contains(mouse, row1.children[i]))
                    row1.children[i].mark()
                else
                    row1.children[i].unmark()
            }

            for (i = 0; i < row2.children.length; i++) {
                if (contains(mouse, row2.children[i]))
                    row2.children[i].mark()
                else
                    row2.children[i].unmark()
            }

            for (i = 0; i < row3.children.length; i++) {
                if (contains(mouse, row3.children[i])) {
                    row3.children[i].mark()                    

                } else {

                    if (i == row3.children.length - 1 && row3.children[i].marked)
                        bsAutoRepeater.running = false

                    row3.children[i].unmark()
                }

            }

            for (i = 0; i < row4.children.length; i++) {
                if (contains(mouse, row4.children[i]))
                    row4.children[i].mark()
                else
                    row4.children[i].unmark()
            }
        }

        onReleased: {

            if (bsAutoRepeater.running)
                bsAutoRepeater.running = false

            var i

            if (mouseY > row1.y && mouse.y < row1.height + row1.y) {
                for (i = 0; i < row1.children.length; i++) {
                    if (contains(mouse, row1.children[i])) {
                        row1.children[i].unmark()
                        row1.children[i].clicked()
                    }
                }
            } else if (mouseY > row2.y && mouse.y < row2.height + row2.y) {
                for (i = 0; i < row2.children.length; i++) {
                    if (contains(mouse, row2.children[i])) {
                        row2.children[i].unmark()
                        row2.children[i].clicked()
                    }
                }
            } else if (mouseY > row3.y && mouse.y < row3.height + row3.y) {
                for (i = 0; i < row3.children.length; i++) {
                    if (contains(mouse, row3.children[i])) {
                        row3.children[i].unmark()
                        row3.children[i].clicked()
                    }
                }
            } else if (mouseY > row4.y && mouse.y < row4.height + row4.y) {
                for (i = 0; i < row4.children.length; i++) {
                    if (contains(mouse, row4.children[i])) {
                        row4.children[i].unmark()
                        row4.children[i].clicked()
                    }
                }
            }

            vibra.start(25, 50)

        }
    }

    function contains(mouse, object) {
        if (mouse.x > main.mapFromItem(object.parent, object.x, object.y).x && mouse.x < main.mapFromItem(object.parent, object.x, object.y).x + object.width &&
                mouse.y > main.mapFromItem(object.parent, object.x, object.y).y && mouse.y < main.mapFromItem(object.parent, object.x, object.y).y + object.height) {
            return true
        } else {
            return false
        }
    }

    function shift() {

        var i = 0
        // Normal State
        if (shiftButton.state != "toggled" && numButton.state != "toggled" && specButton.state != "toggled") {
            for (i = 0; i < row1.children.length; i++)
                row1.children[i].state = "normal"
            for (i = 0; i < row2.children.length; i++)
                row2.children[i].state = "normal"
            for (i = 1; i < row3.children.length - 1; i++)
                row3.children[i].state = "normal"
            for (i = 2; i < row4.children.length - 1; i++)
                row4.children[i].state = "normal"

//            console.log("Normal State")

            return
        }

        // Shifted Normal State
        if (shiftButton.state == "toggled" && numButton.state != "toggled" && specButton.state != "toggled") {
            for (i = 0; i < row1.children.length; i++)
                row1.children[i].state = "normalShifted"
            for (i = 0; i < row2.children.length; i++)
                row2.children[i].state = "normalShifted"
            for (i = 1; i < row3.children.length - 1; i++)
                row3.children[i].state = "normalShifted"
            for (i = 2; i < row4.children.length - 1; i++)
                row4.children[i].state = "normalShifted"

//            console.log("ShiftedNormal State")

            return
        }

        // Num State
        if (shiftButton.state != "toggled" && numButton.state == "toggled" && specButton.state != "toggled") {

            for (i = 0; i < row1.children.length; i++)
                row1.children[i].state = "num"
            for (i = 0; i < row2.children.length; i++)
                row2.children[i].state = "num"
            for (i = 1; i < row3.children.length - 1; i++)
                row3.children[i].state = "num"
            for (i = 2; i < row4.children.length - 1; i++)
                row4.children[i].state = "num"

//            console.log("Num State")

            return
        }

        // ShiftedNum State
        if (shiftButton.state == "toggled" && numButton.state == "toggled" && specButton.state != "toggled") {
            for (i = 0; i < row1.children.length; i++)
                row1.children[i].state = "numShifted"
            for (i = 0; i < row2.children.length; i++)
                row2.children[i].state = "numShifted"
            for (i = 1; i < row3.children.length - 1; i++)
                row3.children[i].state = "numShifted"
            for (i = 2; i < row4.children.length - 1; i++)
                row4.children[i].state = "numShifted"

//            console.log("ShiftedNum State")

            return
        }

        // Spec State
        if (shiftButton.state != "toggled" && numButton.state != "toggled" && specButton.state == "toggled") {
            for (i = 0; i < row1.children.length; i++)
                row1.children[i].state = "spec"
            for (i = 0; i < row2.children.length; i++)
                row2.children[i].state = "spec"
            for (i = 1; i < row3.children.length - 1; i++)
                row3.children[i].state = "spec"
            for (i = 2; i < row4.children.length - 1; i++)
                row4.children[i].state = "spec"

//            console.log("Spec State")

            return
        }

        // ShiftedSpec State
        if (shiftButton.state == "toggled" && numButton.state != "toggled" && specButton.state == "toggled") {
            for (i = 0; i < row1.children.length; i++)
                row1.children[i].state = "specShifted"
            for (i = 0; i < row2.children.length; i++)
                row2.children[i].state = "specShifted"
            for (i = 1; i < row3.children.length - 1; i++)
                row3.children[i].state = "specShifted"
            for (i = 2; i < row4.children.length - 1; i++)
                row4.children[i].state = "specShifted"

//            console.log("ShiftedSpec State")

            return
        }
    }
}
