import QtQuick 1.0

import "keyboard.js" as KeyboardEngine

Rectangle {
    id: main

    width: 45; height: 70
    radius: 3; smooth: true

    property string value: ""
    property string normalValue: ""
    property string normalShiftedValue: ""
    property string numValue: ""
    property string numShiftedValue: ""
    property string specValue: ""
    property string specShiftedValue: ""

    property bool toggle: false
    property bool image: false
    property bool marked: false

    property int colorScheme: 0 // 0 is dark, 1 is light

    property Rectangle popups

    signal clicked()

    gradient: Gradient {
        GradientStop { id: gradStopLight; position: 0.00; color: "#777777" }
        GradientStop { id: gradStopDark; position: 1.00; color: "#3e3e3e" }
    }

    Text {
        id: text

        anchors.fill: parent
        font.pixelSize: 18
        text: value; color: "white"; style: Text.Raised

        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }

    states: [
        State { name: "normal"; PropertyChanges { target: main; value: normalValue } },
        State { name: "normalShifted"; PropertyChanges { target: main; value: normalShiftedValue } },
        State { name: "num"; PropertyChanges { target: main; value: numValue } },
        State { name: "numShifted"; PropertyChanges { target: main; value: numShiftedValue } },
        State { name: "spec"; PropertyChanges { target: main; value: specValue } },
        State { name: "specShifted"; PropertyChanges { target: main; value: specShiftedValue } }
    ]

    function mark() {
        if (!marked) {
            marked = true

            gradStopLight.color = "#6015E6"
            gradStopDark.color = "#4A1D99"

            if (text.text != " ")
                KeyboardEngine.createPopup(text.text, main)
        }
    }

    function unmark() {
        if (marked) {
            marked = false

            gradStopLight.color = "#777777"
            gradStopDark.color = "#3e3e3e"

            if (text.text != " ")
                KeyboardEngine.destroyPopup()
        }
    }

    Component.onCompleted: main.state = "normal"

}
