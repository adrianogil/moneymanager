import QtQuick 1.0

Rectangle {
    id: main

    color: Qt.lighter("blue")
    radius: 3; smooth: true
    x: 0; y: -62
    width: 33; height: 60

    property alias text: text.text

    Text {
        id: text

        anchors.fill: parent
        font.pixelSize: 18
        color: "white"; style: Text.Raised;

        verticalAlignment: Text.AlignVCenter;
        horizontalAlignment: Text.AlignHCenter;
    }
}
