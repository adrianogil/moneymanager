import QtQuick 1.0

Rectangle {
    id: main

    width: 50; height: 70
    radius: 3; smooth: true

    property string value: ""

    property bool image: false
    property bool marked: false

    property int colorScheme: 0 // 0 is dark, 1 is light

//    property Rectangle popups

    signal clicked()

    gradient: Gradient {
        GradientStop { id: gradStopLight; position: 0.00; color: "#D0D0D0" }
        GradientStop { id: gradStopDark; position: 1.00; color: "#8E8E8E" }
    }

    Text {
        id: text

        anchors.fill: parent
        font.pixelSize: 18
        color: "white"; style: Text.Raised

        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter

        visible: false
    }

    Image {
        id: image

        anchors.centerIn: parent
        scale: 0.5

        visible: false
    }

    function mark() {
        if (!marked) {
            marked = true

            gradStopLight.color = "#6015E6"
            gradStopDark.color = "#4A1D99"
        }
    }

    function unmark() {
        if (marked) {
            marked = false

            gradStopLight.color = "#D0D0D0"
            gradStopDark.color = "#8E8E8E"
        }
    }

    Component.onCompleted: {
        if (main.image) {
            image.source = value
            image.visible = true
        } else {
            text.text = value
            text.visible = true
        }
    }
}
