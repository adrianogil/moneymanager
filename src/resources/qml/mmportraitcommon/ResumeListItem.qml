import QtQuick 1.0
import Entity 1.0

Item {
    id: wrapper

    width:  itemBackground.width; height: itemBackground.height

    property int indexItem: -1

    property QtObject category

    signal clicked

    Image {
        id: itemBackground
        source: "qrc:/images/categories/barra_tabela_grafico.png"
    }

    Rectangle {
        x: 15
        width: 20; height: 20;
        radius: 5; color: "white"
        visible: category.percentage > 0
        anchors.verticalCenter: wrapper.verticalCenter
        Rectangle {
            anchors.centerIn: parent
            width: 15; height: 15;
            radius: 4; color: category.colorRgb
        }
    }

    Column {
        id: firstColumn

        width: parent.width - parent.width/3
        Text {
            x: parent.width/7
            width: firstColumn.width
            height: wrapper.height
            color:  "black"
            font.pixelSize: 18;
            text: category.name
            verticalAlignment: "AlignVCenter"
            elide: Text.ElideRight
        }
    }
    Column {
        anchors.left: firstColumn.right
        width: parent.width - firstColumn.width
        Text {
            width: parent.width; height: wrapper.height
            font.pixelSize: 18;
            color:  "black"
            text: (Math.floor(category.percentage*100))/100+ "%"
            verticalAlignment: "AlignVCenter"
            horizontalAlignment: "AlignHCenter"
        }
    }
    MouseArea {
        anchors.fill: wrapper

        onClicked: parent.clicked()
    }
}
