import QtQuick 1.0

Item {
    id: menu

    property bool multipleRemoveEnabled: false

    signal clicked(string btnClicked);

    height: closeButton.height
    width: closeButton.width + okButton.width + passwordButton.width + contextButton.width

    Button {
        id: closeButton
        anchors.left: menu.left
        pressedImage: 'qrc:/images/bottom-menu/fechar_azul.png'
        buttonImage: 'qrc:/images/bottom-menu/fechar_cinza.png'
        onClicked: menu.clicked('close')
    }

    Button {
        id: okButton
        pressedImage: 'qrc:/images/bottom-menu/ok_azul.png'
        buttonImage: 'qrc:/images/bottom-menu/ok_cinza.png'
        visible: true
        onClicked: menu.clicked('ok')
    }
    Button {
        id: passwordButton
        pressedImage: 'qrc:/images/bottom-menu/senha_azul.png'
        buttonImage: 'qrc:/images/bottom-menu/senha_cinza.png'
        visible: true
        onClicked: menu.clicked('password')
    }
    Button {
        id: contextButton
        anchors.right: menu.right
        pressedImage: 'qrc:/images/bottom-menu/mais_azul.png'
        buttonImage: 'qrc:/images/bottom-menu/mais_cinza.png'
        onClicked: menu.clicked('context')
    }
    Button {
        id: cancelButton
        pressedImage: 'qrc:/images/bottom-menu/cancelar_azul.png'
        buttonImage: 'qrc:/images/bottom-menu/cancelar_cinza.png'
        onClicked: menu.clicked('cancel')
    }
    Button {
        id: categoryButton
        pressedImage: 'qrc:/images/bottom-menu/categorias_azul.png'
        buttonImage: 'qrc:/images/bottom-menu/categorias_cinza.png'
        onClicked: menu.clicked('category')
    }
    Button {
        id: graphButton
        pressedImage: 'qrc:/images/bottom-menu/grafico_azul.png'
        buttonImage: 'qrc:/images/bottom-menu/grafico_cinza.png'
        onClicked: menu.clicked('show')
    }
    Button {
        id: addButton
        pressedImage: 'qrc:/images/bottom-menu/adicionar_azul.png'
        buttonImage: 'qrc:/images/bottom-menu/adicionar_cinza.png'
        onClicked: menu.clicked('add')
    }
    Button {
        id: add2Button
        pressedImage: 'qrc:/images/bottom-menu/adicionar1_azul.png'
        buttonImage: 'qrc:/images/bottom-menu/adicionar1_cinza.png'
        onClicked: menu.clicked('add')
    }
    Button {
        id: removeButton
        pressedImage: "qrc:/images/bottom-menu/remover_azul.png"
        buttonImage: "qrc:/images/bottom-menu/remover_cinza.png"
        onClicked: menu.clicked('remove')
    }
    onMultipleRemoveEnabledChanged: {
        if (multipleRemoveEnabled) {
            console.log('BottomMenu.qml - onMultipleRemoveEnabledChanged - Force Press Value to true')
            removeButton.forcePressValue(true);
        } else {
            removeButton.removeForcedValue(false);
        }
    }

    Button {
        id: editButton
        pressedImage: 'qrc:/images/bottom-menu/editar_azul.png'
        buttonImage: 'qrc:/images/bottom-menu/editar_cinza.png'
        onClicked: menu.clicked('edit')
    }

    states: [
        State {
            name: 'login'
            PropertyChanges { target: okButton; visible: true
                x: closeButton.width
                y:0
                color: "#0d0101"
            }
            PropertyChanges { target: passwordButton; visible: true
                x: okButton.width + okButton.x
                y:0
                color: "#140303"
            }
            PropertyChanges { target: cancelButton; visible: false}
            PropertyChanges { target: categoryButton; visible: false}
            PropertyChanges { target: addButton; visible: false}
            PropertyChanges { target: removeButton; color: "#160404"; visible: false}
            PropertyChanges { target: graphButton; visible: false}
            PropertyChanges { target: editButton; color: "#140101"; visible: false}
            PropertyChanges { target: add2Button; visible: false}

//            PropertyChanges {
//                target: menu
//                height: 40
//            }

            PropertyChanges {
                target: contextButton
                color: "#090000"
            }
        },
        State {
            name: 'new-account'
            PropertyChanges { target: okButton; visible: true
                x: closeButton.width
                color: "#0d0101"
            }
            PropertyChanges { target: passwordButton; visible: false}
            PropertyChanges { target: cancelButton; x: okButton.x + okButton.width; y: 0; color: "#0f0303"; visible: true}
            PropertyChanges { target: categoryButton; visible: false}
            PropertyChanges { target: graphButton; visible: false}
            PropertyChanges { target: addButton; visible: false}
            PropertyChanges { target: removeButton; color: "#0d0202"; visible: false}
            PropertyChanges { target: editButton; visible: false}
            PropertyChanges { target: add2Button; visible: false}

            PropertyChanges {
                target: contextButton
                color: "#0f0000"
            }
        },
        State {
            name: 'goal-list'
            PropertyChanges { target: okButton; visible: false}
            PropertyChanges { target: passwordButton; visible: false}
            PropertyChanges { target: cancelButton; visible: false}
            PropertyChanges { target: categoryButton; visible: false}
            PropertyChanges { target: graphButton; visible: false}
            PropertyChanges { target: addButton; visible: true
                x: editButton.x + editButton.width
                y: 0
                color: "#110202"
                border.color: "#ecdbdb"
            }
            PropertyChanges { target: removeButton; visible: true
                x: addButton.x + addButton.width
                color: "#140101"
            }
            PropertyChanges { target: editButton; visible: true
                x: closeButton.width;
            }
            PropertyChanges { target: add2Button; visible: false}

            PropertyChanges {
                target: contextButton
                color: "#110202"
            }
        },
        State {
            name: 'money-add'
            PropertyChanges { target: okButton; visible: true
                x: closeButton.width
            }
            PropertyChanges { target: passwordButton; visible: false}
            PropertyChanges { target: cancelButton; visible: true
                x: okButton.x + okButton.width
                color: "#00eee1e1"
            }
            PropertyChanges { target: categoryButton; visible: false}
            PropertyChanges { target: addButton; visible: false}
            PropertyChanges { target: removeButton; visible: false}
            PropertyChanges { target: graphButton; visible: false}

            PropertyChanges {
                target: contextButton
                color: "#ece1e1"
            }
            PropertyChanges { target: editButton; visible: false}
            PropertyChanges { target: add2Button; visible: false}
        },
        State {
            name: 'resume'
            PropertyChanges { target: okButton; visible: false}
            PropertyChanges { target: passwordButton; visible: false}
            PropertyChanges { target: cancelButton; visible: false}
            PropertyChanges { target: graphButton; visible: true
                x: closeButton.width
                color: "#160202"
            }
            PropertyChanges { target: categoryButton; visible: true
                x: graphButton.x + graphButton.width
            }
            PropertyChanges { target: addButton; visible: false}
            PropertyChanges { target: removeButton; visible: false}

            PropertyChanges {
                target: contextButton
                color: "#0f0000"
            }
            PropertyChanges { target: editButton; visible: false}
            PropertyChanges { target: add2Button; visible: false}
        },
        State {
            name: 'goal-add'
            PropertyChanges { target: okButton; visible: true
                x: closeButton.width
            }
            PropertyChanges { target: passwordButton; visible: false}
            PropertyChanges { target: cancelButton; visible: true
                x: okButton.x + okButton.width
                color: "#00eee1e1"
            }
            PropertyChanges { target: categoryButton; visible: false}
            PropertyChanges { target: addButton; visible: false}
            PropertyChanges { target: removeButton; visible: false}
            PropertyChanges { target: graphButton; visible: false}

            PropertyChanges {
                target: contextButton
                color: "#ece1e1"
            }
            PropertyChanges { target: editButton; visible: false}
            PropertyChanges { target: add2Button; visible: false}
        },
        State {
            name: 'cashflow'
            PropertyChanges { target: okButton; visible: false}
            PropertyChanges { target: passwordButton; visible: false}
            PropertyChanges { target: cancelButton; visible: false}
            PropertyChanges { target: categoryButton; visible: false}
            PropertyChanges { target: graphButton; visible: false}
            PropertyChanges { target: addButton; visible: true
                x: editButton.x + editButton.width
                border.color: "#eadfdf"
            }
            PropertyChanges { target: removeButton; visible: true
                x: addButton.x + addButton.width
            }
            PropertyChanges { target: editButton; visible: true
                x: closeButton.width;
            }
            PropertyChanges { target: add2Button; visible: false}
        },
        State {
            name: 'goal-edit'
            PropertyChanges { target: okButton; visible: true
                x: closeButton.width
            }
            PropertyChanges { target: passwordButton; visible: false}
            PropertyChanges { target: cancelButton; visible: true
                x: okButton.x + okButton.width
                color: "#00eee1e1"
            }
            PropertyChanges { target: categoryButton; visible: false}
            PropertyChanges { target: addButton; visible: false}
            PropertyChanges { target: removeButton; visible: false}
            PropertyChanges { target: graphButton; visible: false}

            PropertyChanges {
                target: contextButton
                color: "#ece1e1"
            }
            PropertyChanges { target: editButton; visible: false}
            PropertyChanges { target: add2Button; visible: false}
        },
        State {
            name: "hide"
            PropertyChanges { target: bottomMenu; visible: false }
        }
    ]
}
