import QtQuick 1.0
import '../utils.js' as Utils

View {
    id: categoriesView

    property int menuShowIndex: -1
    property int selectedIndex: -1
    property int newCategoryIndex: -1
    property bool pendingEdit: false

    viewId: "categories-view"
    width:  topBar.width; height: column.height

    Column {
        id: column
        Image {
            id: topBar

            source:  "qrc:/images/categories/barra_superior.png"

            Button {
                id: editButton
                anchors.top: topBar.top
                anchors.right: topBar.right
                scale: 0.9; opacity: 0.75
                pressedImage: 'qrc:/images/bottom-menu/editar_azul.png'
                buttonImage: 'qrc:/images/bottom-menu/editar_cinza.png'
                //onClicked: menu.clicked('edit')
            }
            Text {
                id: title

                anchors.fill: topBar
                text: qsTr("Categorias cadastradas")
                font {pixelSize: 18; family: "Series 60 Sans"; weight: Font.Bold}
                horizontalAlignment: "AlignHCenter"
                verticalAlignment: "AlignVCenter"
            }

        }
        Image {
            id: coreBackground

            source: "qrc:/images/common/background.png"

            Image {
                id: hintImageAboutListItemSize
                source: "qrc:/images/categories/resume_item.png"
                visible: false
            }
            ListView {
                id: categories

                width: hintImageAboutListItemSize.width;
                height: coreBackground.height - 0.15 * categoriesView.height;
                anchors.topMargin: 10
                anchors.horizontalCenter: coreBackground.horizontalCenter
                spacing: 1; clip: true
                model:  daoManager.categoryList
                delegate: CategoriesListItem {
                    id: listElement
                    category: model.modelData
                    onMenuVisibleChanged: {
                        if (menuVisible) {
                            menuShowIndex = index;

                            if (index == categories.count - 1){
                                height = height + 100
                                categories.positionViewAtIndex(index, ListView.Contain)
                            }
                        } else if (index == categories.count - 1)
                            height = height - 100
                    }
                    selected: selectedIndex == index
                    onSelectedItem: {
                        selectedIndex = index;
                    }
                    onAddedItem: {
                        daoManager.addNewEmptyCategory()
                        Utils.setTimerFunction(2000, function() {
                                                   categories.positionViewAtIndex(categories.count-1,
                                                                                  ListView.Contain)
                                               }, categoriesView);
                    }
                    onRemovedItem: {
                        daoManager.removeCategory(index)
                    }
                    onEditedItem: {
                        if(newCategoryName.length < 2)
                            caractereInvalidVisible = true
                        else {
                            editModeEnabled = false;
                            caractereInvalidVisible = false
                            daoManager.updateCategory(index, newCategoryName)
                            categoriesView.pendingEdit = false
                            categories.positionViewAtIndex(categories.count-1,
                                                           ListView.Contain)
                            daoManager.catEditEnabled = false
                        }

                    }
                    Component.onCompleted: {
                        if (category && category.name == "") {
                            categoriesView.pendingEdit = true
                            categoriesView.newCategoryIndex = index;
                        }
                        categoriesView.menuShowIndexChanged.connect(
                                    function() {
                                        listElement.menuVisible = (menuShowIndex == index)
                                    }
                                    )
                        categoriesView.selectedIndexChanged.connect(
                                    function() {
                                        if (selectedIndex != index)
                                            listElement.menuVisible = false
                                        if (editModeEnabled)
                                            removeFocusFromLineEdit()
                                    }
                                    )
                        categoriesView.pendingEditChanged.connect(
                                    function() {
                                        if (!pendingEdit && editModeEnabled)
                                            removeFocusFromLineEdit()
                                    }
                                    )
                    }
                }
            }
        }
        Image {
            id: backgroundBottomImage

            source: "qrc:/images/categories/barra_inferior.png"
            MouseArea {
                anchors.fill: parent
                onClicked: categoriesView.pendingEdit = false
            }
        }
    }
}
