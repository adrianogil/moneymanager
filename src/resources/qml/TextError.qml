import QtQuick 1.0

Text {
    id: errorMsg

    property QtObject targetEdit
    property int fontPixelSize: 12

    width: targetEdit.width
    anchors.top: targetEdit.bottom
    anchors.topMargin: 10
    color: "red";
    wrapMode: Text.WordWrap
    horizontalAlignment: Text.AlignHCenter
    font { pixelSize: fontPixelSize}
}
