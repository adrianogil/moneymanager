﻿import QtQuick 1.0
import "mmportraitcommon" as Common

Common.View {
    id: splashMainView

    viewId: "splashMainView"
    width: splashView.width
    height: splashView.height

    Common.SplashView {
        id:splashView
        z:100
        anchors.top: splashMainView.top
        onLoadView: loadMainView()
    }

    function loadMainView() {
        var component = Qt.createComponent("MainView.qml");
        if (component.status == Component.Error) {
            console.log("main.qml - loadMainView: " + component.errorString())
        }else if (component.status == Component.Ready) {
            var mainView = component.createObject(splashMainView);
            mainView.width = splashMainView.width
            mainView.height = splashMainView.height
        } else {
            component.statusChanged.connect(
                        function() {
                            if (component.status == Component.Ready) {
                                var mainView = component.createObject(splashMainView);
                                mainView.width = splashMainView.width
                                mainView.height = splashMainView.height
                            } else if (component.status == Component.Error){
                                console.log("main.qml - error: " + component.errorString())
                            }

                        })

        }
    }


}
