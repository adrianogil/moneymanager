﻿var dropDownObj
var messageBoxObj

var dropDownComponent = Qt.createComponent("mmportraitcommon/DropDownList.qml");
var messageBoxComponent = Qt.createComponent("mmportraitcommon/MessageBox.qml");

// Designed to be acessed in the following way:
//          lastSubViewByState['State'] = 'subview'
var lastSubViewByState = {}

function map2View(state, subView)
{
    var view = ""
    switch(state) {
    case "login":
        if (subView == "login") {
            view = "login-view"
        }
        break;
    case "new-account":
        view = "new-account-view"
        break;
    case "cashflow":
        if (subView == "cashflow-list") {
            view = "cashflow-view"
        } else if (subView == "money-add" ||
                   subView == "money-edit") {
            view = "money-add-view"
        }
        break;
    case "resume":
        if (subView == "resume-view") {
            view = "resume-view"
        } else if (subView == "categories") {
            view = "categories-view"
        }
        break;
    case "goal":
        if (subView == "goal-list") {
            view = "targets-view"
        } else if (subView == "goal-add" ||
                   subView == "goal-edit") {
            view = "targets-add-view"
        }
        break;
    default:
        break;
    }

    return view;
}

function createDropDown(dropDownOptions, propertyName)
{
    if (dropDownObj) {
        dropDownObj.opacity = 0;
        dropDownObj.destroy(500);
    }

    dropDownObj = dropDownComponent.createObject(mainView)
    if (dropDownObj == null)
        console.log("Error creating object");
    else {
        dropDownObj.model = dropDownOptions
        dropDownObj.property2BeAssigned = propertyName
    }
}

function createMessageBox(title, message, options, viewList, stateList, actionList)
{
    if (messageBoxObj) {
        messageBoxObj.opacity = 0;
        messageBoxObj.destroy(500);
    }

    messageBoxObj = messageBoxComponent.createObject(mainView)
    if (messageBoxObj == null)
        console.log("Error creating object");
    else {
        messageBoxObj.title = title
        messageBoxObj.message = message
        messageBoxObj.options = options
        messageBoxObj.viewList = viewList
        messageBoxObj.stateList = stateList
        messageBoxObj.actionList = actionList
    }
}

function addNewGoal()
{
    daoManager.addNewGoal()
    mainView.state = "goal"
    mainView.subview = "goal-list"
}

function changePassword(login, newPassword)
{
    var sucess = daoManager.updatePassword(login, newPassword)
    if (sucess) {
        daoManager.openMessageBox(qsTr("Modificar Senha"),
                                  qsTr("Senha alterada com sucesso"),
                                  [qsTr("Ok")],
                                  ["mainView"],
                                  ["cashflow"],
                                  ["nope"])
        userLogin(login, newPassword)
    } else {
        daoManager.openMessageBox(qsTr("Modificar Senha"),
                                  qsTr("Erro! Tente outra vez"),
                                  [qsTr("Ok")],
                                  ["mainView"],
                                  ["login"],
                                  ["nope"])
    }
    enable()
}

function disable(){
    superiorMenu.enabled =false
    bottomMenu.enabled =false
}
function enable(){
    superiorMenu.enabled = true
    bottomMenu.enabled = true
}

function cancelAddNewMoney(){
    mainView.state = "cashflow"
    mainView.subview = "cashflow-list"
}

function addNewMoney()
{
    var newId = daoManager.addNewMoney()
    cashFlowView.createNewMoneyRegister(newId)
    mainView.state = "cashflow";
    mainView.subview = "cashflow-list"
}

function cancelNewAccount()
{
    superiorMenu.state = "login"
    mainView.state = "login";
    mainView.subview = "login-view"
    newAccountView.eraseFields();
}

function cancelAddGoal()
{
    mainView.state = "goal";
    mainView.subview = "goal-list"
}

function editMoney()
{
    daoManager.updateMoney()
    mainView.state = "cashflow";
    mainView.subview = "cashflow-list"
}

function removeSelectedMoney()
{
    mainView.state = "cashflow"
    cashFlowView.removeCurrentMoneyRegister()
}

function removeSelectedMoneyList()
{
    mainView.state = "cashflow"
    bottomMenu.multipleRemoveEnabled = false;
    cashFlowView.multipleRemoveEnabled = false;
    cashFlowView.removeSelectedMoneyRegisters()
}

function removeSelectedGoal()
{
    mainView.state = "goal"
    targetView.state = "item-remove"
}

function removeSelectedGoalList()
{
    mainView.state = "goal"
    bottomMenu.multipleRemoveEnabled = false;
    targetView.state = "item-remove"
}

function userLogin(login, password)
{
    var userLogged = daoManager.login(login, password)
    loginView.errorLoginMessage = !userLogged

    console.log("ViewManager.js - userLogin() - " + userLogged)

    if (userLogged) {
        superiorMenu.state = "cashflow"
        mainView.state = "cashflow"
        mainView.subview = "cashflow-list"
    }
}

function userLogout()
{
    buttonMenu.state = "hide"
    loginView.clearLogin()
    newAccountView.eraseFields()
    superiorMenu.state = "login"
    mainView.state = "login"
    mainView.subview = "login-view"
    bottomMenu.state = "login"
    buttonMenu.logoutEnabled = false
    loginView.focus = true
    changeEnable()

}

function changeEnable(){

    superiorMenu.enabled=!superiorMenu.enabled
    buttonMenu.enabled =!buttonMenu.enabled
}
function managerOthersView(action){

    changeEnable()

    if(action == "showAbout"){
        aboutview.visible=true
    } else if(action == "showContact"){
        contactview.visible =true
    } else if (action == "showHelp"){
        helpview.visible= true
    } else if (action == "logout"){
        userLogout()
    } else if (action == "showDropBox") {
        dropboxView.state = 'login'
        dropboxView.visible = true
    }

}

function createNewUser() {
    var userLogged = daoManager.createNewUser()

    console.log("ViewManager.js - userLogin() - " + userLogged)
    buttonMenu.logoutEnabled = true

    if (userLogged) {
        superiorMenu.state = "cashflow"
        mainView.state = "cashflow"
        mainView.subview = "cashflow-list"
        buttonMenu.state = "hide"
    }
    newAccountView.eraseFields()
}

function editSelectedGoal(index) {
    if (mainView.state == "goal") {
        mainView.subview = "goal-edit"
        ;
    }
}

function updateGoal() {
    if (mainView.state == "goal" && mainView.subview == "goal-edit") {
        daoManager.updateEditedGoal();
        mainView.subview = "goal-list"
    }

}

function parseDate(stringDate)
{
    return new Date(parseInt(stringDate.slice(6,8)),
                    parseInt(stringDate.slice(3,5)) -1,
                    parseInt(stringDate.slice(0,2)));
}

function monthsDiff(initialDate, finalDate)
{
    var firstDate = parseDate(initialDate);

    var lastDate = parseDate(finalDate);

    var d = (lastDate.getMonth() + 1 + lastDate.getYear() * 12) -
            (firstDate.getMonth() + 1 + firstDate.getYear() * 12)

    var days = (lastDate.getDay() - 30) + (30 - firstDate.getDay())
    if (days > 25) d = d + 1

    console.log("Valor da diferença"+d)
    return d;
}

function dateDiff(initialDate,finalDate)
{
    var firstDate = parseDate(initialDate);

    var lastDate = parseDate(finalDate);

    if(firstDate.getTime()>lastDate.getTime())
        return true;
    return false;
}

function invalidDataGoal() {

    if (daoManager.newGoal.description.length == 0){
        targetAdd.messageError = "Descrição Vazia"
        targetAdd.descriptionError = true
    } else if (daoManager.newGoal.description.length < 2){
        targetAdd.messageError = "Descrição com menos de 2 caracteres"
        targetAdd.descriptionError = true
    } else if (targetAdd.initialDateValue == "") {
        targetAdd.messageError = qsTr("Data inicial vazia")
        targetAdd.initialDateError = true
    } else if (targetAdd.finalDateValue == "") {
        targetAdd.messageError = qsTr("Data final vazia")
        targetAdd.finalDateError = true
    } else if (dateDiff(targetAdd.initialDateValue,targetAdd.finalDateValue)){
        targetAdd.messageError = qsTr("Data Final menor que data inicial")
        targetAdd.finalDateError = true
    }
    else if (parseFloat(targetAdd.targetValue) <= 0 || targetAdd.targetValue == "") {
        targetAdd.messageError = qsTr("Valor precisa ser maior que 0")
        targetAdd.targetError = true
    }



    if (targetAdd.descriptionError || targetAdd.initialDateError ||
            targetAdd.finalDateError || targetAdd.targetError)
        return true
    else return false
}

function invalidDataNewAccount() {

    if (daoManager.newUser.login.length < 2)
        newAccountView.nameError = true
    else if (daoManager.checkUser(daoManager.newUser.login)){
        newAccountView.nameError = false
        newAccountView.loginError = true
    } else newAccountView.loginError = false

    if (newAccountView.passwordText.length < 2)
        newAccountView.passwordError = true
    else
        newAccountView.passwordError = false

    if (newAccountView.confirmPasswordText == "" || newAccountView.confirmPasswordText !== newAccountView.passwordText)
        newAccountView.confirmPasswordError = true
    else
        newAccountView.confirmPasswordError = false

    if (daoManager.newUser.answer == "")
        newAccountView.answerError = true
    else
        newAccountView.answerError = false

    if (daoManager.secretQuestion == "")
        newAccountView.secretQuestionError = true
    else
       newAccountView.secretQuestionError = false

    if (newAccountView.nameError || newAccountView.passwordError || newAccountView.loginError
            || newAccountView.confirmPasswordError || newAccountView.answerError)

        return true
    else return false
}

function manageMenuActions(menuAction) {
    console.log("ViewManager.js - manageMenuActions - " + menuAction)
    switch (menuAction) {
    case "ok":
        if (mainView.state == "cashflow" && mainView.subview == "money-add") {
            if (daoManager.newMoney.description == "") {
                moneyAddView.errorRequiredFields = true
                moneyAddView.errorSourceType = 0;
                moneyAddView.errorDescriptionText = "Descrição vazia"
            } else if (daoManager.newMoney.description.length < 2) {
                moneyAddView.errorRequiredFields = true
                moneyAddView.errorSourceType = 1;
                moneyAddView.errorDescriptionText = "Descrição com menos de 2 caracteres"
            } else if (daoManager.newMoney.amount == 0 || daoManager.calculatorValue == 0) {
                moneyAddView.errorRequiredFields = true
                moneyAddView.errorSourceType = 1;
                moneyAddView.errorDescriptionText = "Valor deve ser diferente de zero"
            } else {
                daoManager.openMessageBox(qsTr("Criar novo Registro"),
                                          qsTr("Deseja realmente adicionar os dados inseridos?"),
                                          [qsTr("Sim"), "Não"],
                                          ["mainView", "mainView"],
                                          ["cashflow","cashflow"],
                                          ["money-agreed2add", "nope"])
            }
        } else if (mainView.state == "cashflow" && mainView.subview == "money-edit") {

            if (daoManager.newMoney.description == "" ||
                    daoManager.newMoney.description.length < 5)
                moneyAddView.errorRequiredFields = true
            else {
                daoManager.openMessageBox(qsTr("Editar Registro"),
                                          "Deseja realmente salvar as alterações?",
                                          [qsTr("Sim"), "Não"],
                                          ["mainView", "mainView"],
                                          ["cashflow","cashflow"],
                                          ["money-agreed2edit", "nope"])
            }
        }else if (mainView.state == "login") {
            userLogin(loginView.userName,loginView.userPassword);
            buttonMenu.logoutEnabled=true
        }else if (mainView.state == "new-account") {

            newAccountView.allFieldsCheck = invalidDataNewAccount()
//            newAccountView.answerError = invalidDataNewAccount()
            if (!newAccountView.allFieldsCheck)
                daoManager.openMessageBox("Criar usuário",
                                          "Deseja realmente salvar o novo usuário?",
                                          [qsTr("Sim"), "Não"],
                                          ["mainView", "mainView"],
                                          ["new-account","new-account"],
                                          ["login-after-account-creation", "nope"])
        } else if (mainView.state == "goal" && mainView.subview == "goal-add") {
            targetAdd.goalError = invalidDataGoal();

            if(!targetAdd.goalError)
                daoManager.openMessageBox(qsTr("Criar Meta"),
                                          qsTr("Deseja realmente salvar a nova meta?"),
                                          [qsTr("Sim"), "Não"],
                                          ["mainView", "mainView"],
                                          ["goal","goal"],
                                          ["goal-agreed2add", "nope"])
        } else if (mainView.state == "goal" && mainView.subview == "goal-edit") {
            targetAdd.goalError = invalidDataGoal();

            if(!targetAdd.goalError)
                daoManager.openMessageBox(qsTr("Editar Meta"),
                                          "Deseja realmente salvar as alterações da meta?",
                                          [qsTr("Sim"), "Não"],
                                          ["mainView", "mainView"],
                                          ["goal","goal"],
                                          ["goal-agreed2edit", "nope"])
        }
        buttonMenu.state = "hide"
        break;
    case "password":
        disable()
        buttonMenu.state = "hide"
        recoveryPassword.visible =true

        break;
    case "cancel":
        buttonMenu.state = "hide"
        if (mainView.subview == "money-add" ||
                mainView.subview == "money-edit") {
            daoManager.openMessageBox("Cancelar Edição de Registro",
                                      "Deseja realmente cancelar a operação?",
                                      [qsTr("Sim"), "Não"],
                                      ["mainView", "mainView"],
                                      ["cashflow","cashflow"],
                                      ["cashflow-list", "nope"])

        } else if (mainView.subview == "goal-add" ||
                   mainView.subview == "goal-edit") {
            daoManager.openMessageBox("Cancelar Edição de Meta",
                                      "Deseja realmente cancelar a operação?",
                                      [qsTr("Sim"), "Não"],
                                      ["mainView", "mainView"],
                                      ["goal","goal"],
                                      ["goal-cancel", "nope"])
            //            mainView.subview = "goal-list"
        }else if (mainView.subview == "login") {
            if (!newAccountView.errorLineEdit)
                daoManager.openMessageBox("Criar usuário",
                                          "Deseja realmente cancelar o cadastro?",
                                          [qsTr("Sim"), "Não"],
                                          ["mainView", "mainView"],
                                          ["login","new-account"],
                                          ["cancel-new-account", "nope"])
        } else if (mainView.state == "new-account") {
            bottomMenu.state = "login";
        }

        break;
    case "add":
        buttonMenu.state = "hide"
        if (mainView.state == "cashflow"){
            moneyAddView.clearFields();
            mainView.subview = "money-add"
        }
        else if (mainView.state == "goal"){
            targetAdd.clearFields();
            mainView.subview = "goal-add"
        }
        break;
    case "show":
        buttonMenu.state = "hide"
        if (mainView.state == "cashflow")
            mainView.subview = "cashflow-list"
        else if (mainView.state == "resume") {
            mainView.subview = 'resume-view'

        }
        break;
    case "edit":
        buttonMenu.state = "hide"
        if (mainView.state == "cashflow" && cashFlowView.selectedItemIndex != -1) {
            mainView.subview = "money-edit"
            daoManager.setMoney2BeEdited(cashFlowView.selectedItemIndex);
        } else if (mainView.state == "goal" && targetView.selectedItem != -1) {
            mainView.subview = "goal-edit"
            daoManager.setGoal2BeEdited(targetView.selectedItem)
        }
        break;
    case "remove":
        buttonMenu.state = "hide"
        if (mainView.state == "cashflow") {
            if (cashFlowView.selectedItemIndex != -1) {
                cashFlowView.multipleRemoveEnabled = false;
                bottomMenu.multipleRemoveEnabled = false;
                daoManager.openMessageBox(qsTr("Excluir dados"),
                                          qsTr("Deseja realmente remover o item selecionado?"),
                                          [qsTr("Sim"), "Não"],
                                          ["mainView", "mainView"],
                                          ["cashflow", "cashflow"],
                                          ["money-agreed2remove", "nope"])
            } else {
                if (cashFlowView.multipleRemoveEnabled) {
                    if (cashFlowView.selectedItemIndexList.length > 0) {
                        var pluralItem = cashFlowView.selectedItemIndexList.length > 1;
                        daoManager.openMessageBox(qsTr("Excluir dados"),
                                                  qsTr("Deseja realmente remover o" + (pluralItem? "s ": " ")) +
                                                  (pluralItem? cashFlowView.selectedItemIndexList.length : "") +
                                                  qsTr(" ite" + (pluralItem? "ns " : "m ") + "selecionado" +
                                                       (pluralItem? "s ?": " ?")),
                                                  [qsTr("Sim"), "Não"],
                                                  ["mainView", "mainView"],
                                                  ["cashflow", "cashflow"],
                                                  ["money-agreed2remove-list", "nope"])
                    } else {
                        cashFlowView.multipleRemoveEnabled = false;
                        bottomMenu.multipleRemoveEnabled = false;
                    }
                } else {
                    console.log("viewmanager.js - ManageMenuActions - Enter Multiple Remove Function");
                    cashFlowView.multipleRemoveEnabled = true;
                    bottomMenu.multipleRemoveEnabled = true;
                }
            }
        } else if (mainView.state == "goal") {
            if (targetView.selectedItem != -1) {
                bottomMenu.multipleRemoveEnabled = false;
                daoManager.openMessageBox(qsTr("Excluir dados"),
                                          qsTr("Deseja realmente remover o item selecionado?"),
                                          [qsTr("Sim"), "Não"],
                                          ["mainView", "mainView"],
                                          ["goal", "goal"],
                                          ["goal-agreed2remove", "nope"])
            } else {
                if (targetView.state == "item-multiple-remove") {
                    if (targetView.selectedItemIndexList.length > 0) {
                        var pluralItem = targetView.selectedItemIndexList.length > 1;
                        daoManager.openMessageBox(qsTr("Excluir dados"),
                                                  qsTr("Deseja realmente remover o" + (pluralItem? "s ": " ")) +
                                                  (pluralItem? targetView.selectedItemIndexList.length : "") +
                                                  qsTr(" ite" + (pluralItem? "ns " : "m ") + "selecionado" +
                                                       (pluralItem? "s ?": " ?")),
                                                  [qsTr("Sim"), "Não"],
                                                  ["mainView", "mainView"],
                                                  ["goal", "goal"],
                                                  ["goal-agreed2remove-list", "nope"])
                    } else {
                        targetView.state = "item-list";
                        bottomMenu.multipleRemoveEnabled = false;
                    }
                } else {
                    console.log("viewmanager.js - ManageMenuActions - Enter Multiple Remove Function");
                    targetView.state = "item-multiple-remove";
                    bottomMenu.multipleRemoveEnabled = true;
                }
            }
        }

        break;
    case "category":
        if (mainView.state == "resume")
            mainView.subview = 'categories'
        break;
    case "close":
        daoManager.openMessageBox(qsTr("Sair"),
                                  qsTr("Deseja realmente sair do aplicativo?"),
                                  [qsTr("Sim"), "Não"],
                                  ["mainView", "mainView"],
                                  [mainView.state, mainView.state],
                                  ["agreed2exit", "nope"])
        break;
    case "context":
        if (buttonMenu.state=="show") {
            buttonMenu.state="hide";
        } else {
            buttonMenu.state="show";
        }
        break;
    default:
        break;
    }
}

function flowManager(currentState, currentView)
{
    console.log("ViewManager.js - flowManager - currentState: " +
                currentState + ", currentView: " + currentView)

    pageSlider.doPageSlider(map2View(currentState, currentView));

    switch(currentState) {
    case "cashflow":
        if (currentView == "money-add") {
            bottomMenu.state = "money-add"
        } else if (currentView == "money-edit") {
            bottomMenu.state = "money-add"
        } else if (currentView == "cashflow-list") {
            bottomMenu.state = "cashflow"
        } else {
            if (lastSubViewByState[currentState] != null) {
                mainView.subview = lastSubViewByState[currentState]
            } else {
                mainView.subview = "cashflow-list"
            }
        }
        break;
    case "goal":
        if (currentView != "goal-list" &&
                currentView != "goal-add" &&
                currentView != "goal-edit") {if (lastSubViewByState[currentState] != null) {
                mainView.subview = lastSubViewByState[currentState]
            } else {
                mainView.subview = "goal-list"
                bottomMenu.state = "goal-list"
            }
        } else if (currentView == "goal-edit") {
            targetAdd.subview = "goal-edit"
            bottomMenu.state = "goal-edit"
        } else if (currentView == "goal-add") {
            targetAdd.subview = "goal-add"
            bottomMenu.state = "goal-add"
        } else bottomMenu.state = currentView
        break;
    case "login":
        bottomMenu.state = "login"
        mainView.subview = "login"
        break;
    case "resume":
        if (currentView != "resume-view" &&
                currentView != "categories") {
            if (lastSubViewByState[currentState] != null) {
                mainView.subview = lastSubViewByState[currentState]
            } else {
                mainView.subview = "resume-view"
            }
        }
        bottomMenu.state = 'resume'
        break;
    default:
        bottomMenu.state = currentState
        break;
    }
    lastSubViewByState[mainView.state] = mainView.subview
}
