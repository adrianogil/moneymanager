﻿import QtQuick 1.0
import "pageslider.js" as SliderManager

Row {
    id: pageSlider

    property QtObject pageSliderAnimation: defaultAnimation

    function init() {
        SliderManager.hideAllViews()
    }
    function doPageSlider(nextViewId) {
        SliderManager.doPageSlider(nextViewId);
    }

    NumberAnimation {
        id: defaultAnimation

        property real newX: 0

        onNewXChanged: {
            console.log("MainView.qml - pageSliderAnimation:onNewXChanged: " + newX)
        }
        onRunningChanged: {
            console.log("MainView.qml - pageSliderAnimation:onRunningChanged: " + running)
        }

        running: false
        target: pageSlider; property: "x";
        to: newX; duration: 1000
        easing.type: Easing.OutCubic
    }
    Component.onCompleted: {
        for (var i = 0; i < pageSlider.children.length; i++)
            SliderManager.registerView(pageSlider.children[i]);
    }

}
