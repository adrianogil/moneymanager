QT += sql network

include($$PWD/src/src.pri)

CONFIG -= app_bundle
CONFIG += mobility
MOBILITY += systeminfo

TARGET = moneymanager
TEMPLATE = app

MOC_DIR = build
OBJECTS_DIR = build
RCC_DIR = build

BADFILES = $${TARGET} \
    *-stamp \
    *.changes \
    *.deb

BADDIRS = obj \
    moc \
    rcc \
    debian \
    build

QMAKE_CLEAN += -r $$BADDIRS $$BADFILES

unix:!symbian {
    #RESOURCES += src/resources/meego/meegoimages.qrc
    RESOURCES += src/resources/symbian/symbianimages.qrc
}

symbian: {
    TARGET.UID3 = 0xEEFF45EC
    # Allow network access on Symbian
    TARGET.CAPABILITY += UserEnvironment NetworkServices

    LIBS += -lcone \
           -leikcore \
           -lavkon \
           -lapgrfx \
           -lws32 \
           -lsysutil

    DEPLOYMENT.installer_header = 0x2002CCCF
    #DEPLOYMENT.installer_header = 0xA000D7CE

    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x020000 0x4000000

    LIBS += -llbs \
        -lefsrv \
        -lcone \
        -leikcore \
        -lavkon \
        -lapgrfx \
        -lws32 \
        -lhwrmvibraclient \
        -lsysutil

    RESOURCES += src/resources/symbian/symbianimages.qrc

    customrules.pkg_prerules = \
        "; Localised Vendor name" \
        "%{\"INdT\"}" \
        " " \
        "; Unique Vendor name" \
        ":\"INdT\"" \
        "; Dependency to Symbian Qt Quick components" \
        "(0x200346DE), 1, 0, 0, {\"Qt Quick components\"}"

    DEPLOYMENT += customrules
    ICON = indt-moneymanager.svg
}

contains(MEEGO_EDITION,harmattan) {
    isEmpty(PREFIX)
    PREFIX = /usr
    BINDIR = $$PREFIX/bin
    DATADIR = $$PREFIX/share

    DEFINES += DATADIR=\\\"$$DATADIR\\\" PKGDATADIR=\\\"$$PKGDATADIR\\\"

    desktopfile.files = indt-moneymanager.desktop
    desktopfile.path = $$DATADIR/applications

    INSTALLS += desktopfile

    icon.files = indt-moneymanager.png
    icon.path = $$DATADIR/icons/hicolor/80x80/apps
    INSTALLS += icon

    target.path = $$BINDIR
    INSTALLS += target

    DEFINES += Q_WS_MEEGO

    RESOURCES +=  src/resources/meego/meegoimages.qrc

    OTHER_FILES += \
        qtc_packaging/debian_harmattan/rules \
        qtc_packaging/debian_harmattan/README \
        qtc_packaging/debian_harmattan/manifest.aegis \
        qtc_packaging/debian_harmattan/copyright \
        qtc_packaging/debian_harmattan/control \
        qtc_packaging/debian_harmattan/compat \
        qtc_packaging/debian_harmattan/changelog

}

include(src/qmlapplicationviewer/qmlapplicationviewer.pri)

RESOURCES +=  src/resources/qmlfiles.qrc

OTHER_FILES += \
    android/src/org/kde/necessitas/origo/QtActivity.java \
    android/src/org/kde/necessitas/origo/QtApplication.java \
    android/src/org/kde/necessitas/ministro/IMinistro.aidl \
    android/src/org/kde/necessitas/ministro/IMinistroCallback.aidl \
    android/res/values-ja/strings.xml \
    android/res/values-pl/strings.xml \
    android/res/values-nl/strings.xml \
    android/res/values-zh-rTW/strings.xml \
    android/res/values-ms/strings.xml \
    android/res/values-es/strings.xml \
    android/res/values-id/strings.xml \
    android/res/drawable-ldpi/icon.png \
    android/res/values-pt-rBR/strings.xml \
    android/res/values-et/strings.xml \
    android/res/values-fa/strings.xml \
    android/res/values-ru/strings.xml \
    android/res/values-nb/strings.xml \
    android/res/values-fr/strings.xml \
    android/res/values-ro/strings.xml \
    android/res/values-it/strings.xml \
    android/res/values-zh-rCN/strings.xml \
    android/res/values-de/strings.xml \
    android/res/drawable/icon.png \
    android/res/drawable/logo.png \
    android/res/values-el/strings.xml \
    android/res/drawable-hdpi/icon.png \
    android/res/drawable-mdpi/icon.png \
    android/res/values/strings.xml \
    android/res/values/libs.xml \
    android/res/values-rs/strings.xml \
    android/res/layout/splash.xml \
    android/AndroidManifest.xml \
    android/version.xml

